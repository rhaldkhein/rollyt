// Modules
var m = require('mithril'),
  md = require('mithril-data'),
  panelTab = require('../app/comp/elements/panelTab');

// Globalize config
window.Config = require('../app/initialize/config');
// Use console wrapper
window.Console = require('../app/libs/console');

// Reconfigure
md.config({
  baseUrl: '/~admin'
});

window.App = {
  Model: {},
  mount: function() {
    m.mount(document.getElementById('root'), {
      view: function() {
        return m(panelTab, {  
            current: 0,
            tabs: [
              'Dashboard', 'Features', 'Messages', 'Rooms',
              'Users & Accounts', 'Logs', 'System'
            ]
          },
          m(require('./tabs/dashboard')),
          m(require('./tabs/features')),
          m(require('./tabs/messages')),
          m(require('./tabs/rooms')),
          m(require('./tabs/users')),
          m(require('./tabs/logs')),
          m(require('./tabs/system'))
        );
      }
    });
  }
};

__app.schemas.Account.push('email');
require('../app/models');