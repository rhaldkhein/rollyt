// Modules
var _ = require('lodash'),
	m = require('mithril'),
	md = require('mithril-data'),
	toast = require('toastr');

module.exports = {
	oninit: function() {
		md.State.assign(this, {
			json: null,
			loaded: false
		});
	},
	oncreate: function() {
		if (!this.json()) {
			var self = this;
			m.request('/~admin/appsettings/features').then(function(data) {
				self.json(data);
			}).catch(function() {
				toast.error('Unable to load features');
			});
		}
	},
	oncreateEditor: function(node) {
		this.editor = new JSONEditor(node.dom, {
			mode: 'tree',
			modes: ['tree', 'code']
		});
	},
	onupdateEditor: function() {
		if (!this.loaded() && this.json()) {
			this.editor.set(this.json());
			this.loaded(true);
		}
	},
	clickSave: function() {
		m.request({
			method: 'POST',
			url: '/~admin/appsettings/features',
			data: this.editor.get()
		}).then(function() {
			toast.info('Saved');
		}).catch(function() {
			toast.error('Unable to save');
		});
	},
	view: function() {
		return m('#features',
			m('.editor', {
				oncreate: this.oncreateEditor.bind(this),
				onupdate: this.onupdateEditor.bind(this)
			}),
			m('div',
				m('a.pure-button pure-button-primary', {
					onclick: this.clickSave.bind(this)
				}, 'Save')
			)
		);
	}
};
