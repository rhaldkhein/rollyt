// Modules 
var _ = require('lodash'),
  m = require('mithril'),
  md = require('mithril-data');

module.exports = {
  oninit: function() {
  },
  view: function() {
    return m('#rooms',
      m('h4.mt0', 'Rooms'),
      m('p', 'Coming soon...')
    );
  }
};