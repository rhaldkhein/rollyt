// Modules
var _ = require('lodash'),
    m = require('mithril'),
    md = require('mithril-data'),
    numeral = require('numeral');

module.exports = {
    oninit: function() {
        this.meta = {
            totalUsers: 0,
            totalAccounts: 0,
        };

        this.searchUser = md.stream();
        this.subjectUser = md.stream();
        this.collectionUsers = App.Model.User.createCollection({
            url: '/user/list'
        });

        this.subjectAccount = md.stream();
        this.searchAccount = md.stream();
        this.collectionAccounts = App.Model.Account.createCollection({
            url: '/account/list'
        });
    },
    oncreate: function() {
        // Load meta data
        var self = this;
        m.request('/~admin/users/meta').then(function(data) {
            _.assign(self.meta, data);
        }).catch(_.noop);
    },
    submitSearchUser: function(e) {
        e.preventDefault();
        this.subjectUser(null);
        this.collectionUsers.fetch({
                q: this.searchUser()
            }, {
                path: 'results',
                clear: true
            })
            .then(m.redraw)
            .catch(alert);
    },
    submitSearchAccount: function(e) {
        e.preventDefault();
        this.subjectAccount(null);
        this.collectionAccounts.fetch({
                q: this.searchAccount()
            }, {
                path: 'results',
                clear: true
            })
            .then(m.redraw)
            .catch(alert);
    },
    clearUsers: function(e) {
        e.preventDefault();
        this.collectionUsers.clear();
        this.subjectUser(null);
        this.searchUser(null);
    },
    clearAccounts: function(e) {
        e.preventDefault();
        this.collectionAccounts.clear();
        this.subjectAccount(null);
        this.searchAccount(null);
    },
    showUser: function(mdl) {
        this.subjectUser(mdl);
    },
    showAccount: function(mdl) {
        this.subjectAccount(mdl);
    },
    view: function() {
        var self = this,
            subUser = this.subjectUser(),
            subAccount = this.subjectAccount();
        return m('#users',
            m('.users',
                m('div',
                    m('form.pure-form', {
                            onsubmit: this.submitSearchUser.bind(this)
                        },
                        m('fieldset',
                            m('legend',
                                m('b', 'Users'),
                                m('span.ml2 f6 lh-copy', 'Total: ' + numeral(this.meta.totalUsers).format('0,0'))
                            ),
                            m('input', {
                                type: 'text',
                                placeholder: 'Search',
                                onchange: m.withAttr('value', this.searchUser),
                                value: this.searchUser()
                            }),
                            m('button.pure-button pure-button-primary ml2', {
                                type: 'submit'
                            }, 'Search'),
                            m('button.pure-button ml2', {
                                onclick: this.clearUsers.bind(this)
                            }, 'Clear')
                        )
                    )
                ),
                m('div',
                    subUser ?
                    m('table.pure-table w-100',
                        m('thead',
                            m('tr',
                                m('th', 'Field'),
                                m('th', 'Value')
                            )
                        ),
                        m('tbody',
                            m('tr', m('td', 'Id'), m('td', subUser.id())),
                            m('tr', m('td', 'Codename'), m('td', subUser.codename())),
                            m('tr', m('td', 'Current Account'), m('td', _.isObject(subUser.current_account()) ?
                                subUser.current_account().id() : subUser.current_account())),
                            m('tr', m('td', 'Date Modified'), m('td', subUser.date_modified())),
                            m('tr', m('td', 'Date Created'), m('td', subUser.date_created()))
                        )
                    ) :
                    m('table.pure-table w-100',
                        m('thead',
                            m('tr',
                                m('th', 'Id'),
                                m('th', 'Code Name')
                            )
                        ),
                        m('tbody',
                            this.collectionUsers.map(function(item) {
                                return m('tr',
                                    m('td',
                                        m('a', {
                                                href: '#',
                                                onclick: function() {
                                                    self.showUser(item);
                                                }
                                            },
                                            item.id()
                                        )
                                    ),
                                    m('td', item.codename())
                                );
                            })
                        )
                    )
                )
            ),
            m('.accounts',
                m('div',
                    m('form.pure-form', {
                            onsubmit: this.submitSearchAccount.bind(this)
                        },
                        m('fieldset',
                            m('legend',
                                m('b', 'Accounts'),
                                m('span.ml2 f6 lh-copy', 'Total: ' + numeral(this.meta.totalAccounts).format('0,0'))
                            ),
                            m('input.mr2', {
                                type: 'text',
                                placeholder: 'Search',
                                onchange: m.withAttr('value', this.searchAccount),
                                value: this.searchAccount()
                            }),
                            m('button.pure-button pure-button-primary', {
                                type: 'submit'
                            }, 'Search'),
                            m('button.pure-button ml2', {
                                onclick: this.clearAccounts.bind(this)
                            }, 'Clear')
                        )
                    )
                ),
                m('div',
                    subAccount ?
                    m('table.pure-table w-100',
                        m('thead',
                            m('tr',
                                m('th', 'Field'),
                                m('th', 'Value')
                            )
                        ),
                        m('tbody',
                            m('tr', m('td', 'Id'), m('td', subAccount.id())),
                            m('tr', m('td', 'Type'), m('td', subAccount.type())),
                            m('tr', m('td', 'Type Id'), m('td', subAccount.type_id())),
                            m('tr', m('td', 'Avatar'), m('td', m('img[src=' + subAccount.avatar() + ']'))),
                            m('tr', m('td', 'Firstname'), m('td', subAccount.firstname())),
                            m('tr', m('td', 'Lastname'), m('td', subAccount.lastname())),
                            m('tr', m('td', 'Email'), m('td', subAccount.email())),
                            m('tr', m('td', 'User'), m('td', _.isObject(subAccount.user()) ?
                                subAccount.user().id() : subAccount.user())),
                            m('tr', m('td', 'Date Modified'), m('td', subAccount.date_modified())),
                            m('tr', m('td', 'Date Created'), m('td', subAccount.date_created()))
                        )
                    ) :
                    m('table.pure-table w-100',
                        m('thead',
                            m('tr',
                                m('th', 'Type Id'),
                                m('th', 'Avatar'),
                                m('th', 'Name'),
                                m('th', 'Email')
                            )
                        ),
                        m('tbody',
                            this.collectionAccounts.map(function(item) {
                                return m('tr',
                                    m('td',
                                        m('a', {
                                                href: '#',
                                                onclick: function() {
                                                    self.showAccount(item);
                                                }
                                            },
                                            item.type_id()
                                        )
                                    ),
                                    m('td.avatar',
                                        m('img', {
                                            width: 34,
                                            src: item.avatar()
                                        })
                                    ),
                                    m('td', (item.firstname() || '') + ' ' + (item.lastname() || '')),
                                    m('td', item.email())
                                );
                            })
                        )
                    )
                )
            )
        );
    }
};