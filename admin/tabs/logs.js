// Modules 
var _ = require('lodash'),
    m = require('mithril'),
    md = require('mithril-data');

module.exports = {
    oninit: function() {
        md.State.assign(this, {
            logs: null
        });
    },
    view: function() {
        return m('#logs',
            m('h4.mt0', 'System Logs'),
            m('iframe.w-100', {
                src: '/~admin/server/logger/dump',
                style: 'height: 260px'
            })
        );
    }
};