// Modules 
var _ = require('lodash'),
    m = require('mithril'),
    md = require('mithril-data'),
    toast = require('toastr');

module.exports = {
    oninit: function() {
        md.State.assign(this, {
            json: null,
            loaded: true
        });
    },
    oncreateEditor: function(node) {
        this.editor = new JSONEditor(node.dom, {
            mode: 'tree',
            modes: ['tree', 'code']
        });
    },
    onupdateEditor: function() {
        if (!this.loaded() && this.json()) {
            this.editor.set(this.json());
            this.loaded(true);
        }
    },
    view: function() {
        var self = this;
        return m('#system',
            m('h4.mt0', 'Logger Tests'),
            m('button.mr2 mb2', {
                onclick: function() {
                    m.request('/~admin/server/logger/test/slack')
                    .catch(function(err) { toast.error(err.message); });
                }
            }, 'Log to Slack'),
            m('button.mr2 mb2', {
                onclick: function() {
                    m.request('/~admin/server/logger/test/file')
                    .catch(function(err) { toast.error(err.message); });
                }
            }, 'Log to File'),
            m('hr'),
            m('h4', 'Server Process Information'),
            m('button.mr2 mb2', {
                onclick: function() {
                    self.loaded(false);
                    self.json(null);
                    m.request('/~admin/server/process/sync').catch(_.noop);
                }
            }, 'Sync Processes'),
            m('button.mr2 mb2', {
                onclick: function() {
                    m.request('/~admin/server/process/info').then(function(data) {
                        self.json(data);
                    }).catch(function() {
                        toast.error('Unable to load features');
                    });
                }
            }, 'Download Processes'),
            m('.editor', {
                oncreate: this.oncreateEditor.bind(this),
                onupdate: this.onupdateEditor.bind(this)
            }),
            m('h4', 'Server Process Tests'),
            m('button', {
                onclick: function() {
                    m.request('/~admin/server/process/exit/1').catch(_.noop);
                }
            }, 'Process Exit (1)'),
            m('span.ml2 f6 red', 'Force Shutdown'),
            m('span.ml2 f6 gray', '- System will NOT do cleanup'),
            m('br'),
            m('br'),
            m('button', {
                onclick: function() {
                    m.request('/~admin/server/process/kill/SIGINT').catch(_.noop);
                }
            }, 'Process Kill (SIGINT)'),
            m('span.ml2 f6 green', 'Graceful Shutdown'),
            m('span.ml2 f6 gray', '- System will try to do cleanup')
        );
    }
};