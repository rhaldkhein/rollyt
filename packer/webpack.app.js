var _ = require('lodash'),
  webpack = require('webpack');

// Extract packages into `vendor` bundle
// Node module packages
var node = [
    // EXTERNALS
    // Core Packages
    'events', // 'util',
    // 3rd Party Packages 
    'mithril', 'lodash', 'mithril-data', 'moment', 'async',
    'node-cache', 'perfect-scrollbar', 'numeral', 'store',
    'validator', 'mousetrap', 'jquery', 'toastr', 'enquire.js', 
    'awesomplete', 'push.js', 'tippy.js',
    // INTERNALS
    'color-hash', 'classnames'
  ],
  // Other packages
  other = ['loaderjs'],
  vendors = _.union(node, other);

module.exports = function(prod) {
  return {
    entry: {
      app: './app/index.js',
      vendor: vendors
    },
    output: {
      filename: 'app.bundle.js',
      pathinfo: true
    },
    externals: (prod ? undefined : {
      'loaderjs': 'LoaderJS',
      'lodash': '_',
      'jquery': 'jQuery',
      'async': 'async',
      'mousetrap': 'Mousetrap',
      'enquire': 'enquire',
      'validator': 'validator',
      'perfect-scrollbar': 'PerfectScrollbar',
      'awesomplete': 'Awesomplete',
      'numeral': 'numeral',
      'moment': 'moment',
      'store': 'store',
      'toastr': 'toastr',
      'push.js': 'Push',
      'tippy.js': 'tippy',
      'mithril': 'm',
      'mithril-data': 'md'
    }),
    resolve: {
      modules: ['node_modules', 'app'],
      extensions: ['.js', '.json', '.ts', '.tsx']
    },
    module: {
      rules: []
    },
    plugins: [
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        filename: 'app.vendor.js'
      })
    ]
  };
};