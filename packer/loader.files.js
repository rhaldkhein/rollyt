/**
 * 1st level array is synchronous loading. This is good if the 2nd index element depends on 1st index element.
 * 2nd level array is asynchronous loading.
 */

function loginToken(data, resolve, reject) {
  if (!data) {
    if (reject) reject(new Error('Invalid parameters'));
    return;
  }
  // if (__app && __app.loader) __app.loader.log('Authenticating');
  // Returns a promise
  window.__app_request(
    {
      url: '/login/google/authstatus'
    },
    function(err, res, body) {
      try {
        if (!err && res.statusCode === 200) {
          body = JSON.parse(body) || {};
          if (body.auth && JSON.parse(global.localStorage.getItem('access_token')) === data.access_token) {
            if (resolve) resolve(body);
          } else {
            window.__app_request(
              {
                url: '/login/google/token',
                method: 'POST',
                headers: data
              },
              function(err, res, body) {
                if (!err && res.statusCode === 200 && body) {
                  // Save access_token to compare later
                  global.localStorage.setItem('access_token', JSON.stringify(data.access_token));
                  if (resolve) resolve(JSON.parse(body));
                } else {
                  if (reject) reject(err || new Error('Invalid token'));
                }
              }
            );
          }
        } else {
          if (reject) reject(err || new Error('Unable to get auth status'));
        }
      } catch (e) {
        reject(e);
      }
    }
  );
}

function logoutToken(resolve) {
  // Returns a promise
  window.__app_request(
    {
      url: '/logout/token',
      method: 'GET'
    },
    function(err, res, body) {
      // 302 means session contains backdoor flag
      if (res && res.statusCode === 302) {
        window.location.reload(true);
      } else {
        resolve(JSON.parse(body));
      }
    }
  );
}

module.exports = function() {
  return [
    [
      function(resolve, reject) {
        try {
          // __app.loader.log('Connecting to Youtube');
          gapi.load('client:auth2', function() {
            gapi.client.setApiKey('AIzaSyAR809J7xJrApSZ8f-OrAzAg2g15EOD9CA');
            Promise.all([
              gapi.client.load('youtube', 'v3'),
              gapi.auth2.init({
                client_id: '285836800359-iffif5lcf7mevsk7p72rul9vddgd53re.apps.googleusercontent.com',
                scope: 'profile https://www.googleapis.com/auth/youtube'
              })
            ]).then(
              function(results) {
                var googleUser = results[1].currentUser.get();
                var attachAccount = function(body) {
                  __app.loader.attach('account', body);
                  resolve();
                };
                if (googleUser.isSignedIn()) {
                  var reloadToken = function() {
                    googleUser.reloadAuthResponse().then(function() {
                      loginToken(
                        {
                          account_type: 'google',
                          account_id: googleUser.getId(),
                          id_token: googleUser.getAuthResponse().id_token,
                          access_token: googleUser.getAuthResponse().access_token
                        },
                        function() {
                          setTimeout(reloadToken, 1000 * googleUser.getAuthResponse().expires_in);
                        }
                      );
                    });
                  };
                  loginToken(
                    {
                      account_type: 'google',
                      account_id: googleUser.getId(),
                      id_token: googleUser.getAuthResponse().id_token,
                      access_token: googleUser.getAuthResponse().access_token
                    },
                    function(body) {
                      setTimeout(reloadToken, 1000 * googleUser.getAuthResponse().expires_in);
                      attachAccount(body);
                    },
                    reject
                  );
                } else {
                  logoutToken(attachAccount, reject);
                }
              },
              function(err) {
                reject(err);
              }
            );
          });
        } catch (err) {
          reject(err);
        }
      },

      // Load the schemas.
      function(resolve, reject) {
        __app.loader.requestAttach('/~rest/schemas', 'schemas', resolve, reject);
      },

      // Load country codes and geoip.
      function(resolve, reject) {
        __app.loader.requestAttach(
          '/app/country-codes.min.json',
          'country_codes',
          function() {
            // Done
            window.__app_request(
              {
                url: '/~rest/geoip'
              },
              function(err, res, body) {
                if (err || res.statusCode >= 400) {
                  reject(err);
                } else {
                  __app.loader.attach('location', JSON.parse(body));
                  resolve();
                }
              }
            );
          },
          reject
        );
      },

      // Load the youtube player api.
      function(resolve, reject) {
        try {
          global.onYouTubeIframeAPIReady = resolve;
          var tag = document.createElement('script');
          tag.setAttribute('src', 'https://www.youtube.com/iframe_api');
          var firstScriptTag = document.getElementsByTagName('script')[0];
          tag.setAttribute('onerror', function(err) {
            reject(err);
          });
          firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        } catch (err) {
          reject(err);
        }
      }
    ]
  ];
};
