var _ = require('lodash'),
  webpack = require('webpack');

// Extract packages into `vendor` bundle
var node = ['mithril', 'lodash', 'mithril-data', 'moment',
    'events', 'util', 'async', 'node-cache', 'perfect-scrollbar',
    'numeral', 'store', 'validator', 'mousetrap', 'jquery',
    'toastr', 'bluebird', 'loaderjs', 'color-hash'
  ],
  // Other packages
  other = [],
  vendors = _.union(node, other);


module.exports = function(prod) {
  // Configuration
  return {
    entry: {
      app: './admin/index.js',
      vendor: vendors,
    },
    output: {
      filename: 'index.bundle.js',
      pathinfo: true
    },
    externals: {
      'bluebird': 'Promise',
      'moment': 'moment',
      'numeral': 'numeral',
      'jquery': 'jQuery',
      'toastr': 'toastr',
      'mithril-data': 'md',
      'lodash': '_',
      'mithril': 'm'
    },
    resolve: {
      modules: ['node_modules', 'app'],
      extensions: ['.js', '.json', '.ts', '.tsx']
    },
    module: {
      rules: []
    },
    plugins: [
      new webpack.optimize.CommonsChunkPlugin({
        name: 'vendor',
        filename: 'index.vendor.js'
      })
    ]
  };
};