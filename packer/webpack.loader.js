module.exports = {
	entry: './packer/loader.js',
	output: {
		filename: 'loader.bundle.js'
	},
	resolve: {
		modules: ['web_modules', 'node_modules', 'bower_components']
	}
};