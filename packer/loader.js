var loader = require('loaderjs'),
  xhr = require('xhr'),
  elLog = document.getElementById('load-log'),
  elLogMsg = document.getElementById('load-log-msg'),
  elLoadLogo = document.getElementById('load-fg'),
  propText = 'innerText' in elLog ? 'innerText' : 'textContent',
  isprod = __app.env === 'production',
  files = require('./loader.files.js')(isprod);

// Injecting xhr
if (typeof window !== 'undefined') {
  window.__app_request = xhr;
  __app.loader = {
    _error: false,
    _slownetid: null,
    _refreshid: null,
    promise: null,
    attach: function(name, obj) {
      __app[name] = obj;
    },
    requestAttach: function(url, attachTo, resolve, reject, options) {
      options = options || {};
      options.url = url;
      xhr(options, function(err, resp, body) {
        if (!err) {
          __app[attachTo] = JSON.parse(body);
          resolve();
        } else {
          __app[attachTo] = null;
          reject(err);
        }
      });
    },
    log: function(strName, strMsg, strClass, noSlowNet) {
      if (strName) elLog[propText] = strName;
      elLogMsg[propText] = strMsg || '\xa0';
      if (strClass) elLog.className = strClass;
      if (strClass === 'log-error') {
        var elRoot = document.getElementById('root');
        elRoot.className += 'load-error';
      }
      if (__app._slownetid) clearTimeout(__app._slownetid);
      if (__app._refreshid) clearTimeout(__app._refreshid);
      if (noSlowNet) return;
      __app._slownetid = setTimeout(function() {
        if (!(window.App && window.App.Info) && !__app._error) {
          __app.loader.log('Please wait ...', 'Looks like connection is unstable', null, true);
          __app._refreshid = setTimeout(function() {
            if (!(window.App && window.App.Info) && !__app._error) {
              // __app.loader.log('Unstable Connection', 'Please manually reload the page', null, true);
              __app.loader.logError({
                name: 'Unstable Connection',
                message: 'Please manually reload the page'
              });
            }
          }, 1000 * 28);
        }
      }, 1000 * 12);
    },
    logError: function(strName, strMsg) {
      if (typeof strName !== 'string') {
        this.logError(
          strName.name || 'Unknown Error',
          strName.message || 'Please check your connection and reload the page'
        );
      } else {
        this.log('Error', strMsg || strName, 'log-error', true);
      }
    }
  };
  __app.browser = (function() {
    var agent = (function() {
      var ua = navigator.userAgent,
        tem,
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
      if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
      }
      if (M[1] === 'Chrome') {
        tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
        if (tem != null)
          return tem
            .slice(1)
            .join(' ')
            .replace('OPR', 'Opera');
      }
      M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
      if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
      return M.join(' ');
    })().split(' ');
    return {
      title: agent[0].toLowerCase(),
      version: parseFloat(agent[1])
    };
  })();
}

function loadApp() {
  // Check browser compatibility
  if (
    !__app.browsers.hasOwnProperty(__app.browser.title) ||
    __app.browser.version < __app.browsers[__app.browser.title]
  ) {
    // Redirect to browser check if not supported
    window.location.replace(
      '/browsercheck' + (isprod ? '?t=' + __app.browser.title + '&v=' + __app.browser.version : '')
    );
    return;
  }
  // Continue loading the resource
  Promise.config({
    cancellation: true
  });
  for (var item, parts, i = files[0].length - 1; i >= 0; i--) {
    item = files[0][i];
    if (typeof item === 'string' || item instanceof String) {
      if (item.lastIndexOf('/node_modules', 0) === 0) {
        parts = item.split('/');
        files[0][i] = '/' + parts[1] + '/' + parts[parts.length - 1];
      }
    }
  }

  __app.loader.log('Loading Resources');
  loader.load(
    files,
    function(err) {
      if (err) {
        __app._error = !!err;
        __app.loader.logError(err);
      } else {
        App.start(function(err) {
          // This will be called after mithril mounts view to root
          // However, If `err` is true, no view is mounted
          __app.loaded = !err;
          __app._error = !!err;
          if (__app._slownetid) clearTimeout(__app._slownetid);
          if (__app._refreshid) clearTimeout(__app._refreshid);
          if (err) {
            __app.loader.logError(err);
          } else {
            __app.account = null;
            __app.browser = null;
            __app.browsers = null;
            __app.location = null;
            __app.loader = null;
            __app = null;
          }
        });
      }
    },
    function(percent) {
      if (percent > 5) elLoadLogo.style.width = percent + '%';
    }
  );
}

function loadResources() {
  // Wrap app to raven context
  if (isprod) {
    Raven.config('https://64923c856685460caf846dd3c6f7b2e2@sentry.io/285750', {
      release: '<%--version--%>'
    }).install();
    Raven.context(loadApp);
  } else {
    loadApp();
  }
}

if (window.WebComponents) {
  // Webcomponents is enabled is the head
  document.addEventListener('WebComponentsReady', loadResources);
} else {
  // Webcomponents is NOT enabled and will not be used.
  document.addEventListener('DOMContentLoaded', loadResources);
}
