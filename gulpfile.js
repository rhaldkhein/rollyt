var _ = require('lodash'),
  pkg = require('./package.json'),
  gulp = require('gulp'),
  webpack = require('webpack'),
  webpackStream = require('webpack-stream'),
  stylus = require('gulp-stylus'),
  jeet = require('jeet'),
  nib = require('nib'),
  http = require('http'),
  tinylr = require('tiny-lr'),
  sequence = require('run-sequence'),
  merge = require('merge-stream'),
  rename = require('gulp-rename'),
  replace = require('gulp-replace'),
  path = require('path'),
  fs = require('fs');

var config = require('./server/config');

gulp.task('run', function(done) {
  sequence('z::dev_webpack_app', 'z::dev_src', 'livereload_reload', done);
});

gulp.task('release', function(done) {
  sequence('z::min_webpack_app',
    'z::min_webpack_admin',
    'z::min_webpack_loader',
    'z::min_stylus_app',
    'z::min_stylus_admin',
    'z::min_stylus_web',
    'z::min_src',
    'z::min_libs',
    'version', 'version_loader',
    'compose_app', done);
});

gulp.task('run_style_app', function(done) {
  sequence('z::dev_stylus_app', 'livereload_reload', done);
});

gulp.task('run_style_web', function(done) {
  sequence('z::dev_stylus_web', 'livereload_reload', done);
});

gulp.task('reload', function(done) {
  sequence('livereload_reload', done);
});

gulp.task('version', function() {
  var file = config.paths.static + '/app/dist/app.bundle.js';
  return gulp.src(file)
    .pipe(replace('<%--version--%>', pkg.version))
    .pipe(gulp.dest(path.dirname(file)));
});

gulp.task('version_loader', function() {
  var file = config.paths.static + '/app/dist/loader.bundle.js';
  return gulp.src(file)
    .pipe(replace('<%--version--%>', pkg.version))
    .pipe(gulp.dest(path.dirname(file)));
});

gulp.task('compose_app', function() {
  var file = config.paths.root + '/build/compose-app.yml';
  return gulp.src(file)
    .pipe(replace(/image: rollyt:.*/g, 'image: rollyt:' + pkg.version))
    .pipe(gulp.dest(path.dirname(file)));
});

/**********************************
 * DEBUG TASKS
 **********************************/

gulp.task('livereload_server', function() {
  var port = 35729;
  tinylr().listen(port, function() {
    console.log('LiveReload listening on %s ...', port);
  });
});

gulp.task('livereload_reload', function(done) {
  http.get('http://localhost:35729/changed?files=*').on('error', function() {
    console.log('LiveReload not running! Run gulp task `livereload_server` first.');
  });
  done();
});

gulp.task('z::dev_webpack_app', function() {
  var wpConfig = require('./packer/webpack.app.js')(false);
  return gulp.src('')
    .pipe(webpackStream(wpConfig, webpack))
    .pipe(gulp.dest(config.paths.static + '/app/dist'));
});

gulp.task('z::dev_webpack_admin', function() {
  return gulp.src('')
    .pipe(webpackStream(require('./packer/webpack.admin.js')(false), webpack))
    .pipe(gulp.dest(config.paths.static + '/admin'));
});

gulp.task('z::dev_webpack_loader', function() {
  return gulp.src('')
    .pipe(webpackStream(require('./packer/webpack.loader.js'), webpack))
    .pipe(gulp.dest(config.paths.static + '/app/dist'));
});

gulp.task('z::dev_stylus_app', function() {
  return gulp.src('./style/index.app.styl')
    .pipe(stylus({
      use: [jeet(), nib()],
      'include css': true
    }))
    .pipe(gulp.dest(config.paths.static + '/app/dist'));
});

gulp.task('z::dev_stylus_web', function() {
  return gulp.src('./style/index.web.styl')
    .pipe(stylus({
      use: [jeet(), nib()],
      'include css': true
    }))
    .pipe(gulp.dest(config.paths.static + '/web/dist'));
});

gulp.task('z::dev_stylus_admin', function() {
  return gulp.src('./admin/styles/index.styl')
    .pipe(stylus({
      use: [jeet(), nib()],
      'include css': true
    }))
    .pipe(gulp.dest(config.paths.static + '/admin'));
});

// Minify all other sources
gulp.task('z::dev_src', function() {
  var src = './static/app/src/';
  var dist = './static/app/dist';
  var tasks = fs.readdirSync(src).map(function(file) {
    file = src + file;
    var ext = path.extname(file);
    var task = gulp.src(file);
    if (ext === '.styl') {
      task = task.pipe(stylus({
        use: [jeet(), nib()],
        'include css': true
      }));
    }
    task = task.pipe(gulp.dest(dist));
    return task;
  });
  return merge(tasks);
});


/**********************************
 * MINIFY TASKS
 **********************************/

var uglifyCSS = require('gulp-clean-css'),
  uglifyJS = require('gulp-uglify'),
  sourceMap = require('gulp-sourcemaps'),
  mapPath = config.paths.root + '/../maps/' + pkg.name + '/' + pkg.version,
  writeOptions = {
    sourceMappingURL: function(file) {
      return '~/app/dist/' + file.relative + '.map';
    }
  };

gulp.task('z::min_webpack_app', function() {
  var wpConfig = require('./packer/webpack.app.js')(true);
  return gulp.src('')
    .pipe(webpackStream(wpConfig, webpack))
    .pipe(sourceMap.init())
    .pipe(uglifyJS())
    .pipe(sourceMap.write(mapPath, writeOptions))
    .pipe(gulp.dest(config.paths.static + '/app/dist'));
});

gulp.task('z::min_webpack_admin', function() {
  return gulp.src('')
    .pipe(webpackStream(require('./packer/webpack.admin.js')(true), webpack))
    .pipe(uglifyJS())
    .pipe(gulp.dest(config.paths.static + '/admin'));
});

gulp.task('z::min_webpack_loader', function() {
  return gulp.src('')
    .pipe(webpackStream(require('./packer/webpack.loader.js'), webpack))
    .pipe(sourceMap.init())
    .pipe(uglifyJS())
    .pipe(sourceMap.write(mapPath, writeOptions))
    .pipe(gulp.dest(config.paths.static + '/app/dist'));
});

gulp.task('z::min_stylus_app', function() {
  return gulp.src('./style/index.app.styl')
    .pipe(stylus({
      use: [jeet(), nib()],
      'include css': true
    }))
    .pipe(uglifyCSS())
    .pipe(gulp.dest(config.paths.static + '/app/dist'));
});

gulp.task('z::min_stylus_admin', function() {
  return gulp.src('./admin/styles/index.styl')
    .pipe(stylus({
      use: [jeet(), nib()],
      'include css': true
    }))
    .pipe(uglifyCSS())
    .pipe(gulp.dest(config.paths.static + '/admin'));
});

gulp.task('z::min_stylus_web', function() {
  return gulp.src('./style/index.web.styl')
    .pipe(stylus({
      use: [jeet(), nib()],
      'include css': true
    }))
    .pipe(uglifyCSS())
    .pipe(gulp.dest(config.paths.static + '/web/dist'));
});

// Minify all other sources
gulp.task('z::min_src', function() {
  var src = './static/app/src/';
  var dist = './static/app/dist';
  var tasks = fs.readdirSync(src).map(function(file) {
    file = src + file;
    var ext = path.extname(file);
    // var name = path.basename(file, ext);
    // Get source
    var task = gulp.src(file);
    // Select method
    if (ext === '.js') task = task.pipe(uglifyJS());
    else if (ext === '.css') task = task.pipe(uglifyCSS());
    else if (ext === '.styl') {
      task = task.pipe(stylus({
          use: [jeet(), nib()],
          'include css': true
        }))
        .pipe(uglifyCSS());
    }
    // Rename and export
    task = task.pipe(gulp.dest(dist));
    return task;
  });
  return merge(tasks);
});

// Minify all unminified libraries
gulp.task('z::min_libs', function() {
  var files = [
    './static/status/style.css',
    './static/app/libs/icomoon/style.css',
    './static/app/libs/icomoon-loading/style.css',
    './static/app/libs/normalize/normalize.css'
  ];
  var tasks = files.map(function(file) {
    var ext = path.extname(file);
    var name = path.basename(file, ext);
    // Get source
    var task = gulp.src(file);
    // Select method
    if (ext === '.js') task = task.pipe(uglifyJS());
    else if (ext === '.css') task = task.pipe(uglifyCSS());
    // Rename and export
    task = task.pipe(rename(name + '.min' + ext))
      .pipe(gulp.dest(path.dirname(file)));
    return task;
  });
  return merge(tasks);
});