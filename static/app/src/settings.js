// Imports
var m = parent.App.Modules.m;
var _ = parent.App.Modules._;
var ui = parent.App.Modules.ui;
var toast = parent.App.Modules.toastr;
var prefs = parent.App.Preference.state;
var skiplist = parent.App.Modules.skiplist;
var ConfirmAction = parent.App.Modules.modal.ConfirmAction;

// Override Globals
Promise = parent.Promise;

// --- Base Component --- 

function getControl(options, state) {
  switch (options.type) {
    case 'boolean':
      return m(ui.switch, {
        value: _.get(parent, options.action),
        onchange: options.action_hook
      });
    case 'button':
      return m('button.ui-button', {
        onclick: options.action
      }, options.text || 'Go');
    case 'range':
      return [
        m('div',
          m('span',
            'Min: ',
            m('input', {
              type: 'number',
              onchange: m.withAttr('value', _.get(parent, options.action[0])),
              value: (_.get(parent, options.action[0]) || _.noop)() // noop is a fix
            })
          )
        ),
        m('div',
          m('span',
            'Max: ',
            m('input', {
              type: 'number',
              onchange: m.withAttr('value', _.get(parent, options.action[1])),
              value: (_.get(parent, options.action[1]) || _.noop)() // noop is a fix
            })
          )
        )
      ];
    case 'dropdown':
      var _prop = _.get(parent, options.action);
      return m('select', {
          onchange: m.withAttr('value', _prop),
          value: (_prop || _.noop)() // noop is a fix
        },
        _.map(options.values, function(item, i) {
          return m('option', {
            value: item
          }, options.texts[i]);
        })
      );
    case 'panel':
      return m('button.ui-button', {
        onclick: function() {
          state.open = !state.open;
        }
      }, state.open ? 'Close' : 'Open');
  }
  return null;
}

var Block = {
  oninit: function() {
    this.open = false;
  },
  view: function(node) {
    return m('.frame-block',
      m('.frame-head',
        m('.info',
          m('.name', node.attrs.item.name + (node.attrs.item.name_hook ? node.attrs.item.name_hook() : '')),
          m('.body', node.attrs.item.desc + (node.attrs.item.desc_hook ? node.attrs.item.desc_hook() : '') || '')
        ),
        m('.control', {
          class: node.attrs.item.type
        }, getControl(node.attrs.item, this))
      ),
      (node.attrs.item.body && this.open ? m('.frame-body', m(node.attrs.item.body)) : null)
    );
  }
};

var Panel = {
  view: function(node) {
    return m('.frame-panel',
      m('h4', node.attrs.panel.name + (node.attrs.panel.name_hook ? node.attrs.panel.name_hook() : '')),
      m('p', node.attrs.panel.desc),
      m('.frame-panel-wrap',
        _.map(node.attrs.panel.items, function(item) {
          return m(Block, {
            item: item
          });
        })
      )
    );
  }
};

var SettingsComponent = {
  view: function() {
    return m('.settings',
      _.map(schema, function(panel) {
        return m(Panel, {
          panel: panel
        });
      })
    );
  }
};

// --- Scheme --- 

var schema = [{
  name: 'General',
  desc: '',
  items: [{
    name: 'Auto Play Related / Mix',
    desc: 'Automatically play related or mix videos of last played video',
    type: 'boolean',
    action: 'App.Preference.state.auto_play_related'
  }, {
    name: 'List Layout Video Library',
    desc: 'Change to list type layout for video library',
    type: 'boolean',
    action: 'App.Preference.state.list_video_library'
  }, {
    name: 'Play Notification',
    desc: 'Disable or enable play notification',
    type: 'boolean',
    action: 'App.Preference.state.play_notification'
  }, {
    name: 'Tooltip',
    desc: 'Disable or enable tooltip for all buttons (Always disabled on mobile)',
    type: 'boolean',
    action: 'App.Preference.state.tooltip'
  }, {
    name: 'Clear on Play',
    desc: 'Automatically clear your player when performing Play',
    type: 'boolean',
    action: 'App.Preference.state.clear_on_play'
  }, {
    name: 'Remove Startup Playlist',
    desc: 'Remove the playlist you set to play automatically on startup',
    type: 'button',
    text: 'Remove',
    action: function() {
      prefs.startup_playlist('');
      toast.success('Startup playlist removed');
    }
  }]
}, {
  name: 'Skiplist',
  items: [{
    name: 'Clear Skiplist',
    desc: 'Remove all videos in skiplist ',
    desc_hook: function() {
      return ' (Total items: ' + skiplist.getList().length + ')';
    },
    type: 'button',
    text: 'Clear',
    action: function() {
      ConfirmAction.show({
        title: 'Skiplist Manager',
        body: 'Are you sure you want to clear the skiplist?',
        confirm: function() {
          skiplist.removeAll().then(function() {
            toast.success('Cleared');
          }).catch(function() {
            toast.error('Error clearing list');
          });
        }
      });
    }
  }, {
    name: 'Remove Last Skipped',
    desc: 'Remove the last video in skiplist',
    type: 'button',
    text: 'Remove',
    action: function() {
      skiplist.removeLast().then(function() {
        toast.success('Removed');
      }).catch(function() {
        toast.error('Error removing last skipped video');
      });
    }
  }, {
    name: 'Skip Shorter Duration',
    desc: 'Automatically skip videos that are shorter than given minutes',
    type: 'dropdown',
    values: [0, 0.5, 1, 1.5, 2],
    texts: ['Disable', '0:30', '1:00', '1:30', '2:00'],
    action: 'App.Preference.state.skip_length_min'
  }, {
    name: 'Skip Longer Duration',
    desc: 'Automatically skip videos that are longer than given minutes',
    type: 'dropdown',
    values: [0, 3, 3.5, 4, 4.5, 5, 6, 7, 10, 15, 20],
    texts: ['Disable', '3:00', '3:30', '4:00', '4:30', '5:00', '6:00', '7:00', '10:00', '15:00', '20:00'],
    action: 'App.Preference.state.skip_length_max'
  }]
}, {
  name: 'Clear / Reset',
  items: [{
      name: 'Default Settings',
      desc: 'Restore settings to default values',
      type: 'button',
      text: 'Restore Defaults',
      action: function() {
        ConfirmAction.show({
          title: 'Default Settings',
          body: 'Are you sure you want to restore default settings?',
          confirm: function() {
            parent.App.Settings._setDefaults();
            parent.App.Preference.setDefaults();
            toast.success('Default settings has been restored!');
          }
        });
      }
    }
    // , {
    //     name: 'Clear & Reset Data',
    //     desc: 'Remove all custom user data and restore default settings',
    //     type: 'button',
    //     text: 'Reset Data',
    //     action: function() {
    //         ConfirmAction.show({
    //             title: 'Reset Data',
    //             body: 'Are you sure you want to reset data? All user data will be lost and settings will be restored to default!',
    //             confirm: function() {
    //                 parent.App.Settings._setDefaults();
    //                 toast.success('Data has been cleared and reset!');
    //             }
    //         });
    //     }
    // }
  ]
}];

// --- Mounting --- 

m.mount(document.getElementById('root'), SettingsComponent);