var _ = require('lodash');
var m = require('mithril');
var util = require('../libs/util');
var redraw = require('./mithril-redraw');

_.extend(m, {

	// Overrides

	redraw: redraw({
		log: false,
		redrawOnTimeout: true
	}),

	// Extensions

	instance: require('./mithril-instance'),

	nextTick: util.nextTick

});
