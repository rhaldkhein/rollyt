var loader = require('loaderjs');

// Add loader for images
loader.addLoader({
	ext: 'jpg,png',
	custom: function(resolve, reject, url) {
		var image = new Image();
		image.onload = function() {
			resolve(image);
			image = null;
			url = null;
		};
		image.onerror = function() {
			reject('Unable to load image ' + url);
			image = null;
			url = null;
		};
		image.onunload = function() {
			image = null;
			url = null;
		};
		image.src = url;
	}
});