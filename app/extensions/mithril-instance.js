var m = require('mithril');

module.exports = function(component, options) {
	return {
		node: undefined,
		component: function() {
			return (this.node = m(component, options));
		}
	};
};
