var _ = require('lodash');
var util = require('libs/util');
var ps = require('perfect-scrollbar');

var ps_initialize = ps.initialize;

_.extend(ps, {

    // Overrides

    initialize: function(elem, settings) {
        if (!util.isMobile) return ps_initialize(elem, settings);
    }

    // Extensions
});