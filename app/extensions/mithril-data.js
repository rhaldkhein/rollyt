var _ = require('lodash');
var md = require('mithril-data');
var async = require('async');

var mapRequestMethods;
var mapTypeModels;

/**
 * This function populates a collection that contains items
 * with property `resource_type` and `resource_id` then will
 * put actual info to propery `resource`
 */

function populateResource() {
  // `this` is the collection instance
  var self = this;
  return new Promise(function(resolve, reject) {
    if (!mapRequestMethods) mapRequestMethods = App.Model.LocalPlaylistItem.requestMethods();
    if (!mapTypeModels) mapTypeModels = App.Model.typeMap;
    var _mapIds = {};
    self.forEach(function(item) {
      var _type = item.resource_type();
      _mapIds[_type] = _mapIds[_type] || [];
      _mapIds[_type].push(item.resource_id());
    });
    async.eachOfSeries(
      _mapIds,
      function(ids, key, done) {
        mapRequestMethods[key](ids, !!self.__options.owned) // Add 2nd param for own local playlist
          .then(function(items) {
            Promise.map(items, function(item) {
              if (item._type === 'local#playlist') {
                // Get ref prop
                var _mdl = App.Model.LocalPlaylist.create(item);
                if (!_mdl.isSaved()) {
                  // WARNING: This individually populate local playlist which is
                  // not efficient though the request is parallel. As it may request
                  // data for the same model (duplicate)
                  return _mdl
                    .fetch()
                    .then(function(mdl) {
                      return mdl.populate();
                    })
                    .then(function(mdl) {
                      return mdl
                        .user()
                        .populate()
                        .return(mdl);
                    })
                    .then(function(mdl) {
                      return mdl.getCopy();
                    });
                }
                return _mdl.getCopy();
              }
              return item;
            }).then(function(results) {
              // Now, every item is objects
              _.forEach(mapTypeModels[key].createModels(results), function(model) {
                var found = self.find({
                  resource_type: key,
                  resource_id: model.id()
                });
                if (found) found.resource(model);
              });
              done(null);
            });
          })
          .catch(done);
      },
      function(err) {
        if (err) reject(err);
        else resolve();
      }
    );
  });
}

function populateLocalPlaylist() {
  // `this` is the collection instance
  var self = this;
  var users = self.pluck('user', function(mdl) {
    return !_.isObject(mdl.user());
  });
  if (users && users.length) {
    return md.store
      .get('~rest/user/list', {
        ids: users.join(',')
      })
      .then(function(res) {
        var promAll = [];
        self.forEach(function(item) {
          if (!(item.user() instanceof md.BaseModel)) {
            promAll.push(
              App.Model.User.create(_.find(res, ['_id', item.user()]))
                .populate()
                .then(function(user) {
                  item.user(user);
                })
              // This has been checked for catch
              // `promAll` is being return, therefore no need to catch here
            );
          }
        });
        return Promise.all(promAll);
      });
  } else {
    return Promise.resolve();
  }
}

/**
 * Local Playlist Collection
 */
function LocalPlaylistCollection(options) {
  md.Collection.call(this, options);
}

LocalPlaylistCollection.prototype = _.create(md.Collection.prototype, {
  populate: function() {
    return populateLocalPlaylist.call(this);
  }
});

/**
 * Local Playlist Item Collection
 */
function ItemCollection(options) {
  md.Collection.call(this, options);
}

ItemCollection.prototype = _.create(md.Collection.prototype, {
  populate: function() {
    return populateResource.call(this);
  }
});

/**
 * Pin Collection
 */
function PinCollection(options) {
  md.Collection.call(this, options);
}

PinCollection.prototype = _.create(md.Collection.prototype, {
  populate: function() {
    return populateResource.call(this);
  }
});

// Injections
_.extend(md, {
  LocalPlaylistCollection: LocalPlaylistCollection,
  ItemCollection: ItemCollection,
  PinCollection: PinCollection
});
