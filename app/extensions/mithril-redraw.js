var m = require('mithril');
var util = require('libs/util');

var m_redraw;
var default_compute_name = '__com__';
var prefix_instance_name = '__ins__';
var id_num = 0;
var pre_comps = [];
var computations = {};
var cluster;
var options = {
	timeout: null,
	log: null,
	redrawOnTimeout: null
};

function getId() {
	return (++id_num);
}

function redraw() {
	if (options.log) {
		var arr_lines = (new Error).stack.split('\n');
		var line = arr_lines[2];
		if (line.indexOf('_redraw') > -1) {
			line = arr_lines[3];
		}
		if (line) {
			var matches = line.match(/(http:|https:).+$/);
			if (matches && matches[0] && line.indexOf('at run') === -1) {
				Console.info('Redraw', '(' + matches[0].replace(')', '') + ')');
			}
		}
	}
	m_redraw.call(m);
}

// Immediately redraw after next tick
redraw.nexttick = function() {
	util.nextTick(m.redraw);
};

// Start and end computation right after next tick
redraw.postcomputation = function(name) {
	if (!name) name = default_compute_name;
	util.nextTick(m.redraw.computation(name));
};

// Create a computation with given name
redraw.computation = function(name) {
	if (!name) name = default_compute_name;
	if (!computations[name]) computations[name] = [];
	var id_timeout = setTimeout(function() {
		// Used 2nd parameter to preserve promise usage. Like, .then (done)
		_redraw(undefined, !options.redrawOnTimeout);

	}, options.timeout);

	function _redraw() {
		clearTimeout(id_timeout);
		var arr_redraw = computations[name];
		if (arr_redraw) {
			arr_redraw.splice(arr_redraw.indexOf(_redraw), 1);
			if (!arr_redraw.length) {
				// Time to redraw
				delete computations[name];
				var cluster_index = cluster ? cluster.indexOf(name) : -1;
				if (cluster_index > -1) {
					// Clustered
					cluster.splice(cluster_index, 1);
					if (!cluster.length) {
						cluster = null;
						if (!arguments[1]) m.redraw();
					}
				} else {
					if (!arguments[1]) m.redraw();
				}
			}
		}
	}
	// Add to computations
	computations[name].push(_redraw);
	// Remove precomps
	var ind = pre_comps.indexOf(name);
	if (ind > -1) pre_comps.splice(ind, 1);
	// Exporse redraw callback
	return _redraw;
};

redraw.precomputation = function(name) {
	if (!name) name = default_compute_name;
	// Only do precomputation if not computing
	if (!this.isComputing(name) && pre_comps.indexOf(name) < 0) {
		// Continue
		pre_comps.push(name);
	}
};

// Create a computation factory by instance
redraw.instance = function() {
	var name = prefix_instance_name + getId();
	return function() {
		return redraw.computation(name);
	};
};

// Add multiple computations to the cluster
redraw.cluster = function() {
	var arr = Array.prototype.slice.call(arguments);
	cluster = cluster ? cluster.concat(arr) : arr;
};

// Checks if redraw is computing
redraw.isComputing = function(name) {
	if (!name) name = default_compute_name;
	return (pre_comps.indexOf(name) > -1) || (!!computations[name]);
};

// Checks if redraw is precomputing
redraw.isPreComputing = function(name) {
	return (pre_comps.indexOf(name || default_compute_name) > -1);
};

// Dump out status
redraw.dump = function() {
	return [computations, cluster, pre_comps];
};

// Exports
module.exports = function(opts) {
	m_redraw = m.redraw;
	opts = opts || {};
	options.log = opts.log || false;
	options.redrawOnTimeout = opts.redrawOnTimeout || false;
	options.timeout = 1000 * (opts.timeout || 45);
	return redraw;
};
