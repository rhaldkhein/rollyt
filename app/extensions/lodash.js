var _ = require('lodash');
var md = require('mithril-data');
var ColorHash = require('color-hash');

// Variables
var lodash = _.runInContext();
var color = new ColorHash({
    lightness: 0.4,
    saturation: 0.6
});

// Overrides and extensions
_.mixin({

    'partialAll': function(object, methods) {
        for (var i = methods.length - 1; i >= 0; i--) {
            object[methods[i]] = _.partialRight(object[methods[i]], object);
        }
        return object;
    },

    'bindAll': function(object, bindMethods, partialMethods) {
        if (bindMethods) lodash.bindAll(object, bindMethods);
        if (partialMethods) _.partialAll(object, partialMethods);
        return object;
    },

    'stream': function(value) {
        return value.constructor === md.stream ? value : md.stream(value);
    },

    'isFalsy': function(value) {
        if (_.isString(value)) {
            switch (value.toLowerCase()) {
                case 'undefined':
                    return true;
                case 'null':
                    return true;
                case '':
                    return true;
                case '0':
                    return true;
                case 'false':
                    return true;
                case 'none':
                    return true;
                case 'rdundefined': // TAG: Mix playlist
                    return true;
                case '[]':
                    return true;
                case '{}':
                    return true;
                default:
                    return false;
            }
        } else {
            return !value;
        }
    },

    'getFullscreenElement': function() {
        return document.fullscreenElement ||
            document.webkitFullscreenElement ||
            document.mozFullScreenElement ||
            document.msFullscreenElement;
    },

    'getFullscreenChangeKey': function() {
        return (_.hasIn(document, 'onfullscreenchange') ? 'onfullscreenchange' : undefined) ||
            (_.hasIn(document, 'onwebkitfullscreenchange') ? 'onwebkitfullscreenchange' : undefined) ||
            (_.hasIn(document, 'onmozfullscreenchange') ? 'onmozfullscreenchange' : undefined) ||
            (_.hasIn(document, 'onmsfullscreenchange') ? 'onmsfullscreenchange' : 'onfullscreenchange');
    },

    'getThumbnail': function(text, w, h) {
        if(!text) text = '##';
        var len = w > h ? h : (w * 0.9);
        var canvas = document.createElement('canvas');
        canvas.width = w;
        canvas.height = h;
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = color.hex(text);
        ctx.fillRect(0, 0, w, h);
        ctx.fillStyle = 'rgba(255, 255, 255, 0.5)';
        ctx.font = (len * 0.6) + 'px Arial';
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillText(_.slice(text.match(/\b(\w)/g), 0, 2).join(''), (canvas.width / 2), (canvas.height / 2));
        var data = ctx.canvas.toDataURL();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvas.remove();
        ctx = null;
        canvas = null;
        return data;
    },

    'securize': function(url) {
        if (lodash.startsWith(url, 'http:'))
            return url.replace('http:', 'https:');
        return url;
    },

    'getNextPage': function(res) {
        var q = res.query;
        return (q.limit * q.page) < res.total ? (q.page + 1) : 0;
    }

});