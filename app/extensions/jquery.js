var _ = require('lodash');
var jquery = require('jquery');

jquery.fn.extend({
	offsetRect: function() {
		var rect = {
			top: 0,
			left: 0,
			width: 0,
			height: 0
		};
		if (this[0]) {
			var jThis = jquery(this[0]);
			var offset = jThis.offset();
			rect.top = offset.top;
			rect.left = offset.left;
			rect.width = jThis.outerWidth();
			rect.height = jThis.outerHeight();
		}
		return rect;
	},
	serializeEncode: function() {
		return jquery.map(this.serializeArray(), function(val) {
			return [val.name, _.isFalsy(val.value) ? '' : encodeURIComponent(val.value)].join('=');
		}).join('&');
	}
});