var _ = require('lodash');
var m = require('mithril');
var util = require('libs/util');
var contextManager = require('comp/contexts/contextManager');
var modalManager = require('comp/modals/modalManager');

var ccEU = [
  'AT', 'BE', 'BG', 'HR', 'CY', 'Re', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU', 'IE',
  'IT', 'LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'Ki', 'GB'
];

module.exports = {
  mousedown: function(e) {
    // Hide contexts when clicked outside of the menu
    if (contextManager.isActive()) {
      contextManager.deactivate(e);
    } else {
      e.redraw = false;
    }
  },
  clickUnderstand: function() {
    m.request('/~rest/acceptcookie')
      .then(function() {
        App.Props.cookieAccepted(true);
        m.redraw();
      });
  },
  viewCookieConsent: function() {
    if (_.indexOf(ccEU, App.Location.country_code()) < 0 || App.Props.cookieAccepted()) {
      return null;
    }
    return m('.cookie', {
        class: 'bg-near-black white absolute left-0 bottom-0 right-0 pa3 tc z-max'
      },
      m('span', 'This website uses cookies to ensure you get the best user experience.'),
      m('button.ml3 ph3 pv2 br1', {
        onclick: this.clickUnderstand
      }, 'I understand')
    );
  },
  view: function(node) {
    return m('div#app', {
      class: 'relative' + (util.isMobile ? ' mobile' : ' desktop'),
      onmousedown: this.mousedown
    }, [
      // Page child tree
      node.children,
      // Modal space
      modalManager.component(),
      // Context menu attach to app root
      contextManager.component(),
      // Try show cookie consent
      this.viewCookieConsent()
    ]);
  }
};