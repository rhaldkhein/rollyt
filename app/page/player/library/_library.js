var m = require('mithril');

module.exports = {
	oninit: function() {
		this.guest = App.User.isGuest();
	},
	oncreate: function() {
		if (this.guest) m.route.set('/player.page/signin');
	},
	view: function(node) {
		return m('div#library', this.guest ? null : node.children);
	}
};
