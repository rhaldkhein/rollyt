var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comChannelCollectionBlock = require('comp/blocks/channelCollectionBlock');
var comSubscribedChannels = require('comp/blocks/subscribedChannels');
var comPinBar = require('comp/pinbar');

module.exports = {

    oninit: function() {
        this.collectionStarredChannels = new md.Collection({
            model: App.Model.Channel,
            url: '/youtube/channel'
        });
    },

    loadStarred: function(options) {
        options = options || {};
        var state = this.state;
        var data = {
            type: 'channel',
            limit: App.Props.blockCollectionColumnsChannel() * state.rows()
        };
        if (!_.isEmpty(state.folder())) {
            data.folder = state.folder();
        }
        if (options.clear) {
            state.page(1);
            this.collection.clear();
        }
        data.page = state.page();
        state.loading(true);
        var self = this;
        md.store.get('~rest/star/all', data)
            .then(function(res) {
                self.state.pageNext(_.getNextPage(res));
                return self.collection.fetch({
                    _id: _.map(res.results, 'channel_id')
                }, {
                    clear: !!options.clear
                });
            })
            .catch(Console.error)
            .finally(function() {
                state.loading(false);
            })
            .then(m.redraw.computation());
    },

    loadMoreStarred: function() {
        var state = this.state;
        state.page(state.page() + 1);
        this._load();
    },

    clickPin: function(e, model) {
        comPinBar.addPin(model);
    },

    view: function() {
        return m('.library-channels', [
            m(comPinBar.component, {
                type: 'channel'
            }),
            m(comChannelCollectionBlock, {
                class: 'starred-channel',
                title: 'Starred Channels',
                folder: true,
                onload: this.loadStarred,
                onmore: this.loadMoreStarred,
                collection: this.collectionStarredChannels,
                onpin: this.clickPin,
                emptyMessage: [
                    m('span', 'You haven\'t starred a channel yet. '),
                    m('a', {
                        href: '#/'
                    }, 'Start Starring')
                ]
            }),
            m(comSubscribedChannels, {
                onpin: this.clickPin
            })
        ]);
    }

};