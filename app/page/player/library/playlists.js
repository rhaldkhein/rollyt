var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comPlaylistCollectionBlock = require('comp/blocks/playlistCollectionBlock');
var comLocalPlaylistCollectionBlock = require('comp/blocks/localplaylistCollectionBlock');
var comCreatedPlaylists = require('comp/blocks/createdPlaylists');
var comPinBar = require('comp/pinbar');
var ModalCreatePlaylist = require('comp/modals/modalCreatePlaylist');

module.exports = {
    oninit: function() {
        this.localPlaylistCollection = new md.LocalPlaylistCollection({
            model: App.Model.LocalPlaylist,
            url: '/localplaylist/list/profile'
        });

        this.starredLocalPlaylistCollection = new md.LocalPlaylistCollection({
            model: App.Model.LocalPlaylist,
            url: '/localplaylist/list'
        });

        this.starredPlaylistCollection = new md.Collection({
            model: App.Model.Playlist,
            url: '/youtube/playlist'
        });
    },

    loadLocalPlaylist: function(options) {
        // `this` context is collection block
        options = options || {};
        var state = this.state;
        var data = {
            limit: App.Props.blockCollectionColumns() * state.rows()
        };
        if (!_.isEmpty(state.folder())) {
            data.folder = state.folder();
        }
        if (options.clear) {
            state.page(1);
            this.collection.clear();
        }
        data.page = state.page();
        state.loading(true);
        var self = this;
        this.collection
            .fetch(
                data,
                {
                    path: 'results'
                },
                function(err, res) {
                    if (!err) self.state.pageNext(_.getNextPage(res));
                }
            )
            .catch(Console.error)
            .finally(function() {
                state.loading(false);
            })
            .then(m.redraw.computation());
    },

    loadMoreLocalPlaylist: function() {
        var state = this.state;
        state.page(state.page() + 1);
        this._load();
    },

    loadStarred: function(options) {
        // `this` context is collection block
        options = options || {};
        var state = this.state;
        var data = {
            type: 'playlist',
            limit: App.Props.blockCollectionColumns() * state.rows()
        };
        if (!_.isEmpty(state.folder())) {
            data.folder = state.folder();
        }
        if (options.clear) {
            state.page(1);
            this.collection.clear();
        }
        data.page = state.page();
        state.loading(true);
        var self = this;
        md.store
            .get('~rest/star/all', data)
            .then(function(res) {
                self.state.pageNext(_.getNextPage(res));
                return self.collection.fetch(
                    {
                        _id: _.map(res.results, 'playlist_id')
                    },
                    {
                        clear: !!options.clear
                    }
                );
            })
            .catch(Console.error)
            .finally(function() {
                state.loading(false);
                self.state.hide(!self.collection.size() && !data.folder);
            })
            .then(m.redraw.computation());
    },

    loadMoreStarred: function() {
        var state = this.state;
        state.page(state.page() + 1);
        this._load();
    },

    loadStarredLocal: function(options) {
        // `this` context is collection block
        options = options || {};
        var state = this.state;
        var data = {
            type: 'localplaylist',
            limit: App.Props.blockCollectionColumns() * state.rows()
        };
        if (!_.isEmpty(state.folder())) {
            data.folder = state.folder();
        }
        if (options.clear) {
            state.page(1);
            this.collection.clear();
        }
        data.page = state.page();
        state.loading(true);
        var self = this;
        md.store
            .get('~rest/star/all', data)
            .then(function(res) {
                self.state.pageNext(_.getNextPage(res));
                if (res.results && res.results.length) {
                    return self.collection.fetch(
                        {
                            _id: _.map(res.results, 'localplaylist_id').join(',')
                        },
                        {
                            clear: !!options.clear,
                            path: 'results'
                        }
                    );
                }
            })
            .then(function() {
                return self.collection.populate();
            })
            .catch(Console.error)
            .finally(function() {
                state.loading(false);
                self.state.hide(!self.collection.size() && !data.folder);
            })
            .then(m.redraw.computation());
    },

    loadMoreStarredLocal: function() {
        var state = this.state;
        state.page(state.page() + 1);
        this._load();
    },

    clickPin: function(e, model) {
        comPinBar.addPin(model);
    },

    clickPinOwn: function(e, model) {
        comPinBar.addPin(model, null, true);
    },

    view: function() {
        return m(
            '.library-playlists',
            m(
                'div',
                m(comPinBar.component, {
                    type: 'playlist'
                }),
                m(comLocalPlaylistCollectionBlock, {
                    class: 'local-playlist',
                    title: 'Rollyt Playlists',
                    newbutton: true,
                    folder: true,
                    onload: this.loadLocalPlaylist,
                    onmore: this.loadMoreLocalPlaylist,
                    collection: this.localPlaylistCollection,
                    onpin: this.clickPinOwn,
                    menuschema: menuSchemaLocalPlaylist
                }),
                m(comLocalPlaylistCollectionBlock, {
                    class: 'starred-localplaylist',
                    title: 'Starred Rollyt Playlists',
                    folder: true,
                    hide: true,
                    onload: this.loadStarredLocal,
                    onmore: this.loadMoreStarredLocal,
                    collection: this.starredLocalPlaylistCollection,
                    onpin: this.clickPin,
                    emptyMessage: [m('span', "You haven't starred a Rollyt playlist yet. ")]
                }),
                m(comPlaylistCollectionBlock, {
                    class: 'starred-playlist',
                    title: 'Starred Youtube Playlists',
                    folder: true,
                    hide: true,
                    onload: this.loadStarred,
                    onmore: this.loadMoreStarred,
                    collection: this.starredPlaylistCollection,
                    onpin: this.clickPin,
                    emptyMessage: [
                        m('span', "You haven't starred a playlist yet. "),
                        m(
                            'a',
                            {
                                href: '#/player.search?q=popular&type=playlist'
                            },
                            'Start Starring'
                        )
                    ]
                }),
                m(comCreatedPlaylists, {
                    onpin: this.clickPin
                })
            )
        );
    }
};

var menuSchemaLocalPlaylist = [
    {
        id: 'create_playlist',
        name: 'New Playlist',
        icon: 'add',
        separate: 'after',
        callback: function() {
            var self = this;
            ModalCreatePlaylist.show({
                title: 'Create Rollyt Playlist',
                folder: this.state.folder(),
                ondone: function() {
                    self.reload();
                }
            });
        }
    }
];
