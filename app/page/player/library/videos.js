var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comList = require('comp/player/list');
var comVideoCollectionBlock = require('comp/blocks/videoCollectionBlock');
var comLikedVideos = require('comp/blocks/likedVideos');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');
var comShare = require('comp/contexts/contextSharing');
var toast = require('toastr');

module.exports = {
  oninit: function() {
    this.starredVideoCollection = new md.Collection({
      model: App.Model.Video,
      url: '/youtube/video'
    });
  },

  loadStarred: function(options) {
    options = options || {};
    var state = this.state;
    var data = {
      type: 'video',
      limit: App.Props.blockCollectionColumns() * state.rows()
    };
    if (!_.isEmpty(state.folder())) {
      data.folder = state.folder();
    }
    if (options.clear) {
      state.page(1);
      this.collection.clear();
    }
    data.page = state.page();
    state.loading(true);
    var self = this;
    md.store
      .get('~rest/star/all', data)
      .then(function(res) {
        self.state.pageNext(_.getNextPage(res));
        return self.collection.fetch(
          {
            _id: _.map(res.results, 'video_id')
          },
          {
            clear: !!options.clear
          }
        );
      })
      .catch(Console.error)
      .finally(function() {
        state.loading(false);
      })
      .then(m.redraw.computation());
  },

  loadMoreStarred: function() {
    var state = this.state;
    state.page(state.page() + 1);
    this._load();
  },

  view: function() {
    return m(
      '.library-videos',
      m(
        'div',
        m(comVideoCollectionBlock, {
          class: 'starred-video',
          title: 'Starred Videos',
          folder: true,
          onload: this.loadStarred,
          onmore: this.loadMoreStarred,
          collection: this.starredVideoCollection,
          menu: menuSchemaStarred,
          viewtype: App.Preference.state.list_video_library() ? 'list' : null,
          emptyMessage: [
            m('span', "You haven't starred a video yet. "),
            m(
              'a',
              {
                href: '#/'
              },
              'Start Starring'
            )
          ]
        }),
        m(comLikedVideos, {
          viewtype: App.Preference.state.list_video_library() ? 'list' : null
        })
      )
    );
  }
};

function filterFolder(id) {
  if (id) {
    var mdlFolder = App.Folder.get(id);
    if (!(comShare.isJoined() && !mdlFolder.is_public())) return false;
  } else if (!comShare.isJoined()) {
    return false;
  }
  toast.error('Not allowed on shared player', 'Private Folder');
  return true;
}

var menuSchemaStarred = [
  {
    name: 'Play',
    icon: 'play_arrow',
    callback: function() {
      if (filterFolder(this.state.folder())) return;
      comList.addAndPlay(
        App.Model.List.create({
          _id: 'starred.video.' + App.User.id() + '.' + (this.state.folder() || '')
        })
      );
    }
  },
  {
    name: 'Queue',
    icon: 'add',
    callback: function() {
      if (filterFolder(this.state.folder())) return;
      comList.add(
        App.Model.List.create({
          _id: 'starred.video.' + App.User.id() + '.' + (this.state.folder() || '')
        })
      );
    }
  },
  {
    name: 'Add to Playlist',
    icon: 'playlist_add',
    separate: 'after',
    callback: function() {
      var self = this;
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          var mdl = App.Model.List.create({
            _id: 'starred.video.' + App.User.id() + '.' + (self.state.folder() || '')
          });
          mdl.populate().then(function() {
            playlist.addItems([
              {
                resource_type: mdl.getKind(),
                resource_id: mdl.id()
              }
            ]);
          });
        }
      });
    }
  }
];
