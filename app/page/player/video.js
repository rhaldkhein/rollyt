var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var moment = require('moment');
var util = require('libs/util');
var youtube = require('libs/youtube');
var comList = require('comp/player/list');
var elButtonIcon = require('comp/elements/buttonIcon');
var comHeader = require('comp/partials/headerVideo');
var comResult = require('comp/search/result');
var comMenu = require('comp/contexts/contextMenu');
var viewComingSoon = require('comp/views/errors/description');
var viewLoading = require('comp/views/loading');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');

var bindMethods = ['request', 'clickLoadMore', 'clickRelated', 'clickMix', 'clickDetails', 'clickExpandDesc'];
var partMethods = ['clickMoreRelated', 'clickMoreMix'];
var defaultParam = {
  part: 'snippet',
  maxResults: Config.maxResults
};

var _state = md.State.create({
  id: null,
  type: null, // 'related', 'mix', 'details'
  mixid: null,
  requesting: true,
  loadMoreToken: null,
  model: null,
  descExpand: false
});

module.exports = {

  oninit: function() {
    _.bindAll(this, bindMethods, partMethods);
    this.state = _state;
    if (_state.id() != m.route.param('id')) {
      _state.id(m.route.param('id'));
      _state.mixid(null);
    }
    _state.type(m.route.param('type') || 'related');
    _state.requesting(true);
    _state.loadMoreToken(null);
    _state.descExpand(!!m.route.param('expand') || false);
    _state.model(!_state.id() ? null : App.Model.Video.create({
      _id: m.route.param('id')
    }));
    this.collection = new md.Collection({
      url: (this.state.type() == 'mix' ? '/youtube/playlistitems' : '/youtube/search'),
      model: (this.state.type() == 'mix' ? App.Model.PlaylistItem : App.Model.Video)
    });
  },

  oncreate: function() {
    util.nextTick(this.request);
  },

  onremove: function() {
    // Will thrown error `requesting prop of undefined (this.state)` 
    // when `request` method tries to overlap
    // this.state = null;
  },

  request: function() {
    var self = this;
    var param = _.clone(defaultParam);
    var done = m.redraw.computation();
    var _model = this.state.model();
    this.state.requesting(true);
    // End function
    var end = function() {
      self.state.requesting(false);
      done();
    };
    // Display no result;
    if (!_model) return end();
    // Mutate token
    if (this.state.loadMoreToken()) param.pageToken = this.state.loadMoreToken();
    else delete param.pageToken;
    // Request for model
    if (!_model.isSaved()) _model.fetch().catch(Console.error).finally(m.redraw.computation());
    // Request for mix playlist
    var promMix = youtube.playlistInfo('RD' + _model.id())
      .then(function(playlist) {
        self.state.mixid(playlist ? playlist.id : null);
      })
      .catch(Console.error).finally(m.redraw.computation());
    // Request variation
    if (this.state.type() == 'mix') {
      promMix.then(function() {
        param.playlistId = self.state.mixid();
        return self.collection.fetch(param, {
          path: 'items'
        }, function(err, response) {
          self.state.loadMoreToken(response && response.nextPageToken);
        });
      }).catch(Console.error).finally(end);
    } else if (this.state.type() == 'related') {
      param.relatedToVideoId = _model.id();
      this.collection.fetch(param, {
        path: 'items'
      }, function(err, response) {
        self.state.loadMoreToken(response && response.nextPageToken);
      }).catch(Console.error).finally(end);
    } else {
      end();
    }
  },

  getModelRelated: function() {
    var videoId = m.route.param('id');
    if (videoId && videoId !== 'undefined') {
      return App.Model.List.create({
        _id: 'related.' + videoId
      });
    }
  },

  getModelMix: function() {
    var videoId = m.route.param('id');
    if (videoId && videoId !== 'undefined') {
      return App.Model.List.create({
        _id: 'related.' + videoId
      });
    }
  },

  clickLoadMore: function(e) {
    e.preventDefault();
    this.state.requesting(true);
    util.nextTick(this.request);
  },

  clickRelated: function(e) {
    var p = _.omit(m.route.param(), 'page');
    p.type = 'related';
    m.route.set('/player.video?' + m.buildQueryString(p));
    e.redraw = false;
  },

  clickMix: function(e) {
    var p = _.omit(m.route.param(), 'page');
    p.type = 'mix';
    m.route.set('/player.video?' + m.buildQueryString(p));
    e.redraw = false;
  },

  clickDetails: function(e) {
    var p = _.omit(m.route.param(), 'page');
    p.type = 'details';
    m.route.set('/player.video?' + m.buildQueryString(p));
    e.redraw = false;
  },

  clickMoreRelated: function(e, self) {
    var mdl = self.state.model();
    if (mdl && mdl.isSaved()) comMenu.show(this, menuRelated, self);
  },

  clickMoreMix: function(e, self) {
    comMenu.show(this, menuMix, self);
  },

  clickExpandDesc: function() {
    this.state.descExpand(!this.state.descExpand());
  },

  view: function() {
    var _type = this.state.type();
    var _model = this.state.model();
    var _saved = _model && _model.isSaved();
    return m('#video', {
        class: (_saved ? '' : 'unsaved')
      },
      m('.list', [
        m(comHeader, {
          loading: this.state.requesting,
          model: this.state.model
        }),
        (_saved || !this.state.requesting() ? m('.head-tab-actions',
          m('span', {
              class: (_type == 'related')
            },
            m('a.tab more', {
              onclick: this.clickRelated
            }, 'Related Videos'),
            m(elButtonIcon, {
              icon: 'more_vert',
              onclick: this.clickMoreRelated
            })
          ),
          m('span', {
              class: (_type == 'mix') + (this.state.mixid() ? '' : ' hide')
            },
            m('a.tab more', {
              onclick: this.clickMix
            }, 'Mix Playlist'),
            m(elButtonIcon, {
              icon: 'more_vert',
              onclick: this.clickMoreMix
            })
          ), (!_saved ? null : m('span', {
              class: (_type == 'details')
            },
            m('a.tab', {
              onclick: this.clickDetails
            }, 'Details')
          ))
        ) : null),
        ((_type == 'details') ? m('.details', !_saved ? viewLoading() : m('.pb4 f6 lh-copy',
          m('.info fw5 pa3', 'Published ' + moment(_model.published()).fromNow()),
          m('.desc', {
              class: (this.state.descExpand() ? 'smore' : 'sless')
            },
            m('.mb4 ph3', _model.description())
          ),
          m('.pa3',
            m('button.mid-gray', {
              onclick: this.clickExpandDesc
            }, this.state.descExpand() ? 'Show Less' : 'Show More')
          ),
          m('.fw5 mv3 pa3 bt b--black-10', 'Comments'),
          viewComingSoon([
            'Comments are not available yet. ',
            m('a', {
              href: 'https://www.youtube.com/watch?v=' + m.route.param('id'),
              target: '_blank'
            }, 'View on Youtube')
          ])
        )) : m(comResult, {
          collection: this.collection,
          loading: this.state.requesting,
          onloadmore: this.clickLoadMore,
          hasnext: this.state.loadMoreToken
        }))
      ])
    );
  }

};

var menuRelated = [{
  name: 'Play',
  icon: 'play_arrow',
  callback: function() {
    comList.addAndPlay(this.getModelRelated());
  }
}, {
  name: 'Queue',
  icon: 'add',
  callback: function() {
    comList.add(this.getModelRelated());
  }
}, {
  name: 'Add to Playlist',
  icon: 'playlist_add',
  callback: function() {
    var model = this.getModelRelated();

    function addPlaylist(mdl) {
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems([{
            resource_type: model.getKind(),
            resource_id: model.id()
          }]);
        }
      });
      // If mdl exist, then trigger by promise
      if (mdl) m.redraw();
    }
    if (!model.isSaved()) {
      model.fetch().then(addPlaylist).catch(Console.error);
    } else {
      addPlaylist();
    }
  }
}];

var menuMix = [{
  name: 'Play',
  icon: 'play_arrow',
  callback: function() {
    comList.addAndPlay(App.Model.List.create({
      _id: 'playlist.' + this.state.mixid()
    }));
  }
}, {
  name: 'Queue',
  icon: 'add',
  callback: function() {
    comList.add(App.Model.List.create({
      _id: 'playlist.' + this.state.mixid()
    }));
  }
}, {
  name: 'Add to Playlist',
  icon: 'playlist_add',
  callback: function() {
    var model = App.Model.List.create({
      _id: 'playlist.' + this.state.mixid()
    });

    function addPlaylist(mdl) {
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems([{
            resource_type: model.getKind(),
            resource_id: model.id()
          }]);
        }
      });
      // If mdl exist, then trigger by promise
      if (mdl) m.redraw();
    }
    if (!model.isSaved()) {
      model.fetch().then(addPlaylist).catch(Console.error);
    } else {
      addPlaylist();
    }
  }
}];