var _ = require('lodash'),
  m = require('mithril');

module.exports = {
  oncreate: function(node) {
    var param = m.route.param();
    m.request({
        url: '/~page/' + param.query,
        data: _.omit(param, ['page', 'query']),
        headers: {
          'X-Requested-With': 'XMLHttpRequest'
        },
        deserialize: function(value) {
          return value;
        }
      })
      .then(function(data) {
        m.render(node.dom, m.trust(data));
      })
      .catch(function() {
        App.Modules.toastr.error('Error while displaying page');
      });
  },
  view: function() {
    return m('div#pageframe');
  }
};