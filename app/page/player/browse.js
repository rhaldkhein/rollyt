var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var util = require('libs/util');
var elButtonIcon = require('comp/elements/buttonIcon');
var elButtonState = require('comp/elements/buttonState');
var viewEmpty = require('comp/views/errors/empty');
var ModalAddBlock = require('comp/modals/modalAddBlock');
var comVideoList = require('comp/blocks/videoList');

var code = App.Location.country_code();
var defaultBlocks = [
  {
    id: 'popular..' + code
  },
  {
    id: 'popular.10.' + code
  }
];

var bindMethods = ['clickArrangeBlocks', 'dragMove', 'clickAddBlock'];
var partMethods = ['remove'];

module.exports = {
  oninit: function() {
    _.bindAll(this, bindMethods, partMethods);
    this.state = md.State.create({
      arranging: ['', 'active'],
      draggable: false
    });
    this.collection = new md.Collection({
      state: {
        collapse: false,
        index: -1
      }
    });
    this.limit = App.PreferenceList.find({ name: 'home_blocks' }).max_value();
    this.read();
  },

  read: function() {
    var blocks = App.Preference.state.home_blocks();
    if (!blocks.length) {
      blocks = _.cloneDeep(defaultBlocks);
      App.Preference.state.home_blocks(blocks);
    } else if (blocks.length > this.limit) {
      blocks = _.slice(blocks, 0, this.limit);
      App.Preference.state.home_blocks(blocks);
    }
    this.collection.clear();
    var self = this;
    _.each(blocks, function(block) {
      if (block.id) {
        self.collection.add(
          App.Model.List.create({
            _id: block.id
          })
        );
      }
    });
  },

  write: function() {
    var blocks = this.collection.map(function(model) {
      return {
        id: model.id(),
        param: model.params()
      };
    });
    App.Preference.state.home_blocks(blocks);
  },

  remove: function(e, item, data, self) {
    self.collection.remove(this.model.id());
    self.write();
  },

  getRows: function(model) {
    // App.Props.blockCollectionColumns
    if (_.startsWith(model.id(), 'channel.')) {
      return util.isMobile ? 2 : 1;
    } else {
      return util.isMobile ? 5 : 2;
    }
  },

  clickAddBlock: function() {
    var self = this;
    ModalAddBlock.show({
      onadd: function() {
        self.read();
      }
    });
  },

  clickArrangeBlocks: function() {
    var self = this;
    this.state.draggable(!!this.state.arranging().current);
    this.collection.forEach(function(model) {
      self.collection.stateOf(model).collapse(self.state.draggable());
    });
  },

  dragMove: function(src, dest) {
    var iSrc = this.collection.findIndex(['_id', src.model.id()]);
    var iDes = this.collection.findIndex(['_id', dest.model.id()]);
    var models = this.collection.models;
    while (iSrc < 0) {
      iSrc += models.length;
    }
    while (iDes < 0) {
      iDes += models.length;
    }
    if (iDes >= models.length) {
      var k = iDes - models.length;
      while (k-- + 1) {
        models.push(undefined);
      }
    }
    models.splice(iDes, 0, models.splice(iSrc, 1)[0]);
    this.write();
  },

  view: function() {
    var self = this;
    return m(
      'div#browse',
      m(
        'div',
        !this.collection.size()
          ? viewEmpty()
          : this.collection.map(function(model, idx) {
              var state = self.collection.stateOf(model);
              state.index(idx);
              return m(comVideoList, {
                key: model.id() + self.collection.size() + self.state.draggable(),
                rows: self.getRows(model),
                collapse: state.collapse,
                draggable: self.state.draggable,
                index: state.index,
                ondelete: self.remove,
                ondragmove: self.dragMove,
                listmodel: model
              });
            })
      ),
      m(
        'div.f6 lh-copy tc gray mt3',
        {
          class: this.state.arranging().current ? '' : 'hide'
        },
        'You can now drag and drop to re-arrange blocks'
      ),
      m(
        '.actions',
        this.collection.size() >= this.limit
          ? null
          : m(elButtonIcon, {
              icon: 'add',
              onclick: this.clickAddBlock,
              title: 'Add New Block'
            }),
        this.collection.size() > 1
          ? m(elButtonState, {
              icon: 'format_line_spacing',
              onclick: this.clickArrangeBlocks,
              state: this.state.arranging(),
              title: 'Re-arrange Blocks'
            })
          : null
      )
    );
  }
};
