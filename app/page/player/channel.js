var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comResult = require('comp/search/result');
var comHeader = require('comp/partials/headerChannel');
var moment = require('moment');
var util = require('libs/util');

var allowedParam = ['id', 'q', 'type', 'sort', 'date', 'open'];
var bindMethods = ['requestModel', 'requestCollection', 'requestAny',
    'clickLoadMore', 'changeHeaderFilter', 'changeHeaderTab', 'searchHeader', 'route'
];

var defaultParam = {
    channelId: '',
    type: 'video',
    maxResults: Config.maxResults,
    order: 'date'
};

module.exports = {
    oninit: function Channel() {
        _.bindAll(this, bindMethods);
        this.channelId = md.stream(m.route.param('id'));
        this.model = App.Model.Channel.create({
            _id: this.channelId()
        });
        this.param = md.State.create(_.assign({
            id: undefined,
            q: undefined,
            type: 'video',
            sort: undefined,
            date: undefined,
            open: undefined
        }, _.pick(m.route.param(), allowedParam)));
        this.state = md.State.create({
            requesting: true,
            loadMoreToken: null,
            totalPlaylist: 0,
            totalVideos: 0
        });
        this.collection = new md.Collection({
            url: '/youtube/search'
        });
        // Dummy computation, released when computation methid is called
        m.redraw.precomputation();
    },
    onremove: function() {
        // Cutting references
        // this.collection.destroy();
        // this.state = null;
        // this.channelId = null;
    },
    oncreate: function() {
        if (this.model.isSaved()) {
            util.nextTick(this.requestAny);
        } else {
            util.nextTick(this.requestModel);
        }
    },
    requestAny: function() {
        if (!m.route.param('type') && this.model.videos() < 1) {
            this.param.type('playlist');
        }
        util.nextTick(this.requestCollection);
    },
    requestModel: function() {
        this.model.fetch().catch(Console.error).finally(this.requestAny);
    },
    requestCollection: function(clear) {
        if (clear) {
            this.collection.clear();
            this.state.loadMoreToken(null);
        }
        this.state.requesting(true);
        var param = _.clone(defaultParam);
        param.channelId = this.channelId();
        //Type
        if (this.param.type())
            param.type = this.param.type();
        // Query
        if (!_.isEmpty(this.param.q()))
            param.q = this.param.q();
        else
            delete param.q;
        // Sort
        if (this.param.sort())
            param.order = this.param.sort();
        // Date
        if (this.param.date()) {
            var date;
            switch (this.param.date()) {
                case 'today':
                    date = moment().subtract(1, 'days');
                    break;
                case 'yesterday':
                    date = moment().subtract(2, 'days');
                    break;
                case 'week':
                    date = moment().subtract(7, 'days');
                    break;
                case 'month':
                    date = moment().subtract(1, 'months');
                    break;
                case 'year':
                    date = moment().subtract(1, 'years');
                    break;
            }
            if (date)
                param.publishedAfter = date.toISOString();
        }
        // Token
        if (this.state.loadMoreToken())
            param.pageToken = this.state.loadMoreToken();
        else
            delete param.pageToken;
        var self = this;
        var done = m.redraw.computation();
        this.collection.opt('model', this.getCollectionModel());
        this.collection.fetch(param, {
                path: 'items'
            }, function(err, response) {
                if (!err) {
                    if (self.param.type() === 'playlist') {
                        self.state.totalPlaylist(response.pageInfo.totalResults);
                    }
                    self.state.loadMoreToken(response.nextPageToken);
                }
            })
            .catch(Console.error)
            .finally(function() {
                self.state.requesting(false);
                done();
            });
    },
    getCollectionModel: function() {
        if (this.param.type() === 'video') {
            return App.Model.Video;
        } else if (this.param.type() === 'playlist') {
            return App.Model.Playlist;
        }
    },
    clickLoadMore: function(e) {
        e.preventDefault();
        this.requestCollection();
    },
    changeHeaderFilter: function(e, state) {
        e.preventDefault();
        e.redraw = false;
        if (state.date())
            this.param.date(state.date());
        if (state.sort())
            this.param.sort(state.sort());
        this.param.open(state.open());
        this.route();
    },
    changeHeaderTab: function(e, tab, state) {
        if (e) {
            e.preventDefault();
            e.redraw = false;
        }
        this.param.type(tab);
        this.param.open(state.open());
        this.route();
    },
    searchHeader: function(e, keyword, state) {
        e.redraw = false;
        this.param.q(keyword);
        this.param.open(state.open());
        this.route();
    },
    route: function() {
        m.route.set('/player.channel?' + m.buildQueryString(this.param.toJson()));
    },
    view: function() {
        return m('#channel',
            m('.list', [
                m(comHeader, {
                    model: this.model,
                    onsearch: this.searchHeader,
                    onchangefilter: this.changeHeaderFilter,
                    onchangetab: this.changeHeaderTab,
                    stateDate: this.param.date,
                    stateSort: this.param.sort,
                    stateType: this.param.type,
                    totalPlaylist: this.state.totalPlaylist
                }),
                m(comResult, {
                    collection: this.collection,
                    loading: this.state.requesting,
                    onloadmore: this.clickLoadMore,
                    hasnext: this.state.loadMoreToken
                })
            ])
        );
    }
};