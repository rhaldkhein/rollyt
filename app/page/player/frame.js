var _ = require('lodash'),
    m = require('mithril'),
    viewLoadingIcon = require('comp/views/loading/icon');

/* *
 * If you want to add hash. Set it as querystring like "0=<hash value>".
 * Ex:
 *      "/player.frame/help?a=1&0=hash"
 *      "/~frame/help?a=1#hash"
 */

var _loading = false;

module.exports = {
    oninit: function() {
        var param = m.route.param();
        this.url = '/~frame/' + param.query;
        this.hash = param[0];
        this.data = _.omit(param, ['page', 'query', '0']);
        _loading = true;
    },
    onupdateIframe: function(node) {
        m.nextTick(function() {
            if (node.dom.contentWindow && node.dom.contentWindow.document.body)
                node.dom.height = node.dom.contentWindow.document.body.offsetHeight + 60;
        });
    },
    onbeforeremoveIframe: function(node) {
        // Disposing iframe content
        var doc = node.dom.contentDocument || node.dom.contentWindow.document;
        doc.documentElement.innerHTML = '';
    },
    loadFrame: function() {
        _loading = false;
        m.redraw();
    },
    view: function() {
        return m('.frame-page',
            m('iframe', {
                onupdate: this.onupdateIframe,
                onbeforeremove: this.onbeforeremoveIframe,
                class: (_loading ? 'transparent noheight' : ''),
                src: this.url + '?' + m.buildQueryString(this.data) + (this.hash ? '#' + this.hash : ''),
                onload: this.loadFrame
            }),
            (_loading ? viewLoadingIcon() : null)
        );
    }
};