var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comResult = require('comp/search/result');
var elIcon = require('comp/elements/icon');
// var elButtonIcon = require('comp/elements/buttonIcon');
// var elTextbox = require('comp/elements/textbox');
var util = require('libs/util');

var bindMethods = ['request', 'clickLoadMore'];
// var _collectionPlaylist = null;
// var _collectionChannel = null;

module.exports = {
  oninit: function() {
    _.bindAll(this, bindMethods);
    this.model = App.Model.User.create({
      _id: m.route.param('id')
    });
    this.collection = new md.Collection();
    this.state = md.State.create({
      currentTab: m.route.param('type') || 'playlist',
      starred: md.stream(false),
      requesting: true,
      nextToken: null,
      page: 1,
      pageNext: 0,
      limit: Config.maxResults,
      totalPlaylist: 0,
      totalVideos: 0
    });
  },
  oncreate: function() {
    util.nextTick(this.request);
    this.model.fetch().catch(Console.error);
  },
  request: function() {
    // Create collection
    var self = this,
      query,
      path;
    if (this.state.currentTab() == 'playlist') {
      this.collection.opt({
        url: '/localplaylist/list/profile',
        model: App.Model.LocalPlaylist
      });
      path = 'results';
      query = {
        profileid: this.model.id(),
        page: this.state.page(),
        limit: this.state.limit()
      };
    } else if (this.state.currentTab() == 'channel' && m.route.param('id') == App.User.id()) {
      this.collection.opt({
        url: '/youtube/channellist',
        model: App.Model.Channel
      });
      path = 'items';
      query = {
        mine: true,
        maxResults: this.state.limit(),
        part: 'snippet'
      };
      if (this.state.nextToken()) query.pageToken = this.state.nextToken();
    } else {
      return;
    }
    this.state.requesting(true);
    this.collection
      .fetch(
        query,
        {
          path: path
        },
        function(err, response) {
          if (!err) {
            if (response.nextPageToken) {
              self.state.nextToken(response.nextPageToken);
              self.state.pageNext(response.nextPageToken || 0);
            } else if (response.query && response.query.page) {
              self.state.page(response.query.page);
              self.state.pageNext(_.getNextPage(response));
            }
          }
        }
      )
      .catch(Console.error)
      .finally(function() {
        self.state.requesting(false);
        m.redraw.nexttick();
      });
  },
  clickLoadMore: function(e) {
    e.preventDefault();
    e.redraw = false;
    this.state.page(this.state.pageNext());
    util.nextTick(this.request);
  },
  view: function() {
    return m(
      '#profile.page-narrow',
      m('.list', [
        !(this.model && this.model.isSaved())
          ? null
          : m(
              '.header',
              m(
                '.upper',
                m(
                  '.top cf pv3',
                  m(
                    '.avatar fl dib ml4 relative z-1 mr3 br1',
                    m(
                      'a',
                      m('img.db pa1 bg-white', {
                        src: this.model.current_account().avatar()
                      })
                    )
                  ),
                  m(
                    'h1.f3 lh-title',
                    this.model.current_account().firstname() + ' ' + (this.model.current_account().lastname() || '')
                  ),
                  m('.mb2', m('.views sub', util.numberComma(this.model.item_count()) + ' Playlists'))
                )
              ),
              m(
                '.head-tab',
                m(
                  'a',
                  {
                    class: 'tab' + (this.state.currentTab() === 'playlist' ? ' selected' : ''),
                    href: '#/player.profile?type=playlist&id=' + App.User.id()
                  },
                  [
                    m(elIcon, {
                      icon: 'playlist_play'
                    }),
                    App.Info.name + ' Playlists'
                  ]
                ),
                m.route.param('id') != App.User.id()
                  ? null
                  : m(
                      'a',
                      {
                        class: 'tab' + (this.state.currentTab() === 'channel' ? ' selected' : ''),
                        href: '#/player.profile?type=channel&id=' + App.User.id()
                      },
                      [
                        m(elIcon, {
                          icon: 'account_circle'
                        }),
                        'My Channels'
                      ]
                    )
                // m('a', {
                //      onclick: this.clickButtonFilter
                //  },
                //  m(elIcon, {
                //      icon: 'expand_' + (this.state.open() ? 'less' : 'more')
                //  })
                // )
              )
            ),
        m(comResult, {
          collection: this.collection,
          loading: this.state.requesting,
          onloadmore: this.clickLoadMore,
          hasnext: this.state.pageNext
        })
      ])
    );
  }
};
