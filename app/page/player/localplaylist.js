var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var comResult = require('comp/search/result');
var comMenu = require('comp/contexts/contextMenu');
var comList = require('comp/player/list');
var viewError = require('comp/views/errors');
var viewEmpty = require('comp/views/errors/empty');
var util = require('libs/util');
var ContextStar = require('comp/contexts/contextStar');
var starring = require('libs/starring');
var ModalCreatePlaylist = require('comp/modals/modalCreatePlaylist');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');

var bindMethods = ['request', 'clickLoadMore', 'moveItem', 'clickPlay', 'clickAdd', 'clickAddPlaylist'];
var partMethods = ['clickStar', 'clickMoreAction'];

module.exports = {
  oninit: function() {
    _.bindAll(this, bindMethods, partMethods);

    this.model = App.Model.LocalPlaylist.create({
      _id: m.route.param('id')
    });

    this.state = md.State.create({
      requesting: true,
      page: 1,
      pageNext: 0,
      limit: Config.maxResults,
      error: null,
      starred: md.stream(false),
      starFolder: null
    });

    this.collection = new md.ItemCollection({
      model: App.Model.LocalPlaylistItem,
      url: '/localplaylistitem/list'
    });
  },

  onremove: function() {
    // Cutting references
    // this.collection.clear();
    // this.state = null;
    // this.channelId = null;
  },

  oncreate: function() {
    // util.nextTick(this.request);
    var self = this;
    this.model
      .fetch({
        background: true
      })
      .then(function(model) {
        if (!App.User.isGuest())
          starring.check('localplaylist', self.model.id(), self.state.starred, self.state.starFolder);
        self.request();
        self.collection.opt('owned', model.is_mine());
        return model.user().populate();
      })
      .catch(this.state.error)
      .finally(m.redraw.computation());
  },

  request: function(clear) {
    var self = this;
    var param = {};
    // m.startComputation();
    var done = m.redraw.computation();
    this.state.requesting(true);

    if (m.route.param('id')) {
      param.playlistid = m.route.param('id');
    } else {
      // Display no result;
      this.state.requesting(false);
      done();
      return;
    }
    // Page
    param.page = clear ? 1 : this.state.page();
    param.limit = this.state.limit();
    // Fetch
    this.collection
      .fetch(
        param,
        {
          path: 'results',
          clear: !!clear
        },
        function(err, response) {
          if (!err) {
            self.state.page(response.query.page);
            self.state.pageNext(_.getNextPage(response));
            self.collection.sortByOrder(response.order);
          }
        }
      )
      .then(function() {
        return self.collection.populate();
      })
      .catch(Console.error)
      .finally(function() {
        self.state.requesting(false);
        done();
      });
  },

  clickLoadMore: function(e) {
    e.preventDefault();
    this.state.requesting(true);
    this.state.page(this.state.pageNext());
    util.nextTick(this.request);
  },

  clickMoreAction: function(e, self) {
    comMenu.show(this, menuHeaderActions, self);
  },

  moveItem: function(srcModel, desModel) {
    this.model.moveItem(srcModel, desModel);
  },

  clickPlay: function() {
    comList.changePlaylist(this.model);
  },

  clickAdd: function() {
    comList.add(this.model);
  },

  clickStar: function(e, loading, self) {
    var that = this;
    var starred = self.state.starred;
    var type = 'localplaylist';
    var id = self.model.id();
    if (starred()) {
      ContextStar.show(this, self, type, id, starred, self.state.starFolder);
    } else {
      loading(true);
      starring
        .star(type, id, starred)
        .then(function() {
          loading(false);
          ContextStar.show(that, self, type, id, starred, self.state.starFolder);
        })
        .finally(function() {
          loading(false);
          m.redraw();
        });
    }
  },

  clickAddPlaylist: function() {
    var self = this;
    ModalSelectLocalPlaylist.show({
      onselect: function(e, playlist) {
        playlist.addItems([
          {
            resource_type: self.model.getKind(),
            resource_id: self.model.id()
          }
        ]);
      }
    });
  },

  view: function() {
    var viewHeader = null;
    if (this.model && this.model.isSaved()) {
      var imgUrl = this.model.image() || this.model.imageFallback();
      viewHeader = [
        m(
          '.upper cf relative',
          m(
            '.thumbnail fl w-40',
            m(
              'a',
              {
                onclick: this.clickPlay
              },
              m('div', [
                m('img', {
                  src: imgUrl
                }),
                m('img', {
                  src: imgUrl
                }),
                m('img', {
                  src: imgUrl
                })
              ])
            )
          ),
          m(
            '.content fr w-60 pb4',
            m(
              'h1.lh-title mr4',
              {
                class: this.model.title().length > 70 ? 'f4' : 'f3'
              },
              this.model.title()
            ),
            m(
              'div.badges',
              m('span.badge dib ph2 pv1 br2 bg-blue f6', 'Rollyt Playlist'),
              this.model.is_private() ? m('i.icon icon-lock') : null
            ),
            m('.mb2 mt3', m('.videos sub', util.numberComma(this.model.item_count()) + ' items'))
          )
        ),
        !this.model.isSaved()
          ? null
          : m(
              '.action-wrap cf bt b--light-gray pv2 ph3',
              m(
                '.owner dib fl w-50 f3',
                m(elIcon, {
                  icon: 'account_circle'
                }),
                m(
                  'a.v-mid ml4 dib',
                  {
                    class: this.model.channel().title().length > 30 ? 'f6' : 'f5',
                    href: '#/player.profile?id=' + this.model.channel().id()
                  },
                  this.model.channel().title()
                )
              ),
              m(
                '.actions fl w-50 tr',
                m(elButtonIcon, {
                  icon: 'play_arrow',
                  onclick: this.clickPlay,
                  title: 'Play Video'
                }),
                m(elButtonIcon, {
                  icon: 'add',
                  onclick: this.clickAdd,
                  title: 'Queue'
                }),
                App.User.isGuest() || this.model.user().id() === App.User.id()
                  ? null
                  : m(elButtonIcon, {
                      icon: 'star' + (this.state.starred() ? '' : '_border'),
                      onclick: this.clickStar,
                      loading: true,
                      title: 'Star / Bookmark'
                    }),
                App.User.isGuest()
                  ? null
                  : m(elButtonIcon, {
                      icon: 'playlist_add',
                      title: 'Add to Playlist',
                      onclick: this.clickAddPlaylist
                    }),
                App.User.isGuest() || this.model.user().id() !== App.User.id()
                  ? null
                  : m(elButtonIcon, {
                      icon: 'more_vert',
                      onclick: this.clickMoreAction
                    })
              )
            )
      ];
    }
    return m(
      '#playlist',
      viewError.error(this.state.error()) ||
        m('.list', [
          m(
            '.playlist-header',
            {
              key: 'playlist_header'
            },
            viewHeader
          ),
          m(comResult, {
            key: 'is_mine_' + this.model.is_mine(), // To recreate when changed
            collection: this.collection,
            loading: this.state.requesting,
            onloadmore: this.clickLoadMore,
            onmoveitem: this.moveItem,
            showIndex: true,
            editable: this.model.is_mine(),
            extra: this,
            itemContextMenu: itemContextMenu,
            hasnext: this.state.pageNext,
            viewNoResult: viewEmpty(
              [
                m('span', 'Could not find anything to display here. '),
                m('br'),
                m(
                  'a.dib mt2',
                  {
                    href: '#/'
                  },
                  'Start Adding Items'
                )
              ],
              'Empty'
            )
          })
        ])
    );
  }
};

// Menu for Account
var itemContextMenu = [
  {
    name: 'Move to First',
    callback: function() {
      var extra = this.options.extra;
      extra.model
        .moveItem(this.parent, true)
        .then(function() {
          util.nextTick(function() {
            extra.request(true);
          });
        })
        .catch(Console.error);
    }
  },
  {
    name: 'Move to Last',
    callback: function() {
      var extra = this.options.extra;
      extra.model
        .moveItem(this.parent, false)
        .then(function() {
          util.nextTick(function() {
            extra.request(true);
          });
        })
        .catch(Console.error);
    }
  },
  {
    name: 'Set as Playlist Thumbnail',
    callback: function() {
      var res = this.parent.resource(),
        url = res.image();
      if (res.id() && util.isUrlImage(url)) {
        this.options.extra.model.image(url);
        this.options.extra.model.save();
      } else {
        App.Modules.toastr.error('Unable to set that image');
      }
    }
  },
  {
    name: 'Remove',
    icon: 'remove',
    separate: 'before',
    callback: function() {
      var playlist = this.parent.local_playlist();
      this.parent
        .destroy()
        .then(function() {
          if (playlist instanceof App.Model.LocalPlaylist) return playlist.fetch();
        })
        .catch(function() {
          App.Modules.toastr.error('Item not deleted');
        })
        .finally(m.redraw);
    }
  }
];

var menuHeaderActions = [
  {
    name: 'Edit',
    icon: 'mode_edit',
    callback: function() {
      ModalCreatePlaylist.show({
        title: 'Edit Rollyt Playlist',
        playlist: this.model
      });
    }
  },
  {
    id: 'startup_playlist',
    name: 'Set as Startup Playlist',
    separate: 'after',
    callback: function() {
      App.Preference.state.startup_playlist(
        App.Preference.state.startup_playlist() == this.model.id() ? '' : this.model.id()
      );
    }
  },
  {
    name: 'Delete Playlist',
    icon: 'delete',
    callback: function() {
      this.model.destroy().finally(function() {
        m.route.set('/player.library.playlists');
      });
    }
  }
];

menuHeaderActions.parser = function(item) {
  if (item.id == 'startup_playlist') {
    item.checked = this.model.id() == App.Preference.state.startup_playlist();
  }
};
