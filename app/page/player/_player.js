var _ = require('lodash');
var m = require('mithril');
var ps = require('perfect-scrollbar');
var comPlayer = require('comp/player/player');
var comSearchBar = require('comp/searchBar');
var comFooter = require('comp/partials/footer');
var comModalVideoPlayer = require('comp/modals/videoPlayer');
var comFeaturesSideBlock = require('comp/sideblocks/features');
var comMessages = require('comp/messages');
var current = require('comp/player/current');
var elIcon = require('comp/elements/icon');
var enquire = require('enquire.js');
var mode = require('libs/mode');
var util = require('libs/util');
var domScroll = null;

// 6 columns
enquire.register('screen and (min-width:' + 1701 * App.Vars.cssPx + 'rem)', function() {
  App.Props.blockCollectionColumns(6);
  App.Props.blockCollectionColumnsChannel(3);
});
// 5 columns
enquire.register(
  'screen and (min-width:' + 1501 * App.Vars.cssPx + 'rem) and (max-width:' + 1700 * App.Vars.cssPx + 'rem)',
  function() {
    App.Props.blockCollectionColumns(5);
    App.Props.blockCollectionColumnsChannel(3);
  }
);
// 4 columns
enquire.register(
  'screen and (min-width:' + 1301 * App.Vars.cssPx + 'rem) and (max-width:' + 1500 * App.Vars.cssPx + 'rem)',
  function() {
    App.Props.blockCollectionColumns(4);
    App.Props.blockCollectionColumnsChannel(2);
  }
);
// 3 columns
enquire.register(
  'screen and (min-width:' + 1101 * App.Vars.cssPx + 'rem) and (max-width:' + 1300 * App.Vars.cssPx + 'rem)',
  function() {
    App.Props.blockCollectionColumns(util.isMobile ? 2 : 3);
    App.Props.blockCollectionColumnsChannel(2);
  }
);
// 2 columns
enquire.register(
  'screen and (min-width:' + 1001 * App.Vars.cssPx + 'rem) and (max-width:' + 1100 * App.Vars.cssPx + 'rem)',
  function() {
    App.Props.blockCollectionColumns(util.isMobile ? 1 : 2);
    App.Props.blockCollectionColumnsChannel(1);
  }
);
// 3 columns
enquire.register(
  'screen and (min-width:' + 891 * App.Vars.cssPx + 'rem) and (max-width:' + 1000 * App.Vars.cssPx + 'rem)',
  function() {
    App.Props.blockCollectionColumns(util.isMobile ? 2 : 3);
    App.Props.blockCollectionColumnsChannel(2);
  }
);
// 2 columns
enquire.register(
  'screen and (min-width:' + 751 * App.Vars.cssPx + 'rem) and (max-width:' + 890 * App.Vars.cssPx + 'rem)',
  function() {
    App.Props.blockCollectionColumns(util.isMobile ? 1 : 2);
    App.Props.blockCollectionColumnsChannel(1);
  }
);
// 3 columns
enquire.register(
  'screen and (min-width:' + 561 * App.Vars.cssPx + 'rem) and (max-width:' + 750 * App.Vars.cssPx + 'rem)',
  function() {
    App.Props.blockCollectionColumns(util.isMobile ? 2 : 3);
    App.Props.blockCollectionColumnsChannel(2);
  }
);
// 2 columns
enquire.register('screen and (max-width:' + 560 * App.Vars.cssPx + 'rem)', function() {
  App.Props.blockCollectionColumns(util.isMobile ? 1 : 2);
  App.Props.blockCollectionColumnsChannel(1);
});

// Breakpoints
enquire.register('screen and (max-width:' + 1000 * App.Vars.cssPx + 'rem)', {
  match: function() {
    App.Props.breakPointMedium(true);
    m.redraw();
  },
  unmatch: function() {
    App.Props.breakPointMedium(false);
    m.redraw();
  }
});
enquire.register('screen and (max-width:' + 420 * App.Vars.cssPx + 'rem)', {
  match: function() {
    App.Props.breakPointTiny(true);
    m.redraw();
  },
  unmatch: function() {
    App.Props.breakPointTiny(false);
    m.redraw();
  }
});

module.exports = {
  oninit: function() {
    _.bindAll(this, ['oncreateContent', 'onupdateContent']);
    this.expand = App.Settings.playerExpand;
    this.psEnabled = false;
    if (mode.getMode() != 'normal') mode.setMode();
  },

  oncreateContent: function(node) {
    this.onupdateContent(node);
    domScroll = node.dom;
  },

  onupdateContent: function(node) {
    if (m.route.param('page') == 'player.frame') {
      ps.destroy(node.dom);
      this.psEnabled = false;
    } else if (this.psEnabled) {
      ps.update(node.dom);
    } else {
      // NOTE: `ps.initialize` us also filtered in `extensions` folder
      ps.initialize(node.dom);
      this.psEnabled = true;
    }
  },

  oncreateBody: function() {
    if (App.Settings.playerExpand() && App.Props.breakPointMedium()) {
      App.Settings.playerExpand(false);
      m.redraw.nexttick();
    }
    domScroll.scrollTop = 0;
  },

  oninitPage: function() {
    // comModalVideoPlayer.videoModel(null);
  },

  onremovePage: function() {
    // comModalVideoPlayer.videoModel(null);
  },

  clickCover: function() {
    App.Settings.playerExpand(false);
    current.updateQuality();
  },

  view: function(node) {
    return m(
      '#page-player',
      {
        class:
          (this.expand() ? 'expand' : 'collapse') +
          (App.Props.breakPointMedium() ? ' bp-medium' : ' bp-large') +
          (util.isMobile ? ' de-mobile' : ' de-desktop')
      },
      [
        m(comPlayer),
        m(
          '#page',
          {
            oninit: this.oninitPage,
            onremove: this.onremovePage
          },
          m(comSearchBar, {
            key: 'searchbar' + m.route.get()
          }),
          m(
            '.page-content',
            {
              key: 'pagecontent',
              oncreate: this.oncreateContent,
              onupdate: this.onupdateContent
            },
            m('.body-content', [
              // Left panel
              m(
                '.body-wrap',
                {
                  key: 'page' + m.route.get(),
                  oncreate: this.oncreateBody
                },
                comMessages.hasMessage() ? m(comMessages) : null,
                node.children
              ),
              // Right panel
              m(
                '.sideblocks',
                {
                  key: 'sideblock'
                },
                // m(
                // '.sideblocks-wrap',
                // Add Another side block here
                // ---
                // , m(comAnotherSideBlock)
                Config.dev ? null : m(comFeaturesSideBlock)
                // ---
                // , m('hr.footer cf')
                // )
              )
            ]),
            m(comFooter)
          ),
          m(
            '.page-cover',
            {
              key: 'pagecover',
              onclick: this.clickCover
            },
            m(
              'span',
              m(
                'span',
                m(elIcon, {
                  icon: 'navigate_before'
                })
              )
            )
          ),
          (!App.Settings.playerExpand() || !App.Props.breakPointMedium()) && comModalVideoPlayer.videoModel()
            ? m(comModalVideoPlayer, {
                key: 'modalplayer'
              })
            : null
        )
      ]
    );
  }
};
