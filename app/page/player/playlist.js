var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comResult = require('comp/search/result');
var comHeader = require('comp/partials/headerPlaylist');
var util = require('libs/util');

var bindMethods = ['request', 'clickLoadMore'];

var defaultParam = {
    part: 'snippet',
    maxResults: Config.maxResults
};

module.exports = {
    oninit: function() {
        _.bindAll(this, bindMethods);
        this.state = md.State.create({
            requesting: true,
            loadMoreToken: null
        });
        this.collection = new md.Collection({
            url: '/youtube/playlistitems'
        });
    },
    onremove: function() {
        // Cutting references
        this.collection.clear();
        this.state = null;
        this.channelId = null;
    },
    oncreate: function() {
        util.nextTick(this.request);
    },
    request: function() {

        var self = this;
        // m.redraw();
        var done = m.redraw.computation();
        this.state.requesting(true);
        var param = _.clone(defaultParam);

        if (m.route.param('id')) {
            param.playlistId = m.route.param('id');
        } else {
            // Display no result;
            this.state.requesting(false);
            done();
            return;
        }

        // Token
        if (this.state.loadMoreToken())
            param.pageToken = this.state.loadMoreToken();
        else
            delete param.pageToken;

        // Request
        if (!this.collection.hasModel()) {
            this.collection.opt('model', App.Model.PlaylistItem);
        }
        this.collection.fetch(param, {
                path: 'items'
            }, function(err, response) {
                if (!err) self.state.loadMoreToken(response.nextPageToken);
            })
            .catch(Console.error)
            .finally(function() {
                self.state.requesting(false);
                done();
            });

    },
    clickLoadMore: function(e) {
        e.preventDefault();
        this.state.requesting(true);
        util.nextTick(this.request);
    },
    view: function() {
        return m('#playlist',
            m('.list', [
                m(comHeader),
                m(comResult, {
                    collection: this.collection,
                    loading: this.state.requesting,
                    onloadmore: this.clickLoadMore,
                    hasnext: this.state.loadMoreToken
                })
            ])
        );
    }
};