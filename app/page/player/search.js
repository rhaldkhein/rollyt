var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comHeader = require('comp/search/header');
var comResult = require('comp/search/result');
var moment = require('moment');

var bindMethods = ['clickLoadMore', 'changeHeaderFilter'];

// Filter allowed params
var allowedParam = ['q', 'type', 'sort', 'date', 'open'];

// The default search parameters for youtube
var defaultParam = {
	part: 'snippet',
	type: 'video',
	maxResults: window.Config.maxResults,
	order: 'relevance',
};

module.exports = {

	oninit: function() {
		_.bindAll(this, bindMethods);
		// Default and filtered params
		this.param = md.State.create(_.assign({
			q: undefined,
			type: 'video',
			sort: undefined,
			date: undefined
		}, _.pick(m.route.param(), allowedParam)));
		// UI states
		this.state = md.State.create({
			loading: true,
			loadMoreToken: null,
			totalResults: 0
		});
		// Local props
		this.resultCollection = new md.Collection({
			url: '/youtube/search',
			model: this.getCollectionModel()
		});
	},

	onremove: function() {
		this.resultCollection.destroy();
	},

	oncreate: function() {
		this.request();
	},

	request: function() {

		this.state.loading(true);
		var param = _.clone(defaultParam);
		// Query keywords
		if (this.param.q()) {
			param.q = this.param.q();
		} else {
			// Display no result;
			this.state.loading(false);
			m.redraw();
			return;
		}
		// Type
		if (this.param.type())
			param.type = this.param.type();
		// Sort
		if (this.param.sort())
			param.order = this.param.sort();
		// Date
		if (this.param.date()) {
			var date;
			switch (this.param.date()) {
				case 'today':
					date = moment().subtract(1, 'days');
					break;
				case 'yesterday':
					date = moment().subtract(2, 'days');
					break;
				case 'week':
					date = moment().subtract(7, 'days');
					break;
				case 'month':
					date = moment().subtract(1, 'months');
					break;
				case 'year':
					date = moment().subtract(1, 'years');
					break;
			}
			if (date)
				param.publishedAfter = date.toISOString();
		}
		// Add load more token
		if (this.state.loadMoreToken())
			param.pageToken = this.state.loadMoreToken();
		else
			delete param.pageToken;

		// Execute
		var self = this;
		this.resultCollection.fetch(param, {
			path: 'items'
		}, function(err, response) {
			if (!err) {
				self.state.loadMoreToken(response.nextPageToken);
				self.state.totalResults(response.pageInfo.totalResults);
			}
			self.state.loading(false);
			m.redraw();
		});

	},

	getCollectionModel: function() {
		if (this.param.type() === 'video') {
			return App.Model.Video;
		} else if (this.param.type() === 'channel') {
			return App.Model.Channel;
		} else if (this.param.type() === 'playlist') {
			return App.Model.Playlist;
		} else {
			return App.Model.Video;
		}
	},

	clickLoadMore: function(e) {
		e.preventDefault();
		this.request();
	},

	changeHeaderFilter: function(e, state) {
		e.preventDefault();
		e.redraw = false;
		var param = _.pick(_.assign(this.param.toJson(), state.toJson()), allowedParam);
		m.route.set('/player.search?' + m.buildQueryString(param));
	},

	view: function() {
		return m('#search',
			m('.list', [
				m(comHeader, {
					onchange: this.changeHeaderFilter,
					totalResults: this.state.totalResults
				}),
				m(comResult, {
					collection: this.resultCollection,
					loading: this.state.loading,
					onloadmore: this.clickLoadMore,
					hasnext: this.state.loadMoreToken
				})
			])
		);
	}
};
