var m = require('mithril');
var md = require('mithril-data');

module.exports = {

  oninit: function() {
    this.cbValue = md.stream(true);
  },

  view: function() {
    return m('div#dummy',
      m('button', {
        onclick: function(e) {
          e.redraw = false;
          m.route.set('/player.browse');
        }
      }, 'Feed')
    );
  }
};