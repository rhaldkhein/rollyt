var _ = require('lodash');
var m = require('mithril');

var _skipids = [];
var _skipped = [];
var _loop = false;
var _skiplimit = 4; // Dont want to skip infinitely by accident

exports.load = function() {
    exports.clear();
    // pull from server
    if (App.Account.isGuest()) return;
    return m.request('~rest/skip/list')
        .then(function(data) {
            _skipids = data;
        })
        .catch(Console.error); // Continue with empty _skipids
};

exports.add = function(id) {
    if (_.indexOf(_skipids, id) < 0) {
        _skipids.push(id);
        // push to server
        if (App.Account.isGuest()) return;
        return m.request({
            method: 'POST',
            url: '~rest/skip',
            data: { id: id }
        }).then(function() {
            App.Modules.toastr.success('Added to skiplist');
        }).catch(function() {
            App.Modules.toastr.error('Unable to add to skiplist');
        });
    } else {
        App.Modules.toastr.error('Already skipped');
    }
};

exports.remove = function(id) {
    if (_.indexOf(_skipids, id) >= 0) {
        _.pull(_skipids, id);
        // push to server
        if (App.Account.isGuest()) return;
        return m.request({
            method: 'DELETE',
            url: '~rest/skip',
            data: { id: id }
        }).catch(function() {
            App.Modules.toastr.error('Unable to remove from skiplist');
        });
    }
    return Promise.reject();
};

exports.removeAll = function() {
    _skipids.splice(0, _skipids.length);
    if (App.Account.isGuest()) return;
    return m.request({
        method: 'DELETE',
        url: '~rest/skip/clear'
    }).catch(function() {
        App.Modules.toastr.error('Unable to clear skiplist');
    });
};

exports.removeLast = function() {
    return exports.remove(_skipids[_skipids.length - 1]);
};

exports.getList = function() {
    return _skipids;
};

exports.check = function(id) {
    // Check by skiplist
    var skip = _.indexOf(_skipids, id) > -1;
    if (skip) {
        if (_.indexOf(_skipped, id) < 0) {
            _skipped.push(id);
        } else {
            // loop
            _loop = true;
        }
    }
    return skip;
};

exports.skipped = function(id) {
    // Forcely skip this id
    if (_.indexOf(_skipped, id) < 0) {
        _skipped.push(id);
    }
};

exports.clear = function() {
    _skipped.splice(0, _skipped.length);
    _loop = false;
};

exports.isCleared = function() {
    return _skipped.length == 0;
};

exports.isLoop = function() {
    return _loop;
};

exports.isFull = function() {
    return _skipped.length >= _skiplimit;
};