var _ = require('lodash');
var m = require('mithril');
var ControlBase = require('./controlBase');

/*
	options : {
		shuffle: true | false
		starred: true | false
	}
*/

function ControlList(model, controlOptions, collectionOptions) {
	ControlBase.call(
		this,
		model,
		controlOptions,
		_.defaults(collectionOptions, {
			model: App.Model.Video,
			state: {
				playing: false
			}
		})
	);
}

ControlList.prototype = _.create(ControlBase.prototype, {
	currentPlaylistItem: function() {
		return this.currentItem();
	},

	getParam: function() {
		var param = this.model.params();
		param.maxResults = this.options.limit();
		return param;
	},

	currentVideo: function() {
		return this.currentItem();
	},

	// Override load to update url of collection
	load: function(prev, redraw) {
		this.collection.opt('url', this.model.link());
		return ControlBase.prototype.load.call(this, prev).then(function(control) {
			if (redraw) m.redraw();
			return control;
		});
	}
});

module.exports = ControlList;
