var _ = require('lodash');
var numeral = require('numeral');
var moment = require('moment');
var hasWindow = typeof window !== 'undefined';
var isIE = /*@cc_on!@*/ false || !!document.documentMode;

// Get next tick function
function getNextTickMethod() {
  if (hasWindow && window.setImmediate) {
    return function() {
      return window.setImmediate.apply(null, arguments);
    };
  } else if (typeof process === 'object' && typeof process.nextTick === 'function') {
    return process.nextTick;
  }
  return function(fn) {
    setTimeout(fn, 0);
  };
}

function isChrome() {
  var isChromium = window.chrome,
    winNav = window.navigator,
    vendorName = winNav.vendor,
    isOpera = winNav.userAgent.indexOf('OPR') > -1,
    isIEedge = winNav.userAgent.indexOf('Edge') > -1,
    isIOSChrome = winNav.userAgent.match('CriOS');

  if (isIOSChrome) {
    return true;
  } else if (
    isChromium !== null &&
    typeof isChromium !== 'undefined' &&
    vendorName === 'Google Inc.' &&
    isOpera === false &&
    isIEedge === false
  ) {
    return true;
  } else {
    return false;
  }
}

function randId() {
  return Math.random()
    .toString(36)
    .substr(2, 10);
}

// Exports
var util = (module.exports = _.create(null, {
  isBrowser: hasWindow,

  isIE: isIE,

  isChrome: isChrome(),

  nextTick: getNextTickMethod(),

  isMobile: (function() {
    return /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
  })(),

  isInTime: function(timestamp, hours) {
    return Date.now() - timestamp < 3600000 * (hours || 1);
  },

  isLastVisitIn: function(hours) {
    return util.isInTime(App.Settings.visitTimeStamp(), hours);
  },

  genToken: function() {
    // Simple non-crypto token generator
    return _.join(_.times(4, randId), '-');
  },

  getHttpsHost: function() {
    // If port is default, it will display 0 or empty string
    var _host = 'https://' + window.location.hostname,
      _port = window.location.port;
    if (_port && !(_port == 80 || _port == 443)) _host += ':' + (parseInt(_port) + 1);
    return _host;
  },

  stopPropagation: function(e) {
    if (_.isFunction(e.stopPropagation)) {
      e.stopPropagation();
    } else {
      e.cancelBubble = true;
    }
  },

  clearObject: function(obj) {
    for (var member in obj) delete obj[member];
  },

  hasValueOfType: function(obj, type) {
    var keys = _.keys(obj);
    for (var i = 0; i < keys.length; i++) {
      if (obj[keys[i]] instanceof type) {
        return true;
      }
    }
    return false;
  },

  getRandom: function(lower, upper, except, floating) {
    if (_.isBoolean(except)) {
      floating = except;
      except = -1;
    }
    var value = except;
    while (value === except) {
      value = _.random(lower, upper, floating);
    }
    return value;
  },

  getOffsetRect: function(elem) {
    // (1)
    var box = elem.getBoundingClientRect();
    var body = document.body;
    var docElem = document.documentElement;
    // (2)
    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
    // (3)
    var clientTop = docElem.clientTop || body.clientTop || 0;
    var clientLeft = docElem.clientLeft || body.clientLeft || 0;
    // (4)
    var top = box.top + scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;
    return {
      top: Math.round(top),
      left: Math.round(left)
    };
  },

  // Convert number into number with comma.
  numberComma: function(num) {
    return numeral(num).format('0,0');
  },

  // Invokes all functions in object. Returns a new object.
  invokeAll: function(obj) {
    return _.transform(
      obj,
      function(result, value, key) {
        if (_.isFunction(value) && !_.isNil(value())) {
          result[key] = value();
        }
      },
      {}
    );
  },

  dateToISOString: function(date) {
    var _date;
    switch (date) {
      case 'today':
        _date = moment().subtract(1, 'days');
        break;
      case 'yesterday':
        _date = moment().subtract(2, 'days');
        break;
      case 'week':
        _date = moment().subtract(7, 'days');
        break;
      case '2weeks':
        _date = moment().subtract(14, 'days');
        break;
      case '3weeks':
        _date = moment().subtract(21, 'days');
        break;
      case 'month':
        _date = moment().subtract(1, 'months');
        break;
      case '3months':
        _date = moment().subtract(3, 'months');
        break;
      case '6months':
        _date = moment().subtract(6, 'months');
        break;
      case 'year':
        _date = moment().subtract(1, 'years');
        break;
      default:
        _date = moment().subtract(0, 'days');
    }
    return _date.toISOString();
  },

  readableDate: function(date) {
    switch (date) {
      case 'today':
        return 'Today';
      case 'yesterday':
        return 'Yesterday';
      case 'week':
        return 'Week';
      case 'month':
        return 'Month';
      case 'year':
        return 'Year';
      default:
        return '';
    }
  },

  readableOrder: function(order) {
    switch (order) {
      case 'date':
        return 'Date';
      case 'rating':
        return 'Rating';
      case 'relevance':
        return 'Relevance';
      case 'title':
        return 'Title';
      case 'viewCount':
        return 'Views';
      default:
        return '';
    }
  },

  isUrlImage: function(url) {
    return url.match(/\.(jpeg|jpg|gif|png)$/) != null;
  }
}));
