var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');

function request(method, type, id, state, folder, bg) {
	return new Promise(function(resolve, reject) {
		var done,
			data = {
				type: type,
				id: id
			};
		if (folder) {
			data.folder = _.isFunction(folder) ? folder() : folder;
			if (data.folder == 'none') data.folder = null;
		} else {
			data.of = true;
		}
		if (bg) done = m.redraw.computation();
		method
			.call(md.store, '~rest/star', data, {
				background: bg
			})
			.then(function(data) {
				if (state) state(data.star);
				if (_.isFunction(folder)) folder(data.folder || '');
				resolve(data);
				if (done) done();
			})
			.catch(function(err) {
				if (state) state(false);
				if (_.isFunction(folder)) folder('');
				reject(err);
				if (done) done();
			});
		if (state) {
			state(method === md.store.post);
			if (_.isFunction(folder)) folder('');
		}
	});
}

exports.list = function() {};

exports.check = function(type, id, state, folder) {
	return request(md.store.get, type, id, state, folder, true).catch(Console.error);
};

exports.star = function(type, id, state, folder) {
	// Caller should handle error!
	return request(md.store.post, type, id, state, folder, true); //.catch(Console.error);
};

exports.unstar = function(type, id, state) {
	// Caller should handle error!
	return request(md.store.destroy, type, id, state, null, true); //.catch(Console.error);
};
