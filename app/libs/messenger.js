var util = require('libs/util');
var extId,
  msgToken,
  listeners,
  isReady = false;
var extendable = (function() {
  return !!(window.chrome && window.chrome.runtime && App.Vars.msgId);
})();

window.addEventListener(
  'X-Message',
  function(e) {
    if (e.detail && e.detail.token === msgToken) {
      var listener = listeners[e.detail.cmd];
      if (listener) listener(e.detail.data);
    }
  },
  false
);

if (extendable) {
  // extId = 'kofhofbnjaekkabjgefjlinafhlekmac'; // Office
  // extId = 'jpnkiecjgkgebfnggmbpkahdkihnclaf'; // Home
  extId = 'opjmkgcfeidfiafbhigicejodpnpcbbc'; // Chrome Store
  listeners = {};
}

exports.isReady = function() {
  return isReady;
};

exports.isExtendable = function() {
  return extendable;
};

exports.ready = function() {
  if (!extendable) return;
  msgToken = util.genToken();
  exports.send('app#playerready', msgToken);
  isReady = true;
};

exports.send = function(cmd, data, callback) {
  if (!extendable || !extId || !cmd) return;
  if (typeof data === 'function') {
    callback = data;
    data = null;
  }
  window.chrome.runtime.sendMessage(
    extId,
    {
      id: App.Vars.msgId,
      cmd: cmd,
      data: data
    },
    function(data) {
      if (callback) callback(data);
    }
  );
};

exports.listen = function(cmd, callback) {
  if (!extendable || !cmd || !callback) return;
  if (!listeners[cmd]) listeners[cmd] = callback;
};
