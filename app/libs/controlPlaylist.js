var _ = require('lodash');
var m = require('mithril');
var ControlBase = require('./controlBase');

// var arrPlayed = [];
// var lastPlayedId;
/*
	options : {
		shuffle: true | false
		starred: true | false
	}
*/

function ControlPlaylist(model, options) {
	ControlBase.call(this, model, options, {
		model: App.Model.PlaylistItem,
		url: '/youtube/playlistitems',
		state: {
			playing: false
		}
	});
}

ControlPlaylist.prototype = _.create(ControlBase.prototype, {

	currentPlaylistItem: function() {
		return this.currentItem();
	},

	getParam: function() {
		var param = {
			playlistId: this.model.id(),
			part: 'snippet',
			maxResults: this.options.limit()
		};
		return param;
	},

	currentVideo: function() {
		var item = this.currentItem();
		if (item) {
			return item.video();
		}
	},

	videoOfItem: function(item) {
		return item.video();
	},

	load: function(prev, redraw) {
		return ControlBase.prototype.load.call(this, prev).then(function(control) {
			if(redraw) m.redraw();
			return control;
		});
	}
	
});

module.exports = ControlPlaylist;