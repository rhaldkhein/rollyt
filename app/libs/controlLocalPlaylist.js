var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var ControlBase = require('./controlBase');
// var arrPlayed = [];
// var lastPlayedId;
/*
    options : {
        shuffle: true | false
        starred: true | false
        videos: true | false
    }
*/

function ControlLocalPlaylist(model, options, signature) {
  ControlBase.call(
    this,
    model,
    _.defaults(options, {
      number_paging: true,
      limit: App.Vars.playlist_limit,
      path: 'results'
    })
  );
  this.collection = new md.ItemCollection({
    model: App.Model.LocalPlaylistItem,
    url: '/localplaylistitem/' + (options && options.videos ? 'videos' : 'list'),
    state: _.defaults(signature, {
      playing: false
    })
  });
  // Add own option
  this.collection.opt('owned', model.is_mine());
}

ControlLocalPlaylist.prototype = _.create(ControlBase.prototype, {
  currentPlaylistItem: function() {
    return this.currentItem();
  },

  getParam: function() {
    var param = {
      playlistid: this.model.id(),
      limit: this.options.limit()
    };
    return param;
  },

  currentVideo: function() {
    var item = this.currentItem();
    if (item) return item.resource();
  },

  videoOfItem: function(item) {
    return item.resource();
  },

  load: function(prev, redraw) {
    var self = this;
    return ControlBase.prototype.load.call(this, prev).then(function() {
      if (redraw) m.redraw();
      return self.collection.populate();
    });
  }
});

module.exports = ControlLocalPlaylist;
