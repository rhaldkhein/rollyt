var _ = require('lodash');
var md = require('mithril-data');
var EventEmitter = require('events');

var param = {
	part: 'snippet',
	maxResults: 14,
	type: 'video'
};

var maxNextVideoCount = 32;
var maxHistoryCount = 20;

function ControlRelated(model) {
	this._videoModel = md.stream();
	this._loadMoreToken = md.stream();
	this.shuffle = md.stream();
	this.relatedCollection = new md.Collection({
		url: '/youtube/search',
		parser: 'youtube'
	});
	this.historyCollection = new md.Collection();
	this.nextVideoCollection = new md.Collection();
	if (model)
		this.setVideo(model);
}

ControlRelated.prototype = _.create(EventEmitter.prototype, {
	setVideo: function(model, request) {
		this._videoModel(model);
		this._loadMoreToken(null);
		this.relatedCollection.clear();
		if (this.historyCollection.size() >= maxHistoryCount)
			this.historyCollection.shift();
		this.historyCollection.add(model);
		if (request)
			this.requestRelated(true);
	},
	requestRelated: function(changeNextVideo) {
		this.emit('request');
		param.relatedToVideoId = this._videoModel().id();
		if (this._loadMoreToken())
			param.pageToken = this._loadMoreToken();
		else if (_.has(param, 'pageToken'))
			delete param.pageToken;

		if (!this.relatedCollection.hasModel()) {
			this.relatedCollection.opt('model', App.Model.Video);
		}
		var self = this;
		this.relatedCollection.fetch(param, {
			path: 'items'
		}, function(err, response) {
			self._loadMoreToken(response.nextPageToken);
			if (changeNextVideo) {
				self._updateNextVideoCollection();
				self._prependNextVideo();
			}
			self.emit('response');
		});
	},
	getNextVideo: function() {
		if (this.shuffle()) {
			return this._getSampleVideo();
		} else {
			return this.relatedCollection.first();
		}
	},
	getPrevVideo: function() {
		if (this.historyCollection.size() > 1) {
			// Pop the current one
			this.historyCollection.pop();
			// Pop the previous one
			return this.historyCollection.pop();
		}
	},
	clear: function() {
		this.relatedCollection.clear();
		this.historyCollection.clear();
		this.nextVideoCollection.clear();
		this.removeAllListeners();
	},
	clearRelated: function() {
		this.relatedCollection.clear();
	},
	_getSampleVideo: function() {
		var sample = this.nextVideoCollection.sample();
		this.nextVideoCollection.remove(sample.id());
		return sample;
	},
	_prependNextVideo: function() {
		var next = this._getSampleVideo();
		this.relatedCollection.remove(next.id());
		this.relatedCollection.add(next, true);
	},
	_updateNextVideoCollection: function() {
		// Add related to nextvideo collection
		var self = this;
		this.relatedCollection.forEach(function(video) {
			if (self.nextVideoCollection.size() < maxNextVideoCount &&
				!self.nextVideoCollection.contains(video.id()) &&
				!self.historyCollection.contains(video.id())) {
				self.nextVideoCollection.add(video);
			}
		});
	}
});

module.exports = ControlRelated;