var _ = require('lodash');
var md = require('mithril-data');

function ControlBase(model, controlOptions, collectionOptions) {
  if (collectionOptions) this.collection = new md.Collection(collectionOptions);
  this.model = model;
  this.currentItem = md.stream();
  this.nextItem = md.stream();
  this.playCount = md.stream(0);
  this.options = md.State.create(
    _.assign({
      shuffle: md.stream(false)
    }, controlOptions)
  );
  this.state = md.State.create({
    page: 0,
    limit: 0,
    total: 0,
    currentPageToken: undefined,
    firstPageToken: undefined,
    loadMoreToken: undefined,
    loadPrevToken: undefined,
    loading: false,
    loaded: false,
    lastLoad: false,
    ended: false
  });
}

ControlBase.prototype = _.create(null, {

  // Override this
  currentVideo: function() {
    // Must return current video
  },

  // Override this
  getParam: function() {
    return {};
  },

  // Override this
  videoOfItem: function(item) {
    return item;
  },

  hasNext: function() {
    return !!this.nextItem() || !this.state.lastLoad();
  },

  hasNextPage: function() {
    return !this.state.lastLoad();
  },

  hasPrevPage: function() {
    return !!this.state.loadPrevToken() || this.state.page() > 1;
  },

  isPaginated: function() {
    if (this.options.number_paging) {
      return this.state.limit() < this.state.total();
    } else {
      return !!(this.state.prevPageToken() || this.state.nextPageToken());
    }
  },

  getTotalPages: function() {
    return Math.ceil(this.state.total() / this.state.limit());
  },

  setCurrent: function(model) {
    // mixed = item id, item model
    var state,
      item = this.currentItem();
    if (item) {
      state = this.collection.stateOf(item);
      if (state)
        state.playing(false);
    }
    // Find model by id
    if (_.isString(model)) {
      model = this.collection.find(['_id', model]);
    }
    // Change current item
    this.currentItem(model || this.nextItem());
    this.playCount(this.playCount() + 1);
    this.setNextItem();
    item = this.currentItem();
    state = this.collection.stateOf(item);
    if (state) state.playing(true);
  },

  setNextItem: function(model) {
    this.state.ended(false);
    // If model is provided, verify the model and set to it
    if (model && this.collection.contains(model.id())) {
      this.nextItem(model);
      return;
    }
    // Model not provided or can't find it in collection
    // Get next item model
    var size = this.collection.size(),
      currentIndex = this.playCount() > 0 ? this.collection.findIndex(this.currentItem()) : -1;
    if (size > 0) {
      currentIndex++;
      this.nextItem(this.collection.nth(currentIndex));
      if (this.state.lastLoad() && currentIndex >= size) this.state.ended(true);
    } else {
      this.nextItem(undefined);
    }
  },

  getCurrentPage: function() {
    var currentPageToken = this.state.currentPageToken();
    return this.options.number_paging ? this.state.page() : (currentPageToken === this.state.firstPageToken() ? '' : currentPageToken);
  },

  _pageStateToParam: function(param, prev) {
    if (this.options.number_paging) {
      if (_.isNumber(prev)) param.page = prev;
      else param.page = this.state.page() + (prev ? -1 : 1);
    } else {
      // Token
      if (_.isString(prev)) {
        param.pageToken = prev;
      } else if (prev) {
        if (this.state.loadPrevToken()) param.pageToken = this.state.loadPrevToken();
        else return; // Return error
      } else {
        if (this.state.loadMoreToken()) param.pageToken = this.state.loadMoreToken();
        else delete param.pageToken;
      }
    }
    return true;
  },

  _responseToPageState: function(response, prev) {
    if (this.options.number_paging) {
      this.state.page(response.query.page);
      this.state.limit(response.query.limit);
      this.state.total(response.total);
      if (this.state.page() * this.state.limit() < this.state.total()) {
        this.state.lastLoad(false);
      } else {
        this.state.lastLoad(true);
      }
    } else {
      // Save previous token as current token
      if (_.isString(prev)) this.state.currentPageToken(prev);
      else this.state.currentPageToken(prev ? this.state.loadPrevToken() : this.state.loadMoreToken());
      // Store tokens
      if (response.prevPageToken) {
        if (!this.state.loadPrevToken()) this.state.firstPageToken(response.prevPageToken);
      }
      this.state.loadPrevToken(response.prevPageToken);
      if (response.nextPageToken) {
        this.state.loadMoreToken(response.nextPageToken);
        this.state.lastLoad(false);
      } else {
        this.state.lastLoad(true);
      }
      this.state.total((response.pageInfo && response.pageInfo.totalResults) || 0);
    }
  },

  // Methods
  load: function(prev) {
    var self = this;
    if (_.isNil(prev) || _.isBoolean(prev)) {
      if (!prev && !this.hasNextPage()) {
        return Promise.reject('end_of_list');
      } else if (prev && !this.hasPrevPage()) {
        return Promise.reject('beginning_of_list');
      }
    }
    var param = this.getParam();
    if (!this._pageStateToParam(param, prev)) {
      return Promise.reject('cant_load');
    }
    // Start request
    this.state.loading(true);
    return this.collection.fetch(param, {
      path: (this.options.path ? this.options.path() : undefined) || 'items',
      clear: true
    }, function(err, response) {
      self.state.loading(false);
      if (!err) {
        self.state.loaded(true);
        self._responseToPageState(response, prev);
        // Deal with shuffle settings
        if (self.options.shuffle()) {
          self.collection.randomize();
        } else if (response.order) {
          self.collection.sortByOrder(response.order);
        }
        var currItem = self.currentItem();
        self.collection.forEach(function(item) {
          if (currItem && item.id() === currItem.id()) {
            self.collection.stateOf(item).playing(true);
          }
        });
        if (!self.nextItem()) {
          self.setNextItem();
        }
      } else {
        self.nextItem(null);
        self.state.lastLoad(true);
      }
    }).catch(function(err) {
      self.nextItem(null);
      self.state.lastLoad(true);
      throw err;
    }).return(this);
  },

  containsPlaying: function() {
    return this.playingIndex() !== -1;
  },

  playingIndex: function() {
    var self = this,
      index = -1;
    this.collection.forEach(function(item, i) {
      if (self.collection.stateOf(item).playing()) {
        index = i;
        return false;
      }
    });
    return index;
  },

  reset: function(soft) {
    this.currentItem(undefined);
    this.playCount(0);
    this.state.loaded(false);
    this.state.loading(false);
    this.state.loadMoreToken(undefined);
    this.state.ended(false);
    this.state.page(0);
    this.state.limit(0);
    if (!soft) {
      this.state.lastLoad(false);
      this.state.total(0);
    }
  },

  notifyStopPlaying: function() {
    var state = this.collection.stateOf(this.currentItem());
    if (state) state.playing(false);
    if (!this.hasNext() && this.state.lastLoad()) this.reset(true);
  },

  clear: function() {
    this.reset();
    this.collection.clear();
  },

  refresh: function() {
    this.state.loaded(false);
    this.state.loading(false);
    this.collection.clear();
    if (!this.state.loaded() && !this.state.loading()) {
      return this.load(this.getCurrentPage());
    }
    return Promise.reject();
  }

});

module.exports = ControlBase;