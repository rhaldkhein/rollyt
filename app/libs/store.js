var _ = require('lodash');
var m = require('mithril');
var util = require('libs/util');
var youtube = require('libs/youtube');
var records = require('local/records');

/**
 * Custom store to support both local and youtube
 */
function storeEntry(request) {
  // Console.log('storeEntry', request);
  // Filter user input data
  if (request.data && request.data.id && _.isFalsy(request.data.id))
    return Promise.reject(new Error('invalid_data_id'));
  // Continue to request data
  var parts = request.url.split('/'); // ["", "rest", "youtube", "search"]
  var result;
  switch (parts[2]) {
    case 'youtube':
      result = youtubeStore(parts[3], request);
      break;
    case 'local':
      result = localStore(parts[3], request);
      break;
    default:
      result = m.request(request);
  }
  return result;
}

/**
 * Proxy request method. To simulate global loading state.
 */

// function requestFilter(request) {
//  return new Promise(function(resolve, reject) {
//      util.nextTick(function() {
//          m.request(request)
//              .then(function(data) {
//                  resolve(data);
//              })
//              .catch(function(err) {
//                  reject(err);
//              });
//      });
//  });
// }

/**
 * Store function for local records
 */

function localStore(type, request) {
  return new Promise(function(resolve, reject) {
    util.nextTick(function() {
      try {
        var rec = records(type, request.method, request.url, request.data);
        // `rec` may produce array of promise
        // Record returns a promise and should wait
        if (rec instanceof Promise) {
          rec.then(resolve).catch(reject);
        } else {
          if (rec && rec.error) {
            reject(rec);
          } else {
            resolve(rec);
          }
        }
      } catch (ex) {
        reject(ex);
      }
    });
  });
}

/**
 * Store function for youtube
 */
function youtubeStore(type, request) {
  var result;
  switch (type) {
    case 'search':
      result = youtube.search(request.data);
      break;
    case 'playlistitems':
      result = youtube.playlistItems(request.data);
      break;
    case 'channel':
      result = youtube.channelInfo(request.data._id);
      break;
    case 'video':
      result = youtube.videoInfo(request.data._id);
      break;
    case 'playlist':
      result = youtube.playlistInfo(request.data._id);
      break;
    case 'channellist':
      result = youtube.list('channel', request.data);
      break;
    case 'videolist':
      result = youtube.list('video', request.data);
      break;
    case 'playlistlist':
      result = youtube.list('playlist', request.data);
      break;
    case 'playlistitemlist':
      result = youtube.list('playlistitem', request.data);
      break;
    case 'activitylist':
      result = youtube.list('activity', request.data);
      break;
    case 'subscription':
      result = youtube.list('subscription', request.data);
      break;
    case 'subscribed':
      result = youtube.subscribed(request.data._id);
      break;
    case 'subscribeto':
      result = youtube.subscribeTo(request.data._id);
      break;
    case 'unsubscribeto':
      result = youtube.unsubscribeTo(request.data._id);
      break;
    case 'liked':
      result = youtube.liked(request.data._id);
      break;
    case 'like':
      result = youtube.like(request.data._id, request.data);
      break;
    default:
  }
  result.catch(function(err) {
    // Error 401: Auth error, must be logged out from another tab
    if (_.get(err, 'result.error.code') === 401) {
      alert('Google account has been signed out. The page will be reloaded.');
      window.localStorage.removeItem('access_token');
      window.location.reload();
    }
    throw err;
  });
  return result;
}

module.exports = storeEntry;
