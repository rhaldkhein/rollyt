var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var ControlList = require('./controlList');

function ControlLocalList(model, options) {
	ControlList.call(
		this,
		model,
		_.defaults(options, {
			number_paging: true,
			path: 'results'
		})
	);
	this.collection = new md.ItemCollection({
		model: App.Model.LocalPlaylistItem,
		state: {
			playing: false
		}
	});
}

ControlLocalList.prototype = _.create(ControlList.prototype, {
	getParam: function() {
		var param = this.model.params();
		param.limit = this.options.limit();
		return param;
	},
	currentVideo: function() {
		var item = this.currentItem();
		if (item) return item.resource();
	},
	videoOfItem: function(item) {
		return item.resource();
	},
	load: function(prev, redraw) {
		var self = this;
		return ControlList.prototype.load.call(this, prev, false).then(function() {
			if (redraw) m.redraw();
			return self.collection.populate();
		});
	}
});

module.exports = ControlLocalList;
