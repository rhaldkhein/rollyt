var _ = require('lodash');
var m = require('mithril');
var moment = require('moment');
var ControlBase = require('./controlBase');

// var arrPlayed = [];
// var lastPlayedId;
/*
	options : {
		shuffle: true | false
		starred: true | false
	}
*/

function ControlChannel(model, options) {
	ControlBase.call(this, model, options, {
		model: App.Model.Video,
		url: '/youtube/search',
		state: {
			playing: false
		}
	});
}

ControlChannel.prototype = _.create(ControlBase.prototype, {

	currentPlaylistItem: function() {
		return this.currentItem();
	},

	getParam: function() {
		var date = this.options.date();
		var param = {
			channelId: this.model.id(),
			part: 'snippet',
			type: 'video',
			order: this.options.order(),
			videoDuration: this.options.duration(),
			videoCategoryId: this.options.category(),
			maxResults: this.options.limit()
		};
		if (date) {
			date = this.parseDate(date);
			if (date) {
				param.publishedAfter = date.toISOString();
			}
		}
		return param;
	},

	parseDate: function(date) {
		switch (date) {
			case 'today':
				return moment().subtract(1, 'days');
			case 'yesterday':
				return moment().subtract(2, 'days');
			case 'week':
				return moment().subtract(7, 'days');
			case 'month':
				return moment().subtract(1, 'months');
			case 'year':
				return moment().subtract(1, 'years');
		}
	},

	currentVideo: function() {
		return this.currentItem();
	},

	load: function(prev, redraw) {
		return ControlBase.prototype.load.call(this, prev).then(function(control) {
			if (redraw) m.redraw();
			return control;
		});
	}

});

module.exports = ControlChannel;
