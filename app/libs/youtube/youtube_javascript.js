var NodeCache = require('node-cache');
var _ = require('lodash');
var async = require('async');
var youtube = gapi.client.youtube;

// Caches
var cacheChannel = new NodeCache();
var cacheVideo = new NodeCache();
var cachePlaylist = new NodeCache();
var cachePlaylistItem = new NodeCache();

var resultLimit = 50;

// Enums
var KIND = {
  video: 'youtube#video',
  playlist: 'youtube#playlist',
  channel: 'youtube#channel',
  playlistItem: 'youtube#playlistItem'
};

var LIST_ITEM_ID_PATH = {
  video: 'id',
  channel: 'id',
  playlist: 'id',
  playlistitem: 'snippet.resourceId.videoId',
  subscription: 'snippet.resourceId.channelId'
};

function filterParam(param) {
  if (param.relatedToVideoId && _.isFalsy(param.relatedToVideoId)) return true;
  if (param.channelId && _.isFalsy(param.channelId)) return true;
  if (param.playlistId && _.isFalsy(param.playlistId)) return true;
  if (param.maxResults && param.maxResults > resultLimit) param.maxResults = resultLimit;
}

function filterIds(ids) {
  if (_.isArray(ids)) {
    for (var i = 0; i < ids.length; i++) {
      if (_.isFalsy(ids[i])) return true;
    }
    return false;
  } else {
    return _.isFalsy(ids);
  }
}

// Helpers
function pluckIds(arr) {
  var plucked = [],
    item,
    i = 0;
  for (; i < arr.length; i++) {
    item = arr[i];
    if (item.id.kind === KIND.video) {
      plucked.push(item.id.videoId);
    } else if (item.id.kind === KIND.playlist) {
      plucked.push(item.id.playlistId);
    } else if (item.id.kind === KIND.channel) {
      plucked.push(item.id.channelId);
    }
  }
  return plucked;
}

// Request by search. Only writes cache result. Without diffing.
function requestSearch(cacheObj, requestObj, param) {
  if (filterParam(param)) return Promise.reject(new Error('invalid_param'));
  return Promise.resolve(requestObj.list(param)).then(function(response) {
    var item,
      items = response.result.items,
      i = 0;
    for (; i < items.length; i++) {
      item = items[i];
      if (cacheObj && !cacheObj.get(item.id)) cacheObj.set(item.id, item);
    }
    return response;
  });
}

// Request by array of ids. Diffing cache as well.
function requestInfo(cacheObj, requestObj, ids, parts) {
  if (filterIds(ids)) return Promise.reject(new Error('invalid_ids'));
  var cached = cacheObj.mget(ids);
  var toRequestIds = _.pullAll(ids, _.keys(cached));
  var toReturnArr = _.values(cached);
  if (toRequestIds.length > 0) {
    return Promise.resolve(
      requestObj.list({
        part: parts,
        id: toRequestIds.join(',')
      })
    ).then(function(response) {
      var item,
        items = response.result.items,
        i = 0;
      for (; i < items.length; i++) {
        item = items[i];
        cacheObj.set(item.id, item);
      }
      return toReturnArr.concat(items);
    });
  } else {
    return Promise.resolve(toReturnArr);
  }
}

// Default search params
var defaultSearchParam = {
  part: 'snippet',
  type: 'video',
  order: 'relevance',
  maxResults: 5
};

// Constructor
function Youtube() {
  _.bindAll(this, ['channelInfo', 'playlistInfo', 'videoInfo', 'playlistItemInfo']);
}

// Prototypes
Youtube.prototype = {
  // Export kind enums
  KIND: KIND,
  // Populate a reference
  _populateReferenceByPromise: function(promise, method, getIdPath, setToPath) {
    var results;
    return promise
      .then(function(data) {
        results = data;
        return method(_.map(data, getIdPath));
      })
      .then(function(data) {
        var item,
          i = results.length - 1;
        for (; i >= 0; i--) {
          item = results[i];
          _.set(item, setToPath, _.find(data, ['id', _.get(item, getIdPath)]));
        }
        return results;
      });
  },
  _populateReferenceByItems: function(items, method, getIdPath, setToPath) {
    return method(_.map(items, getIdPath)).then(function(data) {
      var item,
        i = items.length - 1;
      for (; i >= 0; i--) {
        item = items[i];
        _.set(item, setToPath, _.find(data, ['id', _.get(item, getIdPath)]));
      }
      return items;
    });
  },
  _populatePlaylistItems: function(items) {
    var self = this;
    return new Promise(function(resolve, reject) {
      async.parallel(
        [
          function(done) {
            self
              ._populateReferenceByItems(items, self.playlistInfo, 'snippet.playlistId', 'snippet.playlist')
              .then(function(data) {
                done(null, data);
              })
              .catch(done);
          },
          function(done) {
            self
              ._populateReferenceByItems(items, self.videoInfo, 'snippet.resourceId.videoId', 'snippet.video')
              .then(function() {
                done();
              })
              .catch(done);
          }
        ],
        function(err, results) {
          if (!err) {
            resolve(results[0]);
          } else {
            reject(err);
          }
        }
      );
    });
  },
  // Search for all types. Video, Playlist, Channel.
  search: function(param) {
    var self = this;
    return new Promise(function(resolve, reject) {
      async.waterfall(
        [
          function(done) {
            param = _.defaults({}, param || {}, defaultSearchParam);
            if (filterParam(param)) {
              done(new Error('invalid_param'));
              return;
            }
            var request = Promise.resolve(youtube.search.list(param));
            request.then(
              function(response) {
                done(null, response.result, param);
              },
              function(err) {
                done(err);
              }
            );
          },
          function(result, param, done) {
            var ids = pluckIds(result.items);
            var callbackResolved = function(response) {
              result.items = response;
              done(null, result);
            };
            var callbackRejected = function(err) {
              done(err);
            };
            switch (param.type) {
              case 'video':
                self.videoInfo(ids).then(callbackResolved, callbackRejected);
                break;
              case 'playlist':
                self.playlistInfo(ids).then(callbackResolved, callbackRejected);
                break;
              case 'channel':
                self.channelInfo(ids).then(callbackResolved, callbackRejected);
                break;
            }
          }
        ],
        function(err, results) {
          if (!err) {
            resolve(results);
          } else {
            reject(err);
          }
        }
      );
    });
  },
  channelInfo: function(id) {
    var isArr = _.isArray(id);
    if (!isArr) id = [id];
    return requestInfo(cacheChannel, youtube.channels, id, 'snippet,statistics,brandingSettings').then(function(data) {
      return isArr ? data : data[0];
    });
  },
  videoInfo: function(id) {
    var isArr = _.isArray(id);
    if (!isArr) id = [id];
    var p = requestInfo(cacheVideo, youtube.videos, id, 'snippet,contentDetails,statistics');
    return this._populateReferenceByPromise(p, this.channelInfo, 'snippet.channelId', 'snippet.channel').then(function(
      data
    ) {
      return isArr ? data : data[0];
    });
  },
  playlistInfo: function(id) {
    var isArr = _.isArray(id);
    if (!isArr) id = [id];
    var p = requestInfo(cachePlaylist, youtube.playlists, id, 'snippet,contentDetails');
    return this._populateReferenceByPromise(p, this.channelInfo, 'snippet.channelId', 'snippet.channel').then(function(
      data
    ) {
      return isArr ? data : data[0];
    });
  },
  playlistItemInfo: function() {
    // Might not need this. Same response with `playlistItems`.
  },
  playlistItems: function(param) {
    // Response from youtube is exactly the same with `playlistItemInfo`
    var self = this;
    return requestSearch(cachePlaylistItem, youtube.playlistItems, param).then(function(response) {
      return self._populatePlaylistItems(response.result.items).then(function(data) {
        response.result.items = data;
        return response.result;
      });
    });
  },
  list: function(ofWhat, paramObject) {
    var requestObj;
    var cacheObj;
    var funcPopulate;
    switch (ofWhat) {
      case 'channel':
        requestObj = youtube.channels;
        funcPopulate = this.channelInfo;
        break;
      case 'video':
        requestObj = youtube.videos;
        funcPopulate = this.videoInfo;
        break;
      case 'playlist':
        requestObj = youtube.playlists;
        funcPopulate = this.playlistInfo;
        break;
      case 'playlistitem':
        requestObj = youtube.playlistItems;
        funcPopulate = this.videoInfo;
        break;
      case 'activity':
        requestObj = youtube.activities;
        funcPopulate = this.videoInfo;
        break;
      case 'subscription':
        requestObj = youtube.subscriptions;
        funcPopulate = this.channelInfo;
        break;
    }
    if (requestObj) {
      return requestSearch(cacheObj, requestObj, paramObject).then(function(responseA) {
        var result = responseA.result;
        var ids = _.map(result.items, LIST_ITEM_ID_PATH[ofWhat]);
        return funcPopulate(ids).then(function(responseB) {
          result.items = responseB;
          return result;
        });
      });
    } else {
      return Promise.reject('invalid list type');
    }
  },
  subscribed: function(id) {
    return youtube.subscriptions
      .list({
        part: 'id',
        mine: true,
        forChannelId: id
      })
      .getPromise()
      .then(function(response) {
        var items = _.get(response, 'result.items');
        return items && items.length > 0 && items[0].id;
      });
  },
  subscribeTo: function(id) {
    return youtube.subscriptions
      .insert({
        part: 'snippet',
        snippet: {
          resourceId: {
            kind: 'youtube#channel',
            channelId: id
          }
        }
      })
      .getPromise()
      .then(function(response) {
        return _.get(response, 'result.id');
      });
  },
  unsubscribeTo: function(id) {
    return youtube.subscriptions
      .delete({
        id: id
      })
      .getPromise();
  },
  liked: function(id) {
    return youtube.videos
      .getRating({
        id: id
      })
      .getPromise()
      .then(function(response) {
        var items = _.get(response, 'result.items');
        return items && items.length > 0 && items[0].rating == 'like';
      });
  },
  like: function(id, data) {
    return youtube.videos
      .rate({
        id: id,
        rating: data.like ? 'like' : 'none'
      })
      .getPromise();
  }
};

// Export singleton object
module.exports = new Youtube();
