/**
 * Console wrapper
 */

// Imports
var _ = require('lodash'),
  m = require('mithril'),
  util = require('./util'),
  renderCount = 0;

// Base custom console
var _console = Config.prod
  ? {}
  : {
      logRender: function() {
        if (util.isChrome) this.log('%c# - - - Render - - - ' + ++renderCount, 'color: #0B486B;');
      },
      logServer: function(data) {
        m.request('/~server/console/log?data=' + data);
      }
    };

if (Config.prod || !util.isChrome) {
  _console.log = _.noop;
  _console.info = _.noop;
  _console.warn = _.noop;
  _console.error = function(err) {
    if (global.__DEBUG__) console.error('X-Error', err);
  };
}

// Create new console object
module.exports = _.create(console, _console);
