var _ = require('lodash');
var m = require('mithril');
var util = require('libs/util');
var init = false;
var modes = null;

var names = {
  normal: 'Normal',
  karaoke: 'Karaoke'
};

function initialize() {
  modes = {
    normal: [
      function() {
        App.Settings.playerExpand(util.isMobile ? true : false);
        App.Settings.playerRemoveAfter(false);
        App.Settings.playerListMinimize(false);
        SearchBar._pintext(null);
        App.Components.Player.state.repeat().current = 0; // 0: off, 1: on
        ContextShare.clickDisconnect();
        m.route.set('/');
      }
    ],
    karaoke: [{
      func: App.Settings.playerExpand,
      value: true,
      key: 'settings.playerExpand'
    }, {
      func: App.Settings.playerRemoveAfter,
      value: true,
      key: 'settings.playerRemoveAfter'
    }, {
      func: SearchBar._pintext,
      value: 'karaoke',
      key: 'settings.searchBarPinText'
    }, function() {
      App.Components.Player.state.repeat().current = 1; // 0: off, 1: on
      if (!App.User.isGuest()) ContextShare.clickShare();
      if (!_.isEmpty(Config.defaultKaraokeChannel))
        m.route.set('/player.channel?id=' + Config.defaultKaraokeChannel);
    }]
  };
  // As of now, settings only.
  // Might include user preference as well in the future.
  App.Settings.onchange = function(key, val) {
    if (exports.getMode() === 'karaoke') {
      var setting = _.find(modes.karaoke, ['key', key]);
      if (setting && val !== setting.value) {
        ModalNotice.show({
          mini: false,
          title: 'View Mode Notice',
          body: m('.ph3 pv1',
            m('div', 'You have been automatically redirected to ', m('b', 'normal'), ' view mode. As you have changed a setting that is required by the previous mode.')
          )
        });
        exports.setMode('normal');
      }
    }
  };
}


exports.getName = function(mode) {
  mode = mode || exports.getMode();
  return names[mode];
};

exports.getMode = function() {
  return App.Settings.mode();
};

exports.setMode = function(mode) {
  if (!init) {
    initialize();
    init = true;
  }
  mode = mode || exports.getMode();
  App.Settings.mode(mode);
  _.forEach(modes[mode], function(item) {
    if (_.isFunction(item)) item();
    else item.func(item.value);
  });
};

exports.isNormal = function() {
  return exports.getMode() === 'normal';
};

var ContextShare = require('comp/contexts/contextSharing');
var SearchBar = require('comp/searchBar');
var ModalNotice = require('comp/modals/modalNotice');