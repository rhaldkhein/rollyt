
exports.separator = ' - ';

exports.set = function(text) {
    if (document) {
        document.title = App.Info.name + exports.separator + (text || App.Info.tagline);
    }
};

exports.playing = function(videoId) {
    if (document && videoId != Config.defaultVideoId) {
        var model = App.Model.Video.create({
            _id: videoId
        });
        (model.isSaved() ? Promise.resolve() : model.fetch()).then(function() {
            document.title = model.title() + exports.separator + App.Info.name;
        }).catch(Console.error);
    } else {
        exports.set();
    }
};

// Init
exports.set();