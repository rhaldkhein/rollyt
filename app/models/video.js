/**
 * Model Video
 */

var _ = require('lodash');
var md = require('mithril-data');
var youtube = require('libs/youtube');

App.Model.Video = md.model({
	name: 'Video',
	url: '/youtube/video',
	props: [
		'title',
		'description',
		'channel',
		'image',
		'image_default',
		'published',
		'duration',
		'views',
		'likes',
		'liked',
		'category'
	],
	refs: {
		channel: 'Channel'
	},
	defaults: {
		title: 'Unknown Video',
		description: 'Unknown Video',
		channel: new App.Model.Channel(),
		image: '/static/images/thumbnails/thumbnail_320x180_trans.png',
		views: 0,
		likes: 0,
		liked: false
	},
	placehold: ['title', 'description'],
	methods: {
		getLiked: function() {
			return md.store
				.get('/~rest/youtube/liked', {
					_id: this.id()
				})
				.then(this.liked);
		},
		setLiked: function(tf) {
			this.liked(tf);
			return md.store.get('/~rest/youtube/like', {
				_id: this.id(),
				like: tf
			});
		}
	},
	parser: function(data) {
		if (data && data.kind === youtube.KIND.video) {
			if (!data.contentDetails) {
				data.contentDetails = {
					duration: 0
				};
			}
			if (!data.statistics) {
				data.statistics = {
					viewCount: 0,
					likeCount: 0
				};
			}
			return {
				_id: data.id,
				title: data.snippet.title,
				published: data.snippet.publishedAt,
				description: data.snippet.description,
				image: data.snippet.thumbnails ? _.securize(data.snippet.thumbnails.medium.url) : undefined,
				image_default: data.snippet.thumbnails ? _.securize(data.snippet.thumbnails.default.url) : undefined,
				channel: data.snippet.channel,
				duration: data.contentDetails.duration,
				views: data.statistics.viewCount,
				likes: data.statistics.likeCount,
				category: data.snippet.categoryId
			};
		}
		return data;
	}
});
