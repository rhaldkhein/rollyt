/**
 * Model Account
 */

var md = require('mithril-data');

App.Model.UserPreference = md.model({
	name: 'UserPreference',
	props: ['user', 'preference', 'preference_value', 'value', '_id'],
	refs: {
		preference: 'Preference'
	}
});