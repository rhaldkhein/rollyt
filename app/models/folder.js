/**
 * Model Folder
 */

var _ = require('lodash'),
  md = require('mithril-data');

App.Model.Folder = md.model({
  name: 'Folder',
  props: ['user', 'parent', 'name', 'is_public', 'date_created', '_id', 'item_count'],
  refs: {
    user: 'User',
    parent: 'Folder'
  },
  defaults: {
    item_count: 0
  },
  methods: {
    updateItemCount: function() {
      var self = this;
      return md.store
        .get('/~rest/folder/item_count', {
          id: this.id()
        })
        .then(function(data) {
          self.item_count(data.sum);
        });
    }
  }
});
