/**
 * Model User.
 */

var _ = require('lodash'),
	md = require('mithril-data');

App.Model.User = md.model({
	name: 'User',
	props: ['sid', 'codename', 'current_account', 'date_created', 'date_modified', '_id', 'item_count'],
	refs: {
		current_account: 'Account'
	},
	defaults: {
		item_count: 0
		, current_account: new App.Model.Account()
	},
	methods: {
		isGuest: function() {
			var id = this.id();
			return (!_.isEmpty(id) && _.trim(id) === 'guest');
		},
		title: function() {
			var acc = this.current_account();
			return acc.firstname ? acc.firstname() : '';
		}
	}
});