/**
 * Model Account
 */

var _ = require('lodash'),
  md = require('mithril-data');

App.Model.Account = md.model({
  name: 'Account',
  props: [
    'type',
    'type_id',
    'firstname',
    'lastname',
    'avatar',
    'friends',
    'email',
    'user',
    'date_created',
    'date_modified',
    '_id'
  ],
  refs: {
    user: 'User'
  },
  defaults: {
    firstname: 'Unknown'
  },
  placehold: ['firstname'],
  methods: {
    isGuest: function() {
      var id = this.id();
      return !_.isEmpty(id) && _.trim(id) === 'guest';
    },
    fullname: function() {
      return _.trim((this.firstname() || '') + ' ' + (this.lastname() || ''));
    }
  }
  // , parser: function(data) {
  //  var picked = _.pick(data, [
  //    '_id', 'type', 'type_id', 'firstname', 'lastname', 'avatar',
  //    'email', 'access_token', 'user', 'date_created', 'date_modified'
  //  ]);
  //  return picked;
  // }
});
