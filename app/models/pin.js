/**
 * Model Folder
 */

var _ = require('lodash');
var md = require('mithril-data');
var mapRequestMethods;

App.Model.Pin = md.model({
    name: 'Pin',
    props: ['user', 'resource_type', 'resource_id', 'flags', 'date_created', '_id', 'resource'],
    refs: {
        user: 'User'
    },
    defaults: {
        flags: {
            playLimit: 0,
            shuffle: false,
            itemLimit: 12,
            // channel only, but fine to add here
            date: undefined,
            order: 'date',
            duration: 'any'
        },
        resource: new App.Model.Playlist({
            title: '[Deleted pin]'
        })
    },
    methods: {
        populateResource: function(own) {
            var self = this;
            if (!mapRequestMethods) mapRequestMethods = App.Model.LocalPlaylistItem.requestMethods();
            var resource = this.resource();
            if (resource.id()) return Promise.resolve(resource);
            return mapRequestMethods[this.resource_type()](this.resource_id(), own).then(function(data) {
                return self.resource(App.Model.typeMap[self.resource_type()].create(data[0] || data));
            });
        }
    }
});
