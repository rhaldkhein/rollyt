/**
 * Model Account
 */

var md = require('mithril-data');

App.Model.StarredVideo = md.model({
	name: 'StarredVideo',
	props: ['user', 'video_id', 'folder', 'date_created', '_id'],
	refs: {
		user: 'User'
	}
});