/**
 * Playlist Video
 */

var _ = require('lodash');
var md = require('mithril-data');
var youtube = require('libs/youtube');

App.Model.Playlist = md.model({
	name: 'Playlist',
	url: '/youtube/playlist',
	props: ['title', 'channel', 'image', 'image_default', 'published', 'videos'],
	refs: {
		channel: 'Channel'
	},
	defaults: {
		title: 'Unknown Playlist',
		channel: new App.Model.Channel(),
		image: '/static/images/thumbnails/thumbnail_320x180_trans.png',
		videos: 0
	},
    placehold: ['title'],
	parser: function(data) {
		if (data && data.kind === youtube.KIND.playlist) {
			return {
				_id: data.id,
				title: data.snippet.title,
				published: data.snippet.publishedAt,
				image: data.snippet.thumbnails ? _.securize(data.snippet.thumbnails.medium.url) : undefined,
				image_default: data.snippet.thumbnails ? _.securize(data.snippet.thumbnails.default.url) : undefined,
				channel: data.snippet.channel,
				videos: data.contentDetails.itemCount
			};
		}
		return data;
	}
});