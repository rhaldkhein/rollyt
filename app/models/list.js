/**
 * Channel Model
 */

var _ = require('lodash');
var md = require('mithril-data');

App.Model.List = md.model({
	name: 'List',
	props: ['title', 'link', 'kind', 'type', 'params', 'settings'],
	url: '/local/list',
	defaults: {
		title: 'Videos',
		type: 'Video'
	},
	placehold: ['title'],
	parser: function(data) {
		if (data && data.id) {
			data._id = data.id;
		}
		return data;
	},
	methods: {
		image: function() {
			return _.getThumbnail(this.title(), 320, 180);
		}
	}
});
