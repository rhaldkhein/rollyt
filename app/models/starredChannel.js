/**
 * Model Account
 */

var md = require('mithril-data');

App.Model.StarredChannel = md.model({
	name: 'StarredChannel',
	props: ['user', 'channel_id', 'folder', 'date_created', '_id'],
	refs: {
		user: 'User'
	}
});