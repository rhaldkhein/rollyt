/**
 * Model Folder
 */

var _ = require('lodash');
var md = require('mithril-data');

App.Model.LocalPlaylistItem = md.model({
	name: 'LocalPlaylistItem',
	props: ['local_playlist', 'resource_type', 'resource_id', 'flags', '_id', 'resource'],
	refs: {
		local_playlist: 'LocalPlaylist'
	},
	defaults: {
		resource: new App.Model.Video({
			title: '[Unknown Video]'
		})
	},
	methods: {
		populateResource: function() {
			var self = this;
			var resource = this.resource();
			if (resource && resource._id()) return Promise.resolve(resource);
			return App.Model.requestMap[this.resource_type()](this.resource_id()).then(function(data) {
				return self.resource(App.Model.typeMap[self.resource_type()].create(data));
			});
		}
	},
	statics: {
		requestMethods: function() {
			return App.Model.requestMap;
		}
	},
	parser: function(data) {
		if (data.video_id) {
			// Create new object with correct data
			return {
				_id: data._id,
				resource_type: 'youtube#video',
				resource_id: data.video_id
			};
		}
		return data;
	}
});
