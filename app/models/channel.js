/**
 * Channel Model
 */

var _ = require('lodash');
var md = require('mithril-data');
var youtube = require('libs/youtube');

App.Model.Channel = md.model({
  name: 'Channel',
  url: '/youtube/channel',
  props: ['title', 'image', 'image_default', 'banner', 'published', 'videos', 'subscribers', 'views'],
  defaults: {
    title: 'Unknown Channel',
    image: '/static/images/thumbnails/thumbnail_88x88_dark.png',
    videos: 0,
    subscribers: 0,
    views: 0,
    subscribed: '' // Subscription ID
  },
  placehold: ['title'],
  methods: {
    // LAST CODE:
    // Subscribed
    getSub: function() {
      return md.store
        .get('/~rest/youtube/subscribed', {
          _id: this.id()
        })
        .then(this.subscribed);
    },
    setSub: function(tf) {
      var self = this;
      var subId = this.subscribed();
      this.subscribed(tf);
      if (tf) {
        return md.store
          .get('/~rest/youtube/subscribeto', {
            _id: self.id()
          })
          .catch(function(err) {
            self.subscribed(subId);
            throw err;
          });
      } else {
        return md.store
          .get('/~rest/youtube/unsubscribeto', {
            _id: subId
          })
          .catch(function(err) {
            self.subscribed(subId);
            throw err;
          });
      }
    }
  },
  parser: function(data) {
    if (data && data.kind === youtube.KIND.channel) {
      return {
        _id: data.id,
        title: data.snippet.title,
        published: data.snippet.publishedAt,
        image: data.snippet.thumbnails ? _.securize(data.snippet.thumbnails.default.url) : undefined,
        image_default: data.snippet.thumbnails ? _.securize(data.snippet.thumbnails.default.url) : undefined,
        banner: data.brandingSettings ? _.securize(data.brandingSettings.image.bannerMobileMediumHdImageUrl) : undefined,
        subscribers: data.statistics.subscriberCount,
        videos: data.statistics.videoCount,
        views: data.statistics.viewCount
      };
    }
    return data;
  }
});
