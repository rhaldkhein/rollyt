/**
 * Model Account
 */

var md = require('mithril-data');

App.Model.Preference = md.model({
	name: 'Preference',
	props: ['name', 'description', 'data_type', 'default_value', 'min_value', 'max_value', '_id']
});
