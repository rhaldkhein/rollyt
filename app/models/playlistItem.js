/**
 * Model Playlist Item
 */

var md = require('mithril-data');
var youtube = require('libs/youtube');

App.Model.PlaylistItem = md.model({
    name: 'PlaylistItem',
    url: '/youtube/playlistitem',
    props: ['playlist', 'video', 'position'],
    refs: {
        playist: 'Playlist',
        video: 'Video'
    },
    defaults: {
        playlist: new App.Model.Video(),
        video: new App.Model.Video(),
        position: -1
    },
    parser: function(data) {
        if (data && data.kind === youtube.KIND.playlistItem) {
            if (!data.snippet.video) {
                data.snippet.video = {
                    kind: 'youtube#video',
                    snippet: {
                        title: '[' + data.snippet.title + ']',
                        description: data.snippet.description
                    }
                };
            }
            return {
                _id: data.id,
                playlist: data.snippet.playlist,
                video: data.snippet.video,
                position: data.snippet.position
            };
        }
        return data;
    }
});