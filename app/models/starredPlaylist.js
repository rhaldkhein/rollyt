/**
 * Model Account
 */

var md = require('mithril-data');

App.Model.StarredPlaylist = md.model({
	name: 'StarredPlaylist',
	props: ['user', 'playlist_id', 'folder', 'date_created', '_id'],
	refs: {
		user: 'User'
	}
});