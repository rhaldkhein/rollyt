/**
 * Model Account
 */

var md = require('mithril-data');

App.Model.StarredSubitem = md.model({
	name: 'StarredSubitem',
	props: ['user', 'item_id', 'subitem_id', 'date_created', '_id'],
	refs: {
		user: 'User'
	}
});