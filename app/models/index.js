// Create models
// Auto bound to `App.Model` namespace
require('./account');
require('./list');
require('./user');
require('./channel');
require('./video');
require('./playlist');
require('./playlistItem');
require('./preference');
require('./userPreference');
require('./starredChannel');
require('./starredPlaylist');
require('./starredSubitem');
require('./starredVideo');
require('./folder');
require('./localPlaylist');
require('./localPlaylistItem');
require('./pin');

Console.log('Models %O', App.Model);

var _ = require('lodash');
var md = require('mithril-data');
var youtube = require('libs/youtube');

// Links type maps for models

App.Model.classMap = {
    'Video': 'youtube#video',
    'Channel': 'youtube#channel',
    'Playlist': 'youtube#playlist',
    'List': 'local#list',
    'LocalPlaylist': 'local#playlist',
    'LocalPlaylistItem': 'local#playlistitem'
};

App.Model.typeMap = {
    'youtube#video': App.Model.Video,
    'youtube#channel': App.Model.Channel,
    'youtube#playlist': App.Model.Playlist,
    'local#list': App.Model.List,
    'local#playlist': App.Model.LocalPlaylist,
    'local#playlistitem': App.Model.LocalPlaylistItem
};

App.Model.requestMap = {
    'youtube#video': youtube.videoInfo,
    'youtube#channel': youtube.channelInfo,
    'youtube#playlist': youtube.playlistInfo,
    'local#list': function(ids) {
        return md.store.get('/~rest/local/list', {
            _id: ids
        });
    },
    'local#playlist': function(ids, own) {
        return md.store.get('/~rest/localplaylist/list', {
            _id: _.isArray(ids) ? ids.join(',') : ids,
            own: !!own
        }).then(function(data) {
            return data.results;
        });
    },
    'local#playlistitem': function(ids) {
        // `localplaylistitem/list` is used to related to playlist
        // `localplaylistitem/ids` is used to get items by multiple ids
        return md.store.get('/~rest/localplaylistitem/list/ids', {
            _id: _.isArray(ids) ? ids.join(',') : ids
        }).then(function(data) {
            return data.results;
        });
    }
};

// Clear shemas downloaded from server