/**
 * Model Folder
 */

var _ = require('lodash');
var md = require('mithril-data');
var toast = require('toastr');

App.Model.LocalPlaylist = md.model({
  name: 'LocalPlaylist',
  props: [
    'user',
    'title',
    'image',
    'folder',
    'is_private',
    'flags',
    'date_modified',
    'date_created',
    '_id',
    'item_count'
  ],
  refs: {
    user: 'User',
    folder: 'Folder'
  },
  defaults: {
    flags: {
      repeat: false,
      shuffle: false
    },
    user: new App.Model.User()
  },
  placehold: ['title'],
  methods: {
    channel: function() {
      return this.user();
    },
    is_mine: function() {
      var user = this.user();
      return user && user.id() === App.User.id();
    },
    addItems: function(items) {
      var self = this;
      return md.store
        .post('/~rest/localplaylistitem/all', {
          _id: this.id(),
          items: items
        })
        .then(function(resData) {
          self.item_count(self.item_count() + resData.data.add_length);
          toast.success('Added to playlist');
          return resData.data;
        })
        .catch(function() {
          toast.error('Unable to add to playlist');
        });
    },
    moveItem: function(src, des) {
      var isbool = _.isBoolean(des);
      return md.store.post(this.url() + '/moveitem', {
        _id: this.id(),
        src: src.id(),
        des: !isbool ? des.id() : undefined,
        top: isbool ? des : undefined
      });
    },
    imageFallback: function() {
      return _.getThumbnail(this.title(), 320, 180);
    },
    image_default: function() {
      return this.image();
    }
  }
});
