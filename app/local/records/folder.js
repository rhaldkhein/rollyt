var _ = require('lodash');
var store = require('store');
var validate = require('validator');
var key = 'folders';

if (!_.isArray(store.get(key))) {
	store.set(key, []);
}

function validateName(data) {
	var name = data.name();
	if (validate.isEmpty(name) ||
		!validate.matches(name, /^[a-z0-9 _-]+$/i)) {
		return {
			error: true,
			code: 'REBR'
		};
	}
}

function validateId(data) {
	var id;
	if (_.isFunction(data.id)) {
		id = data.id();
	} else if (data._id) {
		id = data._id;
	}
	if (validate.isEmpty(id)) {
		return {
			error: true,
			code: 'REBR'
		};
	}
}

exports.get = function(data, url) {
	if (_.endsWith(url, '/folder/all')) {
		// Get all
		return store.get(key);
	}
	// Return the record
	return [];
};

exports.post = function(data) {
	// Validations
	var result = validateName(data);
	if (result) return result;
	// Query
	var folders = store.get(key);
	var name = data.name();
	var doc = _.find(folders, ['name', name]);
	if (doc) {
		return {
			error: true,
			code: 'REAX'
		};
	} else {
		doc = {
			_id: _.uniqueId(),
			name: name
		};
		folders.push(doc);
		store.set(key, folders);
		return doc;
	}
};

exports.put = function(data) {
	// Validations
	var result = validateName(data);
	if (result) return result;
	result = validateId(data);
	if (result) return result;
	// Query
	var folders = store.get(key);
	var name = data.name();
	var doc = _.find(folders, ['name', name]);
	if (doc) {
		return {
			error: true,
			code: 'REAX'
		};
	} else {
		var index = _.findIndex(folders, ['_id', data.id()]);
		folders[index].name = name;
		store.set(key, folders);
		return folders[index];
	}
};

exports.delete = function(data) {
	var result = validateId(data);
	if (result) return result;
	var folders = store.get(key);
	var index = _.findIndex(folders, ['_id', data._id]);
	folders.splice(index, 1);
	store.set(key, folders);
	return;
};