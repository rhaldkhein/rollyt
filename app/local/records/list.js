var _ = require('lodash');
var util = require('libs/util');
var records = require('./index');
var moment = require('moment');

exports.get = function(data) {
	var self = this;

	function fnMap(id) {
		var idParts = id.split('.');
		return self[idParts[0]](id, idParts);
	}
	if (_.isArray(data._id)) {
		return _.map(data._id, fnMap);
	} else {
		return fnMap(data._id);
	}
};

exports.liked = function(id) {
	return {
		kind: 'youtube#liked',
		id: id,
		title: 'Liked Videos',
		link: '/youtube/videolist',
		params: {
			part: 'snippet',
			myRating: 'like'
		}
	};
};

exports.related = function(id, parts) {
	// id = related.<id>
	var promise = null;
	var rec = {
		kind: 'youtube#related',
		id: id,
		title: 'Related',
		link: '/youtube/search',
		params: {
			part: 'snippet',
			type: 'video'
		}
	};
	parts.shift();
	var videoId = _.trim(parts.join('.'));
	if (videoId && videoId !== 'undefined') {
		var videoModel = App.Model.Video.create({
			_id: videoId
		});
		rec.params.relatedToVideoId = videoId;
		if (videoModel.isSaved()) {
			rec.title += ' : ' + videoModel.title();
		} else {
			// Load reference model
			promise = videoModel.fetch().then(function(model) {
				rec.title += ' : ' + model.title();
				return rec;
			});
		}
	}
	return promise || rec;
};

exports.popular = function(id, parts) {
	// id = popular.<category>.<country>
	var rec = {
		kind: 'youtube#popular',
		id: id,
		title: 'Popular',
		link: '/youtube/videolist',
		params: {
			part: 'snippet',
			chart: 'mostPopular'
		}
	};
	if (parts[1]) {
		rec.params.videoCategoryId = parts[1]; // Category
		rec.title +=
			' ' +
			records('category', 'get', null, {
				_id: rec.params.videoCategoryId
			}).title;
	}
	if (parts[2]) {
		rec.params.regionCode = parts[2].toUpperCase(); // Region
		rec.title += ' in ' + _.find(App.Resources.countryCodes, ['ISO3166-1-Alpha-2', rec.params.regionCode]).name;
	}
	return rec;
};

exports.search = function(id, parts) {
	// id = search.<query>.<category>.<country>.<date>.<order>
	var rec = {
		kind: 'youtube#search',
		id: id,
		title: 'Search',
		link: '/youtube/search',
		params: {
			part: 'snippet'
		}
	};
	parts.shift();
	var order = parts.pop();
	var date = parts.pop();
	var country = parts.pop();
	var category = parts.pop();
	var query = _.trim(parts.join('.'));
	if (query && query !== 'undefined') {
		rec.title += ' : ' + query;
		rec.params.q = query;
	}
	if (category && query !== 'undefined') {
		rec.params.videoCategoryId = category;
		rec.title +=
			' : ' +
			records('category', 'get', null, {
				_id: rec.params.videoCategoryId
			}).title;
	}
	if (country && query !== 'undefined') {
		rec.params.regionCode = country;
		rec.title += ' : ' + _.find(App.Resources.countryCodes, ['ISO3166-1-Alpha-2', rec.params.regionCode]).name;
	}
	if (date && date !== 'undefined') {
		rec.title += ' : ' + util.readableDate(date);
		rec.params.publishedAfter = util.dateToISOString(date);
	}
	if (order && order !== 'undefined') {
		rec.title += ' : ' + util.readableOrder(order);
		rec.params.order = order;
	}
	return rec;
};

exports.channel = function(id, parts) {
	// id = channel.<id>.<date>.<order>
	var promise = null;
	var rec = {
		kind: 'youtube#channel',
		id: id,
		title: 'Channel',
		link: '/youtube/search',
		params: {
			part: 'snippet'
		}
	};
	parts.shift();
	var order = parts.pop();
	var date = parts.pop();
	var channelId = _.trim(parts.join('.'));
	if (channelId && channelId !== 'undefined') {
		var channelModel = App.Model.Channel.create({
			_id: channelId
		});
		rec.params.channelId = channelId;
		if (channelModel.isSaved()) {
			rec.title += ' : ' + channelModel.title();
		} else {
			// Load reference model
			promise = channelModel.fetch().then(function(model) {
				rec.title += ' : ' + model.title();
				return rec;
			});
		}
	}
	if (date && date !== 'undefined') {
		rec.title += ' : ' + util.readableDate(date);
		rec.params.publishedAfter = util.dateToISOString(date);
	}
	if (order && order !== 'undefined') {
		rec.title += ' : ' + util.readableOrder(order);
		rec.params.order = order;
	}
	return promise || rec;
};

exports.playlist = function(id, parts) {
	// id = playlist.<id>
	var promise = null;
	var rec = {
		kind: 'youtube#playlist',
		id: id,
		title: 'Playlist',
		link: '/youtube/playlistitemlist',
		params: {
			part: 'snippet'
		}
	};
	parts.shift();
	var playlistId = _.trim(parts.join('.'));
	if (playlistId && playlistId !== 'undefined') {
		var playlistModel = App.Model.Playlist.create({
			_id: playlistId
		});
		rec.params.playlistId = playlistId;
		if (playlistModel.isSaved()) {
			rec.title += ' : ' + playlistModel.title();
		} else {
			// Load reference model
			promise = playlistModel.fetch().then(function(model) {
				rec.title += ' : ' + model.title();
				return rec;
			});
		}
	}
	return promise || rec;
};

exports.starred = function(id, parts) {
	// id = starred.<type>.<userid>.<folder>
	// type = video | playlist | localplaylist | channel
	// but as for now, only supported type is `video`
	var rec = {
		kind: 'local#starred',
		id: id,
		title: 'Starred Videos',
		link: '/star/all',
		params: {
			type: 'video' // Change type here in future
		}
	};
	var proms = [];
	var folder = parts.pop();
	var userid = parts.pop();

	if (folder && folder !== 'undefined') {
		rec.params.folder = folder;
		var mdlFolder = App.Model.Folder.create({
			_id: folder
		});
		if (mdlFolder.isSaved()) {
			rec.title += ' : ' + mdlFolder.name();
		} else {
			proms.push(
				mdlFolder.fetch().then(function(mdl) {
					rec.title += ' : ' + mdl.name();
				})
			);
		}
	}
	if (userid && userid !== 'undefined') {
		rec.params.userid = userid;
		var mdlUser = App.Model.User.create({
			_id: userid
		});
		if (mdlUser.isSaved() && _.isObject(mdlUser.current_account())) {
			rec.title += ' : ' + mdlUser.current_account().fullname();
		} else {
			proms.push(
				mdlUser.fetch().then(function(mdl) {
					rec.title += ' : ' + mdl.current_account().fullname();
				})
			);
		}
	}

	return (
		Promise.all(proms)
			// WARNING: To allow unknown-folder error
			// which is trying to get not owned folder
			.catch(_.noop)
			.return(rec) || rec
	);
};

exports.topic = function(id, parts) {
	// id = topic.<main>.<sub>
	var rec = {
		kind: 'youtube#search',
		id: id,
		title: 'Topic : ',
		link: '/youtube/search',
		params: {
			part: 'snippet',
			order: 'relevance',
			publishedAfter: util.dateToISOString('2weeks')
		}
	};

	var doc;
	var name;
	var topicSub = parts.pop();
	var topicMain = parts.pop();

	if (topicMain && topicMain !== 'undefined') {
		doc = records('topic', 'get', null, {
			_id: topicMain
		});
		name = doc.title;
		rec.params.topicId = topicMain;
		if (doc.category) {
			rec.params.videoCategoryId = doc.category;
			// If music, only get videos after 2 weeks
			// to get most relevance music
			if (doc.category == 10) {
				rec.params.publishedBefore = moment()
					.subtract(14, 'days')
					.toISOString();
				rec.params.publishedAfter = moment()
					.subtract(16, 'days')
					.toISOString();
			}
		}
	}

	if (topicSub && topicSub !== 'undefined') {
		rec.params.topicId = topicSub;
		name =
			records('topic', 'get', null, {
				_id: topicMain + '.sub.' + topicSub
			}).title +
			' ' +
			name;
	}
	rec.title += name;
	return rec;
};
