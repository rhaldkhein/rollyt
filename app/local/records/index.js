// Exports
module.exports = function(type, method, url, data) {
	return _records[type][method.toLowerCase()](data, url);
};

// Compiled local records
var _records = {
	list: require('./list'),
	folder: require('./folder'),
	category: require('./category'),
	topic: require('./topic')
};