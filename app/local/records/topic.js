var _records = {
	'/m/04rlf': {
		_id: '/m/04rlf',
		title: 'Music',
		category: 10,
		sub: {
			'/m/02mscn': {
				_id: '/m/02mscn',
				title: 'Christian'
			},
			'/m/0ggq0m': {
				_id: '/m/0ggq0m',
				title: 'Classical'
			},
			'/m/01lyv': {
				_id: '/m/01lyv',
				title: 'Country'
			},
			'/m/02lkt': {
				_id: '/m/02lkt',
				title: 'Electronic'
			},
			'/m/0glt670': {
				_id: '/m/0glt670',
				title: 'Hip Hop'
			},
			'/m/05rwpb': {
				_id: '/m/05rwpb',
				title: 'Indie'
			},
			'/m/03_d0': {
				_id: '/m/03_d0',
				title: 'Jazz'
			},
			'/m/064t9': {
				_id: '/m/064t9',
				title: 'Pop'
			},
			'/m/06cqb': {
				_id: '/m/06cqb',
				title: 'Reggae'
			},
			'/m/06j6l': {
				_id: '/m/06j6l',
				title: 'Rhythm and Blues'
			},
			'/m/06by7': {
				_id: '/m/06by7',
				title: 'Rock'
			},
			'/m/0gywn': {
				_id: '/m/0gywn',
				title: 'Soul'
			}
		}
	},
	'/m/0bzvm2': {
		_id: '/m/0bzvm2',
		title: 'Gaming',
		category: 20,
		sub: {
			'/m/025zzc': {
				_id: '/m/025zzc',
				title: 'Action'
			},
			'/m/02ntfj': {
				_id: '/m/02ntfj',
				title: 'Adventure'
			},
			'/m/0b1vjn': {
				_id: '/m/0b1vjn',
				title: 'Casual'
			},
			'/m/02hygl': {
				_id: '/m/02hygl',
				title: 'Music Game'
			},
			'/m/04q1x3q': {
				_id: '/m/04q1x3q',
				title: 'Puzzle'
			},
			'/m/01sjng': {
				_id: '/m/01sjng',
				title: 'Racing'
			},
			'/m/0403l3g': {
				_id: '/m/0403l3g',
				title: 'Role Playing'
			},
			'/m/021bp2': {
				_id: '/m/021bp2',
				title: 'Simulation'
			},
			'/m/022dc6': {
				_id: '/m/022dc6',
				title: 'Sports Game'
			},
			'/m/03hf_rm': {
				_id: '/m/03hf_rm',
				title: 'Strategy'
			}
		}
	},
	'/m/06ntj': {
		_id: '/m/06ntj',
		title: 'Sports',
		category: 17,
		sub: {
			'/m/0jm_': {
				_id: '/m/0jm_',
				title: 'American Football'
			},
			'/m/018jz': {
				_id: '/m/018jz',
				title: 'Baseball'
			},
			'/m/018w8': {
				_id: '/m/018w8',
				title: 'Basketball'
			},
			'/m/01cgz': {
				_id: '/m/01cgz',
				title: 'Boxing'
			},
			'/m/09xp_': {
				_id: '/m/09xp_',
				title: 'Cricket'
			},
			'/m/02vx4': {
				_id: '/m/02vx4',
				title: 'Football'
			},
			'/m/037hz': {
				_id: '/m/037hz',
				title: 'Golf'
			},
			'/m/03tmr': {
				_id: '/m/03tmr',
				title: 'Ice Hockey'
			},
			'/m/01h7lh': {
				_id: '/m/01h7lh',
				title: 'Mixed Martial Arts'
			},
			'/m/0410tth': {
				_id: '/m/0410tth',
				title: 'Motorsport'
			},
			'/m/07bs0': {
				_id: '/m/07bs0',
				title: 'Tennis'
			},
			'/m/07_53': {
				_id: '/m/07_53',
				title: 'Volleyball'
			}
		}
	},
	'/m/02jjt': {
		_id: '/m/02jjt',
		title: 'Entertainment',
		category: 24,
		sub: {
			'/m/09kqc': {
				_id: '/m/09kqc',
				title: 'Humor'
			},
			'/m/02vxn': {
				_id: '/m/02vxn',
				title: 'Movies'
			},
			'/m/05qjc': {
				_id: '/m/05qjc',
				title: 'Performing Arts'
			},
			'/m/066wd': {
				_id: '/m/066wd',
				title: 'Professional Wrestling'
			},
			'/m/0f2f9': {
				_id: '/m/0f2f9',
				title: 'TV Shows'
			}
		}
	},
	'/m/019_rr': {
		_id: '/m/019_rr',
		title: 'Lifestyle',
		sub: {
			'/m/032tl': {
				_id: '/m/032tl',
				title: 'Fashion'
			},
			'/m/027x7n': {
				_id: '/m/027x7n',
				title: 'Fitness'
			},
			'/m/02wbm': {
				_id: '/m/02wbm',
				title: 'Food'
			},
			'/m/03glg': {
				_id: '/m/03glg',
				title: 'Hobby'
			},
			'/m/068hy': {
				_id: '/m/068hy',
				title: 'Pets'
			},
			'/m/041xxh': {
				_id: '/m/041xxh',
				title: 'Beauty'
			},
			'/m/07c1v': {
				_id: '/m/07c1v',
				title: 'Technology'
			},
			'/m/07bxq': {
				_id: '/m/07bxq',
				title: 'Tourism'
			},
			'/m/07yv9': {
				_id: '/m/07yv9',
				title: 'Vehicles'
			}
		}
	},
	'/m/098wr': {
		_id: '/m/098wr',
		title: 'Society',
		sub: {
			'/m/09s1f': {
				_id: '/m/09s1f',
				title: 'Business'
			},
			'/m/0kt51': {
				_id: '/m/0kt51',
				title: 'Health'
			},
			'/m/01h6rj': {
				_id: '/m/01h6rj',
				title: 'Military'
			},
			'/m/05qt0': {
				_id: '/m/05qt0',
				title: 'Politics'
			},
			'/m/06bvp': {
				_id: '/m/06bvp',
				title: 'Religion'
			}
		}
	},
	'/m/01k8wb': {
		_id: '/m/01k8wb',
		title: 'Knowledge',
		sub: {}
	}
};

var _ = require('lodash');

exports.get = function(data) {
	if (data && data._id) return _.get(_records, data._id);
	else return _.values(_records);
};