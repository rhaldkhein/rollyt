var m = require('mithril');
var md = require('mithril-data');
var util = require('libs/util');
var elButtonRrssb = require('comp/elements/buttonRrssb');

var Social = {
  _svg: null,
  preloaded: md.stream(false),
  loaded: md.stream(false),
  load: function() {
    var loader = App.Modules.loaderjs;
    if (!Social.loaded()) {
      var files = [
        '/static/app/libs/rrssb/css/rrssb.css',
        function(resolve, reject) {
          m
            .request('/static/images/logos/socials/social-svg-paths.json')
            .then(function(data) {
              Social._svg = data;
            })
            .then(resolve)
            .catch(reject);
        }
      ];
      if (!window.jQuery) files.push('/node_modules/jquery.min.js');
      loader
        .loadMany(files)
        .then(function() {
          Social.preloaded(true);
          m.redraw();
        })
        .then(function() {
          return loader.loadMany(['/static/app/libs/rrssb/js/rrssb.min.js', 'https://platform.twitter.com/widgets.js']);
        })
        .then(function() {
          Social.loaded(true);
          m.redraw();
        })
        .catch(Console.error);
    } else {
      m.nextTick(function() {
        if (window.rrssbInit) window.rrssbInit();
        if (window.twttr) window.twttr.widgets.load();
      });
    }
  },
  LikeFollow: {
    view: function() {
      return m(
        'div',
        !Social.preloaded()
          ? 'Loading...'
          : m(
              'div.mb3 follow-us',
              m(
                'div.follow-facebook pa2 br2 mb2 ba b--light-silver',
                m('iframe', {
                  src:
                    'https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Frollytplayer&width=276&layout=standard&action=like&size=large&show_faces=true&share=false&height=80&appId=447699735377359',
                  width: 276,
                  height: 80,
                  scrolling: 'no',
                  frameborder: 0,
                  allowTransparency: true
                })
              ),
              m(
                'div.follow-twitter pa2 br2 ba b--light-silver',
                m(
                  'a',
                  {
                    href: 'https://twitter.com/rollytplayer?ref_src=twsrc%5Etfw',
                    class: 'twitter-follow-button',
                    'data-show-count': true,
                    'data-size': 'large'
                  },
                  ''
                )
              )
            )
      );
    }
  },
  Share: {
    view: function() {
      return m(
        'div',
        !Social.preloaded()
          ? 'Loading...'
          : m(
              '.share-us',
              m(
                'ul.rrssb-buttons mb3 share-us',
                m(elButtonRrssb, {
                  name: 'facebook',
                  href: 'https://www.facebook.com/sharer/sharer.php?u=https://www.rollyt.com',
                  svg: Social._svg.facebook,
                  viewBox: 29,
                  popup: true
                }),
                m(elButtonRrssb, {
                  name: 'twitter',
                  href:
                    'https://twitter.com/intent/tweet?text=Rollyt%20-%20The%20Ultimate%20Player%20for%20YouTube%20%20https%3A%2F%2Fwww.rollyt.com',
                  svg: Social._svg.twitter,
                  viewBox: 28,
                  popup: true
                }),
                m(elButtonRrssb, {
                  name: 'email',
                  href: 'mailto:?Subject=Rollyt.com%20-%20The%20Ultimate%20Player%20for%20YouTube',
                  svg: Social._svg.email,
                  viewBox: 24
                }),
                !util.isMobile
                  ? null
                  : m(elButtonRrssb, {
                      name: 'whatsapp',
                      href:
                        'whatsapp://send?text=Check%20out%20www.rollyt.com%20-%20The%20Ultimate%20Player%20for%20YouTube',
                      svg: Social._svg.whatsapp,
                      viewBox: 90,
                      action: 'share/whatsapp/share'
                    })
              )
            )
      );
    }
  }
};

module.exports = Social;
