var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var util = require('libs/util');

var bindMethods = ['videoProgress', 'playerStateChange', 'playerReady', 'playerError', '_playTimeout'];

var playerComponents = {
  Youtube: {
    oncreate: function() {
      if (!this.videoId()) return;
      this.videoPlayer = new window.YT.Player(this.iframeId(), {
        videoId: this.videoId(),
        playerVars: {
          rel: 0,
          enablejsapi: 1,
          origin: window.location.origin,
          modestbranding: 1,
          showinfo: 1
        },
        events: {
          onReady: this.playerReady,
          onStateChange: this.playerStateChange,
          onError: this.playerError
        }
      });
      this.lastVideoId(this.videoId());
    },
    onupdate: function() {
      if (this.videoPlayer) {
        if (this.state.ended() || this.videoId() !== this.lastVideoId()) {
          this.videoPlayer.loadVideoById(this.videoId(), undefined, this.state.lastQuality());
          this.state.manualEnded(false);
          this.state.ended(false);
          this.lastVideoId(this.videoId());
        }
        if (this.stop() && this.videoPlayer.stopVideo) {
          this.videoPlayer.stopVideo();
          this.stop(false);
        }
      }
    },
    onremove: function() {
      if (this.videoPlayer) {
        this.videoPlayer.getIframe().src = 'about:blank';
        this.videoPlayer.destroy();
      }
    },
    getWidth: function() {
      if (this.videoPlayer) {
        return this.videoPlayer.getIframe().clientWidth;
      }
    },
    isFullscreen: function() {
      if (this.videoPlayer && _.getFullscreenElement() === this.videoPlayer.getIframe()) {
        return true;
      }
      return false;
    },
    updateQuality: function(quality) {
      if (this.videoPlayer && this.videoPlayer.getPlaybackQuality) {
        if (this.state.lastQuality() == quality) return;
        if (this.videoPlayer.getPlaybackQuality() != quality) {
          this.videoPlayer.setPlaybackQuality(quality);
          this.state.lastQuality(quality);
        }
      }
    },
    view: function() {
      return m('div', {
        id: this.iframeId()
      });
    }
  }
};

// Listen to fullscren change and auto change to `auto` quality
document[_.getFullscreenChangeKey()] = function() {
  var el = _.getFullscreenElement();
  if (el) {
    YT.get(el.id).setPlaybackQuality('auto');
  }
};

module.exports = {
  player: playerComponents.Youtube,

  oninit: function(node) {
    _.bindAll(this, bindMethods);
    this.options = node.attrs || {};
    this.lastVideoId = md.stream();
    this.state = md.State.create({
      playTimeout: !!this.options.playTimeout,
      pauseEnd: !App.Vars.msgId,
      timeout: Config.playerErrorSkipDelay,
      duration: -1,
      manualEnded: false,
      playerStateNumber: -1,
      lastTimeoutId: undefined,
      lastQuality: undefined,
      ended: false
    });
    this.videoId = md.toStream(this.options.videoId);
    this.iframeId = md.toStream(this.options.iframeId);
    this.stop = md.toStream(this.options.stop || false);
    this.videoPlayer;
    this.endCount = 0;
  },

  onremove: function(node) {
    this.player.onremove.call(this, node);
    this.videoPlayer = null;
    this.videoId = null;
    this.iframeId = null;
  },

  oncreate: function(node) {
    this.player.oncreate.call(this, node);
    if (this.videoPlayer) {
      if (this.state.pauseEnd()) this.videoPlayer.addEventListener('onVideoProgress', this.videoProgress);
      if (App.dev) App.dev.player = this.videoPlayer;
    }
  },

  onupdate: function(node) {
    this.player.onupdate.call(this, node);
  },

  playerReady: function() {
    if (this.options.autoplay && !util.isMobile) {
      // this.videoPlayer.mute();
      this.videoPlayer.playVideo();
    } else {
      var self = this;
      setTimeout(function() {
        self._ended(true);
      }, 1000);
    }
  },

  playerError: function() {
    // `e.data` = ...
    // 2 - Invalid parameter value
    // 5 - Content cannot be played
    // 100 - Video requested was not found
    // 101 - Not allowed to be played in embedded players
    // 150 - Copyright errors & others
    this._createPlayTimeout();
  },

  videoProgress: function(e) {
    // WARNING: Cross check this! This may cause memory leak!
    if (e.data >= this.state.duration() && !this.state.manualEnded()) {
      this.state.manualEnded(true);
      this.videoPlayer.pauseVideo();
    }
  },

  playerStateChange: function(e, end) {
    // -1 – unstarted
    // 0 – ended
    // 1 – playing
    // 2 – paused
    // 3 – buffering
    // 5 – video cued
    this.state.playerStateNumber(e.data);
    if (e.data === 0 && (!this.state.pauseEnd() || end)) {
      // Ended state
      this._ended();
    } else if (e.data === 1) {
      // Playing state
      this.state.duration(this.videoPlayer.getDuration() - 1);
      this._clearPlayTimeout();
      this.endCount = 0;
    } else if (e.data === 2 && this.state.manualEnded()) {
      var self = this;
      setTimeout(function() {
        self.playerStateChange(
          {
            data: 0
          },
          true
        );
      }, 800);
    } else if (e.data === 5) {
      this.endCount = 0;
    }
    if (this.options.onchangestate) {
      this.options.onchangestate.call(this, e);
    }
  },

  _playTimeout: function() {
    // This method should only be trigger by `playerError` method.
    // Check if state is 1 (Ok) or -1 (Not Playing) after timeout
    // Limit the auto end to 10 times consicutive
    if (this.state.playerStateNumber() === -1 && this.endCount < 10) {
      // then skip next
      this.endCount++;
      this._ended(null, true);
    }
  },

  _createPlayTimeout: function() {
    // This method should only be trigger by `playerError` method.
    this._clearPlayTimeout();
    if (this.state.playTimeout()) this.state.lastTimeoutId(setTimeout(this._playTimeout, this.state.timeout()));
  },

  _clearPlayTimeout: function() {
    var id = this.state.lastTimeoutId();
    if (id) clearTimeout(id);
  },

  _ended: function(byInitial, byTimeout) {
    // `initial` is when player is ready for the first time
    if (this.options.onended) {
      this.options.onended(byInitial, byTimeout);
      m.redraw.nexttick();
      // Ensure that `manualEnded` works
      setTimeout(m.redraw, 3000);
    }
  },

  changeQuality: function(quality) {
    // Check fullscreen and if so, always `auto` quality
    if (this.player.isFullscreen.call(this)) quality = 'auto';
    this.player.updateQuality.call(this, quality);
  },

  updateQuality: function() {
    // Updates the quality based on video size
    this.changeQuality(this.player.getWidth.call(this) > 500 ? 'auto' : 'small');
  },

  view: function(node) {
    if (!_.isFunction(node.attrs.videoId) && this.videoId() !== node.attrs.videoId) {
      this.videoId(node.attrs.videoId);
    }
    return m(
      'div',
      {
        class: 'video-player'
      },
      this.player.view.call(this, node)
    );
  }
};
