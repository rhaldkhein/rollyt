var _ = require('lodash');
var m = require('mithril');
var util = require('libs/util');
var elButtonIcon = require('comp/elements/buttonIcon');
var topBarMenu = require('comp/topBarMenu');
var current = require('./current');
var ContextSharing = require('comp/contexts/contextSharing');

var bindMethods = ['clickCollapse', 'errorLogo'];
var partMethods = ['clickSharing'];

module.exports = {
  oninit: function() {
    _.bindAll(this, bindMethods, partMethods);
    this.expand = App.Settings.playerExpand;
    this._lfb = false; // Logo fallback
  },
  getShareColor: function() {
    if (ContextSharing.isHost()) return 'light-red';
    else if (ContextSharing.isJoined()) return 'light-green';
    else if (ContextSharing.isConnected()) return 'light-gray';
    return 'moon-gray';
  },
  getLogo: function(headOnly) {
    if (this._lfb) return headOnly ? 'head-logo.png' : 'head-logo-title.png';
    return headOnly ? 'head-logo.svg' : 'head-logo-title.svg';
  },
  clickCollapse: function() {
    this.expand(false);
    current.updateQuality();
  },
  clickSharing: function(e, self) {
    ContextSharing.show(this, self);
  },
  dblclickTopBar: function(e) {
    if (e.target.className === 'topbar') App.Settings.playerExpand(!App.Settings.playerExpand());
  },
  errorLogo: function() {
    this._lfb = true;
  },
  view: function() {
    var isExpand = this.expand();
    var headOnly = App.Props.breakPointMedium() && !isExpand;
    return m(
      '.topbar',
      {
        ondblclick: util.isMobile ? undefined : this.dblclickTopBar
      },
      [
        m(
          '.logo',
          m(
            'a',
            {
              href: '#/'
            },
            m('img', {
              key: 'logo-image-' + headOnly,
              src: '/static/images/player/' + this.getLogo(headOnly),
              onerror: this.errorLogo
            })
          )
        ),
        mode.getMode() === 'normal' ? null : m('.mode', m('.br1', mode.getName() + ' Mode')),
        !isExpand && App.Props.breakPointMedium()
          ? null
          : m('.actions.share', [
              m(elButtonIcon, {
                class: this.getShareColor(),
                icon: 'wifi_tethering',
                title: 'Player Sharing',
                onclick: this.clickSharing
              })
            ]),
        !isExpand ? null : m(topBarMenu),
        !isExpand
          ? null
          : m(
              '.actions',
              m(elButtonIcon, {
                // navigate_next
                icon: 'navigate_before',
                class: 'panel-nav',
                onclick: this.clickCollapse,
                title: 'Collapse Player View'
              })
            )
      ]
    );
  }
};

var mode = require('libs/mode');
