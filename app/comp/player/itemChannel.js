var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var ControlChannel = require('libs/controlChannel');
var localCategories = require('local/records/category');

// Constructors
var comItemBaseList = require('./itemBaseList');

// Prototype
module.exports = _.create(comItemBaseList, {
    _mapOrder: {
        date: 'Date',
        rating: 'Rating',
        relevance: 'Relevance',
        title: 'Title',
        viewCount: 'Views'
    },

    _mapDuration: {
        any: 'Date',
        long: 'Long',
        medium: 'Medium',
        short: 'Short'
    },

    _mapDate: {
        today: 'Today',
        yesterday: 'Yesterday',
        week: 'Week',
        month: 'Month',
        year: 'Year'
    },

    oninit: function ItemChannel(node) {
        // Add settings for base list
        node.attrs.settings = _.assign(
            {
                order: md.stream('date'), // date, rating, relevance, title, viewCount
                duration: md.stream('any'), // any, long, medium, short
                date: md.stream(), // RFC 3339 format date-time,
                category: md.stream()
            },
            node.attrs.settings
        );
        comItemBaseList.oninit.call(this, node);
    },

    getControl: function() {
        if (!this.control) {
            this.control = new ControlChannel(this.model, {
                shuffle: this.settings.shuffle,
                limit: this.settings.itemLimit,
                order: this.settings.order,
                date: this.settings.date,
                category: this.settings.category,
                duration: this.settings.duration
            });
        }
        return this.control;
    },

    getSettingsName: function(key, val) {
        switch (key) {
            case 'order':
                return val !== 'date' ? 'Sort: ' + this._mapOrder[val] : null;
            case 'duration':
                return val !== 'any' ? 'Duration: ' + this._mapDuration[val] : null;
            case 'date':
                return val ? 'Date: ' + this._mapDate[val] : null;
        }
        return comItemBaseList.getSettingsName.call(this, key, val);
    },

    getBulletIcon: function() {
        return 'account_circle';
    },

    getMenuSchema: function() {
        // return _.concat((comShare.isJoined() ? [] : menuSchema), comItemBaseList.getMenuSchema.call(this) || []);
        return _.concat(menuSchema, comItemBaseList.getMenuSchema.call(this) || []);
    }
});

var comShare = require('../contexts/contextSharing');

// Menu - -

var itemDateCallback = function(target, item) {
    if (comShare.isJoined()) return comShare.adapter.command('set_item_setting', this.getModel().id(), 'date', item.id);
    this.settings.date(item.id);
    this.getControl()
        .refresh()
        .then(m.redraw)
        .catch(Console.error);
    this.saveSettings();
};
var itemDateSubmenu = [
    {
        id: 'today',
        name: 'Today',
        callback: itemDateCallback
    },
    {
        id: 'yesterday',
        name: 'Yesterday',
        callback: itemDateCallback
    },
    {
        id: 'week',
        name: 'Week',
        callback: itemDateCallback
    },
    {
        id: 'month',
        name: 'Month',
        callback: itemDateCallback
    },
    {
        id: 'year',
        name: 'Year',
        callback: itemDateCallback
    },
    {
        name: 'All Time',
        callback: itemDateCallback
    }
];
itemDateSubmenu.parser = function(item) {
    item.icon = this.settings.date() == item.id ? 'check' : null;
};

// - - -

var itemSortCallback = function(target, item) {
    if (comShare.isJoined())
        return comShare.adapter.command('set_item_setting', this.getModel().id(), 'order', item.id);
    this.settings.order(item.id);
    this.getControl()
        .refresh()
        .then(m.redraw)
        .catch(Console.error);
    this.saveSettings();
};
var itemSortSubmenu = [
    {
        id: 'date',
        name: 'Date',
        callback: itemSortCallback
    },
    {
        id: 'rating',
        name: 'Rating',
        callback: itemSortCallback
    },
    {
        id: 'relevance',
        name: 'Relevance',
        callback: itemSortCallback
    },
    {
        id: 'title',
        name: 'Title',
        callback: itemSortCallback
    },
    {
        id: 'viewCount',
        name: 'Views',
        callback: itemSortCallback
    }
];
itemSortSubmenu.parser = function(item) {
    item.icon = this.settings.order() == item.id ? 'check' : null;
};

// - - -

var itemCategoryCallback = function(target, item) {
    if (comShare.isJoined())
        return comShare.adapter.command('set_item_setting', this.getModel().id(), 'category', item.id);
    this.settings.category(item.id);
    this.getControl()
        .refresh()
        .then(m.redraw)
        .catch(Console.error);
    this.saveSettings();
};
// var filteredCategories = [2, 10, 17, 19, 20, 22, 24, 25];
var filteredCategories = [];
var itemCategorySubmenu = _.map(
    _.filter(localCategories.get(), function(item) {
        if (filteredCategories.length) {
            return _.indexOf(filteredCategories, parseInt(item._id)) > -1;
        } else {
            return true;
        }
    }),
    function(item) {
        return {
            id: item._id,
            name: item.title,
            callback: itemCategoryCallback
        };
    }
);
itemCategorySubmenu.unshift({
    name: 'All',
    callback: itemCategoryCallback
});
itemCategorySubmenu.parser = function(item) {
    item.icon = this.settings.category() == item.id ? 'check' : null;
};

var itemDurationCallback = function(target, item) {
    if (comShare.isJoined())
        return comShare.adapter.command('set_item_setting', this.getModel().id(), 'duration', item.id);
    this.settings.duration(item.id);
    this.getControl()
        .refresh()
        .then(m.redraw)
        .catch(Console.error);
    this.saveSettings();
};
var itemDurationSubmenu = [
    {
        id: 'any',
        name: 'Any',
        callback: itemDurationCallback
    },
    {
        id: 'short',
        name: 'Short (< 4 mins)',
        callback: itemDurationCallback
    },
    {
        id: 'medium',
        name: 'Medium (4 to 20 mins)',
        callback: itemDurationCallback
    },
    {
        id: 'long',
        name: 'Long (> 20 mins)',
        callback: itemDurationCallback
    }
];
itemDurationSubmenu.parser = function(item) {
    item.icon = this.settings.duration() == item.id ? 'check' : null;
};

var menuSchema = [
    {
        name: 'Date',
        icon: 'date_range',
        submenu: itemDateSubmenu
    },
    {
        name: 'Sort By',
        icon: 'format_line_spacing',
        submenu: itemSortSubmenu
    },
    {
        name: 'Duration', // any, long, medium, short
        icon: 'av_timer',
        separate: 'after',
        submenu: itemDurationSubmenu
    }
];
