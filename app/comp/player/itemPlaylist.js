var _ = require('lodash');
var m = require('mithril');
var comItemBaseList = require('./itemBaseList');
var ControlPlaylist = require('libs/controlPlaylist');

// Prototype
module.exports = _.create(comItemBaseList, {

    oninit: function(node) {
        comItemBaseList.oninit.call(this, node);
    },

    getControl: function() {
        if (!this.control) {
            this.control = new ControlPlaylist(this.model, {
                shuffle: this.settings.shuffle,
                limit: this.settings.itemLimit
            });
        }
        return this.control;
    },

    getTime: function() {
        var playIndex;
        if (this.control && this.control.currentPlaylistItem()) {
            playIndex = this.control.currentPlaylistItem().position() + 1;
        }
        return (playIndex ? playIndex + ' / ' : '') + (this.model.videos() || '');
    },

    getBulletIcon: function() {
        return 'playlist_play';
    },

    getMenuSchema: function() {
        return _.concat(menuSchema, comItemBaseList.getMenuSchema.call(this) || []);
    }

});

// Menu
var menuSchema = [{
    name: 'View Playlist',
    icon: 'playlist_play',
    separate: 'after',
    callback: function() {
        m.route.set('/player.playlist?id=' + this.model.id());
    }
}];
