// Imports
var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var elCheckbox = require('comp/elements/checkbox');
var comMenu = require('../contexts/contextMenu');
var comList = require('./list');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');

// Statics
var bindMethods = ['clickItemLink', 'dragCallback'];
var dragSourceItem;
var dragHoverItem;

// Prototype
module.exports = {
  // Override this
  play: function() {
    // Play this item.
    this.state.playing(true);
    m.redraw.nexttick();
  },

  // Override this
  playNext: function() {
    // Play the next video of this item.
  },

  // Override this
  playEnded: function(stop) {
    // Notify this item that play has ended.
    if (stop) {
      this.state.playing(false);
      m.redraw.nexttick();
    }
  },

  // Override this
  hasNext: function() {
    return false;
  },

  // Override this
  getBulletIcon: function() {
    return 'info_outline';
  },

  // Override this
  getTime: function() {
    return '00:00';
  },

  // Override this
  getMenuSchema: function() {
    return null;
  },

  getModel: function() {
    return this.prevModel || this.model;
  },

  _playNext: function() {
    var redraw;
    if (!this.hasNext()) {
      redraw = comList.playNext();
    } else {
      redraw = true;
    }
    if (redraw) m.redraw.nexttick();
  },

  saveSettings: function() {
    var playlistItemModel = this.getModel();
    if (playlistItemModel instanceof App.Model.LocalPlaylistItem) {
      playlistItemModel.flags(this.settings.toJson());
      playlistItemModel.save();
    }
    comList.store('list');
    if (comShare.isHost()) comList.broadcast();
  },

  // - Events - - - - - - - - - - - - - - - - - - - - -

  oninit: function(node) {
    // Binds
    _.bindAll(this, bindMethods);
    this.clickContentMenu = _.partialRight(this.clickContentMenu, this);
    // Properties
    this.options = node.attrs || {};
    this.checked = this.options.state.selected;
    var _model = this.options.model;
    if (_model.options.name == 'LocalPlaylistItem') {
      this.model = _model.resource();
      this.prevModel = _model;
    } else {
      this.model = _model;
      this.prevModel = null;
    }
    this.state = md.State.create({
      loading: false,
      playing: false,
      status: -1,
      play: this.options.state.play,
      index: this.options.state.index,
      ended: this.options.state.ended,
      subitem: this.options.state.subitem,
      subitemPage: this.options.state.subitemPage,
      drag: false
    });
    // Settings
    this.settings =
      this.options.settings && this.options.settings.toJson
        ? this.options.settings
        : md.State.create(this.options.settings);
    // Expose settings to item state
    this.options.state.settings(this.settings);
  },

  onupdate: function() {
    if (comShare.isJoined()) return;
    var play = this.state.play();
    if (play) {
      if (this.state.ended() && this.state.status() !== 0) {
        Console.log('%cITEM', 'color: #f18900;', 'ended |', this.model.id());
        this.playEnded();
        process.nextTick(this._playNext.bind(this));
        this.state.status(0);
      } else if (this.state.status() !== 1) {
        Console.log('%cITEM', 'color: #f18900;', 'play |', this.model.id());
        process.nextTick(this.play);
        this.state.status(1);
      }
    } else if (this.state.playing() && this.state.status() !== -1) {
      Console.log('%cITEM', 'color: #f18900;', 'stop |', this.model.id());
      this.playEnded(true);
      this.state.status(-1);
    }
  },

  clickContentMenu: function(e, self) {
    comMenu.show(this, _.concat(self.getMenuSchema() || [], menuSchemaBase), self);
  },

  clickItemLink: function() {
    if (!this.state.play()) {
      comList.playByModel(this.getModel());
    }
  },

  dragCallback: function(e) {
    if (e.type === 'dragstart') {
      e.dataTransfer.setData('text/html', 'player-item-drag');
      dragSourceItem = this;
      e.redraw = false;
    } else if (e.type === 'dragend') {
      if (dragHoverItem) {
        // Move the source to hover item
        comList.moveItem(dragSourceItem.getModel(), dragHoverItem.getModel());
        dragHoverItem.state.drag(false);
        dragHoverItem = null;
      }
      dragSourceItem = null;
      this.state.drag(false);
    } else if (e.type === 'dragenter' && dragHoverItem !== this) {
      if (dragHoverItem) dragHoverItem.state.drag(false);
      dragHoverItem = this;
      this.state.drag(true);
    } else if (e.type === 'drag') {
      comList.dragScroll(e);
      e.redraw = false;
    }
  },

  // - Views - - - - - - - - - - - - - - - - - - - - -

  viewSubItems: function() {
    return null;
  },

  view: function() {
    var fnDrag;
    var draggable;
    // Check checkbox status
    var cbState = this.options.checkboxState().current;
    // Drag direction
    var dragDown = dragSourceItem && dragHoverItem && dragSourceItem.state.index() < dragHoverItem.state.index();
    if (cbState === 0 || cbState === 2) this.checked(cbState === 2);
    if (cbState) {
      draggable = true;
      fnDrag = this.dragCallback;
    }
    // View template
    return m(
      'li',
      {
        class:
          'item' +
          (this.state.play() ? ' playing' : '') +
          (this.state.drag() ? (dragDown ? ' drag-spot-down' : ' drag-spot-up') : ''),
        draggable: draggable,
        ondragstart: fnDrag,
        ondragenter: fnDrag,
        ondragend: fnDrag,
        ondrag: fnDrag
      },
      m(
        '.edit',
        m(elCheckbox, {
          value: this.checked
        })
        // m(elIcon, {
        //  icon: 'drag_handle'
        // })
      ),
      m(
        'span.right',
        m('span.time', this.getTime()),
        m(elButtonIcon, {
          class: 'menu',
          icon: 'more_vert',
          onclick: this.clickContentMenu
        })
      ),
      m(
        'a.item-link',
        {
          onclick: this.clickItemLink
        },
        m(
          'span',
          {
            class: 'bullet'
          },
          m(elIcon, {
            icon: this.getBulletIcon(),
            loading: this.state.loading
          })
        ),
        m(
          'span.title',
          m('span.name', this.model.title()),
          m('span.owner', this.model.channel ? this.model.channel().title() : null)
        )
      ),
      this.viewSubItems()
    );
  }
};

var submenuMisc = [
  {
    name: 'Remove Other Items',
    icon: 'remove',
    callback: function() {
      // Combines all models (above and below) for one shot remove
      var idx = comList.list.findIndex(['_id', this.getModel().id()]);
      comList.remove(_.concat(comList.list.slice(0, idx), comList.list.slice(idx + 1)));
    }
  },
  {
    name: 'Remove Items Above',
    icon: 'remove',
    callback: function() {
      var idx = comList.list.findIndex(['_id', this.getModel().id()]);
      comList.remove(comList.list.slice(0, idx));
    }
  },
  {
    name: 'Remove Items Below',
    icon: 'remove',
    callback: function() {
      var idx = comList.list.findIndex(['_id', this.getModel().id()]);
      comList.remove(comList.list.slice(idx + 1));
    }
  }
];

var menuSchemaBase = [
  {
    name: 'Add to Playlist',
    icon: 'playlist_add',
    callback: function() {
      var self = this;
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems([
            {
              resource_type: self.model.getKind(),
              resource_id: self.model.id()
            }
          ]);
        }
      });
    }
  },
  {
    name: 'View Channel',
    icon: 'account_circle',
    callback: function() {
      if (this.model instanceof App.Model.LocalPlaylist) {
        m.route.set('/player.profile?id=' + this.model.channel().id());
      } else if (this.model instanceof App.Model.Channel) {
        m.route.set('/player.channel?id=' + this.model.id());
      } else if (this.model.channel) {
        m.route.set('/player.channel?id=' + this.model.channel().id());
      } else {
        App.Modules.toastr.error('Unable to view channel');
      }
    }
  },
  {
    name: 'Misc...',
    submenu: submenuMisc
  },
  {
    name: 'Remove',
    icon: 'remove',
    separate: 'before',
    callback: function() {
      comList.remove(this.getModel());
    }
  }
];

var comShare = require('../contexts/contextSharing');
