/**
 * NOTE: This components is also used as a placholder for items that are falsy like
 * local playlist item with missing resource, deleted item, or private item. As `item`
 * can be any such us Channel, Playlist, List or Local Playlist
 */

var _ = require('lodash');
var m = require('mithril');
var moment = require('moment');
var comPlayerCurrent = require('./current');
var comModalVideoPlayer = require('comp/modals/videoPlayer');
var comList = require('./list');
var ContextStar = require('comp/contexts/contextStar');
var starring = require('libs/starring');

// Constructors
var comItemBase = require('./itemBase');

// Variables
var bindMethods = ['play'];

// Prototype
module.exports = _.create(comItemBase, {
  oninit: function(node) {
    comItemBase.oninit.call(this, node);
    _.bindAll(this, bindMethods);
  },
  play: function() {
    if (this.model.id()) {
      comPlayerCurrent.loadVideo(this.model.id());
      comList.addHistory(this.getModel().id() + '//');
    } else {
      // No ID. Manual set invalid ID to be skipped by video player
      comPlayerCurrent.loadVideo(this.model.id() + _.uniqueId());
    }
    comItemBase.play.call(this);
  },
  getBulletIcon: function() {
    return 'slideshow';
  },
  getTime: function() {
    var duration = this.model.duration && this.model.duration();
    if (duration) {
      var objDuration = moment.duration(this.model.duration());
      // For unknown reason, youtube is ahead 1 second, need to subtruct it.
      return moment.utc(objDuration.asMilliseconds() - 1000).format(objDuration.hours() > 0 ? 'H:mm:ss' : 'm:ss');
    } else {
      return '';
    }
  },
  getMenuSchema: function() {
    return menuSchema;
  },
  viewSubItems: function() {
    return null;
  }
});

// Menu
var menuSchema = [
  {
    name: 'Star / Bookmark',
    icon: 'star',
    hide: App.User.isGuest(),
    callback: function(el) {
      var id = this.model.id();
      starring
        .star('video', id)
        .then(function(data) {
          ContextStar.show(el, null, 'video', id, true, data.folder);
        })
        .catch(Console.error)
        .finally(m.redraw);
    }
  },
  {
    name: 'View Video',
    icon: 'slideshow',
    callback: function() {
      m.route.set('/player.video?id=' + this.model.id());
    }
  },
  {
    name: 'View in Modal',
    icon: 'flip_to_front',
    separate: 'after',
    callback: function() {
      comModalVideoPlayer.videoModel(this.model);
    }
  }
];
