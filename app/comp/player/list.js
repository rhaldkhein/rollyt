var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var j = require('jquery');
var ps = require('perfect-scrollbar');
var elButtonIcon = require('comp/elements/buttonIcon');
var elButtonState = require('comp/elements/buttonState');
var moment = require('moment');
var async = require('async');
var youtube = require('libs/youtube');
var util = require('libs/util');
var mousetrap = require('mousetrap');
var ModalCreatePlaylist = require('comp/modals/modalCreatePlaylist');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');
var ControlLocalPlaylist = require('libs/controlLocalPlaylist');
var comModalVideoPlayer = require('comp/modals/videoPlayer');
var toast = require('toastr');
var ContextStar = require('comp/contexts/contextStar');
var starring = require('libs/starring');

var comMenu = require('../contexts/contextMenu');

// Variables. Make sure all methods exists or it will throw error.
var bindMethods = [
  'play',
  'playNext',
  '_loadPlaylist',
  '_loadPlayerItems',
  '_getClass',
  'clickSkipNext',
  'clickSkipPrev',
  'clickRepeat',
  'clickRandom',
  'clickStop',
  'playEnded',
  'oncreateItemsContainer',
  'onupdateItemsContainer',
  'onremoveItemsContainer',
  'clickNextPage',
  'clickPrevPage',
  'longpressSkipNext',
  'updateListRect'
];

var blacklistedChannels = ['UCwTRjvjVge51X-ILJ4i22ew'];

// Listen to resize and update elListRect.
window.addEventListener('resize', function() {
  _list.updateListRect();
});

// Controller
function List() {
  _.bindAll(this, bindMethods);
  this.elList;
  this.elListRect;
  // Signature state for creating collection
  this._stateSignature = {
    play: false,
    selected: false,
    index: -1,
    ended: false,
    subitem: null,
    subitemPage: null,
    settings: null // Item will generate this or provided through add method
  };
  // Create initial collection
  this._baseList = new md.Collection({
    state: this._stateSignature
  });
  this.list = this._baseList;
  // Player states
  this.state = md.State.create({
    // 0: hidden & all unchecked, 1: shown, 2: shown & all checked
    checkbox: ['hidden', 'shown', 'checked'],
    ready: false, // When player is ready to be used (after boot video autoplay)
    loaded: false, // When all items are loaded
    random: ['off', 'on'],
    repeat: ['off', 'on'],
    title: undefined,
    subtitle: undefined,
    isProjectPlaylist: false,
    projectPlaylistId: null,
    maxItems: 4,
    currentPage: undefined,
    currentModel: undefined,
    currentSubitem: undefined,
    currentSubitemPage: undefined,
    randomIndexes: undefined,
    randomCurrent: undefined,
    autoSave: false,
    removeAfter: App.Settings.playerRemoveAfter
  });
  this._historyLimit = 10;
  this._history = [];
  this._playlist = null;
  this._playlistControl = null;
  this._related = null;
  this._related_by = null; // String video id that triggers related playlist
  this._title = null;
  this.messenger = App.Modules.messenger;
}

List.prototype = {
  oncreateItemsContainer: function(node) {
    ps.initialize(node.dom);
    this.elList = node.dom; // 'div.items' element. not 'div.list'
    this.updateListRect();
    this.list.clear();
    this._playlist = null;
    this._playlistControl = null;
    m.nextTick(this._loadPlayerItems);
  },

  onupdateItemsContainer: function(node) {
    ps.update(node.dom);
  },

  onremoveItemsContainer: function() {
    this.elList = null;
  },

  updateListRect: function() {
    // Update list for .dragScroll method
    this.elListRect = j(this.elList).offsetRect();
  },

  startAutoPlay: function() {
    var startupPlaylistId = App.Preference.state.startup_playlist();
    var currPlaylist = App.Settings.playerCurrPlaylist();
    var currItems = App.Settings.playerCurrItems();
    var currPlaying = App.Settings.playerCurrPlaying();
    var self = this;
    if (startupPlaylistId) {
      this._title = 'Autoplaying...';
      this.store('controls');
      return Promise.delay(Config.autoplayDelay)
        .then(function(argument) {
          return self.changePlaylist(
            App.Model.LocalPlaylist.create({
              _id: startupPlaylistId
            })
          );
        })
        .finally(function() {
          self._title = null;
        });
    } else if (currPlaylist) {
      this._title = 'Restoring...';
      this.store('controls');
      return Promise.delay(Config.autoplayDelay)
        .then(function(argument) {
          return self.changePlaylistById(currPlaylist, -1);
        })
        .then(function() {
          if (currPlaying) {
            if (util.isLastVisitIn(8)) self.playByString(currPlaying);
            else App.Settings.playerCurrPlaying(null);
          }
        })
        .catch(function() {
          toast.error('Could not restore previous playlist', 'Error Occurred');
          self.clear();
        })
        .finally(function() {
          self._title = null;
        });
    } else if (currItems && currItems.length) {
      this._title = 'Restoring...';
      var currControls = App.Settings.playerCurrControls();
      var currRelated = App.Settings.playerCurrRelated();
      self.state.loaded(false);
      this.store('controls');
      return Promise.delay(Config.autoplayDelay).then(function() {
        var mdl, _state;
        _.each(currItems, function(item) {
          mdl = App.Model.typeMap[item.tp].create({
            _id: item.id
          });
          self.list.add(mdl);
          _state = self.list.stateOf(mdl);
          _state.settings(item.st);
        });
        if (App.Props.breakPointMedium()) App.Settings.playerExpand(true);
        return self
          ._loadItems()
          .then(function() {
            self.state.repeat().current = currControls[0];
            self.state.random().current = currControls[1];
            if (currControls[1]) {
              self._clearRandom();
              self._createRandom();
            }
            if (currPlaying) {
              if (util.isLastVisitIn(8)) self.playByString(currPlaying);
              else App.Settings.playerCurrPlaying(null);
            }
            if (currRelated) self._related = self.list.find(['_id', currRelated]);
          })
          .catch(function() {
            toast.error('Could not restore previous items', 'Error Occurred');
            self.clear();
          })
          .finally(function() {
            self.state.loaded(true);
            self._title = null;
            m.redraw();
          });
      });
    }
    return Promise.resolve();
  },

  toArrayList: function() {
    var self = this;
    return this.list.transform(function(result, model) {
      var state = self.list.stateOf(model);
      var settings = state.settings();
      result.push({
        tp: model.getKind(),
        id: model.id(),
        pl: state.play(),
        st: settings && settings.toJson ? settings.toJson() : settings
      });
    }, []);
  },

  toArrayControls: function() {
    return [
      // 0 - Int : Repeat
      this.state.repeat().current,
      // 1 - Int : Shuffle
      this.state.random().current,
      // 2 - String : Playlist Title (left side)
      this._getTitle(),
      // 3 - String : Playlist Subtitle (right side)
      this._getSubTitle(),
      // 4 - Boolean : Is local playlist
      !!this._playlist,
      // 5 - String : Current video id
      comPlayerCurrent.getVideoId(),
      // 6 - String : Local playlist list
      this._playlist ? this._playlist.id() : null,
      // 7 - Int : Current player state
      comPlayerCurrent.getPlayerState(),
      // 8 - Int : Player volume
      comPlayerCurrent.getPlayerVolume(),
      // 9 - String: Mode
      App.Settings.mode()
    ];
  },

  // Broadcast to collected clients (includes extension)
  broadcast: function(id, tag, onlyControls) {
    // If id is present, only broadcast to that id.
    // Otherwise to all
    if (comShare.isHost() && !(this._playlist && this._playlist.is_private())) {
      Console.log('%cBROADCAST', 'color: #05a800;', id, tag);
      // Make sure we do NOT broadcast private playlist
      var data = [this.toArrayControls(), onlyControls ? null : this.toArrayList()];
      if (id) comShare.adapter.socket.emit('project_one', id, data, tag);
      else comShare.adapter.socket.emit('project', data, tag);
    }
    // Broadcast to extensions
    if (this.messenger.isReady()) {
      var currModel = comPlayerCurrent.getModel();
      if (currModel) {
        this.messenger.send('app#current', {
          playlist: this._getTitle(),
          image: currModel.image(),
          title: currModel.title(),
          channel: currModel.channel().title()
        });
      } else {
        this.messenger.send('app#current', null);
      }
    }
  },

  // Store player to local storage
  store: function(type) {
    // Do not store when using player sharing
    if (comShare.isJoined()) return;
    switch (type) {
      case 'controls':
        App.Settings.playerCurrControls(this.toArrayControls());
        break;
      case 'list':
        App.Settings.playerCurrItems(this.toArrayList());
        break;
      case 'playing':
        App.Settings.playerCurrPlaying(this.state.currentModel() ? this.getHistory() : null);
        break;
      case 'related':
        App.Settings.playerCurrRelated(this._related ? this._related.id() : null);
        break;
      case 'playlist':
        App.Settings.playerCurrPlaylist(this._playlist ? this._playlist.id() : null);
        break;
      default:
        App.Settings.playerCurrControls(this.toArrayControls());
        App.Settings.playerCurrItems(this.toArrayList());
        App.Settings.playerCurrPlaying(this.state.currentModel() ? this.getHistory() : null);
        App.Settings.playerCurrRelated(this._related ? this._related.id() : null);
        App.Settings.playerCurrPlaylist(this._playlist ? this._playlist.id() : null);
        break;
    }
  },

  storeClear: function() {
    App.Settings.playerCurrItems([]);
    App.Settings.playerCurrPlaying(null);
    App.Settings.playerCurrRelated(null);
    App.Settings.playerCurrPlaylist(null);
  },

  project: function(player) {
    // We must be in room
    if (!comShare.isJoined()) return;
    Console.log('%cPROJECT', 'color: #05a800;', player);
    // Player must be clean
    if (this._playlist) throw new Error('Player must be cleared');
    var self = this,
      mdl,
      _state,
      currvid = player[0][5];
    // Other things in player
    this.state.repeat().current = player[0][0];
    this.state.random().current = player[0][1];
    this.state.title(player[0][2]);
    this.state.subtitle(player[0][3]);
    this.state.isProjectPlaylist(player[0][4]);
    this.state.projectPlaylistId(player[0][6]); // 5 is at top
    this.state.checkbox().current = 0;
    if (currvid) comPlayerCurrent.videoId(currvid);
    // Prepare for playlist project
    if (this.state.isProjectPlaylist()) {
      // Playlist items
      if (!(this.list instanceof md.ItemCollection)) {
        this.list = new md.ItemCollection({
          model: App.Model.LocalPlaylistItem,
          url: '/localplaylistitem/list/ids',
          state: _.defaults(this._stateSignature, {
            playing: false
          })
        });
      }
    } else if (this.list !== this._baseList) {
      this.list = this._baseList;
    }
    comPlayerCurrent.project(player[0]);
    // Directly clear list collection
    if (player[1]) {
      this.list.clear();
      if (player[1].length) {
        // Items
        _.each(player[1], function(item) {
          mdl = App.Model.typeMap[item.tp].create({
            _id: item.id
          });
          self.list.add(mdl);
          _state = self.list.stateOf(mdl);
          _state.play(item.pl);
          _state.settings(item.st);
        });
      } else {
        // Clear player
        comPlayerCurrent.videoId(null);
      }
      // Load and populate each items
      return this._loadItems().catch(Console.error);
    } else {
      return Promise.resolve();
    }
  },

  showNotReadyPlayer: function() {
    toast.error('Player is not ready yet', 'Please Wait');
  },

  changePlaylist: function(playlist, autoPlayIndex) {
    if (!this.state.ready() || !this.state.loaded()) {
      this.showNotReadyPlayer();
      return Promise.reject();
    }
    if (!(playlist instanceof App.Model.LocalPlaylist)) return Promise.reject(new Error('not playlist'));
    // Filter for private local#playlist on shared player
    if (comShare.isConnected() && playlist.is_private()) {
      toast.error('Private playlist is not allowed to play on shared player', 'Not Allowed');
      return Promise.reject(new Error('not allowed private playlist'));
    }
    if (comShare.isJoined()) {
      comShare.adapter.command('change_playlist', playlist.id());
      return Promise.resolve();
    }
    this.clear();
    if (App.Props.breakPointMedium()) App.Settings.playerExpand(true);
    this._playlist = playlist;
    this.store('playlist');
    var self = this;
    var prom = !this._playlist.isSaved() ? this._playlist.fetch() : Promise.resolve();
    this.state.loaded(false);
    return prom
      .then(function() {
        return self._loadPlaylist(autoPlayIndex).then(function() {
          if (comShare.isHost()) self.broadcast();
          util.nextTick(self.updateListRect);
        });
      })
      .catch(function(err) {
        self.state.loaded(true);
        toast.error('Could not play the playlist', 'Error Occurred');
        m.redraw.nexttick();
        throw err;
      });
  },

  changePlaylistById: function(id, autoPlayIndex) {
    return this.changePlaylist(
      App.Model.LocalPlaylist.create({
        _id: id
      }),
      autoPlayIndex
    );
  },

  addHistory: function(key) {
    key = (this._playlist ? this._playlistControl.getCurrentPage() : '') + '/' + key;
    if (this._history[this._history.length - 1] === key) return;
    if (this._history.length > this._historyLimit) this._history.shift();
    this._history.push(key);
    this.store('playing');
  },

  playHistory: function(depth) {
    // Less 2 because it includes the current one
    if (this._history.length < 2) return;
    this._history.pop(); // Current (not this one)
    if (!depth) depth = 1;
    var pulled = _.takeRight(this._history, depth);
    _.pullAll(this._history, pulled);
    if (pulled[0]) {
      this._renewRandom();
      this.playByString(pulled[0]);
    }
  },

  getHistory: function(index) {
    if (!index) index = this._history.length - 1;
    return this._history[index];
  },

  setAllPlaylimit: function(limit) {},

  add: function(model, flags) {
    if (!this.state.ready() || !this.state.loaded()) {
      this.showNotReadyPlayer();
      return Promise.resolve();
    }
    if (!this._playlist && this.list.size() >= App.Vars.playlist_limit) {
      toast.error('Click here for more info', 'Player Limit Reached', {
        onclick: function() {
          m.route.set('/player.frame/about/help/playerlimit');
        }
      });
      return Promise.resolve();
    }
    // Filter for private local#playlist on shared player
    if (comShare.isConnected() && model instanceof App.Model.LocalPlaylist && model.is_private()) {
      toast.error('Private playlist is not allowed to play on shared player', 'Not Allowed');
      return Promise.resolve();
    }
    // Filter for adding item only to owned player
    if (comShare.isJoined() && this.state.isProjectPlaylist()) {
      toast.error('You can only add item to a playlist with your own player', 'Not Allowed');
      return Promise.resolve();
    }
    // Filter for adding item only to owned playlist
    if (this._playlist && this._playlist.user().id() != App.User.id()) {
      toast.error('You dont own the currently playing playlist', 'Not Allowed');
      return Promise.resolve();
    }
    // If repeat is enable, add item with playlimit to 1 for non-video item
    if (this.list.size() > 0 && this.state.repeat().current && !(model instanceof App.Model.Video)) {
      flags = flags || {};
      flags.playLimit = 1;
      if (this.list.size() === 1) {
        var first = this.list.first();
        var state = this.list.stateOf(first);
        state.settings().playLimit(1);
        if (this._playlist) {
          first.flags(state.settings().toJson());
          first.save();
        }
      }
    }
    // If local playlist, upload and add to that playlist
    var self = this;
    if (this._playlist && !(model instanceof App.Model.LocalPlaylistItem)) {
      // Model needs to be fetched first before we add to playlist.
      // This is to avoid adding unknown item to playlist.
      return (model.isSaved() ? Promise.resolve() : model.fetch())
        .then(function() {
          // Still not saved, server maybe responsded with empty data
          if (!model.isSaved()) throw new Error('not_saved');
          return self._playlist
            .addItems([
              {
                resource_type: model.getKind(),
                resource_id: model.id(),
                flags: flags
              }
            ])
            .then(function(data) {
              // Resync playlist, if less than limit
              if (self.list.size() < App.Vars.playlist_limit && data && data.items_new[0]) {
                var plItem = App.Model.LocalPlaylistItem.create(data.items_new[0]);
                return plItem
                  .fetch()
                  .then(function() {
                    return plItem.populateResource();
                  })
                  .then(function() {
                    return self.add(plItem);
                  })
                  .then(function() {
                    m.redraw.nexttick();
                    return plItem;
                  });
              } else {
                return model;
              }
            });
        })
        .catch(function(err) {
          toast.error('Unable to add that to player');
          self.broadcast();
          throw err;
        });
    } else if (!this.list.contains(model.id())) {
      // For Unsaved Playlist
      return (model.isSaved() ? Promise.resolve() : model.fetch())
        .then(function() {
          // if (comShare.isHost()) throw new Error('Test error');
          // Still not saved, server maybe responsded with empty data
          if (!model.isSaved()) throw new Error('not_saved');
          // Check for blacklisted items
          if (model instanceof App.Model.Video && _.includes(blacklistedChannels, model.channel().id()))
            throw new Error('blacklisted');
          // All check. Ok to add
          self.list.add(model);
          var _state = self.list.stateOf(model);
          if (flags) _state.settings(flags);
          // Flags need to be set first before storing
          self.store('list');
          self._renewRandom();
          if (comShare.isJoined()) comShare.adapter.add(model, flags);
          else self.broadcast(null, 'added');
          if (App.Props.breakPointMedium() && !App.Settings.playerExpand() && !this._playlist)
            toast.success('Added to player');
          m.redraw();
          return model;
        })
        .catch(function(err) {
          if (err.message === 'blacklisted') toast.error('Video is blacklisted');
          else toast.error('Unable to add that to player');
          self.broadcast();
          throw err;
        });
    } else {
      toast.error('Item already exist in the player');
      return Promise.resolve();
    }
  },

  addById: function(id, kind, flags) {
    try {
      var model = App.Model.typeMap[kind].create({
        _id: id
      });
    } catch (ex) {
      return Promise.reject(new Error('error_while adding'));
    }
    return this.add(model, flags).return(model);
  },

  addAndPlay: function(model, flags, dontClear) {
    if (!this.state.ready()) {
      this.showNotReadyPlayer();
      return Promise.resolve();
    }
    if (comShare.isJoined() && comShare.adapter.mode === 'karaoke') {
      return Promise.resolve();
    }
    var self = this;
    if (!dontClear && App.Preference.state.clear_on_play() && !comShare.isJoined()) {
      // Will clear list
      if (this.list.contains(model.id())) {
        this.clear(false);
        return Promise.delay(300).then(function() {
          return self.addAndPlay(model, flags, true);
        });
      } else {
        this.clear(false);
        return self.addAndPlay(model, flags, true);
      }
    }
    if (App.Props.breakPointMedium()) App.Settings.playerExpand(true);
    return this.add(model, flags)
      .then(function(item) {
        var mdl = item || model;
        self.playByModel(mdl);
        m.redraw.nexttick();
        return mdl;
      })
      .catch(Console.error);
  },

  addByIdAndPlay: function(id, kind, flags, dontClear) {
    var self = this;
    if (!dontClear && App.Preference.state.clear_on_play() && !comShare.isJoined()) {
      this.clear(false);
      return Promise.delay(300).then(function() {
        return self.addByIdAndPlay(id, kind, flags, true);
      });
    }
    if (App.Props.breakPointMedium()) App.Settings.playerExpand(true);
    return this.addById(id, kind, flags)
      .then(function(model) {
        self.playByModel(model);
        m.redraw.nexttick();
        return model;
      })
      .catch(Console.error);
  },

  remove: function(item) {
    if (!item) return;
    // `item` can be model or array of models or plain objects
    if (_.isArray(item) && item.length && _.isPlainObject(item[0])) {
      // Convert to model
      var self = this;
      item = _.map(item, function(obj) {
        // WARNING: Should also check for item kind
        return self.list.find(['_id', obj.id]);
      });
    }
    this.list.remove(item);
    this.store('list');
    // Share adapter trims `item` before sending
    if (item && comShare.isJoined()) comShare.adapter.remove(item);
    else this.broadcast();
  },

  removeById: function(id, kind) {
    var mdl = this.list.find(['_id', id]);
    if (mdl && mdl.getKind() === kind) this.remove(mdl);
    // WARNING: Possible same id but different kind
  },

  moveItem: function(srcModel, desModel) {
    var iSrc = this.list.findIndex(['_id', srcModel.id()]);
    var iDes = this.list.findIndex(['_id', desModel.id()]);
    var models = this.list.models;
    while (iSrc < 0) {
      iSrc += models.length;
    }
    while (iDes < 0) {
      iDes += models.length;
    }
    if (iDes >= models.length) {
      var k = iDes - models.length;
      while (k-- + 1) {
        models.push(undefined);
      }
    }
    models.splice(iDes, 0, models.splice(iSrc, 1)[0]);
    if (this._playlist) this._playlist.moveItem(srcModel, desModel);
  },

  getCurrentIndex: function() {
    var model = this.state.currentModel();
    return model ? this.list.findIndex(['_id', model.id()]) : -1;
  },

  getCurrentModel: function() {
    return this.state.currentModel() || null;
  },

  getCurrentModelId: function() {
    var model = this.state.currentModel();
    return model ? model.id() : null;
  },

  /**
   * This method is called whenever an item is ended
   * including the default video startup
   */
  playEnded: function(addToSkiplist) {
    // Always skip player share client
    if (comShare.isJoined()) return;
    // Run autoplay playlist and items
    if (!this.state.ready()) {
      this.state.ready(true);
      this.startAutoPlay().finally(function() {
        App.Settings.visitTimeStamp(Date.now());
      });
    }
    var idx = this.getCurrentIndex();
    if (idx > -1) {
      // Currently in same page
      var curItem = this.list.nth(idx);
      if (curItem) {
        this.list.stateOf(curItem).ended(true);
      }
      if (addToSkiplist) {
        App.Modules.skiplist.add(comPlayerCurrent.model.id());
      }
    } else {
      // Currently in other page
      this.playNext();
    }
    // Notify current that a video has ended
    comPlayerCurrent.ended();
  },

  // Get next video to play
  playNext: function() {
    var newIndex,
      size = this.list.size(),
      curIndex = this.getCurrentIndex();
    if (!size) return;
    if (this.state.random().current) {
      var curRandIndex = this.state.randomCurrent(),
        newRandIndex = curRandIndex + 1;
      if (newRandIndex >= size) {
        // End of random list
        if (this._playlist && this._playlistControl.hasNextPage()) {
          this._loadPlaylistPage(null, true).catch(Console.error);
          return;
        } else if (this.state.repeat().current && this._playlist && this._playlistControl.isPaginated()) {
          this._loadPlaylistPage(1, true).catch(Console.error);
          return;
        } else {
          newRandIndex = this.state.repeat().current ? 0 : curRandIndex;
        }
      }
      newIndex = this.state.randomIndexes()[newRandIndex];
      this.state.randomCurrent(newRandIndex);
    } else {
      newIndex = curIndex + 1;
      if (newIndex >= size) {
        // End of list
        if (this._playlist && this._playlistControl.hasNextPage()) {
          this._loadPlaylistPage(null, true).catch(Console.error);
          return;
        } else if (this.state.repeat().current && this._playlist && this._playlistControl.isPaginated()) {
          this._loadPlaylistPage(1, true).catch(Console.error);
          return;
        } else {
          newIndex = this.state.repeat().current ? 0 : curIndex;
        }
      }
    }
    if (curIndex === newIndex) {
      // End of overall playlist
      if (this.state.repeat().current && this._playlist && this._playlistControl.isPaginated()) {
        // Load page 1
        this._loadPlaylistPage(1, true).catch(Console.error);
        return;
      } else {
        this.stop();
        var self = this;
        if (this.list.size() === 1 && this.state.repeat().current) {
          if (App.Settings.mode() == 'normal') {
            if (this._playlist) {
              var pl = this._playlist;
              this.clear(true);
              setTimeout(function() {
                self.changePlaylist(pl);
              }, 300);
            } else {
              var model = this.list.first();
              var settings = this.list.stateOf(model).settings();
              this.clear(true);
              setTimeout(function() {
                self.addAndPlay(model, settings.toJson(), true);
                m.redraw.nexttick();
              }, 300);
            }
          } else if (this.state.removeAfter()) {
            // Karaoke mode
            this.clear(true);
            toast.warning('Auto removed');
          }
        } else {
          if (!this._playlist && comPlayerCurrent.lastModelId) {
            if (App.Preference.state.auto_play_related() && App.Settings.mode() == 'normal') {
              var _modelId = comPlayerCurrent.getVideoId() || comPlayerCurrent.lastModelId;
              youtube
                .playlistInfo('RD' + _modelId)
                .then(function(playlist) {
                  return App.Model.List.create({
                    // WARNING: `RD` is a prefix to get MIX videos. Youtube might change this in future.
                    _id: (playlist ? 'playlist.RD' : 'related.') + _modelId
                  });
                })
                .then(function(related) {
                  self._related_by = _modelId;
                  return self
                    .addAndPlay(
                      related,
                      {
                        shuffle: true
                      },
                      !self.state.removeAfter()
                    )
                    .then(function() {
                      // Success
                      self._related = related;
                      self.store('related');
                      m.redraw();
                    });
                })
                .catch(Console.error);
            } else {
              // Karaoke mode
              if (this.state.removeAfter()) {
                this.clear(true);
                toast.warning('Auto removed');
              }
            }
          }
        }
      }
    } else {
      this.playByIndex(newIndex);
    }
    return true;
  },

  // Start playing a video by current index
  play: function() {
    var item,
      state,
      index = this.getCurrentIndex(),
      size = this.list.size(),
      subitemId = this.state.currentSubitem(),
      ranged = 0 <= index && index < size,
      i = size - 1;
    for (; i >= 0; i--) {
      item = this.list.nth(i);
      state = this.list.stateOf(item);
      state.play(ranged && index === i);
      state.ended(false);
      // Playing item state. Add sub item to play.
      if (state.play()) {
        state.subitem(subitemId || undefined);
        if (this._isPageNum(item)) {
          state.subitemPage(parseInt(this.state.currentSubitemPage() || 1));
        } else {
          state.subitemPage(this.state.currentSubitemPage() || ''); // Empty string for first page
        }
        if (comShare.isJoined()) comShare.adapter.play(item);
      }
    }
    // Making sure its playing
    setTimeout(m.redraw, 1000);
  },

  playByModel: function(modelItem, subItemId, subItemPage) {
    if (!modelItem) {
      // TODO display error message, item not found in list, might be moved or deleted
      return;
    }
    if (subItemId instanceof md.BaseModel) subItemId = subItemId.id();
    if (!this._playlist && comShare.isHost() && this.state.removeAfter()) {
      // Remove after does NOT remove in server. It's local-only operation.
      var index = this.getCurrentIndex();
      if (index > -1 && index !== this.list.findIndex(['_id', modelItem.id()])) {
        this.remove(this.list.nth(this.getCurrentIndex()));
        if (!this.list.size()) return;
        this._renewRandom();
        toast.warning('Auto removed');
      }
    }
    if (this._related && this._related !== modelItem) {
      this.remove(this._related);
      this._related = null;
      this._related_by = null;
      this.store('related');
      if (!this.list.size()) return;
      this._renewRandom();
    }
    this.state.currentModel(modelItem);
    this.state.currentSubitemPage(subItemPage);
    this.state.currentSubitem(subItemId);
    this.play();
    return true;
  },

  playById: function(id, kind) {
    var mdl = this.list.find(['_id', id]);
    if (mdl && mdl.getKind() === kind) return this.playByModel(mdl);
    // WARNING: Possible same id but different kind
  },

  // Set the current index and start playing it
  playByIndex: function(indexItem) {
    return this.playByModel(this.list.nth(indexItem));
  },

  playByString: function(strKey) {
    // strKey = <Playlist Page>/<Item Id>/<Subitem Page>/<Subitem Id> (1/iid002/2/sid003)
    if (!strKey) return;
    var self = this;
    var parts = strKey.split('/');
    if (parts.length === 4 && this._playlist) {
      // Navigate to playlist page (Check page difference)
      var playlistPage = parseInt(parts[0]);
      if (this._playlistControl.getCurrentPage() !== playlistPage) {
        // Load and navigate
        this._navigatePlaylistPageTo(playlistPage)
          .then(function() {
            self.playByModel(self.list.find(['_id', parts[1]]), parts[3], parts[2]);
            m.redraw.nexttick();
          })
          .catch(Console.error);
        return true;
      }
    }
    m.redraw.nexttick();
    return this.playByModel(this.list.find(['_id', parts[1]]), parts[3], parts[2]);
  },

  stop: function(dontBroadcast) {
    if (comShare.isJoined()) {
      comShare.adapter.command('stop'); // stop the host player
      return;
    }
    var curModel = this.state.currentModel();
    if (curModel) {
      var state = this.list.stateOf(curModel);
      if (state) {
        state.play(false);
        state.ended(true);
      }
    }
    this.state.currentModel(null);
    comPlayerCurrent.stop(true);
    if (!dontBroadcast) this.broadcast();
    this.store('playing');
    App.Modules.title.set();
  },

  clear: function(persistState, dontStop) {
    // Clear first then stop. As stop broadcasts the list status.
    this.list.clear();
    if (!dontStop) this.stop(true); // stop will trigger broadcast
    this.list = this._baseList;
    if (this._playlist) {
      this._clearRandom();
      this.state.random().current = 0;
      this.state.repeat().current = 0;
    }
    this._playlist = null;
    this._playlistControl = null;
    this.state.title(null);
    this.state.subtitle(null);
    this.state.isProjectPlaylist(false);
    this.state.projectPlaylistId(null);
    this._history = [];
    this.storeClear();
    this.broadcast();
    if (comShare.isJoined()) comShare.adapter.clear();
  },

  clearFilter: function() {
    var idx = this.getCurrentIndex();
    if (!comShare.isJoined() && !this._playlist && idx > -1) {
      this.remove(_.concat(this.list.slice(0, idx), this.list.slice(idx + 1)));
      return;
    }
    this.clear(null, this._playlist ? null : true);
  },

  dragScroll: function(e) {
    var pad = this.elListRect.height * 0.2; // 20 %
    if (this.elListRect.top + pad > e.pageY) {
      // Scroll up
      this.elList.scrollTop -= 8;
    } else if (this.elListRect.top + this.elListRect.height - pad < e.pageY) {
      // Scroll down
      this.elList.scrollTop += 8;
    }
  },

  getSelected: function() {
    var list = this.list;
    return list.filter(function(item) {
      return list.stateOf(item).selected() ? item : null;
    });
  },

  _isPageNum: function(item) {
    item = item instanceof App.Model.LocalPlaylistItem ? item.resource() : item;
    return (
      item instanceof App.Model.LocalPlaylist || (item instanceof App.Model.List && item.kind() === 'local#starred')
    );
  },

  _navigatePlaylistPageTo: function(page) {
    return this._playlistControl
      .load(page)
      .then(this._loadPlayerItems)
      .finally(m.redraw.nexttick);
  },

  _loadPlaylist: function(autoPlayIndex) {
    if (this._playlistControl) this._playlistControl.clear();
    Console.log('Playlist %O', this._playlist && this._playlist.id());
    var shuffle = (this.state.random().current = this._playlist.flags().shuffle);
    this.state.repeat().current = this._playlist.flags().repeat;
    if (_.isUndefined(autoPlayIndex)) autoPlayIndex = true; // Coz shuffle is 1 or 0
    this._playlistControl = new ControlLocalPlaylist(this._playlist, null, this._stateSignature);
    this.list = this._playlistControl.collection;
    return this._loadPlaylistPage(null, autoPlayIndex);
  },

  _loadPlaylistPage: function(page, autoPlayIndex) {
    // `autoPlayIndex` can be boolean or index, false or -1 will never play
    if (this._playlistControl) {
      var self = this;
      var prom = this._playlistControl
        .load(_.isNumber(page) && page < 1 ? 1 : page)
        .then(this._loadPlayerItems)
        .finally(m.redraw.nexttick);
      if (autoPlayIndex === true || (_.isNumber(autoPlayIndex) && autoPlayIndex > -1)) {
        prom.then(function() {
          // Auto play
          setTimeout(function() {
            if (autoPlayIndex === true) self.playNext();
            else self.playByIndex(autoPlayIndex);
            m.redraw.nexttick();
          }, 300);
        });
      }
      // We must not catch if we return a promise
      return prom;
    } else {
      return Promise.resolve();
    }
  },

  _loadItems: function() {
    var self = this;
    return new Promise(function(resolve) {
      var i,
        item,
        idVideos = [],
        idChannels = [],
        idPlaylists = [],
        idLocalLists = [],
        idLocalPlaylists = [],
        idLocalPlaylistitems = [];
      // Preload
      for (i = self.list.size() - 1; i >= 0; i--) {
        item = self.list.nth(i);
        // Only load those that are not loaded
        if (!item.isSaved()) {
          if (item instanceof App.Model.Video) idVideos.push(item.id());
          else if (item instanceof App.Model.Playlist) idPlaylists.push(item.id());
          else if (item instanceof App.Model.Channel) idChannels.push(item.id());
          else if (item instanceof App.Model.List) idLocalLists.push(item.id());
          else if (item instanceof App.Model.LocalPlaylist) idLocalPlaylists.push(item.id());
          else if (item instanceof App.Model.LocalPlaylistItem) idLocalPlaylistitems.push(item.id());
        }
      }
      resolve([idVideos, idPlaylists, idChannels, idLocalLists, idLocalPlaylists, idLocalPlaylistitems]);
    })
      .then(function(result) {
        var ids = result[0];
        if (ids.length)
          return youtube.videoInfo(ids).then(function() {
            return result;
          });
        return result;
      })
      .then(function(result) {
        var ids = result[1];
        if (ids.length)
          return youtube.playlistInfo(ids).then(function() {
            return result;
          });
        return result;
      })
      .then(function(result) {
        var ids = result[2];
        if (ids.length)
          return youtube.channelInfo(ids).then(function() {
            return result;
          });
        return result;
      })
      .then(function(result) {
        var ids = result[3];
        if (ids.length)
          return App.Model.requestMap['local#list'](ids).then(function() {
            return result;
          });
        return result;
      })
      .then(function(result) {
        var ids = result[4];
        if (ids.length)
          return App.Model.requestMap['local#playlist'](ids).then(function() {
            return result;
          });
        return result;
      })
      .then(function(result) {
        var ids = result[5];
        if (ids.length)
          return App.Model.requestMap['local#playlistitem'](ids).then(function() {
            return result;
          });
        return result;
      })
      .then(function() {
        var i,
          item,
          tofetch = [];
        for (i = self.list.size() - 1; i >= 0; i--) {
          item = self.list.nth(i);
          tofetch.push(item.fetch());
        }
        if (tofetch.length) {
          return Promise.all(tofetch);
        }
      })
      .then(function() {
        if (self.state.isProjectPlaylist()) {
          return self.list.populate();
        }
      })
      .then(m.redraw);
    // Return a promise. Do not catch here.
  },

  // Load and prepare all items
  _loadPlayerItems: function() {
    var self = this,
      size = this.list.size();
    if (this.state.random().current) this._renewRandom();
    return new Promise(function(resolve, reject) {
      async.waterfall(
        [
          function(done) {
            var i,
              item,
              idVideos = [],
              idChannels = [],
              idPlaylists = [];
            // Preload
            for (i = size - 1; i >= 0; i--) {
              item = self.list.nth(i);
              // Only load those that are not loaded
              if (!item.isSaved()) {
                if (item instanceof App.Model.Video) {
                  idVideos.push(item.id());
                } else if (item instanceof App.Model.Channel) {
                  idChannels.push(item.id());
                } else if (item instanceof App.Model.Playlist) {
                  idPlaylists.push(item.id());
                }
              }
            }
            done(null, [idVideos, idPlaylists, idChannels]);
          },
          function(ids, done) {
            var videos = ids[0];
            if (videos.length) {
              youtube
                .videoInfo(videos)
                .then(function() {
                  done(null, ids);
                })
                .catch(done);
            } else {
              done(null, ids);
            }
          },
          function(ids, done) {
            var playlists = ids[1];
            if (playlists.length) {
              youtube
                .playlistInfo(playlists)
                .then(function() {
                  done(null, ids);
                })
                .catch(done);
            } else {
              done(null, ids);
            }
          },
          function(ids, done) {
            var channels = ids[2];
            if (channels.length) {
              youtube
                .channelInfo(channels)
                .then(function() {
                  done(null);
                })
                .catch(done);
            } else {
              done(null);
            }
          },
          function(done) {
            var i,
              item,
              loadCount = 0;
            // Fetch (Should fetch from cache)
            if (size) {
              for (i = size - 1; i >= 0; i--) {
                item = self.list.nth(i);
                if (!item.isSaved()) {
                  item.fetch(function() {
                    loadCount++;
                    if (loadCount === size) {
                      done();
                    }
                  });
                } else {
                  loadCount++;
                  if (loadCount === size) {
                    done();
                  }
                }
              }
            } else {
              done();
            }
          }
        ],
        function(err) {
          self.state.loaded(true);
          if (err) reject(err);
          else {
            // Relink currently playing item
            var idx = self.getCurrentIndex();
            if (idx > -1) {
              try {
                var item = self.list.nth(idx);
                var state = self.list.stateOf(item);
                // ISSUE: #ROLLYT-8 Cannot read property 'split' of undefined
                // FIX: Added try catch
                var hisKey = self.getHistory().split('/');
                state.play(true);
                state.ended(false);
                // Playing item state. Add sub item to play.
                state.subitem(hisKey[3] || undefined);
                state.subitemPage(hisKey[2] || '');
                state.index(idx);
                if (state.subitem()) m.redraw.nexttick();
              } catch (ex) {
                // Just continue to resolve
                Console.error(ex);
              }
            }
            resolve();
          }
        }
      );
    });
  },

  // Get the value for element class property
  _getClass: function() {
    return (
      (util.isMobile ? '' : ' ps-container ps-theme-default ') +
      ((this.state.checkbox().current || 0) === 0 ? 'static' : 'checkbox') +
      (this._playlist ? ' local-playlist' : '') +
      (this._playlistControl && this._playlistControl.isPaginated() ? ' paginated' : '')
    );
  },

  _getTitle: function() {
    if (this.state.title()) return this.state.title();
    if (this._playlist && this._playlist.isSaved()) return this._playlist.title();
    else if (this.list.size()) return 'Unsaved Playlist';
    else return this._title || '';
  },

  _getSubTitle: function() {
    if (!this.state.loaded()) return 'Loading...';
    if (this.state.subtitle()) return this.state.subtitle();
    var state = this.list.stateOf(this.state.currentModel());
    if (this._playlistControl && this._playlist && this._playlist.isSaved()) {
      var page = (this._playlistControl.isPaginated() ? this._playlistControl.state.page() : 1) - 1;
      return (state ? App.Vars.playlist_limit * page + state.index() + 1 + ' / ' : '') + this._playlist.item_count();
    } else {
      return (state ? state.index() + 1 + ' / ' : '') + (this.list.size() || '');
    }
  },

  _createRandom: function() {
    if (!this.state.randomIndexes()) {
      var randomArr = _.shuffle(_.range(this.list.size()));
      if (randomArr[0] === this.getCurrentIndex()) {
        _.reverse(randomArr);
      }
      this.state.randomIndexes(randomArr);
      this.state.randomCurrent(-1);
    }
  },

  _clearRandom: function() {
    if (this.state.randomIndexes()) {
      this.state.randomIndexes(null);
      this.state.randomCurrent(null);
    }
  },

  _renewRandom: function() {
    if (this.state.random().current) {
      this._clearRandom();
      this._createRandom();
    }
  },

  _getCheckboxIcon: function() {
    var cbState = this.state.checkbox().current;
    if (cbState === 2) {
      return 'first_page';
    } else if (cbState === 1) {
      return 'done_all';
    } else {
      return 'mode_edit';
      // return 'last_page';
    }
  },

  _getItemCompKey: function(model, state) {
    var key = model.id() || model.lid();
    if (comShare.isJoined()) {
      var stateSettings = state.settings(),
        objSettings = stateSettings && stateSettings.toJson ? stateSettings.toJson() : stateSettings;
      return key + state.play() + _.values(objSettings).join('');
    } else {
      return key;
    }
  },

  // - Events - - - - - - - - - - - - - - - - - - - - -

  longpressSkipNext: function() {
    this.playEnded(true);
    // Only this method needs redraw
    m.redraw();
  },

  clickMenu: function() {
    comMenu.show(this, menuSchema);
  },

  clickVideo: function() {
    // if (comPlayerCurrent.model) m.route.set('/player.video?type=details&expand=1&id=' + comPlayerCurrent.model.id());
    if (comPlayerCurrent.model) m.route.set('/player.video?id=' + comPlayerCurrent.model.id());
  },

  clickSkipNext: function() {
    if (comShare.isJoined()) comShare.adapter.skipNext();
    else this.playEnded();
  },

  clickSkipPrev: function(e) {
    if (e) e.redraw = false;
    if (comShare.isJoined()) comShare.adapter.skipPrev();
    else this.playHistory();
  },

  clickStop: function() {
    this.stop();
  },

  clickNextPage: function(e, loading) {
    loading(true);
    this._navigatePlaylistPageTo()
      .catch(Console.error)
      .finally(function() {
        loading(false);
      });
  },

  clickPrevPage: function(e, loading) {
    loading(true);
    this._navigatePlaylistPageTo(true)
      .catch(Console.error)
      .finally(function() {
        loading(false);
      });
  },

  clickRepeat: function() {
    if (comShare.isJoined()) {
      comShare.adapter.command('toggle_repeat');
      return;
    }
    // Recreate random list
    if (this.state.random().current) {
      this._clearRandom();
      this._createRandom();
    }
    if (this._playlist) {
      this._playlist.flags().repeat = this.state.repeat().current;
      this._playlist.save();
    }
    this.broadcast();
    this.store('controls');
  },

  clickRandom: function() {
    if (comShare.isJoined()) {
      comShare.adapter.command('toggle_random');
      return;
    }
    if (this.state.random().current) {
      this._createRandom();
    } else {
      this._clearRandom();
    }
    if (this._playlist) {
      this._playlist.flags().shuffle = this.state.random().current;
      this._playlist.save();
    }
    this.broadcast();
    this.store('controls');
  },

  dblclickItemsContainer: function(e) {
    if (_.includes(e.target.className, 'pltoggle')) App.Settings.playerExpand(!App.Settings.playerExpand());
  },

  // - View - - - - - - - - - - - - - - - - - - - - -

  component: function() {
    var self = this;
    var _name, _reso, _comp, _state;
    return m(
      'div',
      {
        class:
          'list' +
          (self.state.currentModel() ? ' playing' : '') +
          ((this._playlist && this._playlist.isSaved()) || this.state.isProjectPlaylist() ? ' is-playlist' : ''),
        ondblclick: util.isMobile ? undefined : self.dblclickItemsContainer
      },
      m('div.list-title', m('span.title', this._getTitle()), m('span.sub', this._getSubTitle())),
      m(
        'div.actions pltoggle',
        m(
          'ul.right',
          !comPlayerCurrent.hasVideoId()
            ? null
            : m(elButtonIcon, {
                key: 'slideshow',
                icon: 'slideshow',
                class: 'video',
                onclick: self.clickVideo,
                title: 'Video Page'
              }),
          m(elButtonIcon, {
            key: 'skip_previous',
            class: !(this._history.length > 1) && !comShare.isJoined() ? 'hide' : '',
            icon: 'skip_previous',
            onclick: self.clickSkipPrev,
            title: 'Previous'
          }),
          m(elButtonIcon, {
            key: 'skip_next',
            icon: 'skip_next',
            onclick: self.clickSkipNext,
            onlongpress: self.longpressSkipNext,
            title: 'Next'
          }),
          m(elButtonIcon, {
            key: 'stop',
            icon: 'stop',
            onclick: self.clickStop,
            title: 'Stop'
          }),
          m(elButtonIcon, {
            key: 'menu',
            icon: 'menu',
            onclick: self.clickMenu
          })
        ),
        m(
          'ul.left',
          comShare.isJoined()
            ? null
            : m(elButtonState, {
                class: 'edit-mode',
                key: 'edit',
                icon: self._getCheckboxIcon(),
                state: self.state.checkbox(),
                title: 'Edit'
              }),
          m(elButtonState, {
            icon: 'repeat',
            key: 'repeat',
            onclick: self.clickRepeat,
            state: self.state.repeat(),
            title: 'Repeat'
          }),
          m(elButtonState, {
            icon: 'shuffle',
            key: 'shuffle',
            onclick: self.clickRandom,
            state: self.state.random(),
            title: 'Shuffle'
          })
        )
      ),
      m(
        'div.items pltoggle',
        {
          class: self._getClass(),
          oncreate: self.oncreateItemsContainer,
          onupdate: self.onupdateItemsContainer,
          onremove: self.onremoveItemsContainer
        },
        m(
          'ul.pltoggle',
          !self.state.loaded()
            ? null
            : self.list.map(function(modelItem, index) {
                if (modelItem.resource) {
                  _reso = modelItem.resource();
                  // For missing resource, we use video as placholder
                  _name = _reso ? _reso.options.name : 'Video';
                } else {
                  _name = modelItem.options.name;
                }
                _comp = self._mapCompOf[_name];
                _state = self.list.stateOf(modelItem);
                _state.index(index);
                return m(_comp, {
                  model: modelItem, // If playlist, this is playlistitem, not the resource model.
                  state: _state,
                  checkboxState: self.state.checkbox,
                  key: self._getItemCompKey(modelItem, _state),
                  settings: _state.settings() || (modelItem.flags ? modelItem.flags() : undefined)
                });
              })
        )
      ),
      !(this._playlistControl && this._playlistControl.isPaginated())
        ? null
        : m(
            'div.actions bottom',
            m('span', this._playlistControl.state.page() + ' / ' + this._playlistControl.getTotalPages()),
            m(
              'ul.right',
              m(elButtonIcon, {
                icon: 'navigate_next',
                onclick: self.clickNextPage,
                loading: true
              })
            ),
            m(
              'ul.left',
              m(elButtonIcon, {
                icon: 'navigate_before',
                onclick: self.clickPrevPage,
                loading: true
              })
            )
          )
    );
  }
};

// Create singleton controller
var _list = (module.exports = new List());
App.Components.Player = _list;
if (App.dev) App.dev.list = _list;

// Bind globally to keyboard `skip next`
mousetrap.bindGlobal('ctrl+space', function() {
  _list.playEnded();
  m.redraw.nexttick();
  return false;
});

// Bind globally to keyboard `skip next and add to skiplist`
mousetrap.bindGlobal('ctrl+shift+space', function() {
  _list.playEnded(true);
  m.redraw.nexttick();
  return false;
});

// Bind globally to ctrl pressed
function fnCtrlPressed(e) {
  App.Props.ctrlPressed(e.type == 'keydown');
}
mousetrap.bindGlobal('ctrl', fnCtrlPressed, 'keydown');
mousetrap.bindGlobal('ctrl', fnCtrlPressed, 'keyup');

// Post import
var comShare = require('../contexts/contextSharing');
var comPlayerCurrent = require('./current');
List.prototype._mapCompOf = {
  Video: require('./itemVideo'),
  Channel: require('./itemChannel'),
  Playlist: require('./itemPlaylist'),
  LocalPlaylist: require('./itemLocalPlaylist'),
  List: require('./itemList')
};

// SUBMENU - Playlist

var submenuPlaylist = [
  {
    name: 'View Playlist',
    icon: 'visibility',
    callback: function() {
      m.route.set(
        '/player.localplaylist?id=' + (_list._playlist ? _list._playlist.id() : _list.state.projectPlaylistId())
      );
    }
  },
  {
    name: 'Refresh',
    icon: 'refresh',
    callback: function() {
      _list.changePlaylist(_list._playlist);
    }
  },
  {
    name: 'Close',
    icon: 'close',
    separate: 'before',
    callback: function() {
      _list.clear();
    }
  }
];

submenuPlaylist.parser = function(item) {
  if (item.name == 'Close' || item.name == 'View Playlist') {
    item.hide = !(_list._playlist || _list.state.isProjectPlaylist());
  } else if (item.name == 'Refresh') {
    item.hide = !_list._playlist;
  }
};

// SUBMENU - Misc

var submenuMisc = [
  {
    id: 'minimize',
    name: 'Minimize',
    callback: function() {
      App.Settings.playerListMinimize(!App.Settings.playerListMinimize());
    }
  },
  {
    id: 'autoplayrelated',
    name: 'Auto Play Related',
    callback: function() {
      App.Preference.state.auto_play_related(!App.Preference.state.auto_play_related());
    }
  }
];

submenuMisc.parser = function(item) {
  if (item.id === 'minimize') {
    item.checked = App.Settings.playerListMinimize();
    item.hide = !App.Settings.playerExpand();
  } else if (item.id === 'autoplayrelated') {
    item.checked = App.Preference.state.auto_play_related();
    item.hide = comShare.isJoined();
  }
};

// SUBMENU - Selected

var submenuSelected = [
  {
    name: 'Add To Playlist',
    icon: 'playlist_add',
    callback: function() {
      var flags;
      var arrObj = _list.getSelected().map(function(model) {
        flags = _list.list
          .stateOf(model)
          .settings()
          .toJson();
        if (model.getKind() === 'local#playlistitem') model = model.resource();
        return {
          resource_type: model && model.getKind(),
          resource_id: model && model.id(),
          flags: _.isEmpty(flags) ? undefined : flags
        };
      });
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems(arrObj);
        }
      });
    }
  },
  {
    name: 'Remove',
    icon: 'remove',
    callback: function() {
      _list.remove(_list.getSelected());
      _list._renewRandom();
    }
  }
];

// SUBMENU - Current Playing

var submenuCurrentPlaying = [
  {
    authonly: true,
    name: 'Add to Skiplist',
    icon: 'block',
    callback: function() {
      _list.playEnded(true);
    }
  },
  {
    authonly: true,
    name: 'Add to Playlist',
    icon: 'playlist_add',
    callback: function() {
      if (comPlayerCurrent.model) {
        ModalSelectLocalPlaylist.show({
          onselect: function(e, playlist) {
            playlist.addItems([
              {
                resource_type: comPlayerCurrent.model.getKind(),
                resource_id: comPlayerCurrent.model.id()
              }
            ]);
          }
        });
      }
    }
  },
  {
    authonly: true,
    name: 'Star / Bookmark',
    icon: 'star',
    separate: 'after',
    callback: function(el) {
      var id = comPlayerCurrent.model.id();
      starring
        .star('video', id)
        .then(function(data) {
          ContextStar.show(el, null, 'video', id, true, data.folder);
        })
        .catch(Console.error)
        .finally(m.redraw);
    }
  },
  {
    name: 'View Video',
    icon: 'slideshow',
    callback: function() {
      if (comPlayerCurrent.model) {
        m.route.set('/player.video?id=' + comPlayerCurrent.model.id());
      }
    }
  },
  {
    name: 'View Channel',
    icon: 'account_circle',
    callback: function() {
      if (comPlayerCurrent.model) {
        m.route.set('/player.channel?id=' + comPlayerCurrent.model.channel().id());
      }
    }
  },
  {
    name: 'View in Modal',
    icon: 'flip_to_front',
    callback: function() {
      if (comPlayerCurrent.model) {
        comModalVideoPlayer.videoModel(comPlayerCurrent.model);
      }
    }
  }
];

submenuCurrentPlaying.parser = function(item) {
  if (item.authonly) item.hide = App.User.isGuest();
};

// Context menu schema
var menuSchema = [
  {
    name: 'Selected',
    icon: 'check',
    submenu: submenuSelected
  },
  {
    id: 'playing',
    name: 'Current Video',
    icon: 'slideshow',
    separate: 'after',
    submenu: submenuCurrentPlaying
  },
  {
    name: 'Playlist',
    icon: 'library_books',
    separate: 'after',
    submenu: submenuPlaylist,
    callback: _.noop
  },
  {
    name: 'Open',
    icon: 'open_in_new',
    callback: function() {
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          _list.changePlaylist(playlist);
        }
      });
    }
  },
  {
    id: 'save_as',
    name: 'Save',
    icon: 'save',
    callback: function() {
      var arrObj = _list.list.map(function(model) {
        var flags = _list.list
          .stateOf(model)
          .settings()
          .toJson();
        return {
          resource_type: model.getKind(),
          resource_id: model.id(),
          flags: _.isEmpty(flags) ? undefined : flags
        };
      });
      ModalCreatePlaylist.show({
        title: 'Save as Rollyt Playlist',
        data: arrObj,
        flags: {
          repeat: _list.state.repeat().current,
          shuffle: _list.state.random().current
        },
        ondone: function(playlist) {
          _list.changePlaylist(playlist, _list.getCurrentIndex());
        }
      });
    }
  },
  {
    name: 'Sort',
    icon: 'format_line_spacing',
    separate: 'before',
    submenu: [
      {
        name: 'By Title',
        icon: 'sort_by_alpha',
        callback: function() {
          _list.list.sort('title');
        }
      },
      {
        name: 'By Channel',
        icon: 'account_circle',
        callback: function() {
          _list.list.sort([
            function(item) {
              return item.channel ? item.channel.title : undefined;
            },
            'title'
          ]);
        }
      },
      {
        name: 'By Duration',
        icon: 'av_timer',
        callback: function() {
          _list.list.sort([
            function(item) {
              return item.duration ? moment.duration(item.duration).asSeconds() : undefined;
            },
            'title'
          ]);
        }
      },
      {
        name: 'Reverse',
        icon: 'swap_vert',
        separate: 'before',
        callback: function() {
          _list.list.reverse();
        }
      },
      {
        name: 'Randomize',
        callback: function() {
          _list.list.randomize();
        }
      }
    ]
  },
  {
    name: 'Misc...',
    submenu: submenuMisc
  },
  {
    name: 'Clear',
    icon: 'remove',
    separate: 'before',
    callback: function() {
      _list.clearFilter();
    }
  }
];

menuSchema.parser = function(item) {
  if (item.name === 'Open' || item.name === 'Sort') {
    item.hide = comShare.isJoined();
  }

  if (item.id === 'save_as') {
    item.hide = comShare.isJoined() || !!_list._playlist;
  }

  if (item.name === 'Selected') {
    item.hide = !_list.getSelected().length;
  }

  if (item.name === 'Playlist') {
    item.hide = !(_list._playlist || _list.state.isProjectPlaylist());
  }

  if (item.id === 'playing') {
    item.hide = !comPlayerCurrent.hasVideoId();
  }
};
