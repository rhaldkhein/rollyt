var _ = require('lodash');
var m = require('mithril');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var util = require('libs/util');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');
var ContextStar = require('comp/contexts/contextStar');
var starring = require('libs/starring');

var bindMethods = ['clickPlaylist'];
var partMethods = ['clickStar'];

module.exports = {
  oninit: function(node) {
    _.bindAll(this, bindMethods, partMethods);
    this.options = node.attrs || {};
  },

  clickPlaylist: function() {
    var model = this.options.model;
    if (model) {
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems([
            {
              resource_type: model.getKind(),
              resource_id: model.id()
            }
          ]);
        }
      });
    }
  },

  clickStar: function(el, self) {
    var model = self.options.model;
    var that = this;
    if (model) {
      starring
        .star('video', model.id())
        .then(function(data) {
          ContextStar.show(that, null, 'video', model.id(), true, data.folder);
        })
        .catch(Console.error)
        .finally(m.redraw);
    }
  },

  clickEnlarge: function() {
    App.Settings.playerListMinimize(!App.Settings.playerListMinimize());
  },

  view: function() {
    return m(
      '.current-body',
      m(
        'h1.title',
        m(
          'a',
          {
            href: '#/player.video?id=' + this.options.model.id()
          },
          this.options.model.title()
        )
      ),
      m(
        '.channel',
        m(
          'a.text',
          {
            href: '#/player.channel?id=' + this.options.model.channel().id()
          },
          this.options.model.channel().title()
        )
      ),
      m(
        '.bar',
        m(
          '.left',
          m(
            'span.views',
            m(elIcon, {
              icon: 'visibility'
            }),
            util.numberComma(this.options.model.views()) + ' views'
          ),
          m(
            'span.likes',
            m(elIcon, {
              icon: 'thumb_up'
            }),
            util.numberComma(this.options.model.likes()) + ' likes'
          )
        ),
        m(
          '.actions',
          m(elButtonIcon, {
            key: 'btnPlAdd',
            icon: 'playlist_add',
            title: 'Add to Playlist',
            onclick: this.clickPlaylist
          }),
          m(elButtonIcon, {
            key: 'btnStar',
            icon: 'star',
            title: 'Star / Bookmark',
            onclick: this.clickStar
          }),
          m(elButtonIcon, {
            key: 'btnZoom' + App.Settings.playerListMinimize(),
            icon: App.Settings.playerListMinimize() ? 'fullscreen_exit' : 'zoom_out_map',
            title: (App.Settings.playerListMinimize() ? 'Shrink' : 'Enlarge') + ' Video',
            onclick: this.clickEnlarge
          })
        )
      ),
      m('.description', this.options.model.description()),
      m(
        '.descmore',
        m(
          'a',
          {
            href: '#/player.video?type=details&expand=1&id=' + this.options.model.id()
          },
          'Full Description'
        )
      )
    );
  }
};
