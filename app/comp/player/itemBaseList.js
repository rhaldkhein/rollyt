// Imports
var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var util = require('libs/util');
var comPlayerCurrent = require('./current');
var comList = require('./list');
var numeral = require('numeral');

// Constructors
var comItemBase = require('./itemBase');

// Statics
var bindMethods = ['play', 'playNext', 'clickBottomExpand', 'clickBottomLoadPrev', 'clickBottomLoadNext'];

var countUnableToPlay;

// Prototype
module.exports = _.create(comItemBase, {
  oninit: function(node) {
    // Add settings for base list
    node.attrs.settings = _.assign(
      {
        playLimit: md.stream(0), // 0 will play all
        shuffle: md.stream(false),
        itemLimit: md.stream(12)
      },
      node.attrs.settings
    );
    // Construct super
    comItemBase.oninit.call(this, node);
    // Binds
    _.bindAll(this, bindMethods);
    this.clickSubItem = _.partialRight(this.clickSubItem, this);
    // Properties
    this.control = null;
    this.playCount = md.stream(0);
    this.expand = md.stream(false);
    this.lastPlayIndex = md.stream(-1);
  },

  onupdate: function() {
    // Check subitem id here and tell parent that its changed or not
    var playing = this.state.playing();
    if (playing) {
      var current = this.getControl().currentItem();
      var subitemId = this.state.subitem();
      if (current && subitemId !== undefined && current.id() !== subitemId) this.state.status(-1);
    }
    comItemBase.onupdate.call(this);
  },

  getSettingsName: function(key, val) {
    switch (key) {
      case 'shuffle':
        return val ? 'Shuffle: On' : null;
      case 'playLimit':
        return val ? 'Play: ' + val : null;
      case 'itemLimit':
        return val !== 12 ? 'Size: ' + val : null;
    }
  },

  setControl: function(control) {
    this.control = control;
  },

  getTime: function() {
    var ctrl = this.getControl();
    var vids = ctrl ? ctrl.state.total() : undefined;
    return vids ? numeral(vids).format('0a') : '';
  },

  play: function() {
    // Triggered play from parent list (Entry point).
    // Load the subitem first then play.
    var self = this;
    var control = this.getControl();
    if (control.state.loading()) return;
    var subitemId = this.state.subitem();
    var subitemPage = this.state.subitemPage();
    var subitemModel = control.collection.get(subitemId);
    var itemCurrentPage = control.getCurrentPage();
    // Filter items for security reason
    // `getKind` is property of all models. `kind` is only for List models.
    if (comShare.isHost() && this.model.getKind() == 'local#list' && this.model.kind() == 'youtube#liked') {
      App.Modules.toastr.error('Not allowed to play on shared player');
      // Tell all clients to wait while we skipp to nex item
      comShare.adapter.notifyAll('"' + this.model.title() + '" is private. Skipping...', 'error');
      control.setNextItem(null);
      control.state.lastLoad(true);
      control.state.ended(true);
      Promise.delay(Config.playerErrorSkipDelay).then(function() {
        comList.playEnded();
        m.redraw();
      });
      return; // Break
    }

    if (!control.state.loaded() || subitemPage !== itemCurrentPage) {
      control
        .load(subitemPage)
        .then(function() {
          // NOTE: If `.play()` loops, issue may be the `subitemPage` is not correct type eg. '' !==  0
          // Which is '' (empty string) is for youtube paging and `0` (integer) local list paging
          if (control.collection.size()) {
            self.play();
            countUnableToPlay = 0;
          } else {
            throw new Error('no_items');
          }
        })
        // When error occurred while loading subitem page
        .catch(function(err) {
          self.state.loading(false);
          if (err.message === 'no_items') {
            App.Modules.toastr.error('Current item has no videos');
          } else if (err.code === 'REPC') {
            App.Modules.toastr.error('Current item is private');
            if (comShare.isHost()) comShare.adapter.notifyAll('"' + self.model.title() + '" is private', 'error');
          } else {
            // Emergency exit for infinit loop
            if (countUnableToPlay > App.Vars.playlist_limit) {
              comList.clear();
              return;
            }
            // ISSUE: This can go infinit loop
            App.Modules.toastr.error('Unable to play the current item');
            if (comShare.isHost())
              comShare.adapter.notifyAll('"' + self.model.title() + '" is unable to play', 'error');
            countUnableToPlay++;
          }
          // If shared player client. Don't skip to next.
          if (comShare.isJoined()) return;
          return Promise.delay(Config.playerErrorSkipDelay).then(function() {
            comList.playEnded();
            m.redraw();
          });
        })
        // Catch for `playEnded` in catch above
        .catch(Console.error);
      this.state.loading(true);
      m.redraw();
      return; // Break
    }

    if (!this.state.playing()) this.playCount(0);
    control.setNextItem(subitemModel ? subitemModel : undefined);
    this.state.subitem(undefined);
    this._playNextVideo();
    comItemBase.play.call(this);
  },

  playEnded: function(redraw) {
    this.getControl().notifyStopPlaying();
    comItemBase.playEnded.call(this, redraw);
  },

  hasNext: function() {
    if (this.getControl().hasNext()) {
      if (this.settings.playLimit() === 0) {
        return true;
      } else if (this.playCount() < this.settings.playLimit()) {
        return true;
      }
    }
    return false;
  },

  getMenuSchema: function() {
    // return (comShare.isJoined() ? [] : menuSchema);
    return menuSchema;
  },

  _playNextVideo: function() {
    var control = this.getControl(),
      nextItem = control.nextItem(),
      lastLoad = control.state.lastLoad();
    if (nextItem) {
      this._playVideo();
    } else if (!lastLoad) {
      if (!control.state.loading()) {
        var self = this;
        control
          .load()
          .then(function() {
            self.state.subitemPage(control.getCurrentPage());
            self.play();
          })
          .catch(Console.error);
        this.state.loading(true);
      }
    } else {
      util.nextTick(comList.playNext);
    }
    this.state.ended(false); // This is important
  },

  _playVideo: function(itemId) {
    // If `itemId` is undefined, it will set the next video
    this.state.loading(false);
    var model = this.getModel();
    var control = this.getControl();
    control.setCurrent(itemId);
    var videoId = control.currentVideo().id();
    comPlayerCurrent.loadVideo(videoId);
    this.playCount(this.playCount() + 1);
    this.lastPlayIndex(control.playingIndex());
    comList.addHistory(
      model.id() +
        '/' +
        (control.getCurrentPage() || '') +
        '/' +
        ((control.currentPlaylistItem ? control.currentPlaylistItem().id() : videoId) || '')
    );
    m.redraw.nexttick();
  },

  // Events - - - - - - - - - - - - - - - - - -

  clickSubItem: function(e, self) {
    comList.playByModel(self.getModel(), this.getAttribute('subitemid'), self.getControl().getCurrentPage());
  },

  clickBottomExpand: function(e, loading) {
    var self = this;
    var ctrl = this.getControl();
    if (!ctrl.collection.size()) {
      // Load list
      loading(true);
      ctrl
        .refresh()
        .then(function() {
          self.expand(!self.expand());
        })
        .catch(function() {
          App.Modules.toastr.error('Error while expanding subitems');
        })
        .finally(function() {
          loading(false);
          m.redraw();
        });
    } else {
      this.expand(!this.expand());
    }
  },

  clickBottomLoadPrev: function(e, loading) {
    loading(true);
    this.getControl()
      .load(true)
      .finally(function() {
        loading(false);
        m.redraw();
      });
  },

  clickBottomLoadNext: function(e, loading) {
    loading(true);
    this.getControl()
      .load()
      .finally(function() {
        loading(false);
        m.redraw();
      });
  },

  // Views - - - - - - - - - - - - - - - - - -

  viewSubItems: function() {
    // if (!this.control) return null;
    var self = this;
    var name, itemState, itemVideo;
    var ctrl = this.getControl();
    var collapseCount = 2;
    var startCount = false;
    var playingIndex = this.lastPlayIndex();
    if (playingIndex === -1) {
      // Verify last play index
      playingIndex = ctrl.playingIndex();
      playingIndex = playingIndex > -1 ? playingIndex : 0;
    }
    return m(
      '.itemsub',
      m(
        'div',
        {
          class: self.expand() ? 'expand' : ''
        },
        m(
          '.settings',
          m(
            'ul',
            _.map(self.settings, function(value, key) {
              if (key === 'toJson' || key === '_options') return null;
              name = self.getSettingsName(key, value());
              return name ? m('li', name) : null;
            })
          )
        ),
        m(
          'ul.itemsub-list',
          ctrl.collection.map(function(item, i) {
            itemState = ctrl.collection.stateOf(item);
            itemVideo = ctrl.videoOfItem(item);
            if (!self.expand()) {
              // Collapsed
              if (i === playingIndex) {
                startCount = true;
              }
              if (startCount && collapseCount > 0) {
                collapseCount--;
              } else {
                // Do not display this item
                return null;
              }
            }
            return m(
              'li',
              {
                class: self.state.playing() && itemState.playing() ? 'playing' : '',
                key: item.id() || item.lid()
              },
              m(elIcon, {
                icon: 'slideshow'
              }),
              m(
                'a',
                {
                  subitemid: item.id(),
                  onclick: self.clickSubItem
                },
                m('span', itemVideo.title())
              )
            );
          })
        ),
        m(
          '.bottom',
          {
            class: comShare.isJoined() ? 'hide' : ''
          },
          // 1st button
          self.expand()
            ? m(elButtonIcon, {
                key: 'a', // To force recreate
                class: ctrl.hasPrevPage() ? '' : 'transparent',
                icon: 'navigate_before',
                onclick: self.clickBottomLoadPrev,
                loading: true
              })
            : null,
          // 2nd button
          m(elButtonIcon, {
            key: 'b', // To force recreate
            icon: self.expand() ? 'expand_less' : 'expand_more',
            onclick: self.clickBottomExpand,
            loading: true
          }),
          // 3rd button
          self.expand()
            ? m(elButtonIcon, {
                key: 'c', // To force recreate
                class: ctrl.hasNextPage() ? '' : 'transparent',
                icon: 'navigate_next',
                onclick: self.clickBottomLoadNext,
                loading: true
              })
            : null
        )
      )
    );
  }
});

var comShare = require('../contexts/contextSharing');

// Menu

var itemLimitCallback = function(target, item) {
  if (comShare.isJoined())
    return comShare.adapter.command('set_item_setting', this.getModel().id(), 'playLimit', item.value);
  this.settings.playLimit(item.value);
  this.saveSettings();
};
var itemLimitSubmenu = [
  {
    name: '1',
    value: 1,
    callback: itemLimitCallback
  },
  {
    name: '2',
    value: 2,
    callback: itemLimitCallback
  },
  {
    name: '3',
    value: 3,
    callback: itemLimitCallback
  },
  {
    name: '5',
    value: 5,
    callback: itemLimitCallback
  },
  {
    name: '10',
    value: 10,
    callback: itemLimitCallback
  },
  {
    name: 'All',
    value: 0,
    callback: itemLimitCallback
  }
];
itemLimitSubmenu.parser = function(item) {
  item.icon = this.settings.playLimit() == item.value ? 'check' : null;
};

// ---

var itemMaxCallback = function(target, item) {
  if (comShare.isJoined())
    return comShare.adapter.command('set_item_setting', this.getModel().id(), 'itemLimit', parseInt(item.name));
  this.settings.itemLimit(parseInt(item.name));
  this.getControl()
    .refresh()
    .then(m.redraw)
    .catch(Console.error);
  this.saveSettings();
};
var itemMaxSubmenu = [
  {
    name: '5',
    callback: itemMaxCallback
  },
  {
    name: '10',
    callback: itemMaxCallback
  },
  {
    name: '12',
    callback: itemMaxCallback
  },
  {
    name: '15',
    callback: itemMaxCallback
  },
  {
    name: '20',
    callback: itemMaxCallback
  }
];
itemMaxSubmenu.parser = function(item) {
  item.icon = this.settings.itemLimit() == parseInt(item.name) ? 'check' : null;
};

// ---

var menuSchema = [
  {
    name: 'Play Limit',
    submenu: itemLimitSubmenu
  },
  {
    name: 'Shuffle / Unshuffle',
    icon: 'shuffle',
    callback: function() {
      if (comShare.isJoined())
        return comShare.adapter.command('set_item_setting', this.getModel().id(), 'shuffle', !this.settings.shuffle());
      // Toggle shuffle
      this.settings.shuffle(!this.settings.shuffle());
      this.getControl()
        .refresh()
        .then(m.redraw)
        .catch(Console.error);
      this.saveSettings();
    }
  },
  {
    name: 'Refresh',
    icon: 'refresh',
    callback: function() {
      if (comShare.isJoined()) return comShare.adapter.command('set_item_setting', this.getModel().id(), 'refresh');
      this.getControl()
        .refresh()
        .then(m.redraw)
        .catch(Console.error);
    }
  },
  {
    name: 'Max Items',
    separate: 'after',
    submenu: itemMaxSubmenu
  }
];
