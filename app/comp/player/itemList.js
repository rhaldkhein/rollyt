var _ = require('lodash');
var ControlList = require('libs/controlList');
var ControlLocalList = require('libs/controlLocalList');

// Constructors
var comItemBaseList = require('./itemBaseList');

// Prototype
module.exports = _.create(comItemBaseList, {
    oninit: function(node) {
        comItemBaseList.oninit.call(this, node);
    },

    getControl: function() {
        if (!this.control) {
            var ControlClass =
                this.model.kind() === 'local#starred'
                    ? ControlLocalList
                    : ControlList;
            this.control = new ControlClass(this.model, {
                shuffle: this.settings.shuffle,
                limit: this.settings.itemLimit
            });
        }
        return this.control;
    },

    getBulletIcon: function() {
        return 'list';
    },

    getMenuSchema: function() {
        return _.concat(menuSchema, comItemBaseList.getMenuSchema.call(this));
    }
});

// Menu
var menuSchema = [];
