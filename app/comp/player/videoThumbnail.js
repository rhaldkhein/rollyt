var _ = require('lodash');
var m = require('mithril');
var elButtonIcon = require('comp/elements/buttonIcon');

var bindMethods = ['clickPlayPause', 'clickVolUp', 'clickVolDown'];
var urlThumbnailBlack = '/static/images/player/thumbnail-black.png';
var VideoThumbnail = {
  _delayPromise: null,
  _showControl: false,
  oninit: function(node) {
    _.bindAll(this, bindMethods);
    this.controls = node.attrs.controls;
  },
  clickThumbnail: function(e) {
    VideoThumbnail._showControl = true;
    if (VideoThumbnail._delayPromise) {
      VideoThumbnail._delayPromise.cancel();
      e.redraw = false;
    }
    VideoThumbnail._delayPromise = Promise.delay(5000).then(function() {
      VideoThumbnail._showControl = false;
      VideoThumbnail._delayPromise = null;
      m.redraw();
    });
  },
  isPlaying: function() {
    return this.controls()[7] === 1;
  },
  clickPlayPause: function(e) {
    e.preventDefault();
    e.redraw = false;
    App.Components.Sharing.adapter.command('player_' + (this.isPlaying() ? 'pause' : 'play'));
  },
  clickVolUp: function(e) {
    e.preventDefault();
    e.redraw = false;
    App.Components.Sharing.adapter.command('player_volume', true);
  },
  clickVolDown: function(e) {
    e.preventDefault();
    e.redraw = false;
    App.Components.Sharing.adapter.command('player_volume', false);
  },
  viewGet: function(node) {
    var model = node.attrs.model;
    if (VideoThumbnail._showControl) {
      return m('.anchor an-middle-center f1 nowrap',
        m(elButtonIcon, {
          class: 'pv1 ph2 mr2',
          icon: (this.isPlaying() ? 'pause' : 'play_arrow'), // pause or play
          onclick: this.clickPlayPause
        }),
        m(elButtonIcon, {
          class: 'pv1 ph2',
          icon: 'volume_down',
          onclick: this.clickVolDown
        }),
        m(elButtonIcon, {
          class: 'pv1 ph2',
          icon: 'volume_up',
          onclick: this.clickVolUp
        })
      );
    }
    return m('h3', model ? model.title() : '');
  },
  view: function(node) {
    var model = node.attrs.model;
    return m('.projvidthumb', {
        onclick: this.clickThumbnail
      },
      m('img', {
        src: (model && model.isSaved() ? model.image() : urlThumbnailBlack)
      }),
      this.viewGet(node)
    );
  }
};

module.exports = VideoThumbnail;