var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var ps = require('perfect-scrollbar');
var util = require('libs/util');
var comVideoPlayer = require('./videoPlayer');
var comVideoThumbnail = require('./videoThumbnail');
var enquire = require('enquire.js');
var moment = require('moment');

var bindMethods = ['oncreateCurrent', 'onupdateCurrent', 'oncreatePlayer', 'endedVideo', 'loadModel'];
var compactPlayer = md.stream(false);
var skipTimeoutId = null;

enquire.register('screen and (max-width:' + 840 * App.Vars.cssPx + 'rem)', {
  match: function() {
    compactPlayer(true);
    m.redraw();
  },
  unmatch: function() {
    compactPlayer(false);
    m.redraw();
  }
});

function CurrentPlayer() {
  _.bindAll(this, bindMethods);
  this.defaultId = Config.defaultVideoId;
  this.videoId = md.stream(this.defaultId);
  this.expand = App.Settings.playerExpand;
  this.scrollInit = md.stream(false);
  this.stop = md.stream(false);
  this.controls = md.stream([]);
  this.model = null;
  this.lastModelId = null;
  this.player = null;
  var self = this;
  // Listen to stop changes
  this.stop.map(function(value) {
    // If value is true, then it should stop,
    // and set default video
    if (value) self.videoId(self.defaultId);
  });
  this.videoId.map(function(value) {
    // Others videos or difault video
    if (value) self.loadModel();
    else self.model = null;
  });
}

CurrentPlayer.prototype = {
  oncreateCurrent: function(node) {
    var self = this;
    App.Modules.messenger.listen('app#current', function(data) {
      if (data === 'video') {
        App.Settings.playerExpand(false);
        m.route.set('/player.video?id=' + self.getVideoId());
      }
      m.redraw();
    });
    this.onupdateCurrent(node);
  },

  onupdateCurrent: function(node) {
    if (this.expand() && !compactPlayer()) {
      if (!this.scrollInit()) {
        ps.initialize(node.dom);
        this.scrollInit(true);
      }
      ps.update(node.dom);
    } else {
      if (this.scrollInit()) {
        ps.destroy(node.dom);
        this.scrollInit(false);
      }
    }
  },

  oncreatePlayer: function(node) {
    this.player = node.state;
  },

  endedVideo: function(byInit, byTimeout) {
    if (byTimeout && comShare.isHost()) {
      var mdl = this.getModel();
      comShare.adapter.notifyAll(
        'Unable to play ' + (mdl ? '"' + mdl.title().substring(0, 34) + '"' : 'the video'),
        'error'
      );
    }
    util.nextTick(comList.playEnded);
  },

  getSkipTimeout: function() {
    return setTimeout(function() {
      comList.playEnded();
      m.redraw.nexttick();
    }, Config.skiplistDelay);
  },

  // This should be the entry point to change current video
  loadVideo: function(id) {
    if (skipTimeoutId) {
      clearTimeout(skipTimeoutId);
      skipTimeoutId = null;
    }
    this.videoId(id);
    this.lastModelId = this.getVideoId();
    // Skip by falsy id
    if (!id) {
      skipTimeoutId = this.getSkipTimeout();
      return;
    }
    // Skip by skiplist
    var _skiplist = App.Modules.skiplist;
    if (_skiplist.check(id)) {
      skipTimeoutId = setTimeout(function() {
        if (_skiplist.isLoop()) {
          comList.stop();
          _skiplist.clear();
        } else {
          comList.playEnded();
        }
        m.redraw.nexttick();
      }, Config.skiplistDelay);
      return;
    }
  },

  loadModel: function() {
    var id = this.getVideoId();
    // Skip if no id
    if (!id) {
      this.model = null;
      return;
    }
    // Only new id can pass here
    this.model = App.Model.Video.create({
      _id: id
    });
    if (!this.model.isSaved()) {
      var self = this;
      this.model
        .fetch()
        .then(function() {
          self.finallyLoad();
        })
        .catch(Console.error)
        .finally(m.redraw);
    } else {
      this.finallyLoad();
    }
  },

  finallyLoad: function() {
    if (skipTimeoutId || comShare.isJoined()) return;
    var self = this;
    var _skiplist = App.Modules.skiplist;
    // resolve = unskip, reject = skip
    Promise.resolve()
      .then(function() {
        // Identify related video model
        if (comList._related) {
          var relatedToVideoId = comList._related_by;
          if (relatedToVideoId) {
            var relatedToVideo = App.Model.Video.create({
              _id: relatedToVideoId
            });
            if (!relatedToVideo.isSaved()) {
              return relatedToVideo.fetch();
            } else {
              return relatedToVideo;
            }
          }
        }
      })
      .then(function(relatedToVideo) {
        if (relatedToVideo) {
          // Skip if current video is same with target video
          if (relatedToVideo.id() == self.model.id()) return self.getSkipTimeout();
          // Skip by category, only for category 10 (Music)
          var cat = self.model.category();
          if (!_skiplist.isFull() && cat === 10) {
            if (cat !== relatedToVideo.category()) return self.getSkipTimeout();
            // Skip by length
            return relatedToVideo;
          }
        }
      })
      .then(function(result) {
        // Skip by length
        var minMinutes;
        var maxMinutes;
        if (result instanceof App.Model.Video) {
          // Not skipped previously.
          // Should only be less than 6 or 7 mins (only for short music)
          if (moment.duration(result.duration()).minutes() < 7) {
            minMinutes = moment
              .duration(result.duration())
              .subtract(moment.duration(2, 'm'))
              .asMinutes();
            maxMinutes = moment
              .duration(result.duration())
              .add(moment.duration(2, 'm'))
              .asMinutes();
            result = null;
          }
        }
        if (!_skiplist.isFull() && !result) {
          var durMinutes = moment.duration(self.model.duration()).asMinutes();
          minMinutes = minMinutes || App.Preference.state.skip_length_min();
          maxMinutes = maxMinutes || App.Preference.state.skip_length_max();
          if ((minMinutes && durMinutes < minMinutes) || (maxMinutes && durMinutes > maxMinutes)) {
            // Skip
            result = self.getSkipTimeout();
          }
        }
        return result;
      })
      .then(function(timeoutId) {
        // If `timeoutId`, that means a has been initiated
        if (!timeoutId) {
          _skiplist.clear();
          if (App.Preference.state.play_notification() && !util.isMobile) {
            // ISSUE: #ROLLYT-3 `PushError: permission request declined` when called on HTTP (insecure)
            // FIX: Added rejection handler `_.noop`
            return App.Modules.push.create(self.model.title(), {
              timeout: 5000,
              icon: self.model.image()
            });
          }
        } else {
          App.Modules.skiplist.skipped(self.model.id());
        }
      })
      .catch(function(err) {
        Console.error(err);
        m.redraw.nexttick();
      });
  },

  project: function(controls) {
    this.controls(controls);
    m.redraw();
  },

  updateQuality: function() {
    if (this.player) {
      var self = this;
      // Delay to guarantee player size
      setTimeout(function() {
        self.player.updateQuality();
      }, 100);
    }
  },

  changeStateVideo: function(e) {
    // `this` is comVideoPlayer (this.player)
    // if (e.data === -1) {
    //   // Unstarted
    //   // Error may be `Not allowed to play other website`
    // } else
    if (e.data === 1) {
      // Playing
      comList.broadcast(null, 'playing');
      App.Modules.title.playing(this.videoId());
      this.updateQuality();
    } else if (e.data === 2) {
      // Paused
      comList.broadcast(null, 'paused', true);
    }
  },

  ended: function() {
    if (this.player && this.hasVideoId()) this.player.state.ended(true);
  },

  clear: function() {
    this.lastModelId = null;
    this.stop(true);
  },

  hasVideoId: function() {
    var id = this.videoId();
    return !!(id && id != this.defaultId);
  },

  // This is the right way to get video id.
  // It will return null for default video.
  getVideoId: function() {
    return this.hasVideoId() ? this.videoId() : null;
  },

  getModel: function() {
    return this.hasVideoId() ? this.model : null;
  },

  getPlayerState: function() {
    return _.has(this, 'player.videoPlayer.getPlayerState') ? this.player.videoPlayer.getPlayerState() : -1;
  },

  getPlayerVolume: function() {
    return _.has(this, 'player.videoPlayer.getVolume') ? this.player.videoPlayer.getVolume() : 100;
  },

  component: function() {
    return m(
      '.current',
      {
        oncreate: this.oncreateCurrent,
        onupdate: this.onupdateCurrent
      },
      comShare.isJoined()
        ? m(comVideoThumbnail, {
            key: 'projvidthumb',
            model: this.model,
            controls: this.controls
          })
        : m(comVideoPlayer, {
            oncreate: this.oncreatePlayer,
            key: 'iframe-player-video',
            iframeId: 'iframe-player-video',
            videoId: this.videoId,
            onended: this.endedVideo,
            onchangestate: this.changeStateVideo,
            stop: this.stop,
            playTimeout: true, // Skip when video is not played in time
            autoplay: false // (Config.prod ? true : false)
          }),
      !this.expand()
        ? null
        : m(
            'div',
            {
              key: this.videoId()
            },
            !this.model
              ? null
              : m(comBody, {
                  // videoId: this.videoId,
                  model: this.model
                })
          )
    );
  }
};

// Export singleton class
module.exports = new CurrentPlayer();
App.Components.Current = module.exports;
if (App.dev) App.dev.current = module.exports;

// Circular deps fix
var comBody = require('./currentBody');
var comList = require('./list');
var comShare = require('../contexts/contextSharing');
