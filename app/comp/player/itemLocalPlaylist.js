var _ = require('lodash');
var m = require('mithril');
var comItemBaseList = require('./itemBaseList');
var ControlLocalPlaylist = require('libs/controlLocalPlaylist');

// Prototype
module.exports = _.create(comItemBaseList, {

	oninit: function(node) {
		comItemBaseList.oninit.call(this, node);
	},

	getControl: function() {
		if (!this.control) {
			this.control = new ControlLocalPlaylist(this.model, {
				shuffle: this.settings.shuffle,
				limit: this.settings.itemLimit,
				videos: true
			});
		}
		return this.control;
	},

	getBulletIcon: function() {
		return 'playlist_play';
	},

	getMenuSchema: function() {
		return _.concat(menuSchema, comItemBaseList.getMenuSchema.call(this) || []);
	}

});

// Menu
var menuSchema = [{
	name: 'View Playlist',
	icon: 'playlist_play',
	separate: 'after',
	callback: function() {
		m.route.set('/player.localplaylist?id=' + this.model.id());
	}
}];
