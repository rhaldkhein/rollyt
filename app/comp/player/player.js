var m = require('mithril');
var comTopBar = require('./topbar');
var current = require('./current');
var list = require('./list');
var elIcon = require('comp/elements/icon');

module.exports = {
  oncreate: function() {
    App.Modules.messenger.listen('app#playlists', function() {
      App.Settings.playerExpand(false);
      m.route.set('/player.library.playlists');
      m.redraw();
    });
    App.Modules.messenger.listen('app#playeraction', function(data) {
      switch (data) {
        case 'next':
          list.clickSkipNext();
          break;
        case 'prev':
          list.clickSkipPrev();
          break;
        case 'stop':
          list.clickStop();
          break;
        case 'expand':
          App.Settings.playerExpand(true);
          break;
      }
      m.redraw();
    });
  },
  clickCover: function() {
    App.Settings.playerExpand(true);
    current.updateQuality();
  },
  view: function() {
    return m(
      'div#player',
      m(comTopBar),
      m(
        'div',
        {
          class: 'playlist' + (App.Settings.playerListMinimize() ? ' minimize' : '')
        },
        current.component(),
        list.component()
      ),
      m(
        'div.player-cover',
        {
          onclick: this.clickCover,
          class: current.hasVideoId() ? 'rotate' : ''
        },
        m(
          'span',
          m(
            'span',
            !current.hasVideoId()
              ? m(elIcon, {
                  icon: 'navigate_next'
                })
              : [
                  m(elIcon, {
                    icon: 'slideshow'
                  }),
                  m('span', current.model.title())
                ]
          )
        )
      )
    );
  }
};
