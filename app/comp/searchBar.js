var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var j = require('jquery');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var topBarMenu = require('comp/topBarMenu');
var current = require('comp/player/current');
var tippy = require('tippy.js');
var util = require('libs/util');

var bindMethods = [
  'submit',
  'clickCollapse',
  'clickPin',
  'clickPinRemove',
  'clickPrimeMenuButton',
  'clickShowSearchInput'
];
var partMethods = ['keyupQuery', 'changeQuery', 'selectSuggest'];
var promSuggest = Promise.resolve();
var cntxSearchBar = md.stream();
var blackKeyCodes = [37, 38, 39, 40]; // Arrow keys

function listenMessenger(cntx) {
  App.Modules.messenger.listen('app#search', function() {
    App.Settings.playerExpand(false);
    cntxSearchBar().clickShowSearchInput(null, true);
    m.redraw();
  });
  cntxSearchBar(cntx);
}

var SearchBar = {
  _pintext: App.Settings.searchBarPinText,
  _awesomplete: null,
  oninit: function() {
    _.bindAll(this, bindMethods, partMethods);
    this.state = md.State.create({
      query: m.route.param('q') || '',
      pintext: SearchBar._pintext,
      showMenu: false,
      showSearchInput: false
    });
    var link = m.route.param('page');
    if (_.includes(link, 'video')) {
      this.page = 'videos';
    } else if (_.includes(link, 'playlist')) {
      this.page = 'playlists';
    } else if (_.includes(link, 'channel')) {
      this.page = 'channels';
    } else if (_.includes(link, 'search')) {
      this.page = 'search';
    } else if (!link) {
      this.page = 'home';
    } else {
      this.page = null;
    }
    this.expand = App.Settings.playerExpand;
    this.guest = App.User.isGuest();
    if (this.state.pintext())
      this.state.query(this.state.query().replace(new RegExp('^' + this.state.pintext() + ' '), ''));
    if (App.Modules.messenger.isExtendable()) {
      if (!cntxSearchBar()) listenMessenger(this);
      else cntxSearchBar(this);
    }
  },
  oncreateInputQuery: function(node) {
    SearchBar._awesomplete = new window.Awesomplete(node.dom);
    node.dom.addEventListener('awesomplete-selectcomplete', node.attrs.onselect);
    node.dom.addEventListener('awesomplete-highlight', node.attrs.onhighlight);
  },
  onremoveInputQuery: function(node) {
    if (SearchBar._awesomplete) SearchBar._awesomplete.destroy();
    node.dom.removeEventListener('awesomplete-selectcomplete', node.attrs.onselect);
    node.dom.removeEventListener('awesomplete-highlight', node.attrs.onhighlight);
  },
  onupdateMenuA: function(node) {
    if (
      !util.isMobile &&
      // Tool top for prime buttons
      App.Preference.state.tooltip() &&
      !node.state.tp &&
      // Tooltip for search button
      (node.dom.className === 'search-button' ||
        j(node.dom)
          .find('span')
          .css('display') === 'none')
    ) {
      node.state.tp = tippy(node.dom, {
        size: 'small',
        distance: 5
      });
    } else if (node.dom.attributes.title) {
      node.dom.removeAttribute('title');
    }
  },
  onremoveMenuA: function(node) {
    if (node.state.tp) {
      node.state.tp.destroy(node.state.tp.getPopperElement(node.dom));
      delete node.state.tp;
    }
  },
  submit: function(e) {
    e.preventDefault();
    e.redraw = false;
    var q = this.state.query();
    if (_.isEmpty(q)) return;
    if (this.state.pintext()) q = this.state.pintext() + ' ' + q;
    m.route.set('/player.search?q=' + q);
  },
  clickHome: function(e) {
    if (App.Settings.mode() === 'karaoke' && !_.isEmpty(Config.defaultKaraokeChannel)) {
      e.preventDefault();
      m.route.set('/player.channel?id=' + Config.defaultKaraokeChannel);
    }
  },
  clickCollapse: function() {
    this.expand(true);
    current.updateQuality();
  },
  clickPrimeMenuButton: function() {
    this.state.showSearchInput(false);
    this.state.showMenu(!this.state.showMenu());
  },
  clickShowSearchInput: function(e, show) {
    this.state.showMenu(false);
    this.state.showSearchInput(show || !this.state.showSearchInput());
    if (this.state.showSearchInput()) {
      m.nextTick(function() {
        j('#page .search-input input[type=text]')[0].focus();
      });
    }
  },
  clickPin: function() {
    this.state.pintext(j('#page .search-input input[type=text]').val());
    this.state.query('');
  },
  clickPinRemove: function() {
    this.state.pintext(null);
  },
  keyupQuery: function(e, self) {
    e.preventDefault();
    e.redraw = false;
    self.state.query(value);
    // NOTE: Disabled `.keyCode` as it's deprecated and not standard. Hence, not working on mobile.
    // Only accepted chars and backspace (8)
    // if (!(regexKeys.test(String.fromCharCode(e.keyCode)) || e.keyCode === 8)) return;
    // Do not request with this keyCodes
    if (_.indexOf(blackKeyCodes, e.keyCode) > -1) return;
    var value = this.value;
    promSuggest.cancel();
    if (_.isEmpty(value)) return;
    promSuggest = Promise.delay(300)
      .then(function() {
        return m.jsonp({
          url: 'https://clients1.google.com/complete/search',
          data: {
            client: 'youtube',
            hl: 'en',
            gs_rn: 0,
            gs_ri: 'youtube',
            cp: 3,
            gs_id: 0,
            q: value,
            ds: 'yt'
          }
        });
      })
      .then(function(data) {
        if (data && _.isArray(data[1])) {
          SearchBar._awesomplete.list = _.map(data[1], function(item) {
            return item[0];
          });
        }
      })
      .catch(Console.error);
  },
  changeQuery: function(e, self) {
    e.preventDefault();
    e.redraw = false;
    self.state.query(this.value);
  },
  selectSuggest: function(e, self) {
    if (e.type === 'select') {
      e.preventDefault();
      e.redraw = false;
      return;
    }
    self.changeQuery.call(this, e, self);
    self.submit(e);
  },
  highlightSuggest: function(e) {
    e.preventDefault();
    e.redraw = false;
    this.value = e.text.value;
  },
  viewPrimaryMenu: function() {
    return [
      m(
        'a',
        {
          href: '#/',
          class: this.page === 'home',
          title: 'Home',
          oncreate: this.onupdateMenuA,
          onremove: this.onremoveMenuA,
          onclick: this.clickHome
        },
        m(elIcon, {
          icon: 'home'
        }),
        m('span', 'Home')
      ),
      m(
        'a',
        {
          href: '#/player.library.videos',
          class: this.page === 'videos',
          title: 'Videos',
          oncreate: this.onupdateMenuA,
          onremove: this.onremoveMenuA
        },
        m(elIcon, {
          icon: 'slideshow'
        }),
        m('span', 'Videos')
      ),
      m(
        'a',
        {
          href: '#/player.library.playlists',
          class: this.page === 'playlists',
          title: 'Playlists',
          oncreate: this.onupdateMenuA,
          onremove: this.onremoveMenuA
        },
        m(elIcon, {
          icon: 'playlist_play'
        }),
        m('span', 'Playlists')
      ),
      m(
        'a',
        {
          href: '#/player.library.channels',
          class: this.page === 'channels',
          title: 'Channels',
          oncreate: this.onupdateMenuA,
          onremove: this.onremoveMenuA
        },
        m(elIcon, {
          icon: 'account_circle'
        }),
        m('span', 'Channels')
      )
    ];
  },
  viewSearchInput: function(icon) {
    return m(
      '.search-input',
      {
        key: 'sb-input',
        class: this.state.showSearchInput() ? '' : 'hide'
      },
      m(
        'form',
        {
          onsubmit: this.submit
        },
        [
          m(
            'button',
            {
              type: 'submit'
            },
            m(elIcon, {
              icon: icon || 'search'
            })
          ),
          m(
            'div',
            m('input', {
              type: 'text',
              name: 'q',
              autocomplete: 'off',
              placeholder: 'Search',
              value: this.state.query(),
              onkeyup: this.keyupQuery,
              onchange: this.changeQuery,
              onselect: this.selectSuggest,
              onhighlight: this.highlightSuggest,
              oncreate: this.oncreateInputQuery,
              onremove: this.onremoveInputQuery
            }),
            this.state.pintext()
              ? m(
                  'a',
                  {
                    class: 'pin pintext',
                    onclick: this.clickPinRemove,
                    title: 'Remove Pin'
                  },
                  m('span', this.state.pintext())
                )
              : m(elButtonIcon, {
                  icon: 'attach_file',
                  class: 'pin',
                  title: 'Pin Search',
                  onclick: this.clickPin
                })
          )
        ]
      )
    );
  },
  view: function() {
    var isExpand = this.expand(),
      isTinyBP = App.Props.breakPointTiny();
    return m(
      '.search-bar',
      m(
        '.actions',
        {
          key: 'sb-expand'
        },
        isExpand
          ? null
          : m(elButtonIcon, {
              icon: 'navigate_next',
              class: 'panel-nav',
              onclick: this.clickCollapse,
              title: 'Expand Player View'
            })
      ),
      this.viewSearchInput(),
      m(
        'div',
        {
          class: 'actions prime-menu ' + (isTinyBP ? 'search-bar-menu' : ''),
          key: 'sb-prime-menu-' + isTinyBP
        },
        this.viewPrimaryMenu(),
        m(
          'a.search-button',
          {
            onclick: this.clickShowSearchInput,
            title: 'Search',
            oncreate: this.onupdateMenuA,
            onremove: this.onremoveMenuA
          },
          m(elIcon, {
            icon: this.state.showSearchInput() ? 'expand_less' : 'search'
          })
        )
      ),
      isExpand
        ? null
        : m(topBarMenu, {
            key: 'sb-topbar-menu'
          }),
      !App.Vars.isPopup
        ? null
        : m('div.topborder', {
            key: 'topborder'
          })
    );
  }
};

module.exports = SearchBar;
