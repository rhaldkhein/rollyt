var _ = require('lodash');
var m = require('mithril');
var ContextBase = require('./contextBase');
var comShare = require('comp/social');

function ContextSocial() {
  ContextBase.call(this);
}

ContextSocial.prototype = _.create(ContextBase.prototype, {

  show: function(element, context) {
    comShare.load();
    ContextBase.prototype.show.call(this, element, context);
  },

  // View

  viewContext: function() {
    return m('#context-social',
      (!comShare.preloaded() ? 'Loading...' : [
        m('h4', 'Like & Follow Us'),
        m(comShare.LikeFollow),
        m('h4', 'Share Us'),
        m(comShare.Share)
      ])
    );
  }

});

// Singleton controller
module.exports = new ContextSocial;