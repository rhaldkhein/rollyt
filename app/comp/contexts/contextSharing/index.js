var _ = require('lodash');
var m = require('mithril');
var util = require('libs/util');
var ContextBase = require('../contextBase');
var ModalRecon = require('comp/modals/modalRecon');
var ShareAdapter = require('./adapterShare');
var ConnectAdapter = require('./adapterConnect');

var bindMethods = ['clickShare', 'clickConnect', 'clickDisconnect', 'clickLearnIt'];

function ContextSharing() {
  ContextBase.call(this);
  _.bindAll(this, bindMethods);
  this.url = Config.url.socket_host + '/playersharing';
  this.adapter = null;
  var self = this;
  if (!App.User.isGuest()) {
    if (App.Settings.playerAutoShare()) {
      // Autoshare waits for 1 second
      setTimeout(function() {
        self.clickShare();
      }, 1000);
    } else if (App.Settings.playerShareJoinedUserId()) {
      // Autoconnect is immediately
      m.nextTick(function() {
        self.clickConnect(null, true);
      });
    }
  }
}

ContextSharing.prototype = _.create(ContextBase.prototype, {
  show: function(element, context, data) {
    if (this.adapter) this.adapter.reset();
    ContextBase.prototype.show.call(this, element, context, data);
  },

  clear: function() {
    this.adapter.dispose();
    this.adapter = null;
  },

  setupConnection: function(adapter) {
    var self = this;
    if (this.adapter) return;
    var socket = io(this.url, {
      transports: ['websocket'],
      reconnection: !util.isMobile
    });
    this.adapter = new adapter(this, socket);
    socket.on('clear_disconnect', function(err) {
      if (!err) {
        self.adapter.socket.disconnect();
        self.clear();
      }
    });
    socket.on('error', function(err) {
      if (err == 'SEUA') {
        App.Modules.toastr.error('Sign in to use Player Sharing');
        socket.destroy();
        self.clear();
      }
      m.redraw();
    });
  },

  isConnected: function() {
    return this.adapter && this.adapter.socket.connected;
  },

  isHost: function() {
    return this.adapter instanceof ShareAdapter && !!this.adapter.room;
  },

  isJoined: function() {
    return this.adapter instanceof ConnectAdapter && !!this.adapter.room && this.adapter.room.open;
  },

  // Event

  clickShare: function() {
    this.setupConnection(ShareAdapter);
  },

  clickConnect: function(e, recon) {
    this.setupConnection(ConnectAdapter);
    if (recon && !this.isJoined()) {
      // Show reconnection halt modal
      ModalRecon.show({
        title: 'Connecting to Player',
        info: 'Establishing connection to the previous player'
      });
    }
  },

  clickLearnIt: function() {
    m.route.set('/player.frame/about/help/playersharing');
    this.hide();
  },

  clickDisconnect: function(e, force) {
    if (this.adapter) {
      if (force) {
        // Immediately disconnect by client itself
        this.adapter.socket.disconnect();
        this.adapter.__clear_disconnect();
        this.clear();
      } else {
        // Gracefully disconnect by server
        this.adapter.socket.emit('clear_disconnect');
      }
    }
  },

  // View

  viewContext: function() {
    return m(
      '#context-sharing.f5',
      this.adapter
        ? this.adapter.view()
        : m(
            '.lobby',
            {
              key: 'lobby'
            },
            m('h4', 'Player Sharing'),
            m(
              '.body',
              m(
                '.mb2',
                m(
                  'button.head',
                  {
                    onclick: this.clickShare
                  },
                  'Share this Player'
                )
              ),
              m(
                '.pb1',
                m(
                  'button.head',
                  {
                    onclick: this.clickConnect
                  },
                  'Join to a Player'
                )
              ),
              m(
                '.info f7',
                'What is Player Sharing? ',
                m(
                  'a',
                  {
                    onclick: this.clickLearnIt
                  },
                  'Learn It'
                )
              ),
              m('.info f7 red', '[ BETA ]')
            )
          ),
      this.isConnected()
        ? m(
            '.disconnect',
            {
              key: 'disconnect'
            },
            m(
              'a',
              {
                onclick: this.clickDisconnect
              },
              'Disconnect'
            )
          )
        : null
    );
  }
});

// NOTE: Manual reconnection on mobile is needed for stability
if (util.isMobile) {
  // Reconnect on focus
  window.addEventListener('focus', function() {
    // Check for adapter and it should be join adapter
    if (_contextShare.adapter instanceof ConnectAdapter && !_contextShare.adapter.socket.connected) {
      // Manual reconnect now
      _contextShare.adapter.manualConnect();
    }
  });
}

// Singleton controller
var _contextShare = new ContextSharing();
module.exports = _contextShare;
App.Components.Sharing = _contextShare;
if (App.dev) App.dev.share = _contextShare;
