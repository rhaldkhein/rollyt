var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var elIcon = require('comp/elements/icon');
var elPanelTab = require('comp/elements/panelTab');
var elTextbox = require('comp/elements/textbox');
var viewKey = require('./viewKey');

var postPlay = null; // [kind, id]

var bindShareMethods = ['changeTab', 'clickUseHere'];

// Event receiver handlers for socket
function initEmits(socket, adapter) {
  _.forEach(adapter.__proto__, function(value, key) {
    if (_.startsWith(key, '__')) socket.on(key.substring(2), _.bind(value, adapter));
  });
}

function ShareAdapter(parent, socket) {
  _.bindAll(this, bindShareMethods);
  this.parent = parent;
  this.socket = socket;
  initEmits(this.socket, this);
  this.shareAdminTabCurrent = md.stream(0);
  this.room = null;
  this._dupRoom = null;
  this._editName = null;
  this._editPass = null;
  this._fetching = null;
  this.accounts = new md.Collection({
    model: App.Model.Account,
    url: '/account/list'
  });
  this.reset();
}

ShareAdapter.prototype = {
  reset: function() {
    this.shareAdminTabCurrent(0);
    this._editName = false;
    this._editPass = false;
    this._fetching = false;
    this.accounts.clear();
  },

  notifyAll: function(msg, type) {
    this.socket.emit('notify_all', msg, type);
  },

  sendError: function(errMsg, clientId) {
    if (!clientId) return;
    this.socket.emit('player_error', errMsg, clientId);
  },

  __node: function(index, sid) {
    Console.log('Node', index, sid);
  },

  __connect: function() {
    this.socket.emit('create_room');
  },

  __disconnect: function() {
    this.room = null;
    this._dupRoom = null;
    App.Modules.toastr.info('Disconnected. Player not shared.');
    m.redraw.nexttick();
  },

  __clear_disconnect: function() {
    // Nothing yet
  },

  __create_room: function(err, room) {
    if (!err) {
      this.room = room;
      this._dupRoom = false;
      App.Modules.toastr.success('Your player is now shared');
      m.redraw();
    } else if (err === 'SEDR') {
      // Duplicate room, maybe hosted in another browser
      this._dupRoom = true;
      m.redraw();
    }
  },

  __room_replaced: function() {
    this.parent.clickDisconnect(null, true);
  },

  __users_list: function(err, users) {
    if (_.isEmpty(users)) {
      this.accounts.clear();
      this._fetching = false;
      m.redraw();
      return;
    }
    var self = this;
    this.accounts
      .fetch({
        ids: _.uniq(users).join(',')
      })
      .catch(Console.error)
      .finally(function() {
        self._fetching = false;
        m.redraw();
      });
  },

  __get_project: function(id) {
    comList.broadcast(id);
  },

  __host_command: function(command, args) {
    App.do('player', command, args);
  },

  __player_add: function(kind, id, flags, clientId) {
    var self = this;
    comList
      .addById(id, kind, flags)
      .then(function() {
        if (postPlay) {
          comList.playById(postPlay[1], postPlay[0]);
          postPlay = null;
        }
      })
      .then(m.redraw)
      .catch(function() {
        self.sendError('Unable to add that item', clientId);
      });
  },

  __player_play: function(kind, id) {
    if (comList.playById(id, kind)) {
      // Item exist in list and should play shortly
      m.redraw.nexttick();
    } else {
      // Register post play
      postPlay = [kind, id];
    }
  },

  __player_remove: function(kind, id) {
    comList.removeById(id, kind);
    m.redraw.nexttick();
  },

  __player_remove_many: function(list) {
    comList.remove(list);
    m.redraw.nexttick();
  },

  __player_clear: function() {
    comList.clearFilter();
    m.redraw();
  },

  __player_skipnext: function() {
    comList.clickSkipNext();
    m.redraw();
  },

  __player_skipprev: function() {
    comList.clickSkipPrev();
    // m.redraw();
  },

  changeTab: function(e, value) {
    if (value == 'Users') {
      this._fetching = true;
      this.socket.emit('users_list');
    }
  },

  changePlayerName: function(e, value) {
    App.User.codename(value);
    return App.User.save().catch(function(err) {
      if (err.code === 'REDK') {
        App.Modules.toastr.error('Player name not available');
        err.handled = true;
      }
      throw err;
    });
  },

  changePassword: function(e, value) {
    return m.request({
      method: 'PUT',
      url: '/~rest/user/password',
      data: { pw: value }
    });
  },

  clickUseHere: function() {
    // Force use this player
    this.socket.emit('create_room', true);
  },

  viewTabUsers: function() {
    return m(
      '.users',
      this.accounts.size()
        ? m(
            'ul',
            this.accounts.map(function(item) {
              return m(
                'li',
                m(elIcon, {
                  icon: 'account_circle'
                }),
                m('span.f6 lh-copy', item.firstname())
              );
            })
          )
        : m('.pv4 tc', this._fetching ? 'Please wait...' : 'No Clients')
    );
  },

  viewTabSettings: function() {
    return m(
      '.options tl f6 lh-copy',
      m(
        '.pa2',
        m(
          '.mb2',
          m(
            'label',
            {
              for: 'playername'
            },
            'Player Name'
          ),
          m('br'),
          m(elTextbox, {
            class: 'h2',
            lock: true,
            button: 'Save',
            onclick: this.changePlayerName,
            value: App.User.codename() || this.room.name
          })
        ),
        m(
          '.mb1',
          m('input.v-mid mr2', {
            id: 'autoshare',
            name: 'autoshare',
            type: 'checkbox',
            checked: App.Settings.playerAutoShare(),
            onclick: m.withAttr('checked', App.Settings.playerAutoShare)
          }),
          m(
            'label',
            {
              for: 'autoshare',
              class: 'break'
            },
            'Auto share player'
          )
        ),
        m(
          '.mb2',
          m('input.v-mid mr2', {
            id: 'autoremove',
            name: 'autoremove',
            type: 'checkbox',
            checked: App.Settings.playerRemoveAfter(),
            onclick: m.withAttr('checked', App.Settings.playerRemoveAfter)
          }),
          m(
            'label',
            {
              for: 'autoremove',
              class: 'break'
            },
            'Auto remove played items'
          )
        )
      ),
      m('.tc f7 white bg-light-silver', 'SECURITY'),
      m(
        '.pa2',
        m(
          '.mb1',
          m('input.v-mid mr2', {
            id: 'sharekey',
            name: 'sharekey',
            type: 'checkbox',
            checked: App.Preference.state.player_sharekey(),
            onclick: m.withAttr('checked', App.Preference.state.player_sharekey)
          }),
          m(
            'label',
            {
              for: 'sharekey',
              class: 'break'
            },
            'Share Key',
            m('span.f7 silver ml2', '(Share the key to clients)')
          )
        ),
        m(
          '.mb1',
          m('input.v-mid mr2', {
            id: 'hidden',
            name: 'hidden',
            type: 'checkbox',
            checked: App.Preference.state.player_hidden(),
            onclick: m.withAttr('checked', App.Preference.state.player_hidden)
          }),
          m(
            'label',
            {
              for: 'hidden',
              class: 'break'
            },
            'Hidden',
            m('span.f7 silver ml2', '(Exclude from search result)')
          )
        ),
        m(
          '.mb2',
          m('input.v-mid mr2', {
            id: 'private',
            name: 'private',
            type: 'checkbox',
            checked: App.Preference.state.player_private(),
            onclick: m.withAttr('checked', App.Preference.state.player_private)
          }),
          m(
            'label',
            {
              for: 'private',
              class: 'break'
            },
            'Private',
            m('span.f7 silver ml2', '(Password protected)')
          )
        ),
        m(
          '.mb2',
          m(
            'label',
            {
              for: 'password'
            },
            'Password'
          ),
          m('br'),
          m(elTextbox, {
            class: 'h2',
            password: true,
            lock: true,
            button: 'Save',
            onclick: this.changePassword,
            value: '00000000'
          })
        )
      ),
      m('.tc f7 lh-copy light-red', 'Re-share to apply any changes')
    );
  },

  viewBody: function() {
    if (this._dupRoom) {
      return m(
        '.f6',
        m('.ph3 pv3 lh-copy', 'Your player is already shared on different device'),
        m(
          '.pb1',
          m(
            'button.head',
            {
              onclick: this.clickUseHere
            },
            'Use Here'
          )
        )
      );
    }
    if (this.room) {
      return m(
        elPanelTab,
        {
          tabs: ['Key', 'Users', 'Settings'],
          current: this.shareAdminTabCurrent,
          onchange: this.changeTab
        },
        viewKey.call(this),
        this.viewTabUsers(),
        this.viewTabSettings()
      );
    }
    return m(
      'div',
      m(elIcon, {
        loading: true,
        loadval: true
      }),
      m('span.ml2 f6 hl-copy v-btm pv3', 'Please wait...')
    );
  },

  view: function() {
    return m(
      '.shareadmin',
      {
        key: 'shareadmin'
      },
      m('h4', this.room ? this.room.name : App.Account.fullname().substring(0, 28)),
      m('.body', this.viewBody())
    );
  },

  dispose: function() {
    // this.parent = null;
    // this.socket = null;
  }
};

module.exports = ShareAdapter;

var comList = require('comp/player/list');
