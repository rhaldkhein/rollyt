var _ = require('lodash');
var j = require('jquery');
var m = require('mithril');
var md = require('mithril-data');
var ps = require('perfect-scrollbar');
var elIcon = require('comp/elements/icon');
var elQRScanner = require('comp/elements/qrscanner');
var viewKey = require('./viewKey');
var elTextbox = require('comp/elements/textbox');

var bindConnectMethods = [
  'add',
  'play',
  'clickSearch',
  'clickSearchMore',
  'oncreatePassword',
  'clickPassword',
  'oncreateSearchRes',
  'clickEdit'
];
var partConnectMethods = ['clickPlayerItem'];
var _sqscanloaded = false;

// Event receiver handlers for socket
function initEmits(socket, adapter) {
  _.forEach(adapter.__proto__, function(value, key) {
    if (_.startsWith(key, '__')) socket.on(key.substring(2), _.bind(value, adapter));
  });
}

function ConnectAdapter(parent, socket) {
  _.bindAll(this, bindConnectMethods, partConnectMethods);
  this.parent = parent;
  this.socket = socket;
  initEmits(this.socket, this);
  this.connkey = md.stream();
  this.playing = false;
  this.mode = null;

  this.search = {
    q: '',
    result: null,
    index: 0,
    total: 0,
    limit: 0,
    saved: false,
    working: md.stream(false),
    edit: md.stream(false)
  };

  this.passwording = md.stream(false);
  this.reset(true);
}

ConnectAdapter.prototype = {
  reset: function(hard) {
    this.connkey(undefined);
    this.passwording(false);
    this.player = null;
    this.getroomview = null;
    this.errorKey = false;
    this.errorClose = false;
    this.showkey = false;
    this.ready = false;

    this.search.q = '';
    this.search.result = null;
    this.search.index = 0;
    this.search.total = 0;
    this.search.limit = 0;
    this.search.working(false);
    this.search.edit(false);
    if (hard) {
      this.rejoinIntervalId = null;
      this.rejoinIntervalCount = 0;
    }
  },

  oncreateSearchRes: function(node) {
    ps.initialize(node.dom);
    this.loadSaved();
  },

  onupdateSearchRes: function(node) {
    ps.update(node.dom);
  },

  onremoveSearchRes: function(node) {
    ps.destroy(node.dom);
  },

  oncreatePassword: function() {
    if (!this.player.id || this.player.private === undefined) {
      // We need player info.
      // Either manual name input or saved player
      var self = this;
      m
        .request('/~rest/rooms?name=' + this.player.name + (this.player.user ? '&user=' + this.player.user : ''))
        .then(function(data) {
          if (data && data.room) {
            if (!data.room.private || (data.saved && data.samepass)) {
              self.socket.emit('join_room', 'SR.' + data.room._id);
            } else {
              // Private and not known to joiner, require password input
              // Or password is changed
              self.setPlayer(data.room.name, data.room._id, data.room.private, data.room.user);
            }
          } else {
            self.player.error = true;
          }
        })
        .catch(function() {
          self.player.error = true;
        })
        .finally(m.redraw);
    } else if (this.player.private === true) {
      // Needs password
    } else {
      // No password required
      this.socket.emit('join_room', 'SR.' + this.player.id);
    }
  },

  setPlayer: function(playerName, id, privatePlayer, playerUser) {
    // If only `playerName`, It respond a not-wanted player but only for 'Player<User>'
    this.player = {
      id: id, // Id of room
      user: playerUser, // Owner of active room
      name: playerName, // Player name, non-unique
      private: privatePlayer,
      error: false
    };
  },

  disconnect: function() {
    this.socket.disconnect();
    this.parent.clear();
  },

  // This methods must only be called by `createRejoinInterval`
  joinroom: function() {
    // Room must be closed
    if (this.room && this.room.open) {
      this.clearRejoinInterval();
      return;
    }
    // Disconnect behond threshold
    if (this.rejoinIntervalCount >= Config.playerShareRejoinTryLimit) {
      this.clearRejoinInterval();
      this.disconnect();
      return;
    }
    // Socket must be connected
    if (this.socket && !this.socket.connected) {
      this.clearRejoinInterval();
      this.manualConnect();
      return;
    }
    // Pressume that room is saved
    this.socket.emit('join_room');
    this.rejoinIntervalCount++;
  },

  clearRejoinInterval: function(disconnect) {
    clearInterval(this.rejoinIntervalId);
    this.rejoinIntervalId = null;
    this.rejoinIntervalCount = 0;
    if (disconnect) this.disconnect();
  },

  createRejoinInterval: function(initial) {
    this.clearRejoinInterval();
    this.rejoinIntervalId = setInterval(this.joinroom.bind(this), Config.playerShareRejoinInterval);
    // Trigger now, instead of waiting for first interval
    if (initial) this.joinroom();
  },

  command: function() {
    // (command, args...)
    if (!this.ready) return;
    var args = Array.prototype.slice.call(arguments);
    this.socket.emit('host_command', args.shift(), args);
    // Must return undefined value
    // return undefined;
  },

  add: function(model, flags) {
    if (!this.ready) return;
    this.socket.emit('player_add', model.getKind(), model.id(), flags);
  },

  play: function(model) {
    if (!this.ready) return;
    this.playing = true;
    this.socket.emit('player_play', model.getKind(), model.id());
  },

  remove: function(model) {
    if (!this.ready) return;
    // Safely convert fat model to only kind and id property
    if (_.isArray(model)) {
      this.socket.emit(
        'player_remove_many',
        _.map(model, function(mdl) {
          return {
            kd: mdl.getKind(),
            id: mdl.id()
          };
        })
      );
    } else {
      this.socket.emit('player_remove', model.getKind(), model.id());
    }
  },

  clear: function() {
    if (!this.ready) return;
    this.socket.emit('player_clear');
  },

  skipNext: function() {
    if (!this.ready) return;
    this.socket.emit('player_skipnext');
  },

  skipPrev: function() {
    if (!this.ready) return;
    this.socket.emit('player_skipprev');
  },

  manualConnect: function() {
    this.socket.connect();
    this.__reconnecting();
  },

  // Socket events > > >

  __node: function(index, sid) {
    Console.log('Node', index, sid);
  },

  __connect: function() {
    if (!App.Settings.playerShareJoinedUserId()) {
      m.redraw();
      return;
    }
    var self = this;
    setTimeout(function() {
      self.createRejoinInterval(true);
    }, 300);
  },

  __disconnect: function() {
    if (this.room) {
      this.room.open = false;
      comList.clear();
    }
    App.Modules.toastr.info('Disconnected');
    ModalRecon.hide(null, true);
    m.redraw.nexttick();
  },

  __reconnecting: function() {
    ModalRecon.show({
      title: 'Player Sharing',
      info: 'Reconnecting to the player'
    });
    m.redraw();
  },

  // __connect_error: function() {},

  // __connect_timeout: function() {},

  // __reconnect: function() {},

  // __reconnect_attempt: function() {},

  // __reconnect_error: function() {},

  // __reconnect_failed: function() {},

  // __error: function() {},

  // Socket events < < <

  __room_closed: function() {
    if (this.room) this.room.open = false;
    this.createRejoinInterval();
    App.Modules.toastr.error('Player has been closed');
    ModalRecon.show({
      title: 'Player Closed',
      info: 'Waiting for the player to re-open'
    });
    comList.clear();
    m.redraw();
  },

  __clear_disconnect: function(err) {
    if (!err) {
      if (this.room)
        m.nextTick(function() {
          comList.clear();
        });
      this.room = null;
      this.clearRejoinInterval();
      App.Settings.playerShareJoinedUserId(null);
    }
  },

  __join_room: function(err, room) {
    if (!err) {
      comList.clear();
      this.room = room;
      this.room.open = true;
      // Store room host userid to use when reconnecting
      App.Settings.playerShareJoinedUserId(room.user);
      this.clearRejoinInterval();
      this.socket.emit('get_project');
      ModalRecon.hide(null, true);
      // Make player ready
      comList.state.ready(true);
      comList.state.loaded(true);
      App.Modules.toastr.success('Connected to ' + room.name + ' player');
      // Freez interaction until first project is received
      m.redraw();
    } else {
      this.passwording(false);
      if (err.code == 'SENF') {
        App.Modules.toastr.error('Unable to connect. Player not found.');
        this.errorKey = true;
      } else if (err.code == 'SENA') {
        App.Modules.toastr.error('Unable to connect. Player is still closed.');
        this.errorClose = true;
      } else if (err.code == 'SEIK') {
        App.Modules.toastr.error('Unable to connect. Invalid password.');
        this.errorKey = true;
      }
      m.redraw();
    }
  },

  __project: function(player, tag) {
    // Presuming to play the added and played item
    // If so, do not project
    if (this.playing && tag !== 'playing') return;
    // Projecting
    this.playing = false;
    this.mode = player[0][9];
    if (this.mode === 'karaoke' && App.Settings.searchBarPinText() !== 'karaoke')
      App.Settings.searchBarPinText('karaoke');
    var self = this;
    comList.project(player).then(function() {
      // Only make this adapter ready on first project
      // to avoid sending data to host while loading
      // the list if items
      if (!self.ready) self.ready = true;
    });
  },

  __player_error: function(msg, header) {
    App.Modules.toastr.error(msg, header);
  },

  __notify: function(msg, type) {
    type = _.trim(type);
    if (_.includes(['error', 'info'], type) > -1) {
      var fn = App.Modules.toastr[type];
      if (fn) fn.call(App.Modules.toastr, msg);
    }
  },

  loadSaved: function() {
    this.search.index = 0;
    this.search.result = null;
    this.search.saved = true;
    this.loadSearch();
  },

  loadSearch: function() {
    var self = this;
    m
      .request(
        '/~rest/rooms/' + (this.search.saved ? 'saved' : 'list') + '?q=' + this.search.q + '&idx=' + this.search.index
      )
      .then(function(data) {
        self.search.total = data.total;
        self.search.limit = data.limit;
        if (_.isArray(data.result)) {
          self.search.result = _.concat(self.search.result || [], data.result);
        }
      })
      .catch(function() {
        self.search.result = [];
      })
      .finally(function() {
        self.search.working(false);
        m.redraw();
      });
    this.search.working(true);
  },

  getViewKey: function() {
    if (this.room) {
      return 'viewRoom';
    } else if (this.player) {
      return 'viewPassword';
    } else if (this.getroomview == 'search') {
      return 'viewSearch';
    } else if (this.getroomview == 'qrcode') {
      return 'viewQRCode';
    } else {
      return 'viewJoinOption';
    }
  },

  clickSearch: function(e, q) {
    q = _.trim(q);
    if (_.startsWith(q, '@')) {
      // Immediately proceed to player's view using non-unique player name.
      // This may respond a not-wanted player, if default eg. 'Player<User>'
      this.setPlayer(q.substring(1, q.length));
      return;
    }
    if (_.isEmpty(q)) return;
    this.search.index = 0;
    this.search.q = q;
    this.search.result = null;
    this.search.saved = false;
    this.search.edit(false);
    this.loadSearch();
  },

  clickSearchMore: function() {
    this.search.index++;
    this.loadSearch();
  },

  clickPlayerItem: function(e, self) {
    var el = j(this);
    if (self.search.edit()) {
      // Remove instead
      _.remove(self.search.result, function(n) {
        return n.user === el.data('user');
      });
      m.request({
        method: 'DELETE',
        url: '/~rest/rooms/saved',
        data: { user: el.data('user') }
      });
    } else {
      self.setPlayer(el.data('name'), el.data('pid'), el.data('priv') === 'on', el.data('user'));
    }
  },

  clickPassword: function(e, pass) {
    this.passwording(true);
    this.socket.emit('join_room', 'SR.' + this.player.id + '.' + pass);
  },

  clickEdit: function() {
    this.search.edit(!this.search.edit());
  },

  viewJoinOption: function() {
    var self = this;
    return m(
      '.getroom',
      m('h4', 'Join to Player'),
      m(
        '.body',
        !this.socket.connected
          ? m(
              '.pv3',
              m(elIcon, {
                loading: true,
                loadval: true
              }),
              m('span.ml2 f6 hl-copy v-btm', 'Please wait...')
            )
          : [
              App.Settings.playerShareJoinedUserId()
                ? m(
                    'p.f6 lh-copy pv4',
                    'Trying to join to previously',
                    m('br'),
                    'saved player. Disconnect to join',
                    m('br'),
                    'to another new player.'
                  )
                : m(
                    'div',
                    m('.f6 lh-copy', 'Select join method:'),
                    m(
                      '.scancode',
                      m(
                        'button.head',
                        {
                          onclick: function() {
                            self.getroomview = 'qrcode';
                          }
                        },
                        'Scan QR Code'
                      )
                    ),
                    m('.f6 lh-copy', 'or'),
                    m(
                      '.inputcode',
                      m(
                        'button.head',
                        {
                          onclick: function() {
                            self.getroomview = 'search';
                          }
                        },
                        'Search Player'
                      )
                    )
                  ),
              m('p.f7 lh-copy', m('em', "You're connected to server", m('br'), 'but not yet to a player'))
            ]
      )
    );
  },

  viewQRCode: function() {
    var self = this;
    return m(
      '.getroomqrcode',
      m('h4', 'Join to Player'),
      m(
        '.body',
        m(
          'p.f6 lh-copy mb1',
          {
            class: self.errorKey ? 'light-red' : ''
          },
          self.errorKey ? 'Invalid QR Code' : _sqscanloaded ? 'Scanning...' : 'Please wait...'
        ),
        m(elQRScanner, {
          speed: 500,
          onerror: function() {
            self.errorKey = true;
          },
          ontoken: function(key) {
            self.errorKey = false;
            self.connkey(key);
            self.socket.emit('join_room', _.trim(self.connkey()));
            m.redraw();
          },
          onload: function() {
            _sqscanloaded = true;
          }
        })
      )
    );
  },

  viewSearch: function() {
    return m(
      '.getroomsearch',
      m('h4', 'Search Player'),
      m(
        '.body ph2 f6',
        m('.f6 lh-copy mb1', 'Player Name:'),
        m(elTextbox, {
          class: 'h2 w-100 mb1',
          placeholder: 'Search',
          button: 'Go',
          onclick: this.clickSearch,
          working: this.search.working
        }),
        !this.search.saved || _.isEmpty(this.search.result)
          ? null
          : m(
              '.cf bb b--light-gray pv1 mb2',
              m('.fl w-75 tl', 'Saved Players'),
              m(
                '.fr w-25 tr',
                m(
                  'a.underline-hover',
                  {
                    onclick: this.clickEdit
                  },
                  this.search.edit() ? 'Done' : 'Edit'
                )
              )
            ),
        m(
          '.search-res tl relative',
          {
            class: this.search.edit() ? 'edit' : '',
            oncreate: this.oncreateSearchRes,
            onupdate: this.onupdateSearchRes,
            onremove: this.onremoveSearchRes
          },
          m(
            'div.mb2',
            this.viewSearchResult(),
            !this.search.result || (this.search.index + 1) * this.search.limit > this.search.total
              ? null
              : m(
                  '.tc mt2',
                  {
                    key: 'x-more-btn'
                  },
                  m(
                    'button',
                    {
                      onclick: this.clickSearchMore
                    },
                    'Load More'
                  )
                )
          )
        )
      )
    );
  },

  viewSearchResult: function() {
    var self = this;
    if (_.isArray(this.search.result)) {
      if (this.search.result.length > 0) {
        // Not empty
        return _.map(this.search.result, function(room) {
          return m(
            'a.db cf',
            {
              key: self.search.saved ? room.user : room._id,
              'data-pid': room._id,
              'data-user': room.user,
              'data-name': room.name,
              'data-priv': room.private ? 'on' : 'off',
              onclick: self.clickPlayerItem
            },
            m('img.w2 v-mid mr2', {
              src: room.picture
            }),
            m('span', room.name),
            m.fragment({}, [
              room.private ? m(elIcon, { icon: 'lock' }) : null,
              self.search.edit() ? m(elIcon, { class: 'red', icon: 'remove_circle' }) : null
            ])
          );
        });
      } else {
        // Empty
        return this.search.saved
          ? null
          : m(
              'p.tc gray f7 mv4',
              {
                key: 'x-not-found'
              },
              'Nothing Found'
            );
      }
    } else {
      return null;
    }
  },

  viewPassword: function() {
    return m(
      '.getroompassword',
      {
        oncreate: this.oncreatePassword
      },
      m('h4', this.player.name),
      m(
        '.body ph2 f6 lh-copy',
        this.player.error
          ? m('.pv3', 'Player Not Available')
          : !this.player.id
            ? m('.pv3', this.errorClose ? 'Player Not Available' : 'Please wait...')
            : this.player.private === true
              ? m(
                  'div',
                  m('.f6 lh-copy mb1', 'Password:'),
                  m(elTextbox, {
                    class: 'h2 w-100 mb2',
                    placeholder: 'Password',
                    password: true,
                    button: 'Go',
                    onclick: this.clickPassword,
                    working: this.passwording
                  })
                )
              : m('.pv3', this.errorKey ? 'Player Not Available' : 'Please wait...')
      )
    );
  },

  viewRoom: function() {
    var self = this;
    return m(
      '.inroom',
      m('h4', 'Connected'),
      m(
        '.body',
        m(
          'p.f6 lh-copy',
          m('span.dib mb2', "You're connected to:"),
          m('br'),
          m('img', {
            class: 'w2 v-mid mr2',
            src: this.room.picture
          }),
          m('span.fw5', this.room.name),
          m('br'),
          this.mode === 'karaoke' ? m('div.mt2 orange', 'Karaoke Mode') : null
        ),
        !this.room.token
          ? null
          : this.showkey
            ? viewKey.call(this, true)
            : m(
                'button',
                {
                  onclick: function() {
                    self.showkey = true;
                  }
                },
                'Show Key'
              )
      )
    );
  },

  view: function() {
    var key = this.getViewKey();
    return m(
      'div',
      {
        key: 'connect-' + key
      },
      this[key]()
    );
  },

  dispose: function() {
    // this.parent = null;
    // this.socket = null;
    this.mode = null;
  }
};

module.exports = ConnectAdapter;

var comList = require('comp/player/list');
var ModalRecon = require('comp/modals/modalRecon');
