var m = require('mithril');

// This function should be called with adapter context
module.exports = function(client) {
  var room = this.room;
  return m('.key',
    m('.f6 lh-copy fw5', 'QR Code:'),
    m('.qrcode',
      m('img.mv1', {
        src: 'https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=' +
          (['QC', room.private ? room._id : room.name, room.token].join('.'))
      })
    ),
    (room && !room.private ? null : m('div.pb2',
      m('.f6 lh-copy fw5 mb1', {
        key: 'label-pass'
      }, 'Password:'),
      m('textarea.strcode mid-gray', {
        key: 'input-pass',
        class: (room.password ? 'tracked-mega b' : 'f6'),
        rows: 1
      }, room.password || (client ? '<Custom Password>' : '<Your Password>'))
    ))
  );
};