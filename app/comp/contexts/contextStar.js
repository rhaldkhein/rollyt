var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var starring = require('libs/starring');
var util = require('libs/util');
var ContextBase = require('./contextBase');
var elButton = require('comp/elements/buttonIcon');
var ModalFolder = require('comp/modals/modalFolder');

var bindMethods = ['clickDone', 'clickUnstar', 'clickFolderManager'];

function ContextStar() {
	ContextBase.call(this);
	_.bindAll(this, bindMethods);
	this.callback = null;
	this.state = md.State.create({
		type: null,
		id: null,
		value: false,
		folder: ''
	});
}

ContextStar.prototype = _.create(ContextBase.prototype, {
	show: function(element, context, type, id, value, folder, fnDone) {
		this.state.type(type);
		this.state.id(id);
		this.state.value = _.isFunction(value) ? value : md.stream(value);
		this.state.folder = _.isFunction(folder) ? folder : md.stream(folder);
		this.callback = fnDone;
		ContextBase.prototype.show.call(this, element, context);
	},

	// Event

	clickUnstar: function() {
		var self = this;
		util.nextTick(function() {
			starring
				.unstar(self.state.type(), self.state.id(), null, self.state.folder())
				.then(function() {
					if (self.callback) self.callback(null);
				})
				.catch(self.callback);
		});
		this.hide();
		this.state.value(false);
	},

	clickDone: function() {
		if (!_.isEmpty(this.state.folder())) {
			var self = this;
			util.nextTick(function() {
				starring
					.star(self.state.type(), self.state.id(), null, self.state.folder())
					.then(function(data) {
						if (self.callback) self.callback(null, data);
					})
					.catch(self.callback);
			});
			this.hide();
		} else {
			if (this.callback) this.callback(null);
			this.hide();
		}
	},

	clickFolderManager: function() {
		this.hide();
		ModalFolder.show();
	},

	// View

	viewContext: function() {
		return m(
			'#context-star',
			m('.title', this.state.value() ? 'Starred' : 'Star / Bookmark'),
			m(
				'.folder nowrap',
				m('span', 'Folder:'),
				m(
					'select',
					{
						onchange: m.withAttr('value', this.state.folder),
						value: this.state.folder() || 'none'
					},
					[
						m(
							'option',
							{
								value: 'none'
							},
							'None'
						),
						App.Folder.map(function(folder) {
							return m(
								'option',
								{
									value: folder.id()
								},
								folder.name()
							);
						})
					]
				),
				m(elButton, {
					icon: 'add_box',
					onclick: this.clickFolderManager
				})
			),
			m(
				'.actions',
				m(
					'a',
					{
						class: 'button ui-button',
						onclick: this.clickUnstar
					},
					'Unstar'
				),
				m(
					'a.right',
					{
						class: 'button ui-button',
						onclick: this.clickDone
					},
					'Done'
				)
			)
		);
	}
});

// Singleton controller
module.exports = new ContextStar();
