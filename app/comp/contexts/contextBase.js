var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var j = require('jquery');
var util = require('libs/util');
var contextManager = require('./contextManager');

var bindMethods = ['hide', 'mousedown', 'mouseup', 'oncreateContext'];

function ContextBase() {
  _.bindAll(this, bindMethods);
  this.context = null;
  this.target = null;
  this.data = null;
  this._vm = md.State.create({
    mousedown: false,
    style: {},
    lastElemHeight: 0,
    lastElemWidth: 0,
  });
}

ContextBase.prototype = {

  oncreateContext: function(node) {
    // We dont need to re-position as mobile is handled by css
    if (util.isMobile) return;
    var jElem = j(node.dom);
    var jWindow = j(window);
    var rect = jElem.offsetRect();
    if (rect.top + rect.height > jWindow.height()) {
      node.dom.style.top = (parseFloat(node.dom.style.top) - rect.height - this._vm.lastElemHeight()) + 'px';
    }
    if (rect.left + rect.width > jWindow.width()) {
      var pxVal = ((parseFloat(node.dom.style.left) - rect.width) + this._vm.lastElemWidth());
      if (pxVal < 0) pxVal = 0;
      node.dom.style.left = pxVal + 'px';
      jElem.addClass('pos-left');
    }
  },

  show: function(element, context, data) {
    this.target = element;
    this.context = context;
    this.data = data;
    this._vm.mousedown(false);
    var zElement = j(element);
    var rect = zElement.offsetRect();
    this._vm.style().left = rect.left + 'px';
    this._vm.style().top = rect.top + zElement.height() + 'px';
    this._vm.lastElemWidth(rect.width);
    this._vm.lastElemHeight(rect.height);
    contextManager.activate(this);
  },

  hide: function(e) {
    // Method `canHide()` must return true.
    contextManager.deactivate(e);
  },

  canHide: function() {
    return !this._vm.mousedown();
  },

  mousedown: function(e) {
    e.redraw = false;
    this._vm.mousedown(true);
  },

  mouseup: function(e) {
    e.redraw = false;
    this._vm.mousedown(false);
  },

  view: function() {
    return m('.context', {
        style: (util.isMobile ? undefined : this._vm.style()),
        oncreate: this.oncreateContext,
        onupdate: this.oncreateContext,
        onmousedown: this.mousedown,
        onmouseup: this.mouseup,
        onmouseleave: this.mouseup
      },
      this.viewContext(),
      m('.close', {
        onclick: this.hide
      }, 'Close')
    );
  }

};

// Singleton controller
module.exports = ContextBase;