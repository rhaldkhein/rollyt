var m = require('mithril');

function ContextManager() {
  this.context = null;
}

ContextManager.prototype = {

  isActive: function() {
    return !!this.context;
  },

  activate: function(context) {
    if (context instanceof ContextBase)
      this.context = context;
  },

  deactivate: function(e) {
    if (this.context && this.context.canHide()) {
      this.context = null;
    } else if (e) {
      e.redraw = false;
    }
  },

  // View
  component: function() {
    var visible = this.isActive();
    return m('#contexts', {
        class: visible ? '' : 'hide'
      },
      visible ? this.context.view() : null
    );
  }
};

module.exports = new ContextManager();
var ContextBase = require('./contextBase');