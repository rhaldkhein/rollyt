var _ = require('lodash');
var m = require('mithril');
var j = require('jquery');
var ContextBase = require('./contextBase');

var bindMethods = ['oncreateMenu'];

function ContextMenu() {
    ContextBase.call(this);
    _.bindAll(this, bindMethods);
    this.schema = [];
    this.parser = null;
    this.currentSubmenus = [];
}

ContextMenu.prototype = _.create(ContextBase.prototype, {

    oncreateMenu: function(node) {
        var jElem = j(node.dom);
        var jWindow = j(window);
        var rect = jElem.offset();
        if (jElem.hasClass('submenu') && rect.top + rect.height > jWindow.height()) {
            //elem.style.top = (jWindow.height() - rect.height) + 'px';
        }
    },

    show: function(element, schema, context, parser, data) {
        this.schema = schema;
        this.parser = parser;
        this.currentSubmenus.splice(0, this.currentSubmenus.length);
        ContextBase.prototype.show.call(this, element, context, data);
    },

    // Event

    clickItem: function(e, item, self) {
        if (item.callback) item.callback.call(self.context, self.target, item, self.data);
        self.hide(e);
    },

    clickItemSubmenu: function(e, item, self) {
        if(_.indexOf(self.currentSubmenus, item.submenu) === -1) {
            self.currentSubmenus.push(item.submenu);
        }
    },

    // View

    viewContext: function() {
        return m('div#context-menu',
            getContextMenu(this, this.schema, true)
        );
    }

});

// Singleton controller
module.exports = new ContextMenu;

// Post import
var elIcon = require('comp/elements/icon');

function _parseItem(ctrl, item) {
    // Global parser
    if (ctrl.parser) ctrl.parser.call(ctrl.context, item, ctrl.target);
    return !item.hide ? m('li', {
        class: (item.separate ? 'sep-' + item.separate : '') + (item.class ? ' ' + item.class : '')
    }, [
        m('a', {
            onclick: _.partialRight(item.submenu ? ctrl.clickItemSubmenu : ctrl.clickItem, item, ctrl)
        }, [
            m('span', {
                class: (item.icon || item.checked) ? '' : 'transparent'
            }, m(elIcon, {
                icon: item.icon || 'check'
            })),
            m('span.name', item.name),
            m('span', {
                class: item.submenu ? 'next' : 'transparent'
            }, m(elIcon, {
                icon: 'navigate_next',
            }))
        ]),
        item.submenu ? getContextMenu(ctrl, item.submenu) : null
    ]) : null;
}

function _parseArray(ctrl, arr) {
    return _.map(arr, function(item) {
        if (_.isArray(item)) {
            return _parseArray(ctrl, item);
        } else {
            if (arr.parser) arr.parser.call(ctrl.context, item, ctrl.target);
            return _parseItem(ctrl, item);
        }
    });
}

// Recursive menu structure
function getContextMenu(ctrl, items, base) {
    return m('ul', {
            class: (base ? 'basemenu' : 'submenu' + (_.indexOf(ctrl.currentSubmenus, items) > -1 ? ' show': '')),
            oncreate: ctrl.oncreateMenu
        },
        _parseArray(ctrl, items)
    );
}