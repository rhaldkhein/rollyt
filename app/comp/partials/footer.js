var m = require('mithril');

module.exports = {
	view: function() {
		return m(
			'div#footer',
			m(
				'.cf',
				m(
					'.left',
					m(
						'.disclaimer',
						m('h3', 'Disclaimer'),
						m(
							'span',
							'We do not create or store any Contents such as Videos, Channels, Playlists and Comments on our server. All the Contents available on this Website are directly provided by Users, Google and YouTube through their Data API and Player API.'
						)
					),
					m(
						'.actions',
						m('a[href=#/player.frame/about/privacy]', 'Privacy Policy'),
						m('a[href=#/player.frame/about/terms]', 'Terms & Conditions'),
						m('a[href=#/player.frame/about/disclaimer]', 'Disclaimer'),
						m('a[href=#/player.frame/about/bugreport]', 'Bug Report'),
						m('a[href=#/player.frame/about/help]', 'Help Center'),
						m('a[href=#/player.frame/about]', 'About')
					)
				),
				m(
					'.right',
					m('br'),
					m('br'),
					m(
						'div',
						new Date().getFullYear() + ' ' + App.Info.name,
						' - Handmade with ',
						m('img[src=/static/status/footer-logo-heart.png]'),
						' on Earth'
					),
					m(
						'.subtext',
						'Icons made by ',
						m(
							'a',
							{
								href: 'http://www.google.com',
								target: '_blank'
							},
							'Google'
						),
						' from ',
						m(
							'a',
							{
								href: 'http://www.flaticon.com',
								target: '_blank'
							},
							'flaticon.com'
						),
						' and ',
						m(
							'a',
							{
								href: 'http://www.icomoon.io',
								target: '_blank'
							},
							'icomoon.io'
						)
					)
				)
			)
		);
	}
};
