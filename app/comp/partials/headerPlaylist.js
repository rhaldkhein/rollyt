var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var toast = require('toastr');
var util = require('libs/util');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var comList = require('comp/player/list');
var comMenu = require('comp/contexts/contextMenu');
var starring = require('libs/starring');
var ContextStar = require('comp/contexts/contextStar');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');
var moment = require('moment');

// var cacheModel;
var bindMethods = ['request', 'clickAdd', 'clickPlay', 'clickAddToPlaylist'];

module.exports = {
  oninit: function(node) {
    _.bindAll(this, bindMethods);
    this.clickStar = _.partialRight(this.clickStar, this);
    this.clickMoreAction = _.partialRight(this.clickMoreAction, this);
    this.options = node.attrs || {};
    this.playlistId = md.stream(m.route.param('id'));
    this.model = null;
    this.state = md.State.create({
      starred: md.stream(false),
      starFolder: null
    });
  },
  oncreate: function() {
    util.nextTick(this.request);
  },
  request: function() {
    this.model = App.Model.Playlist.create({
      _id: this.playlistId()
    });
    if (!this.model.isSaved()) {
      this.model.fetch().catch(Console.error);
    }
    if (!App.User.isGuest()) starring.check('playlist', this.model.id(), this.state.starred, this.state.starFolder);
  },
  clickStar: function(e, loading, self) {
    var that = this;
    var starred = self.state.starred;
    var type = 'playlist';
    var id = self.model.id();
    if (starred()) {
      ContextStar.show(this, self, type, id, starred, self.state.starFolder);
    } else {
      loading(true);
      starring
        .star(type, id, starred)
        .then(function() {
          loading(false);
          ContextStar.show(that, self, type, id, starred, self.state.starFolder);
        })
        .catch(Console.error)
        .finally(function() {
          loading(false);
          m.redraw();
        });
    }
  },
  clickAdd: function() {
    comList.add(this.model);
  },
  clickPlay: function() {
    comList.addAndPlay(this.model);
  },
  clickAddToPlaylist: function() {
    var self = this;
    ModalSelectLocalPlaylist.show({
      onselect: function(e, playlist) {
        playlist.addItems([
          {
            resource_type: self.model.getKind(),
            resource_id: self.model.id()
          }
        ]);
      }
    });
  },
  clickMoreAction: function(e, self) {
    comMenu.show(this, menuSchemaBase, self);
  },
  view: function() {
    var view = null;
    if (this.model && this.model.isSaved()) {
      var img = m('img', {
        src: this.model.image()
      });
      view = [
        m(
          '.upper cf relative',
          m(
            '.thumbnail fl w-40',
            m(
              'a',
              {
                onclick: this.clickPlay
              },
              m('div', [img, img, img])
            )
          ),
          m(
            '.content fr w-60 pb4',
            m(
              'h1.lh-title mr4',
              {
                class: this.model.title().length > 70 ? 'f4' : 'f3'
              },
              this.model.title()
            ),
            m('.mb2', m('.videos sub', util.numberComma(this.model.videos()) + ' videos'))
          ),
          m(
            '.published f6 lh-copy fw5 pb3 pr4 black-40 absolute right-0 bottom-0',
            'Published ' + moment(this.model.published()).fromNow()
          )
        ),
        !this.model.isSaved()
          ? null
          : m(
              '.action-wrap cf bt b--light-gray pv2 ph3',
              m(
                '.owner dib fl w-50 f3',
                m(elIcon, {
                  icon: 'account_circle'
                }),
                m(
                  'a.v-mid ml4 dib',
                  {
                    class: this.model.channel().title().length > 30 ? 'f6' : 'f5',
                    href: '#/player.channel?id=' + this.model.channel().id()
                  },
                  this.model.channel().title()
                )
              ),
              m(
                '.actions fl w-50 tr',
                m(elButtonIcon, {
                  icon: 'play_arrow',
                  onclick: this.clickPlay,
                  title: 'Play Video'
                }),
                m(elButtonIcon, {
                  icon: 'add',
                  onclick: this.clickAdd,
                  title: 'Queue'
                }),
                App.User.isGuest()
                  ? null
                  : m(elButtonIcon, {
                      icon: 'star' + (this.state.starred() ? '' : '_border'),
                      onclick: this.clickStar,
                      loading: true,
                      title: 'Star / Bookmark'
                    }),
                App.User.isGuest()
                  ? null
                  : m(elButtonIcon, {
                      icon: 'playlist_add',
                      onclick: this.clickAddToPlaylist,
                      title: 'Add to Playist'
                    }),
                m(elButtonIcon, {
                  icon: 'more_vert',
                  onclick: this.clickMoreAction
                })
              )
            )
      ];
    }
    return m('.playlist-header', view);
  }
};

var menuSchemaBase = [
  {
    name: 'Add to Home',
    icon: 'home',
    callback: function() {
      if (this.model) {
        var id = 'playlist.' + this.model.id();
        var blocks = App.Preference.state.home_blocks();
        if (!_.find(blocks, ['id', id])) {
          blocks.push({
            id: id
          });
          App.Preference.state
            .home_blocks(blocks)
            .then(function() {
              toast.success('Added to home');
            })
            .catch(function() {
              toast.error('Unable to add to home', 'Error Occurred');
            });
        }
      }
    }
  },
  {
    name: 'View on Youtube',
    icon: 'link',
    callback: function() {
      window.open('https://www.youtube.com/playlist?list=' + this.model.id(), '_blank').focus();
    }
  }
];
