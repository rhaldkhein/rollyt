var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var util = require('libs/util');
var moment = require('moment');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var comList = require('comp/player/list');
var starring = require('libs/starring');
var ContextStar = require('comp/contexts/contextStar');
var comModalVideoPlayer = require('comp/modals/videoPlayer');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');
var comMenu = require('comp/contexts/contextMenu');
var numeral = require('numeral');

var bindMethods = [
  'request',
  'clickAdd',
  'clickPlay',
  'clickThumbnail',
  'clickAddToPlaylist',
  'openInModal',
  'clickLike'
];
var partMethods = ['clickStar', 'clickMoreAction'];
var _modelid = md.stream();
var _starred = md.stream(false);

module.exports = {
  oninit: function(node) {
    _.bindAll(this, bindMethods, partMethods);
    this.options = node.attrs || {};
    // Model must exist
    this.model = this.options.model();
    this.state = md.State.create({
      starred: _starred,
      starFolder: null
    });
    if (_modelid() != m.route.param('id')) {
      _modelid(m.route.param('id'));
      this.state.starred(false);
    }
  },
  oncreate: function() {
    util.nextTick(this.request);
  },
  request: function() {
    if (this.model && !App.User.isGuest()) {
      // Get star status
      starring.check('video', this.model.id(), this.state.starred, this.state.starFolder);
      this.model.getLiked().then(m.redraw);
    }
  },
  openInModal: function(e) {
    e.preventDefault();
    comModalVideoPlayer.videoModel(this.model);
  },
  clickThumbnail: function(e) {
    e.preventDefault();
    if (App.Props.ctrlPressed()) {
      comModalVideoPlayer.videoModel(this.model);
    } else {
      comList.addAndPlay(this.model);
    }
  },
  clickStar: function(e, loading, self) {
    var that = this;
    var starred = self.state.starred;
    var type = 'video';
    var id = self.model.id();
    if (starred()) {
      ContextStar.show(this, self, type, id, starred, self.state.starFolder);
    } else {
      loading(true);
      starring
        .star(type, id, starred)
        .then(function() {
          loading(false);
          ContextStar.show(that, self, type, id, starred, self.state.starFolder);
        })
        .catch(Console.error)
        .finally(function() {
          loading(false);
          m.redraw();
        });
    }
  },
  clickAdd: function() {
    comList.add(this.model);
  },
  clickPlay: function() {
    comList.addAndPlay(this.model);
  },
  clickAddToPlaylist: function() {
    var self = this;
    ModalSelectLocalPlaylist.show({
      onselect: function(e, playlist) {
        playlist.addItems([
          {
            resource_type: self.model.getKind(),
            resource_id: self.model.id()
          }
        ]);
      }
    });
  },
  clickMoreAction: function(e, self) {
    comMenu.show(this, menuSchemaBase, self);
  },
  clickLike: function(e) {
    e.preventDefault();
    if (App.User.isGuest()) {
      m.route.set('/player.page/signin');
      return;
    }
    // Toggle liked
    var self = this;
    this.model.setLiked(!this.model.liked()).then(function() {
      if (self.model.liked()) App.Modules.toastr.success('Added to liked videos');
      else App.Modules.toastr.info('Removed from liked videos');
      m.redraw();
    });
  },
  view: function() {
    var view = null;
    if ((this.model && this.model.isSaved()) || !this.options.loading()) {
      // if (this.model && !this.options.loading()) {
      var duration = moment.duration(this.model.duration());
      view = [
        m(
          '.upper cf relative',
          m(
            '.thumbnail fl w-40',
            m(
              'a',
              {
                onclick: this.clickThumbnail
              },
              m('img', {
                src: this.model.image()
              }),
              m(
                'span.duration br2',
                moment.utc(duration.asMilliseconds()).format(duration.hours() > 0 ? 'H:mm:ss' : 'm:ss')
              )
            )
          ),
          m(
            '.content fr w-60 pb4',
            m(
              'h1.lh-title mr4',
              {
                class: this.model.title().length > 70 ? 'f4' : 'f3'
              },
              this.model.title()
            ),
            m('.mb2', m('.views sub', util.numberComma(this.model.views()) + ' views'))
          ),
          m(
            '.published f6 lh-copy fw5 pb3 pr4 black-40 absolute right-0 bottom-0',
            'Published ' + moment(this.model.published()).fromNow()
          )
        ),
        !this.model.isSaved()
          ? null
          : m(
              '.action-wrap cf bt b--light-gray pv2 ph3',
              m(
                '.owner dib fl w-50 f3',
                m(elIcon, {
                  icon: 'account_circle'
                }),
                m(
                  'a.v-mid ml4 dib',
                  {
                    class: this.model.channel().title().length > 30 ? 'f6' : 'f5',
                    href: '#/player.channel?id=' + this.model.channel().id()
                  },
                  this.model.channel().title()
                )
              ),
              m(
                '.actions fl w-50 tr',
                m(
                  'a.rating dib lh-copy',
                  {
                    onclick: this.clickLike,
                    class: this.model.liked() ? 'liked' : ''
                  },
                  m(elIcon, {
                    icon: 'thumb_up'
                  }),
                  m(
                    'span.f5 ml2 mr3 v-mid',
                    numeral(this.model.likes())
                      .format('0a')
                      .toUpperCase()
                  )
                ),
                m(elButtonIcon, {
                  icon: 'play_arrow',
                  onclick: this.clickPlay,
                  title: 'Play Video'
                }),
                m(elButtonIcon, {
                  icon: 'add',
                  onclick: this.clickAdd,
                  title: 'Queue'
                }),
                App.User.isGuest()
                  ? null
                  : m(elButtonIcon, {
                      icon: 'star' + (this.state.starred() ? '' : '_border'),
                      onclick: this.clickStar,
                      loading: true,
                      title: 'Star / Bookmark'
                    }),
                App.User.isGuest()
                  ? null
                  : m(elButtonIcon, {
                      icon: 'playlist_add',
                      onclick: this.clickAddToPlaylist,
                      title: 'Add to Playist'
                    }),
                m(elButtonIcon, {
                  icon: 'more_vert',
                  onclick: this.clickMoreAction
                })
              )
            )
      ];
    }
    return m('.video-header', view);
  }
};

var menuSchemaBase = [
  {
    id: 'add_skiplist',
    name: 'Add to Skiplist',
    icon: 'block',
    callback: function() {
      App.Modules.skiplist.add(this.model.id());
    }
  },
  {
    name: 'Play on Modal',
    icon: 'flip_to_front',
    callback: function() {
      comModalVideoPlayer.videoModel(this.model);
    }
  },
  {
    name: 'View on Youtube',
    icon: 'link',
    callback: function() {
      window.open('https://www.youtube.com/watch?v=' + this.model.id(), '_blank').focus();
    }
  }
];

menuSchemaBase.parser = function(item) {
  if (item.id === 'add_skiplist' && App.User.isGuest()) item.hide = true;
};
