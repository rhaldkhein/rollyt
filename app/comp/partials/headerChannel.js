var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var toast = require('toastr');
var util = require('libs/util');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var elTextbox = require('comp/elements/textbox');
var comFilter = require('comp/search/filter');
var comList = require('comp/player/list');
var comMenu = require('comp/contexts/contextMenu');
var starring = require('libs/starring');
var ContextStar = require('comp/contexts/contextStar');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');
var ModalConfirm = require('comp/modals/modalConfirmAction');
var numeral = require('numeral');
var moment = require('moment');

var bindMethods = [
  'request',
  'clickButtonSearch',
  'clickButtonFilter',
  'changeFilter',
  'clickVideos',
  'clickPlaylists',
  'clickAdd',
  'clickPlay',
  'clickAddToPlaylist',
  'clickSubscribe'
];

var partMethods = ['clickSubscribe', 'clickStar', 'clickMoreAction'];

var _modelid = md.stream();
var _starred = md.stream(false);

module.exports = {
  oninit: function(node) {
    _.bindAll(this, bindMethods, partMethods);
    // this.clickStar = _.partialRight(this.clickStar, this);
    // this.clickMoreAction = _.partialRight(this.clickMoreAction, this);
    this.options = node.attrs || {};
    this.error = md.stream(false);
    // States
    this.state = md.State.create({
      open: m.route.param('open'),
      currentTab: this.options.stateType,
      sort: this.options.stateSort(),
      date: this.options.stateDate(),
      starred: _starred,
      starFolder: null
    });
    if (_modelid() != m.route.param('id')) {
      _modelid(m.route.param('id'));
      this.state.starred(false);
    }
    //this.channelId = this.options.channelId;
    this.searchQuery = md.stream(m.route.param('q'));
    // Model
    this.model = this.options.model;
  },
  onremove: function() {
    // this.model = null;
  },
  oncreate: function() {
    util.nextTick(this.request);
  },
  errorImage: function() {
    this.src = App.Model.Channel.modelOptions.defaults.image;
  },
  request: function() {
    if (!App.User.isGuest()) {
      // Get star status
      starring.check('channel', this.model.id(), this.state.starred, this.state.starFolder);
      // Get subscribe status
      this.model.getSub().then(m.redraw);
    }
  },
  clickButtonSearch: function(e) {
    if (this.options.onsearch) this.options.onsearch(e, this.searchQuery(), this.state);
  },
  clickButtonFilter: function() {
    this.state.open(!this.state.open());
  },
  changeFilter: function(e, state) {
    if (this.options.onchangefilter) {
      this.state.date(state.date());
      this.state.sort(state.sort());
      this.options.onchangefilter(e, this.state);
    }
  },
  clickVideos: function(e) {
    this.state.currentTab('video');
    if (this.options.onchangetab) this.options.onchangetab(e, this.state.currentTab(), this.state);
  },
  clickPlaylists: function(e) {
    this.state.currentTab('playlist');
    if (this.options.onchangetab) this.options.onchangetab(e, this.state.currentTab(), this.state);
  },
  clickStar: function(e, loading, self) {
    var that = this;
    var starred = self.state.starred();
    var type = 'channel';
    var id = self.model.id();
    if (starred) ContextStar.show(this, self, type, id, self.state.starred, self.state.starFolder);
    if (!starred) {
      loading(true);
      starring
        .star(type, id, self.state.starred)
        .then(function() {
          loading(false);
          ContextStar.show(that, self, type, id, self.state.starred, self.state.starFolder);
        })
        .catch(Console.error)
        .finally(function() {
          loading(false);
          m.redraw();
        });
    }
  },
  clickAdd: function() {
    if (!parseInt(this.model.videos())) return;
    comList.add(this.model, {
      date: this.state.date() || undefined,
      order: this.state.sort() || 'date'
    });
  },
  clickPlay: function() {
    if (!parseInt(this.model.videos())) return;
    comList.addAndPlay(this.model, {
      date: this.state.date() || undefined,
      order: this.state.sort() || 'date'
    });
  },
  clickAddToPlaylist: function() {
    var self = this;
    ModalSelectLocalPlaylist.show({
      onselect: function(e, playlist) {
        playlist.addItems([
          {
            resource_type: self.model.getKind(),
            resource_id: self.model.id(),
            flags: {
              date: self.state.date() || undefined,
              order: self.state.sort() || 'date'
            }
          }
        ]);
      }
    });
  },
  clickMoreAction: function(e, self) {
    comMenu.show(this, menuSchemaBase, self);
  },
  clickSubscribe: function(e, self) {
    e.preventDefault();
    if (App.User.isGuest()) {
      m.route.set('/player.page/signin');
      return;
    }
    if (!self.model.subscribed()) {
      self.model
        .setSub(true)
        .catch(function() {
          App.Modules.toastr.error('Please try again later', 'Subscribe Failed');
        })
        .then(function() {
          App.Modules.toastr.success('Subscribed');
          m.redraw();
        });
    } else {
      ModalConfirm.show({
        wait: 0,
        mini: false,
        title: 'Unsubscribe',
        body: m('.ph3 pv1', m('div', 'Are you sure to unsubscribe?')),
        confirm: function() {
          self.model
            .setSub(false)
            .catch(function(err) {
              if (err.status === 404) App.Modules.toastr.error("You're already unsubscribed", 'Unsubscribe Error');
              else App.Modules.toastr.error('Please try again later', 'Unsubscribe Failed');
            })
            .then(function() {
              App.Modules.toastr.info('Unsubscribed');
              m.redraw();
            });
        }
      });
    }
  },
  viewMain: function(self) {
    return [
      m(
        '.upper',
        m(
          '.feature cf pb3 relative',
          m(
            '.banner relative z-0 bg-black-70 mb3',
            m('img.db bg-transparent', {
              src: self.model.banner()
            })
          ),
          m(
            '.avatar fl dib ml4 relative z-1 mr3 br1',
            m(
              'a',
              {
                onclick: self.clickPlay
              },
              m('img.db pa1 bg-white', {
                src: self.model.image()
              })
            )
          ),
          m('h1.f3 lh-title pt2', self.model.title()),
          m(
            'a.badge dib ph3 pv1 br2 sub-btn f6 lh-copy',
            {
              class: self.model.subscribed() ? 'subscribed' : '',
              onclick: self.clickSubscribe
            },
            self.model.subscribed() ? 'Subscribed' : 'Subscribe',
            m(
              'span.ml2 b',
              numeral(self.model.subscribers())
                .format('0.0a')
                .toUpperCase()
            )
          ),
          m(
            '.joined f6 lh-copy fw5 pb3 pr4 black-40 absolute right-0 bottom-0',
            'Joined ' + moment(self.model.published()).fromNow()
          )
        ),
        m(
          '.action-wrap cf bt b--light-gray pv2 ph3',
          m(
            '.search fl w-50',
            m(elTextbox, {
              placeholder: 'Search channel',
              icon: m(elIcon, {
                icon: 'search'
              }),
              button: 'Go',
              onclick: this.clickButtonSearch,
              value: this.searchQuery
            })
          ),
          m(
            '.actions fl w-50 tr',
            !parseInt(self.model.videos())
              ? null
              : m.fragment(
                  {
                    key: 'btnplay'
                  },
                  [
                    m(elButtonIcon, {
                      icon: 'play_arrow',
                      onclick: self.clickPlay,
                      title: 'Play Channel'
                    }),
                    m(elButtonIcon, {
                      icon: 'add',
                      onclick: self.clickAdd,
                      title: 'Queue'
                    })
                  ]
                ),
            App.User.isGuest()
              ? null
              : m.fragment(
                  {
                    key: 'btnstar'
                  },
                  [
                    m(elButtonIcon, {
                      key: 'btnstar',
                      icon: 'star' + (self.state.starred() ? '' : '_border'),
                      onclick: self.clickStar,
                      loading: true,
                      title: 'Star / Bookmark'
                    }),
                    m(elButtonIcon, {
                      key: 'btnpladd',
                      icon: 'playlist_add',
                      onclick: self.clickAddToPlaylist,
                      title: 'Add to Playlist'
                    })
                  ]
                ),
            m(elButtonIcon, {
              key: 'btnmore',
              icon: 'more_vert',
              onclick: self.clickMoreAction
            })
          )
        )
      ),
      m('.head-tab', [
        m(
          'a',
          {
            class: 'tab' + (self.state.currentTab() === 'video' ? ' selected' : ''),
            onclick: self.clickVideos
          },
          [
            m(elIcon, {
              icon: 'slideshow'
            }),
            'Videos',
            self.model.videos() > 0 ? m('span.subtext', '(' + util.numberComma(self.model.videos()) + ')') : null
          ]
        ),
        m(
          'a',
          {
            class: 'tab' + (self.state.currentTab() === 'playlist' ? ' selected' : ''),
            onclick: self.clickPlaylists
          },
          [
            m(elIcon, {
              icon: 'playlist_play'
            }),
            'Playlists',
            self.options.totalPlaylist() > 0
              ? m('span.subtext', '(' + util.numberComma(self.options.totalPlaylist()) + ')')
              : null
          ]
        ),
        m(
          'a',
          {
            class: 'more',
            onclick: self.clickButtonFilter
          },
          m(elIcon, {
            icon: 'expand_' + (self.state.open() ? 'less' : 'more')
          })
        )
      ]),
      !self.state.open()
        ? null
        : m(comFilter, {
            class: 'filter f6 lh-copy',
            date: true,
            sort: true,
            currentDate: self.state.date(),
            currentSort: self.state.sort(),
            onchange: self.changeFilter
          })
    ];
  },
  view: function() {
    var view = null;
    if (this.model && this.model.isSaved()) {
      view = this.viewMain(this);
    }
    return m('.header bb b--light-gray', view);
  }
};

var menuSchemaBase = [
  {
    name: 'Add to Home',
    icon: 'home',
    callback: function() {
      var id = 'channel.' + this.model.id() + '.' + (this.state.date() || '') + '.' + (this.state.sort() || 'date');
      var blocks = App.Preference.state.home_blocks();
      if (!_.find(blocks, ['id', id])) {
        blocks.push({
          id: id
        });
        App.Preference.state
          .home_blocks(blocks)
          .then(function() {
            toast.success('Added to home');
          })
          .catch(function() {
            // NOTE: Error is generic, but used specific for limit.
            toast.error('Maximum home blocks reached', 'Error Occurred');
          });
      }
    }
  },
  {
    name: 'View on Youtube',
    icon: 'link',
    callback: function() {
      window.open('https://www.youtube.com/channel/' + this.model.id(), '_blank').focus();
    }
  }
];
