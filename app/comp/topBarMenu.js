var _ = require('lodash');
var m = require('mithril');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var comMenu = require('./contexts/contextMenu');
var ModalFolder = require('comp/modals/modalFolder');
var ModalConfirm = require('comp/modals/modalConfirmAction');
var ContextSocial = require('comp/contexts/contextSocial');
var config = global.Config;
var GAPI = global.gapi;
var adminCount = {
  avatar: 0,
  menu: 0
};

module.exports = {
  oninit: function() {
    this.clickAccount = _.partialRight(this.clickAccount, this);
    this.clickOther = _.partialRight(this.clickOther, this);
  },
  errorAvatar: function() {
    this.src = App.Model.Channel.modelOptions.defaults.image;
  },
  clickAccount: function(e, self) {
    comMenu.show(this, [accountLoggedInSchema, accountBaseSchema], self);
    adminCount.avatar++;
  },
  clickShare: function() {
    ContextSocial.show(this);
  },
  clickOther: function(e, self) {
    comMenu.show(this, otherBaseSchema, self);
    if (SearchBar._pintext() === App.Info.name) adminCount.menu++;
  },
  view: function() {
    return m('.actions topbar-menu', [
      App.Props.breakPointTiny()
        ? null
        : [
            App.Account.isGuest()
              ? m(
                  'a.signin',
                  {
                    href: '#/player.page/signin'
                  },
                  [
                    m(elIcon, {
                      icon: 'account_box'
                    }),
                    m('span', 'Sign In')
                  ]
                )
              : m(
                  'a.avatar',
                  {
                    onclick: this.clickAccount
                  },
                  m('img', {
                    src: App.Account.avatar(),
                    onerror: this.errorAvatar
                  })
                ),
            m(elButtonIcon, {
              icon: 'share',
              onclick: this.clickShare,
              title: 'Social Media'
            })
          ],
      m(elButtonIcon, {
        icon: 'menu',
        onclick: this.clickOther
      })
    ]);
  }
};

var SearchBar = require('comp/searchBar');
var mode = require('libs/mode');

// Menu for Account
var accountLoggedInSchema = [
  {
    name: 'My Profile',
    icon: 'account_box',
    callback: function() {
      m.route.set('/player.profile?id=' + App.User.id());
    }
  },
  {
    name: 'Upload Video',
    icon: 'file_upload',
    callback: function() {
      window.open(config.url.youtube_upload, '_blank').focus();
    }
  },
  {
    id: 'admin',
    name: 'Admin Area',
    icon: 'font_download',
    callback: function() {
      m.request({
        method: 'POST',
        url: '/~admin/login'
      })
        .then(function() {
          window.location.replace('/~admin');
        })
        .catch(function() {
          App.Modules.toastr.error('Error while logging in as Admin');
        });
    }
  },
  {
    name: 'Logout',
    icon: 'exit_to_app',
    separate: 'before',
    callback: function() {
      var googleAuth = GAPI.auth2.getAuthInstance();
      googleAuth
        .signOut()
        .then(function() {
          window.localStorage.removeItem('access_token');
          if (App.Vars.msgId) return m.request('/logout/ajax');
          else window.location.replace('/logout');
        })
        .then(function() {
          if (App.Vars.msgId) App.Modules.messenger.send('app#reopen');
        })
        .catch(function() {
          App.Modules.toastr.error('Error while logging out');
        });
    }
  }
];

accountLoggedInSchema.parser = function(item) {
  if (item.id == 'admin') {
    item.hide = adminCount.avatar > 5 && adminCount.menu > 5 ? false : true;
    if (!item.hide) {
      adminCount.avatar = 0;
      adminCount.menu = 0;
    }
  }
};

var accountBaseSchema = [];

var modesSchema = [
  {
    name: 'Normal',
    // icon: 'video_label',
    callback: function() {
      mode.setMode('normal');
    }
  },
  {
    name: 'Karaoke',
    // icon: 'queue_music',
    callback: function() {
      mode.setMode('karaoke');
      // ModalConfirm.show({
      //   wait: 0,
      //   mini: false,
      //   title: 'Karaoke Mode',
      //   body: m(
      //     '.ph3 pv1',
      //     m('b', 'Karaoke mode optimizes view for karaoke and following settings are automatically adjusted.'),
      //     m(
      //       'ul',
      //       m('li', 'Expand player view'),
      //       m('li', 'Minimize queue list'),
      //       m('li', 'Disable autoplay of mix/related videos'),
      //       m('li', 'Auto remove items after play'),
      //       m('li', 'Auto pin keyword `karaoke` in search bar'),
      //       m('li', 'Auto share the player')
      //     )
      //   ),
      //   confirm: function() {
      //     mode.setMode('karaoke');
      //   }
      // });
    }
  }
];

modesSchema.parser = function(item) {
  item.checked = item.name.toLowerCase() === mode.getMode();
};
var isGuest = App.User.isGuest();
var objAcc;
if (isGuest) {
  objAcc = {
    id: 'account',
    icon: 'account_box',
    name: 'Sign In',
    callback: function() {
      m.route.set('/player.page/signin');
    }
  };
} else {
  objAcc = {
    id: 'account',
    icon: 'account_box',
    name: 'Account',
    submenu: accountLoggedInSchema
  };
}
// Menu for Settings
var otherBaseSchema = [
  objAcc,
  {
    name: 'Manage Folders',
    icon: 'folder',
    hide: isGuest,
    callback: function() {
      ModalFolder.show();
    }
  },
  {
    id: 'modes',
    name: 'Modes',
    icon: 'web',
    submenu: modesSchema
  },
  {
    name: 'Settings',
    icon: 'settings',
    hide: isGuest,
    callback: function() {
      m.route.set('/player.frame/settings');
    }
  },
  {
    id: 'share',
    name: 'Share',
    icon: 'share',
    callback: function(el) {
      setTimeout(function() {
        ContextSocial.show(el);
        m.redraw();
      }, 300);
    }
  },
  {
    name: 'Help Center',
    icon: 'help',
    separate: 'before',
    callback: function() {
      m.route.set('/player.frame/about/help');
    }
  },
  {
    name: 'Bug Report',
    icon: 'bug_report',
    callback: function() {
      m.route.set('/player.frame/about/bugreport');
    }
  },
  {
    name: 'Feedback',
    icon: 'chat',
    callback: function() {
      m.route.set('/player.frame/about/social');
    }
  },
  {
    name: 'About',
    icon: 'info',
    callback: function() {
      m.route.set('/player.frame/about');
    }
  }
];

otherBaseSchema.parser = function(item) {
  if (item.id === 'modes') item.hide = App.Props.breakPointMedium();
  else if (item.id === 'account' || item.id === 'share') item.hide = !App.Props.breakPointTiny();
};
