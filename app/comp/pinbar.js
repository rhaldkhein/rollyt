var _ = require('lodash'),
  j = require('jquery'),
  m = require('mithril'),
  md = require('mithril-data'),
  comMenu = require('comp/contexts/contextMenu'),
  toast = require('toastr'),
  comList = require('comp/player/list');

var dragSourceItem;
var dragHoverItem;
var fnErrorSave = function() {
  toast.error('Changes not saved to server', 'Error Occured');
};

// Private state
var PinBar = {
  collection: null,
  type: null,
  limit: 0,
  init: function() {
    PinBar.limit = App.PreferenceList.find({ name: 'order_channel' }).max_value();
    PinBar.collection = new md.PinCollection({
      model: App.Model.Pin,
      url: '/pin/list',
      state: {
        dragHover: false,
        dragSource: false,
        index: -1
      }
    });
  },
  remove: function() {
    PinBar.collection.clear();
  },
  create: function(node) {
    var prom, order;
    PinBar.type = node.attrs.type;
    if (PinBar.type == 'channel') {
      order = App.Preference.state.order_channel();
      prom = PinBar.collection.fetch({
        resource_type: 'youtube#channel'
      });
    } else if (PinBar.type == 'playlist') {
      order = App.Preference.state.order_playlist();
      prom = PinBar.collection.fetch({
        resource_type: 'youtube#playlist,local#playlist'
      });
    }
    if (!prom) return;
    prom
      .then(function() {
        PinBar.collection.sortByOrder(order);
        return PinBar.collection.populate();
      })
      .catch(Console.error)
      .then(m.redraw.computation());
  },
  getModelByElement: function(el) {
    var jThis = j(el);
    return PinBar.collection.get({
      resource_type: jThis.data('restype'),
      resource_id: jThis.data('resid')
    });
  },
  saveOrder: function() {
    // Adding and deleting pin also triggers saveOrder
    if (PinBar.type == 'channel') {
      return App.Preference.state.order_channel(PinBar.collection.map('_id'));
    } else if (PinBar.type == 'playlist') {
      var parts = _.partition(PinBar.collection.toArray(), function(pin) {
        return !pin.resource();
      });
      // Pin to delete (parts[0])
      _.each(parts[0], function(pin) {
        pin.destroy();
      });
      // Pin to reorder (parts[1])
      return App.Preference.state.order_playlist(
        _.map(parts[1], function(pin) {
          return pin.id();
        })
      );
    }
  },
  moveItem: function(srcModel, desModel) {
    if (!(srcModel.id() && desModel.id())) return;
    var iSrc = PinBar.collection.findIndex(['_id', srcModel.id()]);
    var iDes = PinBar.collection.findIndex(['_id', desModel.id()]);
    var models = PinBar.collection.models;
    while (iSrc < 0) {
      iSrc += models.length;
    }
    while (iDes < 0) {
      iDes += models.length;
    }
    if (iDes >= models.length) {
      var k = iDes - models.length;
      while (k-- + 1) {
        models.push(undefined);
      }
    }
    models.splice(iDes, 0, models.splice(iSrc, 1)[0]);
    PinBar.saveOrder();
  },
  dragCallback: function(e) {
    if (e.type === 'drag') {
      e.redraw = false;
      return;
    }
    var model = PinBar.getModelByElement(this);
    if (e.type === 'dragstart') {
      dragSourceItem = model;
      PinBar.collection.stateOf(dragSourceItem).dragSource(true);
      e.redraw = false;
    } else if (e.type === 'dragend') {
      if (dragHoverItem) {
        // Move the source to hover item
        PinBar.moveItem(dragSourceItem, dragHoverItem);
        PinBar.collection.stateOf(dragHoverItem).dragHover(false);
        dragHoverItem = null;
      }
      PinBar.collection.stateOf(dragSourceItem).dragSource(false);
      dragSourceItem = null;
      PinBar.collection.stateOf(model).dragHover(false);
    } else if (e.type === 'dragenter' && dragHoverItem !== model) {
      if (dragHoverItem) {
        PinBar.collection.stateOf(dragHoverItem).dragHover(false);
      }
      dragHoverItem = model;
      PinBar.collection.stateOf(dragHoverItem).dragHover(true);
    }
  },
  clickItem: function() {
    var jThis = j(this);
    var model = PinBar.collection.find({
      resource_type: jThis.data('restype'),
      resource_id: jThis.data('resid')
    });
    if (!model) return;
    comMenu.show(this, menuSchemaBase, model);
  }
};

// Public Component
module.exports = {
  addPin: function(model, flags, own) {
    // Check duplicates
    var found = PinBar.collection.find({
      resource_type: model.getKind(),
      resource_id: model.id()
    });
    if (found) {
      toast.error('Item already pinned');
      return;
    }
    // Check pin limit
    if (PinBar.collection.size() >= PinBar.limit) {
      toast.error('Maximum pin reached');
      return;
    }
    var modelPin = App.Model.Pin.create({
      resource_type: model.getKind(),
      resource_id: model.id(),
      flags: flags
    });
    // Type is guaranteed Pin model after here
    modelPin
      .populateResource(own)
      .then(function() {
        PinBar.collection.add(modelPin);
        m.redraw.nexttick();
        return modelPin.save().then(function() {
          // Save order
          return PinBar.saveOrder();
        });
      })
      .catch(function() {
        toast.error('Pin not saved on server', 'Error Occurred');
      });
  },
  component: {
    oninit: PinBar.init,
    oncreate: PinBar.create,
    onremove: PinBar.remove,
    view: function() {
      var stateSource = PinBar.collection.stateOf(dragSourceItem);
      var stateHover = PinBar.collection.stateOf(dragHoverItem);
      var dragDown = dragSourceItem && dragHoverItem && stateSource.index() < stateHover.index();
      return m(
        'div.pinbar',
        {
          class: PinBar.collection && PinBar.collection.size() ? '' : 'hide'
        },
        !PinBar.collection
          ? null
          : PinBar.collection.map(function(pin, index) {
              var resource = pin.resource();
              var state = PinBar.collection.stateOf(pin);
              var unknown = !resource.id();
              state.index(index);
              return m(
                'a',
                {
                  class:
                    'type-' +
                    resource.options.name.toLowerCase() +
                    (state.dragSource() ? ' drag-source' : '') +
                    (state.dragHover() ? (dragDown ? ' drag-spot-down' : ' drag-spot-up') : '') +
                    (unknown ? ' unknown' : ''),
                  key: pin.resource_id(),
                  'data-restype': pin.resource_type(),
                  'data-resid': pin.resource_id(),
                  onclick: PinBar.clickItem,
                  title: resource.title(),
                  draggable: true,
                  ondragstart: PinBar.dragCallback,
                  ondragenter: PinBar.dragCallback,
                  ondragend: PinBar.dragCallback,
                  ondrag: PinBar.dragCallback
                },
                m('img', {
                  src: resource.image() || resource.imageFallback()
                })
              );
            })
      );
    }
  }
};

// Menu

var itemDateCallback = function(target, item) {
  this.flags().date = item.id;
  this.save().catch(fnErrorSave);
};
var itemDateSubmenu = [
  {
    id: 'today',
    name: 'Today',
    callback: itemDateCallback
  },
  {
    id: 'yesterday',
    name: 'Yesterday',
    callback: itemDateCallback
  },
  {
    id: 'week',
    name: 'Week',
    callback: itemDateCallback
  },
  {
    id: 'month',
    name: 'Month',
    callback: itemDateCallback
  },
  {
    id: 'year',
    name: 'Year',
    callback: itemDateCallback
  },
  {
    name: 'All Time',
    callback: itemDateCallback
  }
];
itemDateSubmenu.parser = function(item) {
  var flags = this.flags();
  item.icon = flags && flags.date == item.id ? 'check' : null;
};

// - - -

var itemSortCallback = function(target, item) {
  this.flags().order = item.id;
  this.save().catch(fnErrorSave);
};
var itemSortSubmenu = [
  {
    id: 'date',
    name: 'Date',
    callback: itemSortCallback
  },
  {
    id: 'rating',
    name: 'Rating',
    callback: itemSortCallback
  },
  {
    id: 'relevance',
    name: 'Relevance',
    callback: itemSortCallback
  },
  {
    id: 'title',
    name: 'Title',
    callback: itemSortCallback
  },
  {
    id: 'viewCount',
    name: 'Views',
    callback: itemSortCallback
  }
];
itemSortSubmenu.parser = function(item) {
  var flags = this.flags();
  item.icon = flags && flags.order == item.id ? 'check' : null;
};

// - - -

var itemDurationCallback = function(target, item) {
  this.flags().duration = item.id;
  this.save().catch(fnErrorSave);
};
var itemDurationSubmenu = [
  {
    id: 'any',
    name: 'Any',
    callback: itemDurationCallback
  },
  {
    id: 'short',
    name: 'Short (< 4 mins)',
    callback: itemDurationCallback
  },
  {
    id: 'medium',
    name: 'Medium (4 to 20 mins)',
    callback: itemDurationCallback
  },
  {
    id: 'long',
    name: 'Long (> 20 mins)',
    callback: itemDurationCallback
  }
];
itemDurationSubmenu.parser = function(item) {
  var flags = this.flags();
  item.icon = flags && flags.duration == item.id ? 'check' : null;
};

var menuSchemaChannel = [
  {
    name: 'Date',
    icon: 'date_range',
    submenu: itemDateSubmenu
  },
  {
    name: 'Sort By',
    icon: 'format_line_spacing',
    submenu: itemSortSubmenu
  },
  {
    name: 'Duration', // any, long, medium, short
    icon: 'av_timer',
    separate: 'after',
    submenu: itemDurationSubmenu
  }
];
menuSchemaChannel.parser = function(item) {
  item.hide = this.resource_type() != 'youtube#channel';
};

// ---

var itemLimitCallback = function(target, item) {
  this.flags().playLimit = item.value;
  this.save().catch(fnErrorSave);
};
var itemLimitSubmenu = [
  {
    name: '1',
    value: 1,
    callback: itemLimitCallback
  },
  {
    name: '2',
    value: 2,
    callback: itemLimitCallback
  },
  {
    name: '3',
    value: 3,
    callback: itemLimitCallback
  },
  {
    name: '5',
    value: 5,
    callback: itemLimitCallback
  },
  {
    name: '10',
    value: 10,
    callback: itemLimitCallback
  },
  {
    name: 'All',
    value: 0,
    callback: itemLimitCallback
  }
];
itemLimitSubmenu.parser = function(item) {
  // item.icon = this.settings.playLimit() == item.value ? 'check' : null;
  var flags = this.flags();
  item.icon = flags && flags.playLimit == item.value ? 'check' : null;
};

// ---

var itemMaxCallback = function(target, item) {
  this.flags().itemLimit = parseInt(item.name);
  this.save().catch(fnErrorSave);
};
var itemMaxSubmenu = [
  {
    name: '5',
    callback: itemMaxCallback
  },
  {
    name: '10',
    callback: itemMaxCallback
  },
  {
    name: '12',
    callback: itemMaxCallback
  },
  {
    name: '15',
    callback: itemMaxCallback
  },
  {
    name: '20',
    callback: itemMaxCallback
  }
];
itemMaxSubmenu.parser = function(item) {
  // item.icon = this.settings.itemLimit() == parseInt(item.name) ? 'check' : null;
  var flags = this.flags();
  item.icon = flags && flags.itemLimit == parseInt(item.name) ? 'check' : null;
};

// ---

var menuSchemaFilter = [
  menuSchemaChannel,
  {
    name: 'Play Limit',
    submenu: itemLimitSubmenu
  },
  {
    id: 'shuffle',
    name: 'Shuffle',
    icon: 'shuffle',
    callback: function() {
      // Toggle shuffle
      this.flags().shuffle = !this.flags().shuffle;
      this.save().catch(fnErrorSave);
    }
  },
  {
    name: 'Max Items',
    submenu: itemMaxSubmenu
  }
];

menuSchemaFilter.parser = function(item) {
  if (item.id == 'shuffle') {
    var flags = this.flags();
    item.icon = flags && flags.shuffle ? 'check' : 'shuffle';
  }
};

// ---

var menuSchemaBase = [
  {
    id: 'play',
    name: 'Play',
    icon: 'play_arrow',
    callback: function() {
      if (this.resource_type() == 'local#playlist') {
        comList.changePlaylist(this.resource());
      } else {
        comList.addAndPlay(this.resource(), this.flags());
      }
    }
  },
  {
    id: 'add_player',
    name: 'Queue',
    separate: 'after',
    icon: 'add',
    callback: function() {
      comList.add(this.resource(), this.flags());
    }
  },
  {
    id: 'filters',
    name: 'Filters',
    icon: 'filter_list',
    separate: 'after',
    submenu: menuSchemaFilter
  },
  {
    id: 'view',
    name: 'View',
    icon: 'visibility',
    callback: function() {
      var url;
      switch (this.resource_type()) {
        case 'youtube#channel':
          url = 'channel';
          break;
        case 'youtube#playlist':
          url = 'playlist';
          break;
        case 'local#playlist':
          url = 'localplaylist';
          break;
      }
      if (url) m.route.set('/player.' + url + '?id=' + this.resource_id());
    }
  },
  {
    id: 'remove',
    name: 'Remove',
    icon: 'remove',
    callback: function() {
      this.destroy()
        .then(function() {
          // Save order
          return PinBar.saveOrder();
        })
        .catch(function() {
          toast.error('Unable to remove pinned item', 'Error Occurred');
        })
        .finally(m.redraw);
    }
  }
];
menuSchemaBase.parser = function(item) {
  var type = this.resource_type();
  var resource = this.resource();
  item.hide = !resource.id();
  if (item.id == 'play') item.name = type == 'local#playlist' ? 'Play' : 'Play';
  if (item.id == 'remove') item.hide = false;
};
