var _ = require('lodash');
var m = require('mithril');
var manager = require('./modalManager');
var elButtonIcon = require('comp/elements/buttonIcon');

var bindMethods = ['hide'];

module.exports = {

  oninit: function(node) {
    _.bindAll(this, bindMethods);
    this.options = node.attrs || {};
  },

  show: function(attrs) {
    manager.add(this, attrs);
  },

  hide: function() {
    // `this.options` hides the modal through elements inside modal
    // like buttons or any other elements.
    manager.remove(this.options ? this.__proto__ : this);
  },

  view: function(node) {
    return m('.modal-window', {
        class: this.options.mini ? 'mini' : ''
      },
      m('.modal-content',
        m('.modal-header',
          m('span.title', this.options.title || 'Untitled'),
          m(elButtonIcon, {
            icon: 'close',
            onclick: this.hide
          })
        ),
        this.viewContent(),
        node.attrs.background ? m('.background', null) : null
      )
    );
  }
  
};