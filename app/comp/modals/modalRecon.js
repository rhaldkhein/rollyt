var _ = require('lodash');
var m = require('mithril');
var comModalBase = require('./modalBase');
var viewLoadingIcon = require('comp/views/loading/icon');

var bindMethods = ['clickConfirm'];

module.exports = _.create(comModalBase, {

  oninit: function(node) {
    node.attrs.mini = true;
    node.attrs.title = node.attrs.title || 'Player Sharing';
    node.attrs.info = node.attrs.info || 'Work in progress';
    _.bindAll(this, bindMethods);
    comModalBase.oninit.call(this, node);
  },

  hide: function(e, force) {
    // Prevent hide from other elements
    // Only hide through `clickConfirm`
    if (!force && !this.options) return;
    comModalBase.hide.call(this);
  },

  clickConfirm: function() {
    App.Components.Sharing.clickDisconnect(null, true);
    this.hide();
  },

  viewContent: function() {
    return m('.confirm-action',
      m('.body tc pt3 ph2 ph3-l',
        viewLoadingIcon(),
        m('h3', 'Please wait...'),
        m('p', this.options.info)
      ),
      m('.actions tc pb3',
        m('a', {
          class: 'button ui-button',
          onclick: this.clickConfirm
        }, 'Disconnect')
      )
    );
  }

});