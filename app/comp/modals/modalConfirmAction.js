var _ = require('lodash');
var m = require('mithril');
var comModalBase = require('./modalBase');

var bindMethods = ['clickConfirm'];

module.exports = _.create(comModalBase, {
    oninit: function(node) {
        node.attrs.mini = _.isUndefined(node.attrs.mini) ? true : node.attrs.mini;
        _.bindAll(this, bindMethods);
        comModalBase.oninit.call(this, node);
        this.waitId = true;
    },

    oncreate: function(node) {
        this.waitId = setTimeout(function() {
            node.state.waitId = null;
            m.redraw();
        }, _.isUndefined(this.options.wait) ? 3000 : this.options.wait);
    },

    hide: function() {
        clearTimeout(this.waitId);
        this.waitId = null;
        comModalBase.hide.call(this);
    },

    clickConfirm: function() {
        var prom,
            self = this;
        if (this.options.confirm) prom = this.options.confirm();
        if (prom && prom.then) {
            prom.then(function() {
                self.hide();
            });
            return;
        }
        this.hide();
    },

    viewContent: function() {
        return m(
            '.confirm-action',
            m('.body', this.options.body),
            m(
                '.actions',
                this.waitId
                    ? m('span.light-silver', 'Please wait...')
                    : m(
                          'a.left',
                          {
                              class: 'button ui-button button-primary',
                              onclick: this.clickConfirm
                          },
                          'Confirm'
                      ),
                m(
                    'a.right',
                    {
                        class: 'button ui-button',
                        onclick: this.hide
                    },
                    'Cancel'
                )
            )
        );
    }
});
