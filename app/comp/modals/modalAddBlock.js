var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comModalBase = require('./modalBase');
var elPanelTab = require('comp/elements/panelTab');
var comFilter = require('comp/search/filter');
var topics = require('local/records/topic');
var localCategories = require('local/records/category');

var bindMethods = ['clickAdd', 'changeFilterSearch'];

module.exports = _.create(comModalBase, {
    oninit: function(node) {
        node.attrs.title = node.attrs.title || 'Add New Videos Block';
        comModalBase.oninit.call(this, node);
        _.bindAll(this, bindMethods);
        this.countries = _.filter(App.Resources.countryCodes, function(item) {
            return !!item['ISO3166-1-Alpha-2'];
        });
        this.state = md.State.create({
            tabCurrent: 0,
            topicMain: '/m/04rlf',
            topicSub: '',
            popularCategory: '',
            popularCountry: App.Location.country_code(),
            searchQuery: undefined,
            searchDate: undefined,
            searchOrder: undefined,
            searchCategory: '',
            searchCountry: App.Location.country_code()
        });
    },

    changeFilterSearch: function(e, vals) {
        this.state.searchDate(vals.date());
        this.state.searchOrder(vals.sort());
    },

    clickAdd: function() {
        // 0 = Popular, 1 = Search, 2 = Channel
        var id;
        switch (this.state.tabCurrent()) {
            case 0:
                id = 'topic.' + this.state.topicMain() + '.' + this.state.topicSub();
                break;
            case 1:
                id = 'popular.' + this.state.popularCategory() + '.' + this.state.popularCountry();
                break;
            case 2:
                id = 'search.' + (this.state.searchQuery() || '');
                id += '.' + (this.state.searchCategory() || '') + '.' + (this.state.searchCountry() || '');
                id += '.' + (this.state.searchDate() || '') + '.' + (this.state.searchOrder() || '');
                break;
            default:
                id = null;
        }
        if (id) {
            var blocks = App.Preference.state.home_blocks();
            if (!_.find(blocks, ['id', id])) {
                blocks.push({
                    id: id
                });
                App.Preference.state.home_blocks(blocks).catch(function() {
                    // NOTE: Error is generic, but used specific for limit.
                    App.Modules.toastr.error('Maximum home blocks reached', 'Error Occurred');
                });
                m.redraw.nexttick();
            }
        }
        this.hide();
        if (this.options && this.options.onadd) this.options.onadd();
    },

    viewContent: function() {
        return m(
            '.add-block',
            m(
                elPanelTab,
                {
                    tabs: ['Topic', 'Popular', 'Search', 'More...'],
                    current: this.state.tabCurrent
                },
                // Topic tab content
                m(
                    'div',
                    m('label', 'Topic'),
                    m(
                        'select',
                        {
                            onchange: m.withAttr('value', this.state.topicMain),
                            value: this.state.topicMain()
                        },
                        _.map(topics.get(), function(mainTopic) {
                            return m(
                                'option',
                                {
                                    value: mainTopic._id
                                },
                                mainTopic.title
                            );
                        })
                    ),
                    m('label', 'Subtopic'),
                    m(
                        'select',
                        {
                            onchange: m.withAttr('value', this.state.topicSub),
                            value: this.state.topicSub()
                        },
                        m(
                            'option',
                            {
                                value: ''
                            },
                            'All'
                        ),
                        _.map(
                            _.values(
                                topics.get({
                                    _id: this.state.topicMain() + '.sub'
                                })
                            ),
                            function(subTopic) {
                                return m(
                                    'option',
                                    {
                                        value: subTopic._id
                                    },
                                    subTopic.title
                                );
                            }
                        )
                    )
                ),
                // Popular tab content
                m(
                    'div',
                    m('label', 'Category'),
                    m(
                        'select',
                        {
                            onchange: m.withAttr('value', this.state.popularCategory)
                        },
                        m(
                            'option',
                            {
                                value: ''
                            },
                            'All Categories'
                        ),
                        _.map(localCategories.get(), function(folder) {
                            return m(
                                'option',
                                {
                                    value: folder._id
                                },
                                folder.title
                            );
                        })
                    ),
                    m('label', 'Country'),
                    m(
                        'select',
                        {
                            onchange: m.withAttr('value', this.state.popularCountry),
                            value: this.state.popularCountry()
                        },
                        m(
                            'option',
                            {
                                value: ''
                            },
                            'All Countries'
                        ),
                        _.map(this.countries, function(country) {
                            return m(
                                'option',
                                {
                                    value: country['ISO3166-1-Alpha-2']
                                },
                                country.name
                            );
                        })
                    )
                ),
                // Search tab content
                m(
                    'div',
                    m('label', 'Query'),
                    m('input', {
                        type: 'text',
                        onchange: m.withAttr('value', this.state.searchQuery)
                    }),
                    m('label', 'Category'),
                    m(
                        'select',
                        {
                            onchange: m.withAttr('value', this.state.searchCategory)
                        },
                        m(
                            'option',
                            {
                                value: ''
                            },
                            'All Categories'
                        ),
                        _.map(localCategories.get(), function(folder) {
                            return m(
                                'option',
                                {
                                    value: folder._id
                                },
                                folder.title
                            );
                        })
                    ),
                    m('label', 'Country'),
                    m(
                        'select',
                        {
                            onchange: m.withAttr('value', this.state.searchCountry),
                            value: this.state.searchCountry()
                        },
                        m(
                            'option',
                            {
                                value: ''
                            },
                            'All Countries'
                        ),
                        _.map(this.countries, function(country) {
                            return m(
                                'option',
                                {
                                    value: country['ISO3166-1-Alpha-2']
                                },
                                country.name
                            );
                        })
                    ),
                    m(comFilter, {
                        class: 'filter-options',
                        type: false,
                        date: true,
                        sort: true,
                        onchange: this.changeFilterSearch
                    })
                ),
                // Remaining... tab content
                m(
                    'div',
                    m('b', 'To add a Channel.'),
                    m('p', "Go to the channel's page and select add to home."),
                    m('b', 'To add a Playlist.'),
                    m('p', "Go to the playlist's page and select add to home.")
                )
            ),
            m(
                '.actions',
                m(
                    'a',
                    {
                        class: 'button ui-button',
                        onclick: this.clickAdd
                    },
                    'Add Block'
                )
            )
        );
    }
});
