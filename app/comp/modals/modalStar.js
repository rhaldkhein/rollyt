var _ = require('lodash');
var m = require('mithril');
var comModalBase = require('./modalBase');
var elButtonIcon = require('comp/elements/buttonIcon');

module.exports = _.create(comModalBase, {
  view: function() {
    return m(
      '.modal-window',
      {
        class: 'mini'
      },
      m('.modal-content tc br1 pt1 pb4 mid-gray', m('h1.normal', 'Starred'), m('div', 'Folders'))
    );
  }
});
