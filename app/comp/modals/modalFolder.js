var _ = require('lodash');
var m = require('mithril');
var j = require('jquery');
var comModalBase = require('./modalBase');
var elButtonIcon = require('comp/elements/buttonIcon');
var elIcon = require('comp/elements/icon');
var comMenu = require('comp/contexts/contextMenu');

var bindMethods = ['clickNewFolder'];
var partMethods = ['clickItem', 'blurTextInput'];

module.exports = _.create(comModalBase, {
  oninit: function(node) {
    node.attrs.title = node.attrs.title || 'Folder Manager';
    comModalBase.oninit.call(this, node);
    _.bindAll(this, bindMethods, partMethods);
    this.folders = App.Folder;
  },

  oncreateTextInput: function(node) {
    node.dom.focus();
    node.dom.setSelectionRange(0, node.dom.value.length);
  },

  blurTextInput: function(e, self) {
    var input = j(this);
    var model = self.folders.getByLid(input.closest('li').data('lid'));
    var state = self.folders.stateOf(model);
    var old = model.name();
    model.name(input.val());
    state.edit(false);
    // Save to store
    var cbSave = function(err) {
      if (err) {
        if (err.code && err.code == 'REAX') {
          if (model.name() !== old || !model.isSaved())
            App.Modules.toastr.error('A folder with same name already exists');
          if (model.isSaved()) {
            // Existing
            model.name(old);
            m.redraw();
          } else {
            // New
            model.name(model.name() + _.uniqueId(' '));
            model.save(cbSave).catch(Console.error);
          }
        } else {
          if (model.isSaved()) model.name(old);
          else model.remove();
          if (err.errors && err.errors[0] && err.errors[0].msg) App.Modules.toastr.error(err.errors[0].msg);
          else App.Modules.toastr.error('Not saved due to error while saving');
          m.redraw();
        }
      } else {
        self.folders.sort('name');
        m.redraw();
      }
    };
    model.save(cbSave).catch(Console.error);
  },

  keyupTextInput: function(e) {
    if (e.keyCode === 13) {
      j(this).blur();
    } else {
      e.redraw = false;
    }
  },

  clickNewFolder: function() {
    var newFolder = new App.Model.Folder({
      name: 'New Folder'
    });

    this.folders.add(newFolder);
    var state = this.folders.stateOf(newFolder);
    state.edit(true);
  },

  clickItem: function(e, self) {
    comMenu.show(this, menuSchema, self);
  },

  viewContent: function() {
    var self = this;
    return m(
      '.folder-manager',
      m(
        '.folder-tree ba b--black-10',
        m(
          'ul',
          this.folders.map(function(folder) {
            var state = self.folders.stateOf(folder);
            if (folder.isDestroying()) return null;
            return m(
              'li',
              {
                key: folder.lid(),
                'data-lid': folder.lid()
              },

              m(elIcon, {
                icon: 'folder'
              }),

              state.edit()
                ? m('input', {
                    type: 'text',
                    value: folder.name(),
                    oncreate: self.oncreateTextInput,
                    onblur: self.blurTextInput,
                    onkeyup: self.keyupTextInput
                  })
                : m(
                    'span',
                    folder.name(),
                    folder.is_public()
                      ? m(elIcon, {
                          icon: 'public'
                        })
                      : '',
                    !folder.item_count() ? null : m('span', '(' + folder.item_count() + ' items)')
                  ),
              m(elButtonIcon, {
                icon: 'more_vert',
                onclick: self.clickItem
              })
            );
          })
        )
      ),
      m(
        '.actions',
        m(
          'a',
          {
            class: 'button ui-button',
            onclick: this.clickNewFolder
          },
          'New Folder'
        )
      )
    );
  }
});

var menuSchema = [
  {
    name: 'Rename',
    icon: 'mode_edit',
    callback: function(target) {
      var model = this.folders.getByLid(
        j(target)
          .closest('li')
          .data('lid')
      );
      var state = this.folders.stateOf(model);
      state.edit(true);
    }
  },
  {
    id: 'public',
    name: 'Public',
    icon: 'public',
    callback: function(target) {
      var model = this.folders.getByLid(
        j(target)
          .closest('li')
          .data('lid')
      );
      model.is_public(!model.is_public());
      model.save().catch(Console.error);
    }
  },
  {
    name: 'Count Items',
    callback: function(target) {
      var model = this.folders.getByLid(
        j(target)
          .closest('li')
          .data('lid')
      );
      model
        .updateItemCount()
        .catch(Console.error)
        .then(function() {
          if (!model.item_count()) App.Modules.toastr.error('Folder has no item');
        })
        .finally(m.redraw);
    }
  },
  {
    name: 'Delete',
    icon: 'delete',
    separate: 'before',
    callback: function(target) {
      var model = this.folders.getByLid(
        j(target)
          .closest('li')
          .data('lid')
      );
      model
        .destroy()
        .catch(function() {
          App.Modules.toastr.error('Error while deleting folder');
        })
        .finally(m.redraw);
    }
  }
];

menuSchema.parser = function(item, elem) {
  if (item.id === 'public') {
    var model = this.folders.getByLid(
      j(elem)
        .closest('li')
        .data('lid')
    );
    item.checked = model.is_public();
    item.icon = model.is_public() ? null : 'public';
  }
};
