var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var j = require('jquery');
var toast = require('toastr');
var elIcon = require('comp/elements/icon');
var comModalBase = require('./modalBase');
var viewErrorParam = require('comp/views/errors/errorParam');

var bindMethods = ['submitForm'];

module.exports = _.create(comModalBase, {
  oninit: function(node) {
    node.attrs.title = node.attrs.title || 'Create Rollyt Playlist';
    comModalBase.oninit.call(this, node);
    _.bindAll(this, bindMethods);
    this.playlist = this.options.playlist || App.Model.LocalPlaylist.create();
    var folder = this.playlist.folder();
    this.state = md.State.create({
      folder_id: this.options.folder || (folder ? folder.id() : null) || 'none',
      loading: false,
      errors: []
    });
  },

  oncreateInputTitle: function(node) {
    setTimeout(function() {
      node.dom.focus();
    }, 300);
  },

  show: function(attrs) {
    if (!App.User.isGuest()) comModalBase.show.call(this, attrs);
    else m.route.set('/player.page/signin');
  },

  hide: function() {
    if (this.playlist && !this.playlist.isSaved()) this.playlist.remove();
    comModalBase.hide.call(this);
  },

  submitForm: function(e) {
    e.preventDefault();
    var self = this;
    if (this.state.loading()) return;
    var data = m.parseQueryString(j(e.target).serializeEncode());
    var _folder = this.state.folder_id();
    data.flags = this.options.flags;
    data.folder = _folder && _folder !== 'none' ? _folder : '';
    data = _.omitBy(data, _.isNil);
    this.playlist.setObject(data);
    this.state.loading(true);
    this.playlist
      .save({
        data: {
          items: this.options.data
        }
      })
      .then(function() {
        self.hide();
        if (self.options.ondone) process.nextTick(self.options.ondone, self.playlist);
      })
      .catch(function(err) {
        if (_.isArray(err.errors)) self.state.errors(err.errors);
        toast.error('Unable to create playlist', 'Error Occurred');
      })
      .finally(function() {
        self.state.loading(false);
        m.redraw.nexttick();
      });
  },

  viewContent: function() {
    return m('.create-playlist', [
      m(
        'form',
        {
          onsubmit: this.submitForm
        },
        [
          m(
            '.fields',
            m(
              'label',
              {
                for: 'title'
              },
              'Title'
            ),
            m('input', {
              id: 'title',
              name: 'title',
              type: 'text',
              value: this.playlist.title() || '',
              onchange: m.withAttr('value', this.playlist.title),
              oncreate: this.oncreateInputTitle
            }),
            viewErrorParam(this.state.errors(), 'title'),
            m(
              'label',
              {
                for: 'folder'
              },
              'Folder'
            ),
            m(
              'select',
              {
                id: 'folder',
                name: 'folder',
                onchange: m.withAttr('value', this.state.folder_id),
                value: this.state.folder_id()
              },
              [
                m(
                  'option',
                  {
                    value: 'none'
                  },
                  'None'
                ),
                App.Folder.map(function(folder) {
                  return m(
                    'option',
                    {
                      value: folder.id()
                    },
                    folder.name()
                  );
                })
              ]
            ),
            viewErrorParam(this.state.errors(), 'folder'),
            m(
              '.field',
              m('input', {
                id: 'is_private',
                name: 'is_private',
                type: 'checkbox',
                checked: this.playlist.is_private(),
                onclick: m.withAttr('checked', this.playlist.is_private)
              }),
              m(
                'label',
                {
                  for: 'is_private',
                  class: 'break'
                },
                'Private (',
                m(elIcon, {
                  icon: 'lock'
                }),
                ')'
              )
            )
          ),
          m('.actions', [
            m(
              'button',
              {
                class: 'button ui-button mr2',
                type: 'submit'
              },
              this.playlist.isSaved() ? 'Done' : 'Create'
            ),
            !this.state.loading()
              ? null
              : m(elIcon, {
                  loading: this.state.loading
                })
          ])
        ]
      )
    ]);
  }
});
