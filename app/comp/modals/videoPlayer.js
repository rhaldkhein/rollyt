var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var j = require('jquery');
var moment = require('moment');
var enquire = require('enquire.js');
var ps = require('perfect-scrollbar');
var menu = require('comp/contexts/contextMenu');
var util = require('libs/util');
var ControlRelated = require('libs/controlRelated');
var comVideoPlayer = require('comp/player/videoPlayer');
var ContextStar = require('comp/contexts/contextStar');
var starring = require('libs/starring');
var elIcon = require('comp/elements/icon');
var elButtonState = require('comp/elements/buttonState');
var elButtonIcon = require('comp/elements/buttonIcon');
var elAnimation = require('comp/elements/animation');
var comMenu = require('comp/contexts/contextMenu');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');

// Reference later
var comList;

var videoModel = App.Props.modalVideoId;
var isColumnView = md.stream(false);
var _controlRelated;

var bindMethods = [
  'clickBackground',
  'clickLoadMore',
  'clickSkipNext',
  'clickSkipPrev',
  'response',
  'request',
  'clickAdd',
  'clickPlay',
  'endedVideo'
];

var partialMethods = ['clickThumbnail', 'clickStar', 'clickRelatedItemMenu'];

enquire.register('screen and (min-width:' + 1200 * App.Vars.cssPx + 'rem)', {
  match: function() {
    isColumnView(true);
    m.redraw();
  },
  unmatch: function() {
    isColumnView(false);
    m.redraw();
  }
});

module.exports = {
  videoModel: videoModel,

  // Hooks
  oninit: function() {
    _.bindAll(this, bindMethods, partialMethods);
    this.state = md.State.create({
      requesting: true,
      // firstRequest: true,
      shuffle: ['off', 'on'],
      starred: md.stream(false),
      starFolder: null
    });
    this.controlRelated = _controlRelated || (_controlRelated = new ControlRelated());
    this.controlRelated.clear();
    this.controlRelated.on('request', this.request);
    this.controlRelated.on('response', this.response);
    this.computation = null;
    if (!comList) {
      comList = require('comp/player/list');
    }
  },
  onremove: function() {
    // Cutting references
    this.controlRelated.clear();
    this.state = null;
  },
  oncreate: function(node) {
    this.lastVideoId = '';
    this.onupdate(node);
  },
  onbeforeupdate: function() {
    if (this.isNewModel()) {
      this.state.requesting(true);
      this.state.starred(false);
    }
  },
  onupdate: function() {
    if (this.isNewModel()) {
      var model = videoModel();
      this.lastVideoId = model.id();
      this.controlRelated.setVideo(model, true);
      if (!App.User.isGuest()) starring.check('video', model.id(), this.state.starred, this.state.starFolder);
    }
  },
  oncreateModalVideoPlayer: function(node) {
    if (!isColumnView()) ps.initialize(node.dom);
  },
  onupdateModalVideoPlayer: function(node) {
    if (!isColumnView()) {
      if (j(node.dom).hasClass('ps-container')) {
        ps.update(node.dom);
      } else {
        ps.initialize(node.dom);
      }
    } else if (j(node.dom).hasClass('ps-container')) {
      ps.destroy(node.dom);
    }
  },
  oncreateRelatedResult: function(node) {
    if (isColumnView()) ps.initialize(node.dom);
  },
  onupdateRelatedResult: function(node) {
    if (isColumnView()) {
      if (j(node.dom).hasClass('ps-container')) {
        ps.update(node.dom);
      } else {
        ps.initialize(node.dom);
      }
    } else if (j(node.dom).hasClass('ps-container')) {
      ps.destroy(node.dom);
    }
  },

  // Methods
  request: function() {
    this.state.requesting(true);
    this.computation = m.redraw.computation();
  },
  response: function() {
    this.state.requesting(false);
    // this.state.firstRequest(false);
    if (this.computation) {
      this.computation();
      this.computation = null;
    }
  },
  isNewModel: function() {
    var model = videoModel();
    if (model && this.lastVideoId !== model.id()) return true;
    return false;
  },

  // Events
  clickAdd: function() {
    var model = videoModel();
    if (model) {
      comList.add(model);
    }
  },
  clickPlay: function() {
    var model = videoModel();
    if (model) {
      comList.addAndPlay(model);
    }
  },
  clickAddToPlaylist: function() {
    var model = videoModel();
    if (model) {
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems([
            {
              resource_type: model.getKind(),
              resource_id: model.id()
            }
          ]);
        }
      });
    }
  },
  clickStar: function(e, loading, self) {
    var that = this;
    var starred = self.state.starred;
    var type = 'video';
    var model = videoModel();
    var id = model.id();
    if (starred()) {
      ContextStar.show(this, self, type, id, starred, self.state.starFolder);
    } else {
      loading(true);
      starring
        .star(type, id, starred)
        .then(function() {
          ContextStar.show(that, self, type, id, starred, self.state.starFolder);
        })
        .catch(Console.error)
        .finally(function() {
          loading(false);
          m.redraw();
        });
    }
  },
  clickBackground: function(e) {
    e.preventDefault();
    videoModel(null);
  },
  clickThumbnail: function(e, self) {
    e.preventDefault();
    var id = j(this)
        .closest('li')
        .data('id'),
      model = videoModel();
    if (!model || model.id() !== id) {
      videoModel(self.controlRelated.relatedCollection.get(id));
      self.controlRelated.nextVideoCollection.clear();
      self.controlRelated.clearRelated();
    }
  },
  clickLoadMore: function(e) {
    e.preventDefault();
    this.controlRelated.requestRelated();
  },
  clickClose: function(e) {
    e.preventDefault();
    videoModel(null);
  },
  clickSkipNext: function(e) {
    if (e) e.preventDefault();
    this.controlRelated.shuffle(this.state.shuffle().current);
    videoModel(this.controlRelated.getNextVideo());
    this.controlRelated.clearRelated();
  },
  clickSkipPrev: function(e) {
    e.preventDefault();
    var model = this.controlRelated.getPrevVideo();
    if (model) {
      videoModel(model);
      this.controlRelated.clearRelated();
    } else {
      e.redraw = false;
    }
  },
  clickActionMore: function() {
    menu.show(this, menuSchemaRelatedControl);
  },
  clickLink: function(e) {
    videoModel(null);
    if (e.target.getAttribute('for') !== 'channel') {
      e.preventDefault();
      m.route.set(
        '/player.video?id=' +
          j(this)
            .closest('li')
            .data('id')
      );
      return false;
    }
  },
  clickRelatedItemMenu: function(e, self) {
    comMenu.show(this, menuSchemaRelatedItem, self);
  },
  endedVideo: function(initial) {
    if (!initial) this.clickSkipNext();
  },
  // View
  view: function() {
    var self = this;
    // Video model
    var model = videoModel();
    // Return view
    return m('#modal-video', [
      m(
        '.background',
        {
          onclick: self.clickBackground
        },
        null
      ),
      m(
        '.modal-video-player',
        {
          oncreate: self.oncreateModalVideoPlayer,
          onupdate: self.onupdateModalVideoPlayer
        },
        [
          m('.container-player', [
            m(comVideoPlayer, {
              iframeId: 'iframe-modal-video',
              videoId: model.id(),
              onended: self.endedVideo,
              autoplay: true
            }),
            m('.video-details', [
              m(
                'h1.title',
                m(
                  'a',
                  {
                    href: '#/player.video?id=' + model.id()
                  },
                  model.title()
                )
              ),
              m(
                '.channel',
                m(
                  'a',
                  {
                    href: '#/player.channel?id=' + model.channel().id()
                  },
                  model.channel().title()
                )
              ),
              m('.bar', [
                m('.left', [
                  m('span.views', [
                    m(elIcon, {
                      icon: 'visibility'
                    }),
                    util.numberComma(model.views()) + ' views'
                  ]),
                  m('span.likes', [
                    m(elIcon, {
                      icon: 'thumb_up'
                    }),
                    util.numberComma(model.likes()) + ' likes'
                  ])
                ]),
                m('.actions', [
                  m(elButtonIcon, {
                    icon: 'play_arrow',
                    onclick: self.clickPlay,
                    title: 'Play'
                  }),
                  m(elButtonIcon, {
                    icon: 'add',
                    onclick: self.clickAdd,
                    title: 'Queue'
                  }),
                  App.User.isGuest()
                    ? null
                    : m(elButtonIcon, {
                        icon: 'star' + (self.state.starred() ? '' : '_border'),
                        onclick: self.clickStar,
                        loading: true,
                        title: 'Star / Bookmark'
                      }),
                  App.User.isGuest()
                    ? null
                    : m(elButtonIcon, {
                        icon: 'playlist_add',
                        onclick: self.clickAddToPlaylist,
                        title: 'Add to Playlist'
                      })
                ])
              ]),
              m('.published', m('p', 'Published ' + moment(model.published()).fromNow())),
              m('.description', model.description()),
              m(
                '.descmore',
                m(
                  'a',
                  {
                    href: '#/player.video?type=details&expand=1&id=' + model.id()
                  },
                  'Full Description'
                )
              )
            ])
          ]),
          m('.related', [
            m('.modal-actions', [
              m(elButtonIcon, {
                icon: 'skip_previous',
                onclick: self.clickSkipPrev
              }),
              m(elButtonIcon, {
                icon: 'skip_next',
                onclick: self.clickSkipNext
              }),
              m(elButtonState, {
                icon: 'shuffle',
                state: self.state.shuffle()
              }),
              m(elButtonIcon, {
                icon: 'more_vert',
                onclick: self.clickActionMore
              }),
              m(elButtonIcon, {
                class: 'close',
                icon: 'close',
                onclick: self.clickClose
              })
            ]),
            m(
              '.related-result',
              {
                oncreate: self.oncreateRelatedResult,
                onupdate: self.onupdateRelatedResult
              },
              [
                m(
                  'ul',
                  self.controlRelated.relatedCollection.map(function(modelRelated) {
                    return m(
                      'li',
                      {
                        key: modelRelated.id(),
                        'data-id': modelRelated.id()
                      },
                      [
                        m('div.thumbnail', [
                          m(
                            'a',
                            {
                              onclick: self.clickThumbnail
                            },
                            m('img', {
                              src: modelRelated.image()
                            })
                          ),
                          m(elButtonIcon, {
                            icon: 'more_vert',
                            onclick: self.clickRelatedItemMenu
                          })
                        ]),
                        m(
                          'div.content',
                          {
                            onclick: self.clickLink
                          },
                          [
                            m('a.title', modelRelated.title()),
                            m(
                              'div.channel',
                              m(
                                'a',
                                {
                                  for: 'channel',
                                  href: '#/player.channel?id=' + modelRelated.channel().id()
                                },
                                modelRelated.channel().title()
                              )
                            ),
                            m('div.type', [
                              m(elIcon, {
                                icon: 'slideshow'
                              }),
                              m('span', moment.duration(modelRelated.duration()).humanize())
                            ]),
                            m('div.details', util.numberComma(modelRelated.views()) + ' views')
                          ]
                        )
                      ]
                    );
                  })
                ),
                m('.related-loadmore', [
                  self.state.requesting()
                    ? m(
                        'div.loading',
                        m(elAnimation, {
                          parent: 'spinner',
                          child: 'bounce',
                          count: 3
                        })
                      )
                    : null,
                  self.state.requesting()
                    ? null
                    : m(
                        'a.button ui-button',
                        {
                          onclick: self.clickLoadMore
                        },
                        'Load More'
                      )
                ])
              ]
            )
          ])
        ]
      )
    ]);
  }
};

// Context menu schema
var menuSchemaRelatedControl = [
  {
    name: 'Play related in player',
    icon: 'play_arrow',
    callback: function() {
      var modelVideo = videoModel();
      if (modelVideo && modelVideo.isSaved()) {
        var modelList = App.Model.List.create({
          _id: 'related.' + modelVideo.id()
        });
        comList.addAndPlay(modelList);
      }
    }
  },
  {
    name: 'Add related to player',
    icon: 'add',
    callback: function() {
      var modelVideo = videoModel();
      if (modelVideo && modelVideo.isSaved()) {
        comList.add(
          App.Model.List.create({
            _id: 'related.' + modelVideo.id()
          })
        );
      }
    }
  }
];

var menuSchemaRelatedItem = [
  {
    name: 'Play',
    icon: 'play_arrow',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .data('id');
      comList.addAndPlay(this.controlRelated.relatedCollection.get(id));
    }
  },
  {
    name: 'Queue',
    icon: 'add',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .data('id');
      comList.add(this.controlRelated.relatedCollection.get(id));
    }
  },
  {
    name: 'Add to Playlist',
    icon: 'playlist_add',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .data('id');
      var model = this.controlRelated.relatedCollection.get(id);
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems([
            {
              resource_type: model.getKind(),
              resource_id: model.id()
            }
          ]);
        }
      });
    }
  }
];

menuSchemaRelatedItem.parser = function(item) {
  if (item.name === 'Add to Playlist') {
    item.hide = App.User.isGuest();
  }
};
