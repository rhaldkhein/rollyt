var _ = require('lodash');
var m = require('mithril');
var comModalBase = require('./modalBase');

function ModalTextInput(options) {
	comModalBase.call(this, _.defaults(options, {
		title: 'Input'
	}));
}

ModalTextInput.prototype = _.create(comModalBase.prototype, {
	viewContent: function() {
		return m('.modal-input', [
			m('p', this.options.message)
		]);
	}
});

module.exports = ModalTextInput;