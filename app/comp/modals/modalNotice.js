var _ = require('lodash');
var m = require('mithril');
var comModalBase = require('./modalBase');

var bindMethods = ['clickConfirm'];

module.exports = _.create(comModalBase, {

    oninit: function(node) {
        node.attrs.mini = _.isUndefined(node.attrs.mini) ? true : node.attrs.mini;
        _.bindAll(this, bindMethods);
        comModalBase.oninit.call(this, node);
    },

    hide: function() {
        comModalBase.hide.call(this);
    },

    clickConfirm: function() {
        this.hide();
    },

    viewContent: function() {
        return m('.confirm-action',
            m('.body', this.options.body),
            m('.actions',
                m('a.left', {
                    class: 'button ui-button',
                    onclick: this.clickConfirm
                }, 'Ok')
            )
        );
    }

});