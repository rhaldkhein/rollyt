var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var elIcon = require('comp/elements/icon');
var comModalSelectBase = require('./modalSelectBase');

module.exports = _.create(comModalSelectBase, {
  oninit: function(node) {
    node.attrs.title = node.attrs.title || 'Select Rollyt Playlist';
    comModalSelectBase.oninit.call(this, node);
    this.collection = new md.Collection({
      model: App.Model.LocalPlaylist,
      url: '/localplaylist/list/profile'
    });
    this.limit(8);
  },

  show: function(attrs) {
    if (!App.User.isGuest()) comModalSelectBase.show.call(this, attrs);
    else m.route.set('/player.page/signin');
  },

  load: function() {
    var self = this;
    if (this.loading) return;
    this.collection
      .fetch(
        {
          limit: self.limit(),
          page: self.page() + 1
        },
        {
          path: 'results'
        },
        function(err, res) {
          self.loaded = true;
          self.loading = false;
          if (!err) {
            self.total(res.total);
            self.limit(res.query.limit);
            self.page(res.query.page);
            self.showmore = self.page() * self.limit() < self.total();
          }
          m.redraw();
        }
      )
      .catch(function() {
        App.Modules.toastr.error('Error while retrieving your playlists');
      });
    comModalSelectBase.load.call(this);
  },

  viewItem: function(item) {
    return m(
      'li',
      m(
        'a',
        {
          onclick: _.partialRight(this.selectItem, item)
        },
        m('img', {
          src: item.image() || item.imageFallback()
        }),
        m(
          'div',
          m('.title', item.title()),
          m(
            '.details',
            item.item_count() + ' items ',
            !item.is_private()
              ? null
              : m(elIcon, {
                  icon: 'lock'
                })
          )
        )
      )
    );
  },

  viewEmpty: function() {
    var self = this;
    return m(
      'p.tc mv4 gray',
      m('.mb4 pt2', 'No Rollyt Playlist'),
      m(
        'button.mid-gray',
        {
          onclick: function() {
            App.do('createLocalPlaylist', function() {
              self.page(0);
              self.load();
            });
          }
        },
        'Create One'
      )
    );
  }
});
