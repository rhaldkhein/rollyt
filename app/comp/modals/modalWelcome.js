var _ = require('lodash');
var m = require('mithril');
var comModalBase = require('./modalBase');
var elButtonIcon = require('comp/elements/buttonIcon');

module.exports = _.create(comModalBase, {

  oninit: function(node) {
    comModalBase.oninit.call(this, node);
  },

  show: function() {
    if (App.Settings.showWelcome()) comModalBase.show.call(this);
  },

  clickClose: function() {
    App.Settings.showWelcome(false);
    comModalBase.hide.call(this);
  },

  clickLearnMore: function(e) {
    if (e) e.preventDefault();
    m.route.set('/player.frame/about');
    this.clickClose();
  },

  view: function() {
    return m('.modal-window', {
        class: 'mini'
      },
      m('.modal-content modal-bg1 tc br2 ph3 ph4-l pt4 pb3 mid-gray',
        m(elButtonIcon, {
          class: 'absolute top-0 right-0 w2 h2 f4 gray grow',
          icon: 'close',
          onclick: this.clickClose.bind(this)
        }),
        m('img', {
          class: 'w3 w4-l mt3',
          src: '/static/images/icons/dark/android-chrome-192x192.png'
        }),
        m('h2.lh-title mb4', 'Welcome to Rollyt!'),
        m('p.lh-copy', 'A feature-rich media player for ', m('span', {
          style: 'border-bottom: 0.1rem solid #cc181e;'
        }, 'YouTube'), ' that provides an all-new experience of playing Youtube videos.'),
        m('.mh3 mt3 mb4',
          m('a.f5 blue underline-hover v-mid', {
            onclick: this.clickLearnMore.bind(this)
          }, 'Learn More'),
          m('span.v-mid mh4', ' | '),
          m('a.f5 mt4 blue underline-hover v-mid', {
            onclick: this.clickClose.bind(this)
          }, 'Ok, I got it!')
        )
      )
    );
  }

});