var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var ps = require('perfect-scrollbar');
var comModalBase = require('./modalBase');
var viewLoadingIcon = require('comp/views/loading/icon');
var elTextbox = require('comp/elements/textbox');

var bindMethods = ['load', 'selectItem', 'clickCreate'];

module.exports = _.create(comModalBase, {
    oninit: function(node) {
        node.attrs.title = node.attrs.title || 'Select';
        comModalBase.oninit.call(this, node);
        _.bindAll(this, bindMethods);
        md.State.assign(this, {
            total: 0,
            limit: 0,
            page: 0,
            creating: false
        });
        this.collection = null;
        this.loaded = false;
        this.loading = false;
    },

    oncreate: function() {
        m.nextTick(this.load);
    },

    oncreateItemsContainer: function(node) {
        ps.initialize(node.dom);
    },

    onupdateItemsContainer: function(node) {
        ps.update(node.dom);
    },

    load: function() {
        // To overload by child
        this.loading = true;
    },

    selectItem: function(e, item) {
        this.hide();
        if (this.options.onselect) this.options.onselect(e, item);
    },

    clickCreate: function(e, value) {
        var self = this;
        var playlist = App.Model.LocalPlaylist.create();
        playlist.setObject({ title: value, folder: '', is_private: 'on' });
        return playlist.save().then(function(pl) {
            self.selectItem(null, pl);
        });
    },

    viewItem: function() {
        // To override by child
    },

    viewEmpty: function() {
        return m('p.tc mv4 gray', 'Empty');
    },

    viewList: function() {
        var self = this;
        return [
            m(
                'ul',
                this.collection.map(function(item) {
                    return self.viewItem(item);
                })
            ),
            !this.loaded
                ? viewLoadingIcon()
                : m.fragment({}, [
                      !this.showmore
                          ? null
                          : m(
                                '.tc mb3 mt2',
                                m(
                                    'a',
                                    {
                                        class: 'button ui-button',
                                        onclick: this.load
                                    },
                                    this.loading ? 'Loading...' : ' Load More '
                                )
                            ),
                      m(
                          '.mb1 mt2 pt2 ph1 bt b--light-gray',
                          m(elTextbox, {
                              class: 'h2 w-100 mb3 mt2',
                              placeholder: 'New Playlist',
                              button: 'Create',
                              onclick: this.clickCreate,
                              working: this.creating
                          })
                      )
                  ])
        ];
    },

    viewContent: function() {
        var self = this;
        return m(
            '.select-base',
            m(
                '.list pr3',
                {
                    oncreate: self.oncreateItemsContainer,
                    onupdate: self.onupdateItemsContainer
                },
                this.loaded && !this.collection.size() ? this.viewEmpty() : this.viewList()
            )
        );
    }
});
