var _ = require('lodash');
var m = require('mithril');
var comModalBase = require('./modalBase');
var elButtonIcon = require('comp/elements/buttonIcon');
var comShare = require('comp/social');

module.exports = _.create(comModalBase, {
  oninit: function(node) {
    comModalBase.oninit.call(this, node);
  },

  show: function() {
    if (App.Settings.showShare()) {
      comShare.load();
      comModalBase.show.call(this);
    }
  },

  clickClose: function(e) {
    if (e) e.preventDefault();
    comModalBase.hide.call(this);
  },

  clickDone: function(e) {
    if (e) e.preventDefault();
    App.Settings.showShare(false);
    this.clickClose();
  },

  clickNotNow: function(e) {
    if (e) e.preventDefault();
    App.Settings.visitThreshold(7);
    this.clickClose();
  },

  view: function() {
    return m(
      '.modal-window',
      {
        class: 'mini'
      },
      m(
        '.modal-content modal-bg2 tc br2 ph3 ph4-l pt4 pb3 mid-gray',
        !comShare.preloaded()
          ? m('.fw5', m('.mb3', 'Please wait...'))
          : m.fragment({}, [
              m(elButtonIcon, {
                class: 'absolute top-0 right-0 w2 h2 f4 gray grow',
                icon: 'close',
                onclick: this.clickClose.bind(this)
              }),
              // Keep Rollyt Rolling
              m('h2.lh-title mt0 pb3 dark-gray', 'Help us connect to your friends'),
              m('b', 'Like and Follow'),
              m(comShare.LikeFollow),
              m('b', 'Share to Friends'),
              m(comShare.Share),
              m(
                '.mt3 cf bt b--light-gray',
                m(
                  'a.f6 white mb2 mt2 mr2 dib br1 bg-blue pv1 ph3',
                  {
                    onclick: this.clickDone.bind(this)
                  },
                  'Done'
                ),
                m(
                  'a.f6 white br1 bg-gray pv1 ph3',
                  {
                    onclick: this.clickNotNow.bind(this)
                  },
                  'Not Now'
                )
              )
            ])
      )
    );
  }
});
