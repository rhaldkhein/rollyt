var _ = require('lodash');
var m = require('mithril');

var bindMethods = ['clickBackground'];

function Manager() {
  _.bindAll(this, bindMethods);
  this.modals = [];
  this.attrs = [];
}

Manager.prototype = {

  hasModal: function() {
    return this.modals.length > 0;
  },

  add: function(modal, attrs) {
    if (_.indexOf(this.modals, modal) === -1) {
      this.modals.push(modal);
      this.attrs[_.indexOf(this.modals, modal)] = _.defaults(attrs, {
        background: false
      });
    }
  },

  remove: function(modal) {
    var i = _.indexOf(this.modals, modal);
    if (i > -1) {
      this.modals.splice(i, 1);
      this.attrs.splice(i, 1);
    }
  },

  removeAll: function(e) {
    for (var i = this.modals.length - 1; i >= 0; i--) {
      this.modals[i].hide(e);
    }
  },

  // Events
  clickBackground: function(e) {
    this.removeAll(e);
  },

  // View
  component: function() {
    var self = this;
    return m('#modals', {
      class: this.hasModal() ? '' : 'hide',
    }, [
      m('.background', {
        onclick: this.clickBackground
      }, null),
      _.map(this.modals, function(modal, index, modals) {
        var attrs = self.attrs[index];
        attrs.background = (index !== modals.length - 1);
        return m(modal, attrs);
      })
    ]);
  }
};

module.exports = new Manager();