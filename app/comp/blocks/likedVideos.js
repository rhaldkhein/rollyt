var _ = require('lodash');
var comVideoList = require('./videoList');

module.exports = _.create(comVideoList, {

	oninit: function(node) {
		node.attrs.class = 'liked-videos';
		node.attrs.listmodel = App.Model.List.create({
			_id: 'liked'
		});
		comVideoList.oninit.call(this, node);
	},

	_viewEmpty: function() {
		return 'You haven\'t liked any video on Youtube.';
	}
	
});
