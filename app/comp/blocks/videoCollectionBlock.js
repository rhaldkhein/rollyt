var _ = require('lodash');
var m = require('mithril');
var j = require('jquery');
var moment = require('moment');
var util = require('libs/util');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var comMenu = require('comp/contexts/contextMenu');
var comList = require('comp/player/list');
var comCollectionBlock = require('comp/elements/collectionBlock');
var comModalVideoPlayer = require('comp/modals/videoPlayer');
var comCurrent = require('comp/player/current');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');
var ContextStar = require('comp/contexts/contextStar');
var starring = require('libs/starring');

module.exports = _.create(comCollectionBlock, {
  oninit: function(node) {
    node.attrs.class = node.attrs.class || '';
    comCollectionBlock.oninit.call(this, node);
    this.clickItemMenu = _.partialRight(this.clickItemMenu, this);
    this.clickVideoThumbnail = _.partialRight(this.clickVideoThumbnail, this);
    this.clickVideoTitle = _.partialRight(this.clickVideoTitle, this);
    this.clickAddPlaylist = _.partialRight(this.clickAddPlaylist, this);
  },

  errorImage: function() {
    this.style.display = 'none';
  },

  getModel: function(el) {
    return this.collection.get(
      j(el)
        .closest('li')
        .attr('itemid')
    );
  },

  // Event
  clickItemMenu: function(e, self) {
    comMenu.show(this, menuSchema, self);
  },

  clickVideoThumbnail: function(e, self) {
    var model = self.getModel(this);
    if (App.Props.ctrlPressed()) {
      e.preventDefault();
      comModalVideoPlayer.videoModel(model);
    } else {
      e.preventDefault();
      comList.addAndPlay(model);
    }
  },

  clickVideoTitle: function(e, self) {
    var model = self.getModel(this);
    if (App.Props.ctrlPressed()) {
      e.preventDefault();
      comModalVideoPlayer.videoModel(model);
    } else if (self.options.viewtype == 'list') {
      e.preventDefault();
      comList.addAndPlay(model);
    }
  },

  clickAddPlaylist: function(e, self) {
    e.preventDefault();
    comList.add(self.getModel(this));
  },

  // View

  itemViewParser: function(item) {
    var duration = moment.duration(item.duration());
    var listtype = this.options.viewtype == 'list';
    return item.id() && item.channel().id()
      ? m(
          'li',
          {
            key: item.id(),
            itemid: item.id(),
            class: comCurrent.getVideoId() == item.id() ? 'playing' : ''
          },
          listtype
            ? m(elIcon, {
                icon: 'slideshow'
              })
            : m(
                '.thumbnail',
                m(
                  '.content',
                  util.isMobile
                    ? null
                    : m(
                        '.actions',
                        m(elButtonIcon, {
                          icon: 'add',
                          onclick: this.clickAddPlaylist
                        }),
                        m(elButtonIcon, {
                          icon: 'more_vert',
                          onclick: this.clickItemMenu
                        })
                      ),
                  m(
                    'a.img',
                    {
                      onclick: this.clickVideoThumbnail
                    },
                    m('img', {
                      src: item.image(),
                      onerror: this.errorImage
                    }),
                    m(elIcon, {
                      icon: 'play_arrow'
                    })
                  ),
                  m('span', moment.utc(duration.asMilliseconds()).format(duration.hours() > 0 ? 'H:mm:ss' : 'm:ss'))
                )
              ),
          m(
            '.item-info relative',
            {
              class: util.isMobile ? 'pr4' : ''
            },
            m(
              'a.title',
              {
                href: '#/player.video?id=' + item.id(),
                onclick: this.clickVideoTitle
              },
              item.title()
            ),
            m(
              'a.channel',
              {
                href: '#/player.channel?id=' + item.channel().id()
              },
              item.channel().title()
            ),
            !util.isMobile
              ? null
              : m(elButtonIcon, {
                  icon: 'more_vert',
                  class: 'absolute top-0 right-0 f4 pa1',
                  onclick: this.clickItemMenu
                })
          ),
          listtype
            ? m(elButtonIcon, {
                icon: 'more_vert',
                onclick: this.clickItemMenu
              })
            : null
        )
      : null;
  }
});

var menuSchema = [
  {
    name: 'Play',
    icon: 'play_arrow',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      var model = this.collection.get(id);
      comList.addAndPlay(model);
    }
  },
  {
    name: 'Queue',
    icon: 'add',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      comList.add(this.collection.get(id));
    }
  },
  {
    id: 'add_pl',
    name: 'Add to Playlist',
    icon: 'playlist_add',
    callback: function(el) {
      var model = this.collection.get(
        j(el)
          .closest('li')
          .attr('itemid')
      );
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems([
            {
              resource_type: model.getKind(),
              resource_id: model.id()
            }
          ]);
        }
      });
    }
  },
  {
    id: 'star',
    name: 'Star / Bookmark',
    icon: 'star',
    callback: function(el) {
      var elLi = j(el).closest('li');
      var elCn = elLi.find('.content');
      var id = elLi.attr('itemid');
      // Request star this item, then show context
      starring
        .star('video', id)
        .then(function(data) {
          ContextStar.show(elCn[0], null, 'video', id, true, data.folder, function(err) {
            if (!err) elCn.addClass('o-40'); // Mark as moved
          });
        })
        .catch(Console.error)
        .finally(m.redraw);
    }
  },
  {
    name: 'View Video',
    icon: 'visibility',
    separate: 'before',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      m.route.set('/player.video?id=' + id);
    }
  },
  {
    name: 'Play on Modal',
    icon: 'flip_to_front',
    callback: function(el) {
      var model = this.collection.get(
        j(el)
          .closest('li')
          .attr('itemid')
      );
      comModalVideoPlayer.videoModel(model);
    }
  }
];

menuSchema.parser = function(item) {
  if (item.id === 'add_pl' || item.id === 'star') {
    item.hide = App.User.isGuest();
  }
};
