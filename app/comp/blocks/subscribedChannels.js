var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comChannelCollectionBlock = require('./channelCollectionBlock');

module.exports = _.create(comChannelCollectionBlock, {

    oninit: function(node) {
        node.attrs.class = (node.attrs.class || '') + 'subscribed-channel';
        node.attrs.title = 'Subscribed Channels';
        node.attrs.onload = this.loadSubscribed;
        node.attrs.onmore = this.loadMoreSubscribed;
        node.attrs.collection = new md.Collection({
            model: App.Model.Channel,
            url: '/youtube/subscription'
        });
        comChannelCollectionBlock.oninit.call(this, node);
        md.State.assign(this.state, {
            order: 'relevance'
        });
        if (App.User.isGuest()) {
            this.state.showViewLogin(true);
        }
    },

    getMenuSchema: function() {
        return [
            menuSchema,
            comChannelCollectionBlock.getMenuSchema.call(this)
        ];
    },

    loadMoreSubscribed: function() {
        this._load();
    },

    loadSubscribed: function(options) {
        options = options || {};
        var state = this.state;
        var query = {
            part: 'snippet',
            mine: true,
            maxResults: App.Props.blockCollectionColumnsChannel() * state.rows(),
            order: state.order()
        };
        if (options.clear) {
            state.pageNext(null);
            this.collection.clear();
        }
        if (state.pageNext()) {
            query.pageToken = state.pageNext();
        }
        state.loading(true);
        var done = m.redraw.computation();
        this.collection.fetch(query, {
                path: 'items'
            }, function(err, res) {
                if (!err) state.pageNext(res.nextPageToken || 0);
            })
            .catch(Console.error)
            .finally(function() {
                state.loading(false);
                done();
            });
    },

    _viewEmpty: function() {
        return 'You haven\'t subscribed to any channel on Youtube.';
    }

});

var submenuSortCallback = function(target, item) {
    this.state.order(item.value);
    this.reload();
};

var submenuSort = [{
    value: 'alphabetical',
    name: 'Alphabetical ',
    callback: submenuSortCallback
}, {
    value: 'relevance',
    name: 'Relevance',
    callback: submenuSortCallback
}, {
    value: 'unread',
    name: 'Activity',
    callback: submenuSortCallback
}];

submenuSort.parser = function(item) {
    item.icon = item.value === this.state.order() ? 'check' : null;
};

var menuSchema = [{
    id: 'order',
    name: 'Order',
    icon: 'format_line_spacing',
    separate: 'after',
    submenu: submenuSort
}];