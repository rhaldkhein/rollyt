var _ = require('lodash');
var m = require('mithril');
var j = require('jquery');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var util = require('libs/util');
var comMenu = require('comp/contexts/contextMenu');
var comList = require('comp/player/list');
var comCollectionBlock = require('comp/elements/collectionBlock');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');

module.exports = _.create(comCollectionBlock, {
  oninit: function(node) {
    node.attrs.class = (node.attrs.class || '') + ' collection-block-channel';
    node.attrs.viewtype = 'tile';
    comCollectionBlock.oninit.call(this, node);
    this.clickItemMenu = _.partialRight(this.clickItemMenu, this);
    this.clickVideoThumbnail = _.partialRight(this.clickVideoThumbnail, this);
    this.clickAddPlaylist = _.partialRight(this.clickAddPlaylist, this);
  },

  getModel: function(el) {
    return this.collection.get(
      j(el)
        .closest('li')
        .attr('itemid')
    );
  },

  // Event
  clickItemMenu: function(e, self) {
    comMenu.show(this, menuSchema, self);
  },

  clickVideoThumbnail: function(e, self) {
    e.preventDefault();
    comList.addAndPlay(self.getModel(this), {
      order: 'viewCount'
    });
  },

  clickAddPlaylist: function(e, self) {
    e.preventDefault();
    comList.add(self.getModel(this), {
      order: 'viewCount'
    });
  },

  errorImage: function() {
    this.style.display = 'none';
  },

  // View
  itemViewParser: function(item) {
    return m(
      'li',
      {
        class: 'item-channel ' + (comList.getCurrentModelId() == item.id() ? 'playing' : ''),
        key: item.id(),
        itemid: item.id()
      },
      m(
        '.thumbnail',
        m(
          '.content',
          util.isMobile
            ? null
            : m(
                '.actions',
                m(elButtonIcon, {
                  icon: 'add',
                  onclick: this.clickAddPlaylist
                }),
                m(elButtonIcon, {
                  icon: 'more_vert',
                  onclick: this.clickItemMenu
                })
              ),
          m(
            'a.img',
            {
              // href: '#/player.channel?id=' + item.id()
              onclick: this.clickVideoThumbnail
            },
            m('img', {
              src: item.image(),
              onerror: this.errorImage
            }),
            m(elIcon, {
              icon: 'play_arrow'
            })
          )
        )
      ),
      m(
        '.info relative',
        m(
          'a.title',
          {
            href: '#/player.channel?id=' + item.id()
          },
          item.title()
        ),
        m('div', [
          m(elIcon, {
            icon: 'account_circle'
          }),
          m('span', util.numberComma(item.subscribers()) + ' subscribers')
        ]),
        m('div', [
          m(elIcon, {
            icon: 'visibility'
          }),
          m('span', util.numberComma(item.views()) + ' views')
        ]),
        !util.isMobile
          ? null
          : m(elButtonIcon, {
              icon: 'more_vert',
              class: 'absolute top-0 right-0 f5 pa1',
              onclick: this.clickItemMenu
            })
      )
    );
  }
});

var menuSchema = [
  {
    name: 'Play',
    icon: 'play_arrow',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      var model = this.collection.get(id);
      comList.addAndPlay(model, {
        order: 'viewCount'
      });
    }
  },
  {
    name: 'Queue',
    icon: 'add',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      comList.add(this.collection.get(id), {
        order: 'viewCount'
      });
    }
  },
  {
    name: 'Add to Playlist',
    icon: 'playlist_add',
    callback: function(el) {
      var model = this.collection.get(
        j(el)
          .closest('li')
          .attr('itemid')
      );
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems([
            {
              resource_type: model.getKind(),
              resource_id: model.id(),
              flags: {
                order: 'viewCount'
              }
            }
          ]);
        }
      });
    }
  },
  {
    name: 'View Channel',
    icon: 'visibility',
    separate: 'before',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      m.route.set('/player.channel?id=' + id);
    }
  },
  {
    name: 'Pin to Top',
    icon: 'turned_in_not',
    callback: function(el) {
      if (!this.options.onpin) return;
      var model = this.collection.get(
        j(el)
          .closest('li')
          .attr('itemid')
      );
      this.options.onpin.call(this, null, model);
    }
  }
];

menuSchema.parser = function(item) {
  if (item.name === 'Add to Playlist') {
    item.hide = App.User.isGuest();
  } else if (item.name === 'Pin to Top') {
    item.hide = App.User.isGuest();
  }
};
