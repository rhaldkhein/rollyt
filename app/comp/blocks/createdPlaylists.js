var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comPlaylistCollectionBlock = require('./playlistCollectionBlock');

module.exports = _.create(comPlaylistCollectionBlock, {

  oninit: function(node) {
    node.attrs.class = 'created-playlists thumbnail-playlist';
    node.attrs.title = 'Youtube Playlists';
    node.attrs.onload = this.loadCreatedPlaylist;
    node.attrs.onmore = this.loadMoreCreatedPlaylist;
    node.attrs.collection = new md.Collection({
      model: App.Model.Playlist,
      url: '/youtube/playlistlist'
    });
    comPlaylistCollectionBlock.oninit.call(this, node);
  },

  loadMoreCreatedPlaylist: function() {
    this._load();
  },

  loadCreatedPlaylist: function(options) {
    options = options || {};
    var state = this.state;
    var query = {
      part: 'snippet',
      mine: true,
      maxResults: App.Props.blockCollectionColumns() * state.rows()
    };
    if (options.clear) {
      state.pageNext(null);
      this.collection.clear();
    }
    if (state.pageNext()) {
      query.pageToken = state.pageNext();
    }
    state.loading(true);
    var done = m.redraw.computation();
    this.collection.fetch(query, {
        path: 'items',
        clear: !!options.clear
      }, function(err, res) {
        if (!err) state.pageNext(res.nextPageToken || 0);
      })
      .catch(Console.error)
      .finally(function() {
        state.loading(false);
        done();
      });
  },

  _viewEmpty: function() {
    return 'You haven\'t created a playlist on Youtube.';
  }

});