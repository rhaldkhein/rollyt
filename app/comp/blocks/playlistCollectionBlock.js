var _ = require('lodash');
var m = require('mithril');
var j = require('jquery');
var util = require('libs/util');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var comMenu = require('comp/contexts/contextMenu');
var comList = require('comp/player/list');
var comCollectionBlock = require('comp/elements/collectionBlock');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');

module.exports = _.create(comCollectionBlock, {
  oninit: function(node) {
    node.attrs.class = (node.attrs.class || '') + ' thumbnail-playlist';
    comCollectionBlock.oninit.call(this, node);
    this.clickItemMenu = _.partialRight(this.clickItemMenu, this);
    this.clickVideoThumbnail = _.partialRight(this.clickVideoThumbnail, this);
    this.clickAddPlaylist = _.partialRight(this.clickAddPlaylist, this);
  },

  errorImage: function() {
    this.style.display = 'none';
  },

  getModel: function(el) {
    return this.collection.get(
      j(el)
        .closest('li')
        .attr('itemid')
    );
  },

  // Event
  clickItemMenu: function(e, self) {
    comMenu.show(this, menuSchema, self);
  },

  clickVideoThumbnail: function(e, self) {
    e.preventDefault();
    comList.addAndPlay(self.getModel(this));
  },

  clickAddPlaylist: function(e, self) {
    e.preventDefault();
    comList.add(self.getModel(this));
  },

  // View
  itemViewParser: function(item) {
    var img = m('img', {
      src: item.image(),
      onerror: this.errorImage
    });
    return item.id() && item.channel().id()
      ? m(
          'li',
          {
            key: item.id(),
            itemid: item.id(),
            class: comList.getCurrentModelId() == item.id() ? 'playing' : ''
          },
          m(
            '.thumbnail',
            m(
              '.content',
              util.isMobile
                ? null
                : m(
                    '.actions',
                    m(elButtonIcon, {
                      icon: 'add',
                      onclick: this.clickAddPlaylist
                    }),
                    m(elButtonIcon, {
                      icon: 'more_vert',
                      onclick: this.clickItemMenu
                    })
                  ),
              m(
                'a.img',
                {
                  onclick: this.clickVideoThumbnail
                },
                [
                  img,
                  img,
                  img,
                  m(elIcon, {
                    icon: 'play_arrow'
                  })
                ]
              )
            )
          ),
          m(
            '.relative',
            {
              class: util.isMobile ? 'ps4' : ''
            },
            m(
              'a.title',
              {
                href: '#/player.playlist?id=' + item.id()
              },
              item.title()
            ),
            m(
              'a.channel',
              {
                href: '#/player.channel?id=' + item.channel().id()
              },
              item.channel().title()
            ),
            !util.isMobile
              ? null
              : m(elButtonIcon, {
                  icon: 'more_vert',
                  class: 'absolute top-0 right-0 f4 pa1',
                  onclick: this.clickItemMenu
                })
          )
        )
      : null;
  }
});

var menuSchema = [
  {
    name: 'Play',
    icon: 'play_arrow',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      var model = this.collection.get(id);
      comList.addAndPlay(model);
    }
  },
  {
    name: 'Queue',
    icon: 'add',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      comList.add(this.collection.get(id));
    }
  },
  {
    name: 'Add to Playlist',
    icon: 'playlist_add',
    callback: function(el) {
      var model = this.collection.get(
        j(el)
          .closest('li')
          .attr('itemid')
      );
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems([
            {
              resource_type: model.getKind(),
              resource_id: model.id()
            }
          ]);
        }
      });
    }
  },
  {
    name: 'View Playlist',
    icon: 'visibility',
    separate: 'before',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      m.route.set('/player.playlist?id=' + id);
    }
  },
  {
    name: 'Pin to Top',
    icon: 'turned_in_not',
    callback: function(el) {
      if (!this.options.onpin) return;
      var model = this.collection.get(
        j(el)
          .closest('li')
          .attr('itemid')
      );
      this.options.onpin.call(this, null, model);
    }
  }
];

menuSchema.parser = function(item) {
  if (item.name === 'Add to Playlist') {
    item.hide = App.User.isGuest();
  }
};
