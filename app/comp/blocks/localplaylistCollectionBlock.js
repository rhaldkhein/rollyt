var _ = require('lodash');
var m = require('mithril');
var j = require('jquery');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var comMenu = require('comp/contexts/contextMenu');
var comList = require('comp/player/list');
var comCollectionBlock = require('comp/elements/collectionBlock');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');
var ModalCreatePlaylist = require('comp/modals/modalCreatePlaylist');
var util = require('libs/util');

var bindMethods = ['clickNewPlaylist'];

module.exports = _.create(comCollectionBlock, {
  oninit: function(node) {
    node.attrs.class = (node.attrs.class || '') + ' thumbnail-playlist';
    comCollectionBlock.oninit.call(this, node);
    _.bindAll(this, bindMethods);
    this.clickItemMenu = _.partialRight(this.clickItemMenu, this);
    this.clickVideoThumbnail = _.partialRight(this.clickVideoThumbnail, this);
    this.clickAddPlaylist = _.partialRight(this.clickAddPlaylist, this);
  },

  errorImage: function() {
    this.style.display = 'none';
  },

  getModel: function(el) {
    return this.collection.get(
      j(el)
        .closest('li')
        .attr('itemid')
    );
  },

  getMenuSchema: function() {
    if (this.options.menuschema) {
      return [this.options.menuschema, comCollectionBlock.getMenuSchema.call(this)];
    } else {
      return comCollectionBlock.getMenuSchema.call(this);
    }
  },

  // Event
  clickItemMenu: function(e, self) {
    comMenu.show(this, menuItemSchema, self);
  },

  clickVideoThumbnail: function(e, self) {
    e.preventDefault();
    comList.changePlaylist(self.getModel(this));
  },

  clickAddPlaylist: function(e, self) {
    e.preventDefault();
    comList.add(self.getModel(this));
  },

  clickNewPlaylist: function() {
    var self = this;
    ModalCreatePlaylist.show({
      title: 'Create Rollyt Playlist',
      folder: this.state.folder(),
      ondone: function() {
        self.reload();
      }
    });
  },

  // View
  itemViewParser: function(item) {
    var img = m('img', {
      src: item.image() || item.imageFallback(),
      onerror: this.errorImage
    });
    return m(
      'li',
      {
        key: item.id(),
        itemid: item.id(),
        class: comList._playlist && comList._playlist.id() == item.id() ? 'playing' : ''
      },
      m(
        '.thumbnail',
        m(
          '.content',
          util.isMobile
            ? null
            : m(
                '.actions',
                m(elButtonIcon, {
                  icon: 'add',
                  onclick: this.clickAddPlaylist
                }),
                m(elButtonIcon, {
                  icon: 'more_vert',
                  onclick: this.clickItemMenu
                })
              ),
          m(
            'a.img',
            {
              onclick: this.clickVideoThumbnail
            },
            [
              img,
              img,
              img,
              m(elIcon, {
                icon: 'play_arrow'
              })
            ]
          )
        )
      ),
      m(
        '.relative',
        {
          class: util.isMobile ? 'ps4' : ''
        },
        m(
          'a.title',
          {
            href: '#/player.localplaylist?id=' + item.id()
          },
          m('span', item.title()),
          item.is_private() ? m('i.icon icon-lock') : null
        ),
        m(
          'div.count',
          m(elIcon, {
            icon: 'playlist_play'
          }),
          util.numberComma(item.item_count()) + ' items'
        ),
        m(
          'a.channel',
          {
            href: '#/player.profile?id=' + item.channel().id()
          },
          item.channel().title()
        ),
        !util.isMobile
          ? null
          : m(elButtonIcon, {
              icon: 'more_vert',
              class: 'absolute top-0 right-0 f4 pa1',
              onclick: this.clickItemMenu
            })
      )
    );
  },

  _viewEmpty: function() {
    var self = this;
    return (
      this.options.emptyMessage || [
        m('span', "You don't have any Rollyt playlist. "),
        m(
          'button',
          {
            onclick: function() {
              App.do('createLocalPlaylist', function() {
                self.reload();
              });
            }
          },
          'Create Now'
        )
      ]
    );
  },

  _viewActions: function() {
    return [
      comCollectionBlock._viewActions.call(this),
      !this.options.newbutton
        ? null
        : m(
            'a',
            {
              class: 'button-text',
              onclick: this.clickNewPlaylist
            },
            m(elIcon, {
              icon: 'add_box'
            }),
            m('span', 'New Playlist')
          )
    ];
  }
});

var menuItemSchema = [
  {
    name: 'Play',
    icon: 'play_arrow',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      var model = this.collection.get(id);
      comList.changePlaylist(model);
    }
  },
  {
    name: 'Queue',
    icon: 'add',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      comList.add(this.collection.get(id));
    }
  },
  {
    name: 'Add to Playlist',
    icon: 'playlist_add',
    callback: function(el) {
      var model = this.collection.get(
        j(el)
          .closest('li')
          .attr('itemid')
      );
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems([
            {
              resource_type: model.getKind(),
              resource_id: model.id()
            }
          ]);
        }
      });
    }
  },
  {
    name: 'View Playlist',
    icon: 'visibility',
    separate: 'before',
    callback: function(el) {
      var id = j(el)
        .closest('li')
        .attr('itemid');
      m.route.set('/player.localplaylist?id=' + id);
    }
  },
  {
    name: 'Pin to Top',
    icon: 'turned_in_not',
    callback: function(el) {
      if (!this.options.onpin) return;
      var model = this.collection.get(
        j(el)
          .closest('li')
          .attr('itemid')
      );
      this.options.onpin.call(this, null, model);
    }
  }
];

menuItemSchema.parser = function(item) {
  if (item.name === 'Add to Playlist') {
    item.hide = App.User.isGuest();
  } else if (item.name === 'Pin to Top') {
    item.hide = App.User.isGuest();
  }
};
