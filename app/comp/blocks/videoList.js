var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comList = require('comp/player/list');
var comVideoCollectionBlock = require('./videoCollectionBlock');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');

var VideoList = module.exports = _.create(comVideoCollectionBlock, {

  oninit: function(node) {
    node.attrs.onload = VideoList.loadList;
    node.attrs.onmore = VideoList.loadMoreList;
    this.model = node.attrs.listmodel;
    if (this.model) {
      if (_.isString(this.model)) {
        this.model = App.Model.List.create({
          _id: this.model
        });
      }
      node.attrs.title = this.model.title();
      if (this.model.isSaved()) {
        node.attrs.collection = {
          url: this.model.link(),
          model: md.model.get(this.model.type())
        };
      }
    }
    comVideoCollectionBlock.oninit.call(this, node);
  },

  oncreate: function() {
    if (this.model) {
      if (this.model.isSaved()) {
        this.reload();
      } else {
        // Load the list model if not loaded
        var self = this;
        this.model.fetch()
          .then(function(model) {
            self.setModel(model, true);
          })
          .catch(Console.error);
      }
    }
  },

  setModel: function(model, reload) {
    this.model = model;
    this.state.title(model.title());
    this.collection.opt('url', model.link());
    this.collection.opt('model', md.model.get(model.type()));
    if (reload) this.reload();
  },

  getMenuSchema: function() {
    return [
      menuSchema,
      comVideoCollectionBlock.getMenuSchema.call(this)
    ];
  },

  loadMoreList: function() {
    if (this.model && this.model.isSaved()) {
      this._load();
    }
  },

  loadList: function(options) {
    if (this.model && this.model.isSaved()) {
      options = options || {};
      var state = this.state;
      var query = _.cloneDeep(this.model.params());
      query.maxResults = App.Props.blockCollectionColumns() * state.rows();
      if (options.clear) {
        state.pageNext(null);
        this.collection.clear();
      }
      if (state.pageNext()) {
        query.pageToken = state.pageNext();
      }
      state.loading(true);
      var done = m.redraw.computation();
      this.collection.fetch(query, {
          path: 'items',
          clear: !!options.clear
        }, function(err, res) {
          if (!err) state.pageNext(res.nextPageToken || 0);
        })
        .catch(Console.error).finally(function() {
          state.loading(false);
          done();
        });
    }
  }
});

var menuSchema = [{
  name: 'Play',
  icon: 'play_arrow',
  callback: function() {
    if (this.model && this.model.isSaved()) {
      comList.addAndPlay(this.model);
    }
  }
}, {
  name: 'Queue',
  icon: 'add',
  callback: function() {
    if (this.model && this.model.isSaved()) {
      comList.add(this.model);
    }
  }
}, {
  name: 'Add to Playlist',
  icon: 'playlist_add',
  separate: 'after',
  callback: function() {
    if (this.model && this.model.isSaved()) {
      var self = this;
      ModalSelectLocalPlaylist.show({
        onselect: function(e, playlist) {
          playlist.addItems([{
            resource_type: self.model.getKind(),
            resource_id: self.model.id()
          }]);
        }
      });
    }
  }
}];

menuSchema.parser = function(item) {
  if (item.name === 'Queue') {
    item.separate = App.User.isGuest() ? 'after' : undefined;
  } else if (item.name === 'Add to Playlist') {
    item.hide = App.User.isGuest();
  }
};