var m = require('mithril');
module.exports = function(msg) {
	return m('.subview notfound', [
		m('h2', 'Coming Soon'),
		m('p', msg || 'This content is not available yet.')
	]);
};