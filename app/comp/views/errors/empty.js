var _ = require('lodash');
var m = require('mithril');
var headers = ['Oops!'];

module.exports = function(msg, header) {
	return m('.subview empty', [
		m('h2', header || _.sample(headers)),
		m('p', msg || 'You don\'t have anything to display here.')
	]);
};
