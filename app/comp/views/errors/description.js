var m = require('mithril');

module.exports = function(msg) {
	return m('.subview empty', [
		m('p', msg || 'You don\'t have anything to display here.')
	]);
};
