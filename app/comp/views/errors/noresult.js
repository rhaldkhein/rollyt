var m = require('mithril');
module.exports = function(msg) {
	return m('.subview noresult', [
		m('h2', 'No Result'),
		m('p', msg || 'Could not find anything to display.')
	]);
};