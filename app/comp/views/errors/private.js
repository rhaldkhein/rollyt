var m = require('mithril');
module.exports = function(msg) {
	return m('.subview empty', [
		m('h2', 'Private Content'),
		m('p', msg || 'This content is only available for the owner.')
	]);
};