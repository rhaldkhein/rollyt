var m = require('mithril');
module.exports = function(msg) {
	return m('.subview notfound', [
		m('h2', 'Not Found'),
		m('p', msg || 'The content you\'re trying to get was not found.')
	]);
};