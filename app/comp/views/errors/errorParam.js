var _ = require('lodash');
var m = require('mithril');
var elIcon = require('comp/elements/icon');

module.exports = function(errors, param) {
	return m('.error',
		_.reduce(errors, function(accum, error) {
			if (error.param === param)
				accum.push(m('p',
					m(elIcon, 'arrow_upward'),
					m('span', error.msg)));
			return accum;
		}, [])
	);
};