var m = require('mithril');

function ViewError(msg) {
	return m('.subview error-view', [
		m('h2', 'Error'),
		m('p', msg || 'The server responded with an error.')
	]);
}

ViewError.code = function(code, msg) {
	if (code) {
		var view = _map[code];
		if (view) return view(msg);
		else return ViewError(msg);
	}
};

ViewError.error = function(err) {
	if (err) {
		if (err.code) return ViewError.code(err.code);
		else return ViewError();
	}
};

module.exports = ViewError;

var _map = {
	'REBR': ViewError,
	'RENO': require('./private'),
	'RENF': require('./notfound'),
	'REUA': require('./login')
};
