var m = require('mithril');
module.exports = function(msg) {
	return m('.subview login-view', [
		m('h2', 'Not Available'),
		m('p', msg || 'You must login to view this content.')
	]);
};