var _ = require('lodash');
var m = require('mithril');

module.exports = function(columns) {
	return m('.loading-icons',
		_.map(_.range(columns), function() {
			return m('.masker-icon', {
					class: 'col' + columns
				},
				m('.masker.mask1'),
				m('.masker.mask2'),
				m('.masker.mask3'),
				m('.masker.mask4'),
				m('.masker.mask5'),
				m('.masker.mask6'),
				m('.masker.mask7'),
				m('.masker.mask8')
			);
		})
	);
};
