var _ = require('lodash');
var m = require('mithril');

module.exports = function(rows) {
	if (!rows) rows = 3;
	return m('.loading-sideblock',
		m('.masker.mask0'),
		_.map(_.range(rows), function() {
			return m('.masker-sideblock',
				m('.masker.mask1'),
				m('.masker.mask2'),
				m('.masker.mask3'),
				m('.masker.mask4'),
				m('.masker.mask5'),
				m('.masker.mask6')
			);
		})
	);
};

module.exports.class = 'small dark';