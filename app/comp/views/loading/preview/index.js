var m = require('mithril');
var slice = Array.prototype.slice;

module.exports = function() {
	var args = slice.call(arguments),
		func = views[args.shift()];
	return m('.loading-preview.background', {
			class: func.class,
			key: 'loading-preview' // Bug fix. Link with sideblock
		},
		func.apply(null, args)
	);
};

var views = {
	thumbnails: require('./thumbnails'),
	sideblock: require('./sideblock'),
	icons: require('./icons'),
	list: require('./list')
};
