var m = require('mithril');

module.exports = function(columns) {
    return m('.loading-list',
        m('.masker-list', {
                class: 'col' + columns
            },
            m('.masker.mask1'),
            m('.masker.mask2'),
            m('.masker.mask3'),
            m('.masker.mask4'),
            m('.masker.mask5'),
            m('.masker.mask6'),
            m('.masker.mask7')
        )
    );
};