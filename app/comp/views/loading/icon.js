var m = require('mithril');
var elAnimation = require('comp/elements/animation');
module.exports = function() {
	return m('.loading',
		m(elAnimation, {
			parent: 'spinner',
			child: 'bounce',
			count: 3
		})
	);
};
