var m = require('mithril');
var viewLoadingIcon = require('./icon');
module.exports = function() {
	return m('.subview loading-view',
		viewLoadingIcon()
	);
};