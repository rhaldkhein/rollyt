var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var j = require('jquery');
var elLoadIcon = require('comp/elements/loadIcon');
var bindMethods = ['keyupInput', 'clickButton'];

module.exports = {
  oninit: function(node) {
    _.bindAll(this, bindMethods);
    this.changeInput = _.partialRight(this.changeInput, this);
    this.text = _.isFunction(node.attrs.value) ? node.attrs.value : md.stream(node.attrs.value || null);
    this.clickCallback = node.attrs.onclick;
    this.changeCallback = node.attrs.onchange;
    this.password = node.attrs.password;

    this.edit = md.stream(true);
    this.working = _.isFunction(node.attrs.working) ? node.attrs.working : md.stream(false);
    this.lock = !!node.attrs.lock;
    if (this.lock) this.edit(false);
  },
  onremove: function() {
    this.clickCallback = null;
    this.changeCallback = null;
  },
  onupdate: function(node) {
    if (this.lock && this.edit()) j(node.dom).find('input').focus();
  },
  keyupInput: function(e) {
    if (e.keyCode === 13) {
      this.clickButton(e);
    } else {
      e.redraw = false;
    }
  },
  clickButton: function(e) {
    e.preventDefault();
    if (this.working()) return;
    if (this.lock && !this.edit()) {
      this.edit(true);
      if (this.password) {
        this.text('');
      }
      return;
    }
    if (this.clickCallback) {
      var self = this;
      var prom = this.clickCallback(e, this.text());
      if (prom && prom.then) {
        this.working(true);
        prom.then(function() {
          if (self.lock) self.edit(false);
        }).catch(function(err) {
          if (err.errors && err.errors[0] && err.errors[0].msg)
            App.Modules.toastr.error(err.errors[0].msg);
          else if (!err.handled)
            App.Modules.toastr.error('Error while saving');
        }).finally(function() {
          self.working(false);
          m.redraw();
        });
      } else {
        if (this.lock) this.edit(false);
      }
    } else {
      if (this.lock) this.edit(false);
    }
  },
  changeInput: function(e, self) {
    e.preventDefault();
    if (e) e.redraw = false;
    self.text(this.value);
    if (self.changeCallback)
      self.changeCallback(e, self.text());
  },
  view: function(node) {
    return m('div', {
      class: 'textbox ' + (node.attrs.class || '') + (node.attrs.icon ? ' label-icon' : '')
    }, [
      node.attrs.icon ? m('span.label', node.attrs.icon) : null,
      node.attrs.button ? m('a.button', {
        onclick: this.clickButton
      }, this.working() ? m(elLoadIcon) : (this.edit() ? node.attrs.button : 'Edit')) : null,
      m('div',
        m('input', {
          type: this.password ? 'password' : 'text',
          disabled: !this.edit(),
          value: this.text() || null,
          placeholder: node.attrs.placeholder || '',
          onchange: this.changeInput,
          onkeyup: this.keyupInput
        })
      )
    ]);
  }
};