var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');

module.exports = {
    /**
     * Attributes
     * - current (Number)
     * - tabs (Array String)
     */

    oninit: function(node) {
        this.index = md.toStream(node.attrs.current);
        this.clickTab = _.partialRight(this.clickTab, node);
    },

    clickTab: function(e, node) {
        var val = parseInt(e.target.getAttribute('data-index'));
        if (node.state.index() != val) {
            if (node.attrs.onchange) node.attrs.onchange.call(this, e, node.attrs.tabs[val]);
            node.state.index(val);
        }
    },

    view: function(node) {
        var self = this;
        return m('.panel-tab', [
            m(
                '.tabs',
                {
                    key: 't' + node.state.index()
                },
                _.map(node.attrs.tabs, function(name, i) {
                    return m(
                        'a',
                        {
                            'data-index': i,
                            class: node.state.index() == i,
                            onclick: self.clickTab
                        },
                        name
                    );
                })
            ),
            m(
                '.panels',
                {
                    key: 'p' + node.state.index()
                },
                node.children[node.state.index()]
            )
        ]);
    }
};
