var _ = require('lodash');
var m = require('mithril');
var util = require('libs/util');
var Icon = require('./icon');
var tippy = require('tippy.js');

function removeTippy(node) {
    if (this.tooltip && !App.Preference.state.tooltip()) {
        this.tooltip.destroy(this.tooltip.getPopperElement(node.dom));
        this.tooltip = null;
    }
}

module.exports = _.create(Icon, {

    oninit: function(node) {
        this.tooltip = null;
        this.clickButton = _.partialRight(this.clickButton, this);
        this.clickCallback = node.attrs.onclick;
        this.longPressCallback = node.attrs.onlongpress;
        if (this.longPressCallback) {
            this.mousedownButton = _.partialRight(this.mousedownButton, this);
            this._pressId = null;
            this._longPressed = false;
        }
        Icon.oninit.call(this, node);
    },

    oncreate: function(node) {
        this.onupdate(node);
    },

    onupdate: function(node) {
        if (node.attrs.title && !util.isMobile) {
            if (!this.tooltip && App.Preference.state.tooltip() && !node.attrs.native_tooltip) {
                this.tooltip = tippy(node.dom, {
                    size: 'small',
                    distance: 5
                });
            }
            removeTippy.call(this, node);
        }
    },

    onremove: function(node) {
        this.clickCallback = null;
        removeTippy.call(this, node);
    },

    getClass: function(node) {
        return 'button ' + (node.attrs.class || 'default');
    },

    mousedownButton: function(e, self) {
        self._longPressed = false;
        self._pressId = setTimeout(function() {
            self.longPressCallback.call();
            self._longPressed = true;
        }, 1000);
    },

    clickButton: function(e, self) {
        if (!self._longPressed && self.clickCallback) {
            if (self.loading) {
                if (!self.loading()) {
                    self.clickCallback.call(this, e, self.loading);
                }
            } else {
                self.clickCallback.call(this, e);
            }
        }
        if (self.longPressCallback) {
            clearTimeout(self._pressId);
            self._pressId = null;
            self._longPressed = false;
        }
    },

    view: function(node) {
        return m('a', {
            class: this.getClass(node),
            onclick: this.clickButton,
            onmousedown: (this.longPressCallback ? this.mousedownButton : null),
            title: (App.Preference.state.tooltip() ? node.attrs.title : undefined)
        }, Icon.view.call(this, node));
    }

});