var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');

module.exports = {

	oninit: function(node) {
		this.value = _.isFunction(node.attrs.value) ? node.attrs.value : md.stream(!!node.attrs.value);
	},

	view: function() {
		return m('input', {
			type: 'checkbox',
			onchange: m.withAttr('checked', this.value),
			checked: this.value()
		});
	}
};