var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');

module.exports = {

  oninit: function(node) {
    if (!_.isNil(node.attrs.loading)) {
      // Required to be false at initial state.
      this.loading = _.isFunction(node.attrs.loading) ? node.attrs.loading : md.stream(!!node.attrs.loadval);
    }
  },

  getIcon: function(node) {
    if (this.loading && this.loading() && node.attrs.loading) {
      return 'loading-icon-7 loading-icon';
    } else {
      return node.attrs.icon;
    }
  },

  view: function(node) {
    return m('i', {
      class: 'icon icon-' + this.getIcon(node)
    });
  }

};