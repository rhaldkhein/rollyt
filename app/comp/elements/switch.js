var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var bindMethods = ['changeCheckbox'];

module.exports = {

	oninit: function(node) {
		_.bindAll(this, bindMethods);
		this.value = _.isFunction(node.attrs.value) ? node.attrs.value : md.stream(!!node.attrs.value);
		this.options = node.attrs;
	},

	changeCheckbox: function(e) {
		this.value(!this.value());
		if (this.options.onchange) this.options.onchange.call(this, e, this.value());
	},

	view: function() {
		return m('a.ui-checkbox', {
				class: this.value() ? 'check' : '',
				onclick: this.changeCheckbox
			},
			m('span.ui-checkbox-slider'),
			m('span.ui-checkbox-handle')
		);
	}
};
