var m = require('mithril');

module.exports = {

  view: function() {
    return m('i', {
      class: 'icon icon-loading-icon-7 loading-icon'
    });
  }

};