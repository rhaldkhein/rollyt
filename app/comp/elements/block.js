var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var elButtonIcon = require('comp/elements/buttonIcon');
var comMenu = require('comp/contexts/contextMenu');
var classnames = require('classnames');
var util = require('libs/util');

var dragSourceItem;
var dragHoverItem;

// Statics
var bindMethods = ['clickActs', 'clickTitle'];

module.exports = {
    oninit: function(node) {
        // Binds
        _.bindAll(this, bindMethods);
        this.clickContentMenu = _.partialRight(this.clickContentMenu, this);
        this.options = node.attrs || {};
        if (this.options.draggable) {
            // this.dragCallback = _.partialRight(this.dragCallback, this);
            this.dragCallback = _.bind(this.dragCallback, this);
        } else {
            this.options.draggable = md.stream(false);
        }
        this.state = md.State.create({
            tag: md.stream(),
            title: this.options.title,
            subtitle: this.options.subtitle,
            collapse: this.options.collapse || false,
            showacts: !util.isMobile,
            hide: this.options.hide || false,
            index: this.options.index || -1,
            drag: false
        });
        this.__viewBody = this.options.body || null;
    },

    getMenuSchema: function() {
        return menuSchemaBase;
    },

    // Events - - -

    clickActs: function() {
        this.state.showacts(!this.state.showacts());
    },

    clickContentMenu: function(e, self) {
        comMenu.show(this, self.getMenuSchema(), self);
    },

    clickTitle: function() {
        this.clickActs();
    },

    dragCallback: function(e) {
        e.redraw = false;
        if (e.type === 'dragstart') {
            e.dataTransfer.setData('text/html', 'block-drag');
            dragSourceItem = this;
        } else if (e.type === 'dragend') {
            if (dragHoverItem) {
                if (this.options.ondragmove) {
                    // Move the source to hover item
                    this.options.ondragmove(dragSourceItem, dragHoverItem);
                }
                dragHoverItem.state.drag(false);
                dragHoverItem = null;
            }
            dragSourceItem = null;
            this.state.drag(false);
            e.redraw = true;
        } else if (e.type === 'dragenter' && dragHoverItem !== this) {
            if (dragHoverItem) dragHoverItem.state.drag(false);
            dragHoverItem = this;
            this.state.drag(true);
            e.redraw = true;
        }
    },

    // Views - - -

    _viewTitle: function() {
        return m(
            'h1',
            // {
            //     onclick: this.clickTitle
            // },
            this.state.title() || 'Block'
        );
    },

    _viewActions: function() {
        return this.options.actions;
    },

    _viewBody: function() {
        return this.__viewBody;
    },

    view: function() {
        var fnDrag, draggable, dragDown;
        if (this.options.draggable()) {
            draggable = true;
            fnDrag = this.dragCallback;
            dragDown = dragSourceItem && dragHoverItem && dragSourceItem.state.index() < dragHoverItem.state.index();
        }
        return m(
            'div',
            {
                class:
                    'block' +
                    (this.state.hide() ? ' hide' : '') +
                    (this.state.drag() ? (dragDown ? ' drag-spot-down' : ' drag-spot-up') : ''),
                draggable: draggable,
                ondragstart: fnDrag,
                ondragend: fnDrag,
                ondragenter: fnDrag,
                ondrag: fnDrag
            },
            [
                m(
                    '.block-head',
                    m('.block-head-content', [
                        m(
                            'span.title',
                            {
                                onclick: this.clickActs
                            },
                            this._viewTitle()
                        ),
                        !util.isMobile
                            ? null
                            : m(elButtonIcon, {
                                  icon: 'expand_' + (this.state.showacts() ? 'less' : 'more'),
                                  onclick: this.clickActs
                              }),
                        !this.state.showacts()
                            ? null
                            : m(
                                  '.actions',
                                  this._viewActions(),
                                  _.isEmpty(this.getMenuSchema())
                                      ? null
                                      : m(elButtonIcon, {
                                            icon: 'more_vert',
                                            onclick: this.clickContentMenu
                                        })
                              )
                    ])
                ),
                m(
                    'div',
                    {
                        class: classnames('block-body', {
                            hide: this.state.collapse()
                        })
                    },
                    this._viewBody()
                )
            ]
        );
    }
};

var menuSchemaBase = [
    {
        name: 'Remove',
        icon: 'remove'
    }
];

menuSchemaBase.parser = function(item) {
    if (item.name === 'Remove') {
        if (this.options.ondelete) {
            item.callback = this.options.ondelete;
            item.hide = false;
        } else {
            item.hide = true;
        }
    }
};
