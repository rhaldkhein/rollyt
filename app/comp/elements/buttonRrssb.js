var m = require('mithril');

module.exports = {

    view: function(node) {
        return m('li.rrssb-' + node.attrs.name,
            m('a', {
                    class: (node.attrs.popup ? 'popup' : ''),
                    href: node.attrs.href,
                    'data-action': (node.attrs.action ? node.attrs.action : '')
                },
                m('span.rrssb-icon',
                    m('svg', {
                            xmlns: 'http://www.w3.org/2000/svg',
                            viewBox: '0 0 ' + node.attrs.viewBox + ' ' + node.attrs.viewBox
                        },
                        m('path', {
                            d: node.attrs.svg
                        })
                    )
                ),
                m('span.rrssb-text', node.attrs.name)
            )
        );
    }

};