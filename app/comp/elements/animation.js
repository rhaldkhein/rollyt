var _ = require('lodash');
var m = require('mithril');

module.exports = {

	oninit: function(node) {
		this.parent = node.attrs.parent;
		this.child = node.attrs.child;
		this.count = node.attrs.count;
	},

	view: function() {
		var self = this;
		return m('div.' + this.parent,
			_.times(this.count, function(i) {
				return m('div.' + self.child + ' ' + self.child + (i + 1));
			})
		);
	}

};