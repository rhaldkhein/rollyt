var _ = require('lodash');
var m = require('mithril');
var async = require('async');
var util = require('libs/util');

var _loaded = false;

function loadScripts() {
  // this = oncreate->this
  var self = this;
  if (!window.Webcam) {
    App.Modules.loaderjs.load([
      '/static/app/libs/webcamjs/webcam.min.js',
      '/static/app/libs/jsqrcode/src/grid.js',
      '/static/app/libs/jsqrcode/src/version.js',
      '/static/app/libs/jsqrcode/src/detector.js',
      '/static/app/libs/jsqrcode/src/formatinf.js',
      '/static/app/libs/jsqrcode/src/errorlevel.js',
      '/static/app/libs/jsqrcode/src/bitmat.js',
      '/static/app/libs/jsqrcode/src/datablock.js',
      '/static/app/libs/jsqrcode/src/bmparser.js',
      '/static/app/libs/jsqrcode/src/datamask.js',
      '/static/app/libs/jsqrcode/src/rsdecoder.js',
      '/static/app/libs/jsqrcode/src/gf256poly.js',
      '/static/app/libs/jsqrcode/src/gf256.js',
      '/static/app/libs/jsqrcode/src/decoder.js',
      '/static/app/libs/jsqrcode/src/qrcode.js',
      '/static/app/libs/jsqrcode/src/findpat.js',
      '/static/app/libs/jsqrcode/src/alignpat.js',
      '/static/app/libs/jsqrcode/src/databr.js'
    ], function(err) {
      if (!err) {
        // Object `Webcam` and `qrcode` is now available
        _loaded = true;
        if (self.options.onload) self.options.onload();
        m.redraw();
      }
    });
  } else if (self.options.onload) {
    m.nextTick(self.options.onload);
  }
}

var bindMethods = ['onupdateCamera', 'onremoveCamera', 'onlive'];

module.exports = {

  oninit: function(node) {
    _.bindAll(this, bindMethods);
    this.options = node.attrs;
  },

  oncreate: function() {
    loadScripts.call(this);
  },

  onupdateCamera: function(node) {
    if (_loaded && window.Webcam) {
      // window.Webcam.on('error', function(err) {
      //     Console.log(err);
      // });
      if (!window.Webcam.userMedia && window.Webcam.iOS) {
        // Does not have userMedia and is iOS (no flash either)
        this.attach(node.dom);
        var self = this;
        setTimeout(function() {
          self.snap(function(token) {
            if (!token) {
              if (self.options.onerror)
                self.options.onerror();
            } else if (self.options.ontoken) {
              self.options.ontoken(token);
            }
            m.redraw.nexttick();
          });
        }, 1000);
      } else if (!window.Webcam.live) {
        // Means deps are loaded, safe to attach
        window.Webcam.on('live', this.onlive);
        this.attach(node.dom);
      }
    }
  },

  onremoveCamera: function() {
    if (window.Webcam) {
      window.Webcam.off('live', this.onlive);
      window.Webcam.reset();
    }
  },

  onlive: function() {
    var token;
    var self = this;
    async.doWhilst(
      function(callback) {
        setTimeout(function() {
          if (window.Webcam.loaded && window.Webcam.live) {
            self.snap(function(spantoken) {
              token = spantoken;
              callback(null, token);
            });
          } else {
            callback('not_loaded', token);
          }
        }, self.options.speed || 1000);
      },
      function() {
        return !token;
      },
      function(err, token) {
        if (!err && token) {
          if (self.options.ontoken) {
            self.options.ontoken(token);
          }
        }
      }
    );
  },

  attach: function(dom) {
    if (!window.Webcam) return;
    if (util.isMobile) {
      window.Webcam.set({
        dest_width: 480,
        dest_height: 480,
        constraints: {
          facingMode: { exact: 'environment' }
        }
      });
    }
    window.Webcam.attach(dom);
  },

  snap: function(callback) {
    if (!window.Webcam) return;
    if (!window.Webcam.loaded) return;
    // This is a manual snapshot or capture.
    // All dep objects must be loaded. Otherwise error.
    window.Webcam.snap(function(data_uri, canvas, context) {
      var token;
      window.qrcode.canvas_qr2 = canvas;
      window.qrcode.qrcontext2 = context;
      try { token = window.qrcode.decode(); } catch (e) { /* I know */ }
      callback(token);
    });
  },

  view: function() {
    return m('div',
      m('.cameraview center', {
        oncreate: this.onupdateCamera,
        onupdate: this.onupdateCamera,
        onremove: this.onremoveCamera
      })
    );
  }

};