var m = require('mithril');
var viewLoading = require('comp/views/loading');
var viewError = require('comp/views/errors/error');

module.exports = {

    oninit: function(node) {
        this.opts = node.attrs;
        this.loading = false;
        this.error = false;
    },

    viewBody: function() {
        return null;
    },

    viewLoading: function() {
        return viewLoading();
    },

    viewWrap: function(node) {
        if (this.loading) return this.viewLoading(node);
        if (this.error) return viewError('Could not find anything to display here', 'No Result');
        return m('.sideblock-body h-100', {
            key: 'sideblock-body', // Bug fix. Link with loading layout
        }, this.viewBody(node));
    },

    view: function(node) {
        return m('.sideblock h-100', {
                class: (this.opts.class || '')
            },
            (!this.opts.title ? null : m('.sideblock-head', this.opts.title)),
            this.viewWrap(node)
        );
    }

};