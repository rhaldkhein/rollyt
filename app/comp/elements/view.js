var m = require('mithril');

module.exports = {

  oncreate: function(node) {
    m.request({
        url: '/~view/' + node.attrs.src,
        data: node.attrs.data,
        headers: {
          'X-Requested-With': 'XMLHttpRequest'
        },
        deserialize: function(value) {
          return value;
        }
      })
      .then(function(data) {
        m.render(node.dom, m.trust(data));
      })
      .catch(Console.error);
  },
  view: function() {
    return m('.view');
  }

};