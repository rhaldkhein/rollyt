var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var j = require('jquery');
var util = require('libs/util');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var elTextBox = require('comp/elements/textbox');
var comBlock = require('./block');
var viewLogin = require('comp/views/errors/login');
var viewEmpty = require('comp/views/errors/empty');
var preview = require('comp/views/loading/preview');

var bindMethods = ['itemViewParser', 'clickLoadMore', 'loadMore', 'clickSearchBox'];
var defaultRows = 4;
var rowsFactorsThumbnail = [1, 4, 3, 3, 3, 3, 2];
var rowsFactorsTile = [1, 3, 3, 2];

module.exports = _.create(comBlock, {
  oninit: function(node) {
    comBlock.oninit.call(this, node);
    _.bindAll(this, bindMethods);
    this.options.viewtype = this.options.viewtype || 'thumbnail';
    this.changeFolder = _.partialRight(this.changeFolder, this);
    this.collection =
      this.options.collection instanceof md.Collection
        ? this.options.collection
        : new md.Collection(this.options.collection);
    md.State.assign(this.state, {
      showViewLogin: md.stream(false),
      showSearchBox: md.stream(false),
      loading: md.stream(false),
      folder: md.stream(),
      searchString: md.stream(),
      page: md.stream(0),
      pageNext: md.stream(0),
      pagePrev: md.stream(0),
      moreClicked: md.stream(false),
      rows: md.stream((this.options.rows || defaultRows) * (App.Props.blockCollectionColumns() == 1 ? 2 : 1))
    });
    this.state.loading(true);
  },

  oncreate: function() {
    util.nextTick(this.loadMore);
  },

  _load: function(opt) {
    if (this.options.onload) {
      this.options.onload.call(this, opt);
    }
  },

  getMenuSchema: function() {
    return [this.options.menu || [], menuSchema, comBlock.getMenuSchema.call(this)];
  },

  reload: function() {
    var self = this;
    util.nextTick(function() {
      self._load({
        clear: true
      });
    });
  },

  loadMore: function() {
    if (this.options.onmore) this.options.onmore.call(this);
  },

  loaded: function() {
    // This function should always be called when loading
  },

  // - Events - - - - - - - - - - - -

  clickLoadMore: function() {
    // Change rows to maximum
    if (!this.state.moreClicked()) {
      var rows = this.state.rows();
      if (this.options.viewtype === 'thumbnail') rows = rows * rowsFactorsThumbnail[App.Props.blockCollectionColumns()];
      else if (this.options.viewtype === 'tile')
        rows = rows * rowsFactorsTile[App.Props.blockCollectionColumnsChannel()];
      else if (this.options.viewtype === 'list') rows = rows * 2;
      this.state.rows(rows);
      this.state.loading(true);
      this.reload();
      this.state.moreClicked(true);
    } else {
      this.loadMore();
    }
  },

  clickSearchBox: function() {
    this._load();
  },

  changeFolder: function(e, self) {
    self.state.folder(j(this).val());
    self.reload();
  },

  // - Views - - - - - - - - - - - -

  // Override - Parser for items
  itemViewParser: function() {},

  _viewEmpty: function() {
    return this.options.emptyMessage || null;
  },

  _viewItemsHeader: function() {
    if (this.state.showViewLogin()) {
      return viewLogin();
    } else if (!this.state.loading() && !this.collection.size()) {
      return viewEmpty(this._viewEmpty());
    }
    return null;
  },

  _viewLoading: function() {
    switch (this.options.viewtype) {
      case 'list':
        return preview('list');
      case 'tile':
        return preview('icons', App.Props.blockCollectionColumnsChannel());
      default:
        return preview('thumbnails', App.Props.blockCollectionColumns());
    }
  },

  _viewActions: function() {
    return [
      !this.options.folder
        ? null
        : m(
            'span',
            {
              class: 'folder dropdown'
            },
            m(elIcon, {
              icon: 'folder'
            }),
            m(
              'select.mid-gray',
              {
                onchange: this.changeFolder
              },
              [
                m(
                  'option',
                  {
                    value: ''
                  },
                  'All'
                ),
                App.Folder.map(function(folder) {
                  return m(
                    'option',
                    {
                      value: folder.id()
                    },
                    folder.name()
                  );
                })
              ]
            )
          ),
      !this.state.showSearchBox()
        ? null
        : m(elTextBox, {
            placeholder: 'Search',
            onclick: this.clickSearchBox,
            value: this.state.searchString
          }),
      comBlock._viewActions.call(this)
    ];
  },

  _viewBody: function() {
    var self = this,
      initial_load = this.state.loading() && !this.collection.size();
    return m(
      'div',
      {
        key: 'folder' + this.state.folder(),
        class: 'collection-block expand view-' + this.options.viewtype + ' ' + (this.options.class || '')
      },
      [
        this._viewItemsHeader(),
        initial_load
          ? this._viewLoading()
          : m(
              'ul',
              this.collection.reduce(function(accum, item) {
                item = self.itemViewParser(item);
                if (item) {
                  accum.push(item);
                }
                return accum;
              }, [])
            ),
        initial_load || !self.state.pageNext()
          ? null
          : m('.nav', [
              m(elButtonIcon, {
                icon: 'expand_more',
                onclick: this.clickLoadMore,
                loading: this.state.loading
              })
            ])
      ]
    );
  }
});

// Menu - - -

var submenuRows = [
  {
    name: '1',
    callback: function() {
      this.state.rows(1);
      this.reload();
    }
  },
  {
    name: '2',
    callback: function() {
      this.state.rows(2);
      this.reload();
    }
  },
  {
    name: '4',
    callback: function() {
      this.state.rows(4);
      this.reload();
    }
  },
  {
    name: '6',
    callback: function() {
      this.state.rows(6);
      this.reload();
    }
  },
  {
    name: '8',
    callback: function() {
      this.state.rows(8);
      this.reload();
    }
  }
];

submenuRows.parser = function(item) {
  item.icon = parseInt(item.name) == this.state.rows() ? 'check' : null;
};

var menuSchema = [
  {
    id: 'search',
    name: 'Search',
    icon: 'search',
    callback: function() {
      this.state.showSearchBox(!this.state.showSearchBox());
      if (!this.state.showSearchBox()) {
        this.state.searchString(null);
      }
    }
  },
  {
    name: 'Sort (A-Z)',
    callback: function() {
      this.collection.sort('title');
    }
  },
  {
    name: 'Refresh',
    icon: 'refresh',
    callback: function() {
      this.state.rows(this.options.rows || defaultRows);
      this.reload();
    }
  }
];

menuSchema.parser = function(item) {
  if (item.id === 'search') {
    if (!this.options.search) {
      item.hide = true;
    } else {
      item.name = this.state.showSearchBox() ? 'Hide Searchbox' : 'Search';
    }
  } else if (item.id === 'rows') {
    item.hide = this.options.viewtype == 'list';
  }
};
