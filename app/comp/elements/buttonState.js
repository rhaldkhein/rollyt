var _ = require('lodash');
var ButtonIcon = require('./buttonIcon');

module.exports = _.create(ButtonIcon, {

	defaultState: ['off', 'on'],

	oninit: function(node) {
		this.state = node.attrs.state || _.clone(this.defaultState);
		this.state.current = _.isNumber(this.state.current) ? this.state.current : 0;
		ButtonIcon.oninit.call(this, node);
	},

	getClass: function(node) {
		return ButtonIcon.getClass.call(this, node) + ' ' + (this.state[this.state.current] || 'off');
	},

	clickButton: function(e, self) {
		// Move to next state
		self.state.current++;
		if (self.state.current >= self.state.length)
			self.state.current = 0;
		// Update option's class with current state
		if (self.clickCallback) {
			if (self.loading) {
				if (!self.loading()) { // Prevent from calling again when loading
					self.clickCallback.call(this, e, self.loading);
				}
			} else {
				self.clickCallback.call(this, e);
			}
		}
	},

});