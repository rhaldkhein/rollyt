var _ = require('lodash'),
  m = require('mithril'),
  elIcon = require('comp/elements/icon'),
  elButtonIcon = require('comp/elements/buttonIcon'),
  cn = require('classnames');

// Component
var Messages = {
  messages: __app.msgs,
  hasMessage: function() {
    return !_.isEmpty(Messages.messages);
  },
  getElement: function(item) {
    return m(
      'div',
      {
        class: cn({ closable: item.closable })
      },
      m(
        item.link ? 'a' : 'p',
        {
          class: 'message ' + item.type,
          href: item.link,
          target: item.new_window ? '_blank' : ''
        },
        !item.link
          ? null
          : m(elIcon, {
              icon: 'link'
            }),
        // m('span', item.message)
        m.trust('<span>' + item.message + '</span>')
      ),
      !item.closable
        ? null
        : m(elButtonIcon, {
            icon: 'close',
            onclick: function(e) {
              e.preventDefault();
              item.show = false;
            }
          })
    );
  },
  view: function() {
    return m(
      'div.messages',
      _.map(
        _.filter(Messages.messages, {
          show: true,
          type: 'error'
        }),
        Messages.getElement
      ),
      _.map(
        _.filter(Messages.messages, {
          show: true,
          type: 'warning'
        }),
        Messages.getElement
      ),
      _.map(
        _.filter(Messages.messages, {
          show: true,
          type: 'info'
        }),
        Messages.getElement
      ),
      _.map(
        _.filter(Messages.messages, {
          show: true,
          type: 'success'
        }),
        Messages.getElement
      )
    );
  }
};

__app.msgs = null;
// Load messages from server
// m.request('/~rest/appsettings/messages')
//   .then(function(messages) {
//     Messages.messages = messages;
//   })
//   .catch(Console.error);

module.exports = Messages;

/*

/~rest/appsettings/messages
[
  {
    show: true,
    type: 'info' | 'success' | 'warning' | 'error',
    message: 'Hello World!',
    link: 'https://www.example.com',
    new_window: false,
    closable: false
  },
  
  ...
]

*/
