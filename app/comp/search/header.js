var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var util = require('libs/util');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var comFilter = require('comp/search/filter');
var comList = require('comp/player/list');

var bindMethods = ['clickFilter', 'changeFilter', 'clickPlay', 'clickAdd'];

module.exports = {

    oninit: function(node) {
        _.bindAll(this, bindMethods);
        this.options = node.attrs || {};
        this.state = md.State.create({
            resultCount: 0,
            open: m.route.param('open'),
            type: m.route.param('type'),
            sort: m.route.param('sort'),
            date: m.route.param('date')
        });
    },

    createSearchModel: function() {
        return App.Model.List.create({
            _id: 'search.' + m.route.param('q') + '...' + this.state.date() + '.' + this.state.sort()
        });
    },

    clickFilter: function(e) {
        e.preventDefault();
        this.state.open(!this.state.open());
    },

    changeFilter: function(e, state) {
        if (this.options.onchange) {
            this.state.type(state.type());
            this.state.date(state.date());
            this.state.sort(state.sort());
            this.options.onchange(e, this.state);
        }
    },

    clickPlay: function() {
        var res = comList.addAndPlay(this.createSearchModel());
        if (res.then) {
            res.then(m.redraw);
        } else {
            m.redraw();
        }
    },

    clickAdd: function() {
        comList.add(this.createSearchModel()).then(m.redraw).catch(Console.error);
    },

    view: function(node) {
        return m('div.result-header', [
            m('div.filter',
                m('a.button.filter-button.button-style-a', {
                        onclick: this.clickFilter
                    },
                    m('span', ' Filters'),
                    m(elIcon, {
                        icon: 'expand_' + (this.state.open() ? 'less' : 'more')
                    })
                ),
                m(elButtonIcon, {
                    icon: 'play_arrow',
                    onclick: this.clickPlay
                }),
                m(elButtonIcon, {
                    icon: 'add',
                    onclick: this.clickAdd
                })
            ),
            m('div.stats', m('span', 'About ' + util.numberComma(node.attrs.totalResults()) + ' results')),
            this.state.open() ? m(comFilter, {
                class: 'filter-options',
                type: true,
                date: true,
                sort: true,
                onchange: this.changeFilter
            }) : null
        ]);
    }
};