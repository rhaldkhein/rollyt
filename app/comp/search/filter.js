var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');

module.exports = {
	oninit: function(node) {
		_.bindAll(this, ['clickTable']);
		this.options = node.attrs || {};
		this.state = md.State.create({
			type: m.route.param('type') || this.options.currentType,
			sort: m.route.param('sort') || this.options.currentSort,
			date: m.route.param('date') || this.options.currentDate
		});
		this.showType = md.stream(this.options.type);
		this.showDate = md.stream(this.options.date);
		this.showSort = md.stream(this.options.sort);
	},
	clickTable: function(e) {
		e.preventDefault();
		if (!_.isEmpty(e.target.className) && e.target.className.indexOf('selected') === -1) {
			var classes = e.target.className.split('-');
			if (classes[0] === 'type') {
				this.state.type(classes[1]);
			} else if (classes[0] === 'sort') {
				this.state.sort(classes[1]);
			} else if (classes[0] === 'date') {
				this.state.date(classes[1]);
			}
			if (this.options.onchange)
				this.options.onchange(e, this.state);
		} else {
			e.redraw = false;
		}
	},
	view: function() {
		return m('div', {
				class: this.options.class || ''
			},
			m('table', {
				onclick: this.clickTable
			}, [
				// Header
				m('tr',
					this.showType() ? m('th', 'Type') : null,
					this.showDate() ? m('th', {
						colspan: 2
					}, 'Date') : null,
					this.showSort() ? m('th', {
						colspan: 2
					}, 'Sort') : null
				),
				// Row 1
				m('tr',
					this.showType() ? m('td.type-video' + (this.state.type() === 'video' ? '.selected' : ''), 'Video') : null,
					this.showDate() ? m('td.date-today' + (this.state.date() === 'today' ? '.selected' : ''), 'Today') : null,
					this.showDate() ? m('td.date-month' + (this.state.date() === 'month' ? '.selected' : ''), 'Month') : null,
					this.showSort() ? m('td.sort-relevance' + (this.state.sort() === 'relevance' ? '.selected' : ''), 'Relevance') : null,
					this.showSort() ? m('td.sort-rating' + (this.state.sort() === 'rating' ? '.selected' : ''), 'Rating') : null
				),
				// Row 2
				m('tr',
					this.showType() ? m('td.type-playlist' + (this.state.type() === 'playlist' ? '.selected' : ''), 'Playlist') : null,
					this.showDate() ? m('td.date-yesterday' + (this.state.date() === 'yesterday' ? '.selected' : ''), 'Yesterday') : null,
					this.showDate() ? m('td.date-year' + (this.state.date() === 'year' ? '.selected' : ''), 'Year') : null,
					this.showSort() ? m('td.sort-date' + (this.state.sort() === 'date' ? '.selected' : ''), 'Date') : null,
					this.showSort() ? m('td.sort-viewCount' + (this.state.sort() === 'viewCount' ? '.selected' : ''), 'Views') : null
				),
				// Row 3
				m('tr',
					this.showType() ? m('td.type-channel' + (this.state.type() === 'channel' ? '.selected' : ''), 'Channel') : null,
					this.showDate() ? m('td.date-week' + (this.state.date() === 'week' ? '.selected' : ''), 'Week') : null,
					this.showDate() ? m('td', '') : null,
					this.showSort() ? m('td.sort-title' + (this.state.sort() === 'title' ? '.selected' : ''), 'Title') : null,
					this.showSort() ? m('td', '') : null
				)
			])
		);
	}
};