var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var comItem = require('comp/search/item');
var viewLoading = require('comp/views/loading');
var viewLoadingIcon = require('comp/views/loading/icon');
var viewNoResult = require('comp/views/errors/noresult');

var bindMethods = ['clickLoadMore', 'dragInsert'];

module.exports = {
    oninit: function(node) {
        _.bindAll(this, bindMethods);
        this.options = node.attrs || {};
        this.collection = this.options.collection || new md.Collection();
        this.loading = this.options.loading || md.stream(false);
    },
    clickLoadMore: function(e) {
        if (this.options.onloadmore)
            this.options.onloadmore(e);
    },
    dragScroll: function() {},
    dragInsert: function(srcModel, desModel) {
        var iSrc = this.collection.findIndex(['_id', srcModel.id()]);
        var iDes = this.collection.findIndex(['_id', desModel.id()]);
        var models = this.collection.models;
        while (iSrc < 0) {
            iSrc += models.length;
        }
        while (iDes < 0) {
            iDes += models.length;
        }
        if (iDes >= models.length) {
            var k = iDes - models.length;
            while ((k--) + 1) {
                models.push(undefined);
            }
        }
        models.splice(iDes, 0, models.splice(iSrc, 1)[0]);
        if (this.options.onmoveitem) {
            this.options.onmoveitem(srcModel, desModel);
        }
    },
    view: function() {
        var view = null;
        var hasnext = this.options.hasnext && this.options.hasnext();
        var self = this;
        if (this.collection.size()) {
            view = [
                m('ul', {
                        class: (this.options.showIndex ? 'show-index' : '')
                    },
                    this.collection.map(function(item, index) {
                        if (!item.id()) {
                            return null;
                        } else {
                            return m(comItem, {
                                key: item.id() + index,
                                index: self.options.showIndex ? index : -1,
                                itemContextMenu: self.options.itemContextMenu,
                                editable: self.options.editable,
                                ondragscroll: self.dragScroll,
                                ondraginsert: self.dragInsert,
                                model: item,
                                extra: self.options.extra
                            });
                        }
                    })
                ),
                m('.page-nav', {
                        class: (hasnext ? '' : 'pt3')
                    },
                    this.loading() ? viewLoadingIcon() : m('a.button ui-button', {
                        class: (hasnext ? '' : 'hide'),
                        onclick: this.clickLoadMore
                    }, 'Load More')
                )
            ];
        } else if (this.loading()) {
            view = viewLoading();
        } else {
            view = this.options.viewNoResult || viewNoResult();
        }
        return m('div', {
            class: 'result'
        }, view);
    }
};