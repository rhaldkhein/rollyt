var _ = require('lodash');
var m = require('mithril');
var md = require('mithril-data');
var util = require('libs/util');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var moment = require('moment');
var numeral = require('numeral');
var comMenu = require('comp/contexts/contextMenu');
var comModalVideoPlayer = require('comp/modals/videoPlayer');
var comList = require('comp/player/list');
var ModalSelectLocalPlaylist = require('comp/modals/modalSelectLocalPlaylist');
var ContextStar = require('comp/contexts/contextStar');
var starring = require('libs/starring');

var bindMethods = [
  'clickLink',
  'clickAdd',
  // 'clickPlay',
  'clickAddToPlaylist',
  'clickThumbnail',
  'mousedownDragHandle',
  'mouseupDragHandle',
  'dragCallback'
];
var partMethods = ['errorImage', 'clickMoreActions'];

var dragSourceItem;
var dragHoverItem;

var mapTypes = {
  Video: 'video',
  Channel: 'channel',
  Playlist: 'playlist',
  PlaylistItem: 'playlisiitem',
  LocalPlaylist: 'localplaylist',
  List: 'list'
};

module.exports = {
  oninit: function(node) {
    // All bindings
    _.bindAll(this, bindMethods, partMethods);
    // this.errorImage = _.partialRight(this.errorImage, this);
    // this.clickMoreActions = _.partialRight(this.clickMoreActions, this);
    this.options = node.attrs || {};
    // Get the video from playlist item
    this.model = this.options.model;
    // Properties
    this.state = md.State.create({
      unknown: false,
      draggable: false,
      drag: false,
      type: null,
      index: this.options.index
    });
    if (this.model.options.name === 'PlaylistItem') {
      this.parent = this.model;
      this.model = this.model.video();
    } else if (this.model.options.name === 'LocalPlaylistItem') {
      this.parent = this.model;
      this.model = this.model.resource();
    }
    if (!this.model) this.model = this.createUnknownItem();
    if (!this.model.id()) this.state.unknown(true);
    // Chance state base on model
    this.state.type(mapTypes[this.model.options.name]);
  },
  createUnknownItem: function() {
    return new App.Model.Video({
      title: '[Unknown item]'
    });
  },
  isVideo: function() {
    return this.state.type() === 'video';
  },
  isPlaylist: function() {
    return this.state.type() === 'playlist';
  },
  isChannel: function() {
    return this.state.type() === 'channel';
  },
  isLocalPlaylist: function() {
    return this.state.type() === 'localplaylist';
  },
  isList: function() {
    return this.state.type() === 'list';
  },
  getIcon: function() {
    if (this.state.type() === 'channel') return 'account_circle';
    else if (this.state.type() === 'playlist' || this.state.type() === 'localplaylist') return 'playlist_play';
    else return 'slideshow';
  },
  getLink: function() {
    return '/player.' + this.state.type() + '?id=' + this.model.id();
  },
  errorImage: function(e, self) {
    this.src = App.Model[self.model.options.name].modelOptions.defaults.image;
  },
  clickLink: function(e) {
    e.redraw = false;
    if (e.target.getAttribute('for') !== 'channel') {
      e.preventDefault();
      m.route.set(this.getLink());
      return false;
    }
  },
  clickThumbnail: function(e) {
    e.preventDefault();
    if (this.state.unknown()) {
      App.Modules.toastr.error("Can't play deleted or private item");
      return false;
    }
    if (this.state.type() === 'video' && App.Props.ctrlPressed()) {
      comModalVideoPlayer.videoModel(this.model);
    } else if (this.state.type() === 'localplaylist') {
      comList.changePlaylist(this.model).catch(Console.error);
    } else {
      var self = this;
      // If `extra.model` is Local Playlist, play it and play this item
      if (this.options.extra && this.options.extra.model instanceof App.Model.LocalPlaylist) {
        comList.changePlaylist(this.options.extra.model, -1).then(function() {
          comList.playByString(
            Math.ceil((self.state.index() + 1) / App.Vars.playlist_limit) + '/' + self.parent.id() + '//'
          );
        });
      } else {
        comList.addAndPlay(this.model);
      }
    }
    return false;
  },
  clickAdd: function() {
    comList.add(
      this.model,
      this.model instanceof App.Model.Video
        ? null
        : {
            playLimit: 1
          }
    );
  },
  clickAddToPlaylist: function() {
    var model = this.model;
    ModalSelectLocalPlaylist.show({
      onselect: function(e, playlist) {
        playlist.addItems([
          {
            resource_type: model.getKind(),
            resource_id: model.id(),
            flags:
              this.model instanceof App.Model.Video
                ? {}
                : {
                    playLimit: 1
                  }
          }
        ]);
      }
    });
  },
  clickMoreActions: function(e, self) {
    if (self.state.unknown()) {
      comMenu.show(this, contextMenuUnknown, self, null);
    } else if (!self.options.editable && self.model.options.name == 'Video') {
      comMenu.show(this, contextMenuVideo, self, null);
    } else if (self.options.itemContextMenu) {
      comMenu.show(this, self.options.itemContextMenu, self, null);
    }
  },
  mousedownDragHandle: function() {
    this.state.draggable(true);
  },
  mouseupDragHandle: function() {
    this.state.draggable(false);
  },
  dragCallback: function(e) {
    if (e.type === 'dragstart') {
      e.dataTransfer.setData('text/html', 'search-item-drag');
      dragSourceItem = this;
      e.redraw = false;
    } else if (e.type === 'dragend') {
      if (dragHoverItem) {
        // Move the source to hover item
        this.options.ondraginsert(dragSourceItem.parent, dragHoverItem.parent);
        dragHoverItem.state.drag(false);
        dragHoverItem = null;
      }
      dragSourceItem = null;
      this.state.drag(false);
      this.state.draggable(false);
    } else if (e.type === 'dragenter' && dragHoverItem !== this) {
      if (dragHoverItem) dragHoverItem.state.drag(false);
      dragHoverItem = this;
      this.state.drag(true);
    } else if (e.type === 'drag') {
      this.options.ondragscroll(e);
      e.redraw = false;
    }
  },
  viewVideo: function(self, unknown) {
    return [
      m(
        'div.thumbnail',
        m(
          'a',
          {
            onclick: self.clickThumbnail
          },
          m('img', {
            src: self.model.image()
          }),
          m(elIcon, {
            icon: 'play_arrow'
          })
        )
      ),
      m(
        'div.content',
        {
          onclick: self.clickLink
        },
        m(
          'a.title',
          {
            href: '#' + self.getLink()
          },
          self.model.title()
        ),
        unknown
          ? null
          : [
              m(
                'div.channel',
                m(
                  'a',
                  {
                    for: 'channel',
                    href: '#/player.channel?id=' + self.model.channel().id()
                  },
                  self.model.channel().title()
                )
              ),
              m(
                'div.type',
                m(elIcon, {
                  icon: unknown ? 'help' : self.getIcon()
                }),
                m('span', moment.duration(self.model.duration()).humanize())
              ),
              m(
                'div.details',
                moment(self.model.published()).fromNow(),
                m('span', m.trust(' &bull; '), numeral(self.model.views()).format('0.0a') + ' views')
              )
            ]
      )
    ];
  },
  viewChannel: function(self) {
    return [
      m(
        'div.thumbnail',
        m(
          'a',
          {
            href: '#' + self.getLink(),
            onclick: self.clickThumbnail
          },
          m('img', {
            src: self.model.image(),
            onerror: self.errorImage
          }),
          m(elIcon, {
            icon: 'play_arrow'
          })
        )
      ),
      m(
        'div.content',
        {
          onclick: self.clickLink
        },
        m(
          'a.title',
          {
            href: '#' + self.getLink()
          },
          self.model.title()
        ),
        m(
          'div.type',
          m(elIcon, {
            icon: self.getIcon()
          }),
          m('span', util.numberComma(self.model.videos()) + ' videos')
        ),
        m(
          'div.details',
          moment(self.model.published()).fromNow(),
          m('span', [m.trust(' &bull; '), numeral(self.model.subscribers()).format('0.0a') + ' subs'])
        )
      )
    ];
  },
  viewPlaylist: function(self) {
    var img = m('img', {
      src: self.model.image()
    });
    return [
      m(
        'div.thumbnail',
        m(
          'a',
          {
            href: '#' + self.getLink(),
            onclick: self.clickThumbnail
          },
          [
            img,
            img,
            img,
            m(elIcon, {
              icon: 'play_arrow'
            })
          ]
        )
      ),
      m(
        'div.content',
        {
          onclick: self.clickLink
        },
        m(
          'a.title',
          {
            href: '#' + self.getLink()
          },
          self.model.title()
        ),
        m(
          'div.channel',
          m(
            'a',
            {
              for: 'channel',
              href: '#/player.channel?id=' + self.model.channel().id()
            },
            self.model.channel().title()
          )
        ),
        m(
          'div.type',
          m(elIcon, {
            icon: self.getIcon()
          }),
          m('span', util.numberComma(self.model.videos()) + ' videos')
        ),
        m('div.details', moment(self.model.published()).fromNow())
      )
    ];
  },
  viewLocalPlaylist: function(self) {
    var img = m('img', {
      src: self.model.image() || self.model.imageFallback()
    });
    return [
      m(
        'div.thumbnail',
        m(
          'a',
          {
            href: '#' + self.getLink(),
            onclick: self.clickThumbnail
          },
          [
            img,
            img,
            img,
            m(elIcon, {
              icon: 'play_arrow'
            })
          ]
        )
      ),
      m(
        'div.content',
        {
          onclick: self.clickLink
        },
        m(
          'a.title',
          {
            href: '#' + self.getLink()
          },
          self.model.title()
        ),
        m(
          'div.channel',
          m(
            'a',
            {
              for: 'channel',
              href: '#/player.profile?id=' + self.model.user().id()
            },
            self.model
              .user()
              .current_account()
              .firstname()
          )
        ),
        m(
          'div.type',
          m(elIcon, {
            icon: self.getIcon()
          }),
          m('span', util.numberComma(self.model.item_count()) + ' items')
        ),
        m('div.details', moment(self.model.date_created()).fromNow())
      )
    ];
  },
  viewList: function(self) {
    var img = m('img', {
      src: self.model.image()
    });
    return [
      m(
        'div.thumbnail',
        m(
          'a',
          {
            // href: '#' + self.getLink(),
            onclick: self.clickThumbnail
          },
          [
            img,
            img,
            img,
            m(elIcon, {
              icon: 'play_arrow'
            })
          ]
        )
      ),
      m(
        'div.content',
        m('a.title', self.model.title()),
        m('div.channel', m('a', m('em', 'Generated by ' + App.Info.name)))
      )
    ];
  },
  view: function() {
    var fnDrag, dragDown;
    var playing = comList.getCurrentModelId() === this.model.id();
    var unknown = this.state.unknown();
    if (this.options.editable) {
      fnDrag = this.dragCallback;
      dragDown = dragSourceItem && dragHoverItem && dragSourceItem.state.index() < dragHoverItem.state.index();
    }
    return m(
      'li',
      {
        key: this.model.id(),
        class:
          'item ' +
          ('item-' + this.state.type()) +
          (this.state.drag() ? (dragDown ? ' drag-spot-down' : ' drag-spot-up') : '') +
          (playing ? ' playing' : '') +
          (unknown ? ' unknown' : ''),
        draggable: this.options.editable ? this.state.draggable() : false,
        ondragstart: fnDrag,
        ondragenter: fnDrag,
        ondragend: fnDrag,
        ondrag: fnDrag
      },
      this.state.index() > -1 ? m('div.index', this.state.index() + 1) : null,
      this.options.editable
        ? m(
            'span',
            {
              class: 'drag-handle',
              onmousedown: this.mousedownDragHandle,
              onmouseup: this.mouseupDragHandle
            },
            m(elIcon, {
              icon: 'drag_handle'
            })
          )
        : null,
      this['view' + this.model.options.name](this, unknown),
      m(
        'div.actions',
        unknown
          ? null
          : [
              m(elButtonIcon, {
                class: playing ? 'hide' : '',
                icon: 'play_arrow',
                onclick: this.clickThumbnail,
                title: 'Play',
                native_tooltip: true
              }),
              m(elButtonIcon, {
                class: playing ? 'hide' : '',
                icon: 'add',
                onclick: this.clickAdd,
                title: 'Queue',
                native_tooltip: true
              }),
              App.User.isGuest()
                ? null
                : m(elButtonIcon, {
                    icon: 'playlist_add',
                    onclick: this.clickAddToPlaylist,
                    title: 'Add to Playlist',
                    native_tooltip: true
                  })
            ],
        this.model.options.name == 'Video' || (this.options.editable && this.options.itemContextMenu)
          ? m(elButtonIcon, {
              icon: 'more_vert',
              onclick: this.clickMoreActions
            })
          : null
      )
    );
  }
};

// Menu for Account
var contextMenuVideo = [
  {
    id: 'star',
    name: 'Star / Bookmark',
    icon: 'star',
    separate: 'after',
    callback: function(el) {
      // App.Modules.skiplist.add(this.model.id());
      var id = this.model.id();
      starring
        .star('video', id)
        .then(function(data) {
          ContextStar.show(el, null, 'video', id, true, data.folder);
        })
        .catch(Console.error)
        .finally(m.redraw);
    }
  },
  {
    id: 'add_skiplist',
    name: 'Add to Skiplist',
    icon: 'block',
    callback: function() {
      App.Modules.skiplist.add(this.model.id());
    }
  },
  {
    name: 'Play on Modal',
    icon: 'flip_to_front',
    callback: function() {
      comModalVideoPlayer.videoModel(this.model);
    }
  }
];

// Menu for Account
var contextMenuUnknown = [
  {
    name: 'Remove',
    icon: 'remove',
    callback: function() {
      this.parent
        .destroy()
        .catch(function() {
          App.Modules.toastr.error('Item not deleted');
        })
        .finally(m.redraw);
    }
  }
];

contextMenuVideo.parser = function(item) {
  if (item.id === 'add_skiplist' || item.id === 'star') {
    item.hide = App.User.isGuest();
  }
};
