var m = require('mithril');

module.exports = {
  view: function() {
    return m('main',
      m('header.tc pt5',
        m('img', {
          src: '/static/images/logos/logo-rollyt-medium.png'
        })
      ),
      m('div.mw6 tc center pt5 pb5',
        m('h1', '404 Not Found ', m.trust('<span>&#x1F623;</span>')),
        m('p.gray', 'The page you\'re trying to view does not exist')
      ),
      m('footer.tc f7 mt5 mb5',
        m('p.mv1 gray', '2018 Rollyt — Handmade with ', m('img.v-mid[src=/static/status/footer-logo-heart.png]'), ' on Earth'),
        m('p.mv1 gray',
          m('a.blue[href=/]', 'Home'),
          m('span.mh2', '|'),
          m('a.blue[href=/about]', 'About '),
          m('span.mh2', '|'),
          m('a.blue[href=/about/disclaimer]', 'Disclaimer'),
          m('span.mh2', '|'),
          m('a.blue[href=/about/bugreport]', 'Bug Report')
        )
      )
    );
  }
};