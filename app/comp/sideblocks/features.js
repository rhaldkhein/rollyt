var _ = require('lodash');
var m = require('mithril');
var j = require('jquery');
var util = require('libs/util');
var youtube = require('libs/youtube');
var comSideBlock = require('comp/elements/sideblock');
var elIcon = require('comp/elements/icon');
var elButtonIcon = require('comp/elements/buttonIcon');
var comList = require('comp/player/list');
var numeral = require('numeral');
var preview = require('comp/views/loading/preview');

module.exports = _.create(comSideBlock, {
    oninit: function(node) {
        node.attrs.class = 'features';
        comSideBlock.oninit.call(this, node);
        this.loadCount = 3;
        this.load = _.bind(this.load, this);
        this.loading = true;
        this.features = {};
    },
    oncreate: function() {
        m.nextTick(this.load);
    },
    getPlaylistWithLimit: function(rawPlaylists) {
        return _.filter(rawPlaylists, function(rawValue) {
            return rawValue && rawValue.limit > 0;
        });
    },
    load: function() {
        if (this.loadCount <= 0) {
            this.error = true;
            this.loading = false;
            m.redraw();
            return;
        }
        this.loadCount--;
        var self = this;
        // Features must be loaded models
        m.request('/~rest/appsettings/features')
            .then(function(data) {
                return { json: data };
            })
            .then(function(data) {
                if (data.json.channels) {
                    return youtube.channelInfo(_.map(data.json.channels, 'id'))
                        .then(function(models) {
                            data.channels = App.Model.Channel.createModels(models);
                            return data;
                        });
                }
                return data;
            })
            .then(function(data) {
                if (data.json.playlists) {
                    return youtube.playlistInfo(_.map(data.json.playlists, 'id'))
                        .then(function(models) {
                            data.playlists = App.Model.Playlist.createModels(models);
                            return Promise.mapSeries(self.getPlaylistWithLimit(data.json.playlists), function(item) {
                                return youtube.list('playlistitem', {
                                    part: 'snippet',
                                    playlistId: item.id,
                                    maxResults: item.limit
                                }).then(function(result) {
                                    result = App.Model.Video.createModels(result.items);
                                    result.id = item.id;
                                    return result;
                                });
                            }).then(function(results) {
                                data.playlistItems = results;
                                return data;
                            });
                        });
                }
                return data;
            })
            .then(function(data) {
                if (data.json.videos) {
                    return youtube.videoInfo(_.map(data.json.videos, 'id'))
                        .then(function(models) {
                            data.videos = App.Model.Video.createModels(models);
                            return data;
                        });
                }
                return data;
            })
            .then(function(data) {
                self.features.channels = data.channels;
                self.features.playlists = data.playlists;
                self.features.playlistItems = data.playlistItems;
                self.features.videos = data.videos;
                self.features.raw = data.json;
                self.loading = false;
                m.redraw();
            })
            .catch(function() {
                // Re-fetch
                setTimeout(self.load, 3000);
            });
    },
    clickPlayVideo: function() {
        comList.addAndPlay(App.Model.Video.create({
            _id: j(this).closest('li').attr('itemid')
        }));
    },
    clickPlayPlaylist: function() {
        var mdl = App.Model.Playlist.create({
            _id: j(this).closest('li').attr('itemid')
        });
        if (parseInt(mdl.videos())) comList.addAndPlay(mdl);
        else App.Modules.toastr.error('Unable to play that playlist');
    },
    clickPlayChannel: function(e) {
        e.preventDefault();
        var mdl = App.Model.Channel.create({
            _id: j(this).closest('li').attr('itemid')
        });
        if (parseInt(mdl.videos())) comList.addAndPlay(mdl);
        else m.route.set('/player.channel?id=' + mdl.id());
    },
    viewLoading: function() {
        return preview('sideblock');
    },
    viewChannels: function() {
        if (_.isEmpty(this.features.channels)) return null;
        var self = this;
        return m('div.channels',
            m('h4', 'Featured Channels'),
            m('ul',
                _.map(this.features.channels, function(item) {
                    return m('li', {
                            'itemid': item.id(),
                            class: (comList.getCurrentModelId() == item.id() ? 'playing' : '')
                        },
                        m('a.img', {
                                onclick: self.clickPlayChannel
                            },
                            m('img', {
                                src: item.image()
                            }),
                            m(elIcon, {
                                icon: (parseInt(item.videos()) ? 'play_arrow' : '')
                            })
                        ),
                        m('a.title', {
                                href: '#/player.channel?id=' + item.id()
                            },
                            m('span.title', item.title()),
                            m('span.subs', numeral(item.subscribers()).format('0a').toUpperCase() + ' subscribers')
                        )
                    );
                })
            )
        );
    },
    viewPlaylists: function() {
        if (_.isEmpty(this.features.playlists)) return null;
        var self = this;
        var partitioned = _.partition(this.features.playlists, function(value) {
            var rawValue = _.find(self.features.raw.playlists, ['id', value.id()]);
            return rawValue && rawValue.limit > 0;
        });
        return m('div.playlists x-sticky top-1',
            _.isEmpty(partitioned[1]) ? null : m('h4', 'Featured Playlists'),
            m('ul',
                _.map(partitioned[1], function(playlist) {
                    return m('li', {
                            'itemid': playlist.id(),
                            class: (comList.getCurrentModelId() == playlist.id() ? 'playing' : '')
                        },
                        m('a.img', {
                                onclick: self.clickPlayPlaylist
                            },
                            m('img', {
                                src: playlist.image()
                            }),
                            m(elIcon, {
                                icon: 'play_arrow'
                            })
                        ),
                        m('a.title', {
                                href: '#/player.playlist?id=' + playlist.id()
                            },
                            m('span.title', playlist.title()),
                            m('span.subs', util.numberComma(playlist.videos()) + ' videos')
                        )
                    );
                })
            ),
            _.isEmpty(partitioned[0]) ? null : _.map(partitioned[0], function(playlist) {
                return [
                    m('h4.item', [
                        m(elIcon, {
                            icon: 'playlist_play'
                        }),
                        m('span', playlist.title()),
                        m(elButtonIcon, {
                            icon: 'play_arrow',
                            title: 'Play All',
                            native_tooltip: true,
                            onclick: function() {
                                comList.addAndPlay(playlist);
                            }
                        })
                    ]),
                    m('ul',
                        _.map(_.find(self.features.playlistItems, ['id', playlist.id()]), function(playlistItem) {
                            return m('li', {
                                    'itemid': playlistItem.id(),
                                    class: (comList.getCurrentModelId() == playlistItem.id() ? 'playing' : '')
                                },
                                m('a.img', {
                                        onclick: self.clickPlayVideo
                                    },
                                    m('img', {
                                        src: playlistItem.image()
                                    }),
                                    m(elIcon, {
                                        icon: 'play_arrow'
                                    })
                                ),
                                m('a.title', {
                                        href: '#/player.video?id=' + playlistItem.id()
                                    },
                                    m('span.title', playlistItem.title()),
                                    m('span.subs', numeral(playlistItem.views()).format('0a').toUpperCase() + ' views')
                                )
                            );
                        })
                    )
                ];
            })
        );
    },
    viewVideos: function() {
        if (_.isEmpty(this.features.videos)) return null;
        var self = this;
        return m('div.videos',
            m('h4', 'Featured Videos'),
            m('ul',
                _.map(this.features.videos, function(item) {
                    return m('li', {
                            'itemid': item.id(),
                            class: (comList.getCurrentModelId() == item.id() ? 'playing' : '')
                        },
                        m('a.img', {
                                onclick: self.clickPlayVideo
                            },
                            m('img', {
                                src: item.image()
                            }),
                            m(elIcon, {
                                icon: 'play_arrow'
                            })
                        ),
                        m('a.title', {
                                href: '#/player.video?id=' + item.id()
                            },
                            m('span.title', item.title()),
                            m('span.subs', numeral(item.views()).format('0a').toUpperCase() + ' views')
                        )
                    );
                })
            )
        );
    },
    viewBody: function() {
        return [
            this.viewChannels(),
            this.viewVideos(),
            this.viewPlaylists()
        ];
    }
});

/*

Featured Channels
Featured Playlists
Featured Videos

/~rest/features
{
    channels: [{
        id: 'cid1'
    }, ...],
    
    playlists: [{
        id: 'pid1',
        limit: 1 // optional
    }, ...]

    videos: [{
        id: 'vid1'
    }, ...]
}

*/