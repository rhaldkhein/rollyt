var _ = require('lodash'),
    md = require('mithril-data'),
    util = require('../libs/util');

// User preference manager
function UserPreference(options) {
    _.bindAll(this, ['_store']);
    this._options = options || {};
    this._loaded = false;
    this._model = this._options.model;
    this._list = this._options.list;
    this.state = undefined;
    this.collection = new md.Collection({
        model: this._model,
        url: this._options.url
    });
}

UserPreference.prototype = {
    _parse: function(to, value) {
        // Console.log('UserPreference', '_parse', to, value);
        // Continue parsing
        switch (to) {
            case 'number':
            case 'float':
                value = parseFloat(value);
                break;
            case 'integer':
                value = parseInt(value);
                break;
            case 'boolean':
                value = _.trim(value) === 'true';
                break;
            case 'array':
                try {
                    value = _.isEmpty(value) ? [] : value;
                    value = _.isArray(value) ? value : JSON.parse(value);
                } catch (ex) {
                    value = [];
                }
                break;
            case 'object':
                try {
                    value = _.isEmpty(value) ? {} : value;
                    value = _.isObject(value) ? value : JSON.parse(value);
                } catch (ex) {
                    value = {};
                }
                break;
        }
        return value;
    },
    _store: function(initVal, key) {
        var self = this;
        var prop = function(valNew) {
            if (arguments.length) {
                return self.set(key, valNew);
            } else {
                return self.get(key);
            }
        };
        return prop;
    },
    populate: function() {
        if (this.isInit()) {
            var data = {};
            var self = this;
            this._list.forEach(function(model) {
                var value;
                var _pref = self.collection.get({
                    preference: model.getJson()
                });
                if (_pref) {
                    value = _.isNil(_pref.value()) ? model.default_value() : _pref.value();
                } else {
                    value = model.default_value();
                }
                data[model.name()] = self._parse(model.data_type(), value);
            });
            this.state = md.State.create(data, {
                store: this._store
            });
        } else {
            throw new Error('not_initialized');
        }
    },
    load: function() {
        var self = this;
        return new Promise(function(resolve, reject) {
            if (self._options.local) {
                self._loaded = true;
                util.nextTick(resolve);
            } else {
                self.collection.fetch().then(function() {
                    self._loaded = true;
                    resolve();
                }, reject);
            }
        });
    },
    update: function() {
        return this.load();
    },
    isInit: function() {
        return this._loaded && this._model && this._list.size();
    },
    // Set a single pref and push to server
    set: function(key, value, isLocal) {
        // Console.log('UserPreference', 'set', key, value);
        var self = this;
        return new Promise(function(resolve, reject) {
            if (self.isInit()) {
                var pref = self._list.get({
                    name: key
                });
                if (pref) {
                    var userPref = self.collection.get({
                        preference: pref.getJson()
                    });
                    if (!userPref) {
                        userPref = new self._model({
                            preference: pref.id()
                        });
                        self.collection.add(userPref);
                    }
                    var oldVal = userPref.value();
                    userPref.value(value);
                    if (isLocal || self._options.local) {
                        // Auto resolve. Stored in memory
                        util.nextTick(resolve);
                    } else {
                        util.nextTick(function() {
                            userPref
                                .save({
                                    background: true
                                })
                                .then(
                                    function() {
                                        resolve();
                                    },
                                    function(err) {
                                        userPref.value(oldVal);
                                        reject(err);
                                    }
                                );
                        });
                    }
                } else {
                    reject('no_preference');
                }
            } else {
                reject('not_initialized');
            }
        });
    },
    // Set all prefs to default. Except arrays and objects.
    setDefaults: function() {
        // WARNING: Not efficient as it loops request
        var keys = _.keys(this.state.toJson());
        if (_.isArray(keys) && !_.isEmpty(keys)) {
            var mdlPref, i;
            for (i = keys.length - 1; i >= 0; i--) {
                mdlPref = parent.App.PreferenceList.find(['name', keys[i]]);
                if (mdlPref.data_type() !== 'array' && mdlPref.data_type() !== 'object')
                    this.set(keys[i], this._parse(mdlPref.data_type(), mdlPref.default_value()));
            }
        }
    },
    // Get a pref from server
    get: function(key) {
        // Console.log('UserPreference', 'get', key);
        if (this.isInit()) {
            var pref = this._list.get({
                name: key
            });
            if (pref) {
                var value;
                var userPref = this.collection.get({
                    preference: pref.getJson()
                });
                if (userPref) {
                    value = _.isNil(userPref.value()) ? pref.default_value() : userPref.value();
                } else {
                    value = pref.default_value();
                }
                return this._parse(pref.data_type(), value);
            } else {
                throw new Error('no_preference');
            }
        } else {
            throw new Error('not_initialized');
        }
    },
    // Returns the stream of a preference
    streamOf: function(key) {
        if (this.isInit()) {
            var pref = this._list.get({
                name: key
            });
            if (pref) {
                var userPref = this.collection.get({
                    preference: pref.getJson()
                });
                if (userPref) {
                    return userPref.value.stream;
                } else {
                    throw new Error('no_user_preference');
                }
            } else {
                throw new Error('no_preference');
            }
        } else {
            throw new Error('not_initialized');
        }
    }
};

module.exports = UserPreference;
