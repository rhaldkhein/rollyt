/**
 * Client configuration file
 */
var info = require('./info.json');
var md = require('mithril-data');
var collectionMethods = require('./methodsCollection');
var modelMethods = require('./methodsModel');
var store = require('libs/store');
var production = __app.env === 'production';
var server = __app.host === 'server';

// Configure application
var config = {
  // App info
  app: info,
  // App status
  prod: production, // false,
  dev: !production, // true,
  debug: !production, // true
  // Paths
  path: {
    app: ''
  },
  url: {
    socket_host: 'https://' + (server ? 'www.rollyt.com' : 'rollyt-dev.com:3001'),
    youtube_upload: 'https://www.youtube.com/upload',
    facebook_page: 'https://www.facebook.com/rollytplayer',
    twitter_page: 'https://www.twitter.com/rollytplayer'
  },
  maxResults: 16,
  defaultPage: production ? 'player.browse' : 'player.dummy',
  defaultVideoId: 'U-gc5lSzWMM', // Default video for player
  defaultKaraokeChannel: 'UCaPwSXblS8F0owlKHGc6huw', // 'UCaPwSXblS8F0owlKHGc6huw',
  autoplayDelay: 1000 * 0, // Delay of autoplay playlist after defautl video is played
  skiplistDelay: 1000, // Skip delay when a video is in skip list
  playerErrorSkipDelay: 5000, // Skip delay when a video is not playable
  playerShareRejoinInterval: 1000 * 10, // Miliseconds
  playerShareRejoinTryLimit: 3
};

config.url.bug_report = '//bitbucket.org/rhaldkhein/rollyt/issues';
config.url.bug_report_create = config.url.bug_report + '/new';

// Expose configs
module.exports = config;

// Configure mithril data
md.config({
  baseUrl: '/~rest',
  keyId: '_id', // For mongodb
  store: store,
  storeBackground: true,
  cache: true,
  collectionMethods: collectionMethods,
  modelMethods: modelMethods,
  placeholder: 'Loading...'
});
