module.exports = {
    getKind: function() {
        var kind = App.Model.classMap[this.options.name];
        if (kind) return kind;
        else throw new Error('Invalid model kind');
    }
};