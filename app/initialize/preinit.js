/**
 * 1st level array is synchronous loading. This is good if the 2nd index element depends on 1st index element.
 * 2nd level array is asynchronous loading.
 */

var m = require('mithril');
var loader = require('loaderjs');

function attach(name, obj) {
  __app[name] = obj;
}

function loginToken(data, resolve, reject) {
  if (!data) {
    if (reject) reject(new Error('Invalid parameters'));
    return;
  }
  Promise.resolve()
    .then(function() {
      if (__app.auth.auth && JSON.parse(global.localStorage.getItem('access_token')) === data.access_token) {
        return __app.auth;
      } else {
        return m
          .request({
            url: '/login/google/token',
            method: 'POST',
            headers: data
          })
          .then(function(body) {
            // Save access_token to compare later
            global.localStorage.setItem('access_token', JSON.stringify(data.access_token));
            return body;
          });
      }
    })
    .then(resolve)
    .catch(reject);
}

function logoutToken(resolve) {
  if (!JSON.parse(global.localStorage.getItem('access_token'))) {
    return resolve({ id: 'guest' });
  }
  // Returns a promise
  m
    .request({
      url: '/logout/token',
      method: 'GET'
    })
    .then(function(body) {
      resolve(body);
    });
}

module.exports = function() {
  return loader.loadAsync([
    function(resolve, reject) {
      try {
        gapi.load('client:auth2', function() {
          gapi.client.setApiKey('AIzaSyAR809J7xJrApSZ8f-OrAzAg2g15EOD9CA');
          Promise.all([
            gapi.client.load('youtube', 'v3'),
            gapi.auth2
              .init({
                client_id: '285836800359-iffif5lcf7mevsk7p72rul9vddgd53re.apps.googleusercontent.com',
                scope: 'profile https://www.googleapis.com/auth/youtube'
              })
              .then(function(auth) {
                return new Promise(function(resolve, reject) {
                  var googleUser = auth.currentUser.get();
                  var attachAccount = function(body) {
                    attach('account', body);
                    resolve();
                  };
                  if (googleUser.isSignedIn()) {
                    var reloadToken = function() {
                      googleUser.reloadAuthResponse().then(function() {
                        loginToken(
                          {
                            account_type: 'google',
                            account_id: googleUser.getId(),
                            id_token: googleUser.getAuthResponse().id_token,
                            access_token: googleUser.getAuthResponse().access_token
                          },
                          function() {
                            setTimeout(reloadToken, 1000 * googleUser.getAuthResponse().expires_in);
                          }
                        );
                      });
                    };
                    loginToken(
                      {
                        account_type: 'google',
                        account_id: googleUser.getId(),
                        id_token: googleUser.getAuthResponse().id_token,
                        access_token: googleUser.getAuthResponse().access_token
                      },
                      function(body) {
                        setTimeout(reloadToken, 1000 * googleUser.getAuthResponse().expires_in);
                        attachAccount(body);
                      },
                      reject
                    );
                  } else {
                    logoutToken(attachAccount, reject);
                  }
                });
              })
          ])
            .then(resolve)
            .catch(reject);
        });
      } catch (err) {
        reject(err);
      }
    },

    // Load the youtube player api.
    function(resolve, reject) {
      try {
        global.onYouTubeIframeAPIReady = resolve;
        var tag = document.createElement('script');
        tag.setAttribute('src', 'https://www.youtube.com/iframe_api');
        var firstScriptTag = document.getElementsByTagName('script')[0];
        tag.setAttribute('onerror', function(err) {
          reject(err);
        });
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
      } catch (err) {
        reject(err);
      }
    }
  ]);
};
