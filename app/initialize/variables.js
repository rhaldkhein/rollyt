/**
 * Global variables
 */
var _ = require('lodash');

exports.cssPx = (function() {
	var _basePx = 16;
	return 1 / _basePx * 1;
})();

// For extensions
var msgId,
	idpair = window.location.hash.match(/msgid=[^#]+/);
if (idpair && idpair[0]) {
	idpair = idpair[0].split('=');
	msgId = idpair[1];
}
exports.msgId = msgId;
exports.isPopup = !!msgId;

_.defaults(exports, __app.config);
