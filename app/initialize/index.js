/**
 * This file runs the initialization of the application.
 * Only this file should write to `App` namespace.
 */

// Imports
var _ = require('lodash'),
  md = require('mithril-data'),
  Preference = require('./userPreference'),
  util = require('libs/util'),
  loader = require('loaderjs');

// Apply vendor extensions
require('../extensions');

// Globalize config
global.Config = require('./config');

// Use console wrapper
global.Console = require('../libs/console');

App.Info = global.Config.app;

// Initialize globals
App.Vars = require('./variables');
App.Modules = require('./modules');
App.Props = require('./props');
App.Components = {}; // Namespace

// Attach reference to resources files
App.Resources = require('./resources');

// Attach actions
App.do = require('../actions');

// Models
App.Model = {};
require('../models');

App.Location = md.State.create({
  country_code: __app.location.country,
  region_code: __app.location.region,
  region_name: __app.location.city
});

// Current account
App.Account = App.Model.Account.create({
  _id: __app.account.id
});

// Preference list
App.PreferenceList = new md.Collection({
  model: App.Model.Preference
});
App.PreferenceList.create(__app.prefs);
__app.prefs = null;

// User preferences (server)
App.Preference = new Preference({
  model: App.Model.UserPreference,
  list: App.PreferenceList,
  url: '/userpreference/all',
  local: App.Account.isGuest()
});

// Settings (localStorage)
App.Settings = require('./settings');

// Folder
App.Folder = new md.Collection({
  model: App.Model.Folder,
  url: (App.Account.isGuest() ? '/local' : '') + '/folder/all',
  state: {
    edit: false
  }
});

// Overrides for guest users
if (App.Account.isGuest()) {
  App.Model.Folder.modelOptions.url = '/local/folder';
}

// Export initialize function
module.exports = function() {
  return loader
    .loadAsync([
      function(resolve, reject) {
        App.Account.fetch().then(function() {
          App.User = App.Account.user();
          resolve();
        }, reject);
      },
      function(resolve, reject) {
        App.Preference.load()
          .then(function() {
            resolve();
          })
          .catch(reject);
        // App.PreferenceList.fetch()
        //   .then(function() {
        //     return App.Preference.load();
        //   })
        //   .then(function() {
        //     resolve();
        //   })
        //   .catch(reject);
      },
      function(resolve, reject) {
        App.Folder.fetch().then(function() {
          resolve();
        }, reject);
      },
      function(resolve, reject) {
        if (App.Account.isGuest()) return resolve();
        App.Modules.skiplist.load().then(resolve, reject);
      }
    ])
    .then(function() {
      App.Preference.populate();
      if (!util.isLastVisitIn(8)) App.Settings.playerExpand(false);
    });
};

// GLOBAL ERROR AND REJECTION HANDLER

// setTimeout(function() {
// throw new Error('Custom Error');
// showFailView();
// }, 1000);

/**
 * Codes:
 * 8 - Failed to execute 'removeChild' on 'Node': The node to be removed is no longer a child of this node.
 */

var forceReloadErrorCode = [8];

function filterError(err) {
  if (!err) return true;
  if (_.indexOf(forceReloadErrorCode, err.code) == -1) return true;
  return false;
}

function showFailView(err) {
  if (filterError(err)) return;
  var content = '<div class="modal-fail"><div></div><div class="w-90 mw6"><div class="tc ph4 pv3 br2">';
  content += '<h1 class="f3 lh-title"><span class="f2">&#x1F613;</span> Ohh ow!</h1>';
  content += '<p class="f4 lh-title">Something went wrong.</p>';
  content += '<p class="f6 lh-copy">Please reload the page to continue.</p>';
  content += '<p><a class="reload button ui-button">Reload Page</a></p><br>';
  content +=
    '<p class="links f7 bt b--light-gray pt2"><a href="/about/bugreport">Bug Report</a> <span class="f4 mh2 light-silver">|</span> <a href="/about/social">Contact Us</a></p>';
  content += '</div></div></div>';
  if ($ && __app.loaded) {
    $(document.body)
      .append(content)
      .find('.modal-fail a.reload')
      .click(function(e) {
        e.preventDefault();
        setTimeout(function() {
          window.location.reload(true);
        }, 300);
      });
  }
}

function showFailException(e) {
  e.preventDefault();
  Console.error(e);
  showFailView(e);
}

function showFailPromise(e) {
  e.preventDefault();
  var err;
  if (e.reason) err = e.reason;
  else if (e.detail && e.detail.reason) err = e.detail.reason;
  Console.error(err);
  if (Raven && err) {
    Raven.captureException(err);
    showFailView();
  }
}

if (global.Config.prod) {
  // Runtime unhandled error
  window.addEventListener('error', showFailException);
  // Promise unhandled rejection
  window.addEventListener('unhandledrejection', showFailPromise);
  window.addEventListener('rejectionhandled', showFailPromise);
}
