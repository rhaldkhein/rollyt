var youtube = require('libs/youtube');

module.exports = {
	parseCreate: function(items) {
		var model, kind, item, i = 0;
		for (; i < items.length; i++) {
			item = items[i];
			kind = item.id.kind || item.kind;
			if (kind === youtube.KIND.video && !this.contains(item.id.videoId || item.id)) {
				model = new App.Model.Video();
				model.parseSet(item);
			} else if (kind === youtube.KIND.playlist && !this.contains(item.id.playlistId || item.id)) {
				model = new App.Model.Playlist();
				model.parseSet(item);
			} else if (kind === youtube.KIND.channel && !this.contains(item.id.channelId || item.id)) {
				model = new App.Model.Channel();
				model.parseSet(item);
			}
			if (model) {
				this.add(model);
			}
			model = null;
		}
	},
	getByLid: function(lid) {
		return this.find(['__model.__lid', lid]);
	}
};
