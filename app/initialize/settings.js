var _ = require('lodash'),
  md = require('mithril-data'),
  store = require('store');

// Add initial settings here
var settings = {
  hide: false,
  visitCount: 0,
  visitTimeStamp: null, // Miliseconds
  visitThreshold: 5, // Days (Initial)
  showWelcome: true,
  showShare: true,
  playerExpand: false,
  playerListMinimize: false,
  playerRemoveAfter: false,
  playerShareJoinedUserId: null,
  playerAutoShare: false,
  playerCurrPlaylist: null,
  playerCurrItems: [],
  playerCurrControls: [],
  playerCurrPlaying: null,
  playerCurrRelated: null,
  searchBarPinText: '',
  mode: 'normal',
  // Methods
  _setDefaults: function() {
    for (var prop in settings) {
      if (!_.isFunction(settings[prop])) {
        module.exports[prop](settings[prop]);
      }
    }
  },
  onchange: _.noop
};

// Local storage function
function localStore(initVal, key, factorykey, options) {
  var prefix = options.prefix || '';
  factorykey = factorykey ? (factorykey + '.') : '';
  key = prefix + factorykey + key;
  var stream = md.stream(store.has(key) ? store.get(key) : initVal);
  stream.map(function(value) {
    store.set(key, value);
    if (state && state.onchange) state.onchange(key, value);
  });
  return stream;
}

var state = md.State.create(settings, {
  prefix: 'settings.',
  store: localStore
});

// Exports
module.exports = state;