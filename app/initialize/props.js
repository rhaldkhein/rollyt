/**
 * Global props / state
 */

var md = require('mithril-data');

exports.defaultPage = md.stream(Config.defaultPage);

// Modal video id

exports.modalVideoId = md.stream(null);

// Block Collection Columns

exports.blockCollectionColumns = md.stream(0);
exports.blockCollectionColumnsChannel = md.stream(0);

// Keys

exports.ctrlPressed = md.stream(false);

exports.breakPointMedium = md.stream(false); // 1000px
exports.breakPointTiny = md.stream(false); // 380px

exports.cookieAccepted = md.stream(!!__app.cookieacc);
