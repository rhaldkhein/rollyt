/**
 * Global modules
 */

var util = require('libs/util');

exports._ = require('lodash');
exports.m = require('mithril');

exports.toastr = require('toastr');
exports.toastr.options = {
  // timeOut: 10000,
  // extendedTimeOut: 10000,
  showMethod: 'slideDown',
  hideMethod: 'slideUp',
  positionClass: (util.isMobile ? 'toast-bottom-right' : 'toast-top-right')
};

exports.tippy = require('tippy.js');

exports.title = require('../libs/title');

exports.push = require('push.js');

exports.skiplist = require('../libs/skiplist');

exports.messenger = require('../libs/messenger');

exports.ui = {
  switch: require('comp/elements/switch')
};

exports.modal = {
  ConfirmAction: require('comp/modals/modalConfirmAction')
};

exports.loaderjs = window.LoaderJS;