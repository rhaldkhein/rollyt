var ModalCreatePlaylist = require('comp/modals/modalCreatePlaylist');

module.exports = function(Action) {
	Action.createLocalPlaylist = function(callback) {
		ModalCreatePlaylist.show({
			title: 'Create Rollyt Playlist',
			ondone: callback
		});
	};
};
