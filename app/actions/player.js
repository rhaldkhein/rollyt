var _ = require('lodash');
var m = require('mithril');

module.exports = function(Action) {
  Action.player = function(command, args) {
    var player = App.Components.Player;
    var current = App.Components.Current;
    switch (command) {
      case 'hello':
        Console.log('Hello', args[0]);
        break;
      case 'toggle_repeat':
        player.state.repeat().current = player.state.repeat().current ? 0 : 1;
        player.clickRepeat();
        break;
      case 'toggle_random':
        player.state.random().current = player.state.random().current ? 0 : 1;
        player.clickRandom();
        break;
      case 'stop':
        player.stop();
        break;
      case 'change_playlist':
        player.changePlaylistById(args[0]);
        break;
      case 'set_item_setting':
        var setting,
          item = player.list.find(['_id', args[0]]);
        if (item) {
          setting = player.list.stateOf(item).settings()[args[1]];
          if (setting) setting(args[2]);
          m.redraw();
          player.broadcast();
        }
        break;
      case 'player_pause':
        if (_.has(current, 'player.videoPlayer.pauseVideo')) current.player.videoPlayer.pauseVideo();
        break;
      case 'player_play':
        if (_.has(current, 'player.videoPlayer.playVideo')) current.player.videoPlayer.playVideo();
        break;
      case 'player_volume':
        if (_.has(current, 'player.videoPlayer.getVolume')) {
          var vp = current.player.videoPlayer;
          var vol = vp.getVolume();
          if (args[0]) vol += 10;
          else vol -= 10;
          if (vol > 100) vol = 100;
          else if (vol < 0) vol = 0;
          vp.setVolume(vol);
        }
        break;
      default:
        Console.log(command, args);
        break;
    }
    m.redraw();
  };
};
