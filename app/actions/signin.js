var GAPI = global.gapi;

module.exports = function(Action) {
  Action.signin = function() {
    GAPI.auth2.getAuthInstance().signIn({
      ux_mode: 'popup',
      prompt: 'select_account'
    }).then(function() {
      if (App.Vars.msgId) {
        App.Modules.messenger.send('app#reopen');
      } else {
        window.location.replace('/');
      }
    }).catch(function() {
      // If auth instance failed. Nothing to do more.
    });
  };
};