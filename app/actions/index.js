function Action() {
	var args = Array.prototype.slice.call(arguments);
	Action[args.shift()].apply(global, args);
}

require('./signin')(Action);
require('./createLocalPlaylist')(Action);
require('./player')(Action);

module.exports = Action;
