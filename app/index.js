// Modules
var m = require('mithril'),
  _ = require('lodash'),
  util = require('libs/util'),
  ModalWelcome = require('./comp/modals/modalWelcome'),
  ModalShare = require('./comp/modals/modalShare'),
  isprod = __app.env === 'production';

// Create context for requiring pages at runtime
var requirePage = require.context('./page', true, /^.*$/);

// Private variables
var _templateFilePrefix = '_',
  _rootComponent = requirePage('./' + _templateFilePrefix + 'page');

// Router object
var router = {
  components: null,
  onmatch: function(param) {
    var childComps;
    try {
      var pathNames = _.split(param.page || App.Props.defaultPage(), '.'),
        lastIndex = pathNames.length - 1,
        current = '.';
      childComps = _.reverse(
        _.map(pathNames, function(value, index) {
          current += '/' + value;
          return requirePage(current + (index < lastIndex ? '/' + _templateFilePrefix + value : ''));
        })
      );
    } catch (e) {
      // Display Not Found
      Console.error(e);
      childComps = [require('./comp/error/notfound')];
    }
    // Clear modal video player when route is changed
    App.Props.modalVideoId(null);
    // Flushing child components
    this.components = childComps;
  },
  render: function() {
    return m(
      _rootComponent,
      _.reduce(
        this.components,
        function(child, component, index) {
          return index > 0 ? m(component, child) : m(component);
        },
        null
      )
    );
  }
};

// Post initialization
function postInit() {
  // Send ready signal to browser extension
  App.Modules.messenger.ready();
  // Show welcome modal
  if (App.User.isGuest()) ModalWelcome.show();
  // Show social media
  if (!App.Settings.showWelcome() && App.Settings.showShare()) {
    if (App.Settings.visitCount() >= App.Settings.visitThreshold()) {
      App.Settings.visitCount(0);
      ModalShare.show();
    } else if (!util.isLastVisitIn(8)) {
      App.Settings.visitCount(App.Settings.visitCount() + 1);
    }
  }
}

// Set mode to hash
m.route.prefix('#');

// App namespace
global.App = {};

// Get browser info
__app.browser = (function() {
  var agent = (function() {
    var ua = navigator.userAgent,
      tem,
      M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
      tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
      return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
      tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
      if (tem != null)
        return tem
          .slice(1)
          .join(' ')
          .replace('OPR', 'Opera');
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
    return M.join(' ');
  })().split(' ');
  return {
    title: agent[0].toLowerCase(),
    version: parseFloat(agent[1])
  };
})();

// Check browser compatibility
if (
  !__app.browsers.hasOwnProperty(__app.browser.title) ||
  __app.browser.version < __app.browsers[__app.browser.title]
) {
  // Redirect to browser check if not supported
  window.location.replace(
    '/browsercheck' + (isprod ? '?t=' + __app.browser.title + '&v=' + __app.browser.version : '')
  );
} else {
  // Continue loading the resource
  Promise.config({
    cancellation: true
  });
  // Run the initializer before mounting
  var preinit = require('./initialize/preinit');
  preinit()
    .then(function() {
      var init = require('./initialize');
      return init();
    })
    .then(function() {
      // Create dev namespace for debugging
      if (global.Config.dev) global.App.dev = {};
      // Bind route to page
      m.route(document.getElementById('root'), '/', {
        '/': router,
        '/:page': router,
        '/:page/:query...': router
      });
      // Post init
      setTimeout(postInit, 300);
    });
}
