## Installation & Starting Server

```sh
# 0. Install Node.js 8.9.4 and Mongodb 3.4.14
# 1. Install dependencies
$ npm install
# 2. Setup mongo in separate termina. Match credentials in server's config
$ mongod
# 3. Compile code by runnung gulp task `run`
# 4. Set appropriate environment variables
# 4. Start server
$ node start_server
```

#### Environment Variables

NODE_ENV = production | development (default)  
APP_STATUS = open | close (default) | maintenance | construction  
APP_HOST = server | local (default)  - development purpose, always `server` on online server  
APP_HTTPS = true | false (default) - should server creates an HTTPS express server or not  
APP_ONLY_HTTPS = true | false (default) - to redirect http to https or not
APP_IP = undefined (default) - manual ip. development purpose only  
APP_ENV = beta | data - there are 2 separate database instances. for beta testing and production data  

## Docker

#### Local Docker Setup (Deprecated)

```sh
$ docker pull mongo:3.4.3
$ docker run -d --name mongodbcon mongo:3.4.3
$ docker build -t rollyt:vtest .
$ docker run -d -e NODE_ENV=production -e APP_STATUS=open -e APP_HTTPS=true -p 80:80 -p 443:443 --link mongodbcon --name rollytcon rollyt:vtest
```

- Always tag image with version when publishing `<image name>:<version>`

#### Tips (Deprecated)

```sh
# Stop all containers, If error, try to run on PowerShell (Admin)
$ docker rm -f $(docker ps -a -q)
# Remove unused images
$ docker system prune
```

## AWS EC2

#### Publishing for Staging and Production

- SSH to Instance
- Enter sudo mode with `$ sudo -s`
- CD to project source folder and `git pull origin master`
- CD to `build` folder in git root project
- View `README` for running docker compose instructions
- For `APP_PORT`, use `beta` for staging as production `data` should NOT be used for testing
- Spin the `Database` MongoDB using `./up-db.sh <args...>`
- For `APP_PORT`, if 3000 is on production, then use 3001 for staging. or vice versa
- For `APP_STATUS`, choose desired status of website
- For `APP_ONLY_HTTPS`, to redirect http to https or not
- Spin the `Application` using `./up-app.sh <args...>`
- Login to AWS EC2 Console
- Modify `Load Balancer` listeners to route to correct `Target Group`
- `x-load-balancer-alpha` [80 HTTP and 443 HTTPS] and `...-ontest` [80] (for staging)
- `x-target-group-alpha-a` routes to port `3000` and `...-b` to `3001`

Notes:

- Make sure to change HTTP (80) first then wait and change HTTPS (443) `x-load-balancer-alpha` (Target Group)
- `www.rollyt.com` routes to `x-load-balancer-alpha` (production)
- `ontest.rollyt.com` routes to `x-load-balancer-alpha-ontest` (staging/testing)
- A PDF route visual diagram is available in dropbox

## Sentry.io

#### Upload Release

```sh
# Install sentry-cli and login, omit this step if not necessary
$ sentry-cli login
# Create new relese
$ sentry-cli releases -o rkv -p rollyt new <version>
# Upload sourcemap
$ sentry-cli releases -o rkv -p rollyt files <version> upload-sourcemaps --url-prefix ~/app/dist/ ../maps/rollyt/<version>
```

## More Info 

#### Links

1. [Amazon Elastic File System (Amazon EFS)](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AmazonEFS.html)  
3. [SSH Connect to Your Container Instance](http://docs.aws.amazon.com/AmazonECS/latest/developerguide/instance-connect.html)  