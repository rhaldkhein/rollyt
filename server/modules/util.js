'use strict';

const _ = require('lodash'),
  moment = require('moment');

module.exports = {
  dateToISOString: function(date) {
    let _date;
    switch (date) {
      case 'today':
        _date = moment().subtract(1, 'days');
        break;
      case 'yesterday':
        _date = moment().subtract(2, 'days');
        break;
      case 'week':
        _date = moment().subtract(7, 'days');
        break;
      case 'month':
        _date = moment().subtract(1, 'months');
        break;
      case 'year':
        _date = moment().subtract(1, 'years');
        break;
      default:
        _date = moment().subtract(0, 'days');
    }
    return _date.toISOString();
  },
  processToJson: function(keys) {
    let procJson = _.pickBy(process, function(val) {
      return _.isNumber(val) || _.isString(val) || _.isPlainObject(val);
    });
    procJson.cwd = process.cwd();
    procJson.memoryUsage = process.memoryUsage();
    procJson.uptime = process.uptime();
    return keys ? _.pick(procJson, keys) : procJson;
  },
  envToJson: function(keys) {
    let envJson = keys
      ? _.pick(process.env, keys)
      : _.pickBy(process.env, function(val, key) {
          if (_.includes(key, 'PASS')) return false;
          return _.startsWith(key, 'APP_') || key === 'NODE_ENV';
        });
    return envJson;
  },
  prettifyJson: function(json) {
    return _.join(
      _.map(json, function(val, key) {
        return key + '=' + val;
      }),
      ' | '
    );
  }
};
