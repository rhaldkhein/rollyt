'use strict';
const _ = require('lodash'),
  async = require('async'),
  validator = require('validator'),
  google = require('googleapis'),
  youtube = google.youtube('v3'),
  config = global.CONFIG,
  presetList = require(PATH.server + '/presets/objects/list'),
  Model = require(PATH.model),
  Promise = require('bluebird');

/** * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Playlist Items
 * - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * local_playlist: docPlaylist.id
 * resource_type: item.resource_type
 * resource_id: item.resource_id
 * flags: item.flags
 */

const itemTypes = [
    'youtube#video',
    'youtube#channel',
    'youtube#playlist',
    'local#playlist',
    'local#list'
  ],
  optPlayLimit = {
    min: 0,
    max: 10
  },
  optItemLimit = {
    min: 5,
    max: 20
  },
  optOrder = ['date', 'rating', 'relevance', 'title', 'viewCount'],
  optDuration = ['any', 'short', 'medium', 'long'],
  optDate = ['today', 'yesterday', 'week', 'month', 'year'];

// Expose item types
exports.itemTypes = itemTypes;

// Flag validators
exports.localPlaylistItemFlagValidators = {
  playLimit: function(value) {
    return validator.isInt(value.toString(), optPlayLimit);
  },
  shuffle: function(value) {
    return validator.isBoolean(value.toString());
  },
  itemLimit: function(value) {
    return validator.isInt(value.toString(), optItemLimit);
  },
  order: function(value) {
    return validator.isIn(value, optOrder);
  },
  duration: function(value) {
    return validator.isIn(value, optDuration);
  },
  date: function(value) {
    return validator.isIn(value, optDate);
  }
};

// Get flag validator names
exports.localPlaylistItemFlagKeys = _.keys(
  exports.localPlaylistItemFlagValidators
);

// Validation and sanitization single playlist item
exports.localPlaylistItem = function(item, done) {
  // 1: Validate type
  if (validator.isIn(item.resource_type, itemTypes)) {
    var parts = item.resource_type.split('#'),
      error = false;
    // 2: Validate flags
    if (_.isPlainObject(item.flags)) {
      // Remove unwanted keys
      var flags = (item.flags = _.pick(
        item.flags,
        exports.localPlaylistItemFlagKeys
      ));
      // Validate each flags
      for (var key in flags) {
        if (!flags.hasOwnProperty(key)) continue;
        if (!exports.localPlaylistItemFlagValidators[key](flags[key])) {
          error = true;
          break;
        }
      }
      if (error) {
        done('Invalid flags');
        return;
      }
    }
    if (parts[0] === 'youtube') {
      // 3.1: Validate youtube type id
      youtube[parts[1] + 's'].list(
        {
          part: 'snippet',
          id: item.resource_id,
          key: config.youtube.key,
          maxResults: 1
        },
        function(err, response) {
          if (err) {
            done(err);
          } else {
            if (response.items[0]) {
              done();
            } else {
              done('No result');
            }
          }
        }
      );
    } else if (parts[1] === 'list') {
      // Local list
      presetList
        .get(item.resource_id)
        .then(function() {
          done();
        })
        .catch(done);
    } else {
      // Local playlist
      Model.LocalPlaylist.findById(item.resource_id, function(err, playlist) {
        if (!err && playlist) done();
        else done('No Playlist');
      });
    }
  } else {
    done('Unknown type');
  }
};

exports.localPlaylistItems = function(items) {
  return new Promise(function(resolve, reject) {
    if (!_.isEmpty(items)) {
      if (items.length > config.validation.playerItemsLimit) {
        reject(new Error('Items exceeded'));
      } else {
        var samples = _.sampleSize(
          items,
          config.validation.playlistItemsSampleSize
        );
        async.each(
          samples,
          function(item, done) {
            exports.localPlaylistItem(item, done);
          },
          function(err) {
            if (err) reject(new Error(err));
            else resolve();
          }
        );
      }
    } else {
      reject(new Error('Items must not be empty'));
    }
  });
};
