'use strict';

function code(_code, _category, _message) {
    return {
        code: _code,
        category: _category,
        name: _message
    };
}

// Codes holder.
const codes = {};

/**
 * Generic Error Code.
 */

codes['GECE'] = code('GECE', 'GENERIC_ERROR', 'Custom Error');
codes['GEUE'] = code('GEUE', 'GENERIC_ERROR', 'Unkown Error');
codes['GEIE'] = code('GEIE', 'GENERIC_ERROR', 'Internal Error');
codes['GENF'] = code('GENF', 'GENERIC_ERROR', 'Not Found');

/**
 * REST Error Codes.
 */

codes['RECE'] = code('RECE', 'REST_ERROR', 'Custom Error');
codes['REUE'] = code('REUE', 'REST_ERROR', 'Unknown Error');
codes['REIE'] = code('REIE', 'REST_ERROR', 'Internal Error');
codes['RENF'] = code('RENF', 'REST_ERROR', 'Not Found');
codes['RENA'] = code('RENA', 'REST_ERROR', 'Not Available');
codes['REBR'] = code('REBR', 'REST_ERROR', 'Bad Request');
codes['REUA'] = code('REUA', 'REST_ERROR', 'Unauthorized');
codes['REPC'] = code('REPC', 'REST_ERROR', 'Private Content');
codes['RENO'] = code('RENO', 'REST_ERROR', 'Not Owner');
codes['RENS'] = code('RENS', 'REST_ERROR', 'Document Not Saved');
codes['RENC'] = code('RENC', 'REST_ERROR', 'Document Not Created');
codes['RENU'] = code('RENU', 'REST_ERROR', 'Document Not Updated');
codes['REND'] = code('REND', 'REST_ERROR', 'Document Not Deleted');
codes['RENP'] = code('RENP', 'REST_ERROR', 'Document Not Updatable');
codes['RENR'] = code('RENR', 'REST_ERROR', 'Document Not Creatable');
codes['RENL'] = code('RENL', 'REST_ERROR', 'Document Not Deletable');
codes['REAX'] = code('REAX', 'REST_ERROR', 'Document Already Exist');
codes['RECR'] = code('RECR', 'REST_ERROR', 'Circular Reference Error');
codes['REDK'] = code('REDK', 'REST_ERROR', 'Document Duplicate Key');

/**
 * Socket Error Codes.
 */

codes['SENF'] = code('SENF', 'SOCKER_ERROR', 'Not Found');
codes['SEIE'] = code('SEIE', 'SOCKER_ERROR', 'Internal Error');
codes['SENA'] = code('SENA', 'SOCKER_ERROR', 'Not Available');
codes['SEBR'] = code('SEBR', 'SOCKER_ERROR', 'Bad Request');
codes['SEUA'] = code('SEUA', 'SOCKER_ERROR', 'Unauthorized');
codes['SENO'] = code('SENO', 'SOCKER_ERROR', 'Not Owner');
codes['SEIT'] = code('SEIT', 'SOCKER_ERROR', 'Invalid Token');
codes['SEIK'] = code('SEIK', 'SOCKER_ERROR', 'Invalid Key');
codes['SEDR'] = code('SEDR', 'SOCKER_ERROR', 'Duplicate Room');

/**
 * Export codes.
 */

exports.get = function(code) {
    return codes[code] || this.default('GENERIC_ERROR');
};

exports.generate = function(code) {
    var errObj = exports.get(code);
    return new RequestError(errObj.code, errObj.name, errObj.category);
};

exports.default = function(category) {
    switch (category) {
        case 'GENERIC_ERROR':
            return codes['GEUE'];
        case 'REST_ERROR':
            return codes['REUE'];
    }
};