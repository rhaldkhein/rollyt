'use strict';

/**
 * Log Levels: ALL < TRACE < DEBUG < INFO < WARN < ERROR < FATAL < MARK < OFF
 */

// Imports
const production = process.env.NODE_ENV == 'production';
const config = require('../config/logger');
const log4js = require('log4js');

// Configuration
log4js.configure(config);

// Expose global own console class
const Logger = {
  console: log4js.getLogger(),
  file: log4js.getLogger(production ? 'file' : undefined),
  email: log4js.getLogger(production ? 'email' : undefined),
  slack: log4js.getLogger(production ? 'slack' : undefined)
};

// Exports
global.Logger = module.exports = Logger;