'use strict';

// Imports.
const mongoose = require('mongoose'),
	types = mongoose.Schema.Types,
	config = global.CONFIG;

// Name of the schema.
exports.name = 'PreferenceValue';

// Schema definition.
exports.schema = {
	preference: {
		type: types.ObjectId,
		ref: 'Preference'
	},
	value: String,
	caption: String
};