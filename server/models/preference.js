'use strict';

// Imports.
const mongoose = require('mongoose'),
	types = mongoose.Schema.Types,
	config = global.CONFIG;

// Name of the schema.
exports.name = 'Preference';

// Schema definition.
exports.schema = {
	name: {
		type: String,
		required: true,
		unique: true
	},
	description: String,
	data_type: String,
	default_value: String,
	min_value: Number,
	max_value: Number
};