'use strict';

// Imports.
const mongoose = require('mongoose'),
	types = mongoose.Schema.Types,
	config = global.CONFIG;

// Name of the schema.
exports.name = 'StarredVideo';

// Schema definition.
exports.schema = {
	user: {
		type: types.ObjectId,
		ref: 'User'
	},
	video_id: String,
	folder: {
		type: types.ObjectId,
		ref: 'Folder'
	},
	date_created: {
		type: Date,
		default: Date.now
	}
};