'use strict';

// Imports.
const _ = require('lodash'),
  mongoose = require('mongoose'),
  types = mongoose.Schema.Types,
  google = require('googleapis'),
  youtube = google.youtube('v3'),
  presetList = require(PATH.server + '/presets/objects/list'),
  request = require('request'),
  config = global.CONFIG;

// Name of the schema.
exports.name = 'LocalPlaylist';

// Schema definition.
exports.schema = {
  user: {
    type: types.ObjectId,
    ref: 'User'
  },
  title: {
    type: String
  },
  image: {
    type: String
  },
  folder: {
    type: types.ObjectId,
    ref: 'Folder'
  },
  is_private: {
    type: Boolean,
    default: false
  },
  flags: {
    type: types.Mixed
  },
  order: {
    type: [{ type: types.ObjectId, ref: 'LocalPlaylistItem' }]
  },
  date_modified: {
    type: Date,
    default: Date.now
  },
  date_created: {
    type: Date,
    default: Date.now
  }
};

exports.projection = {
  user: 1,
  title: 1,
  image: 1,
  folder: 1,
  is_private: 1,
  flags: 1,
  date_modified: 1,
  date_created: 1,
  item_count: {
    $size: '$order'
  }
};

// Hidden fields.
exports.hidden = ['order'];

exports.options = {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
};

exports.config = function(schema) {
  schema.pre('save', function(next) {
    this.date_modified = new Date();
    next();
  });

  schema.virtual('item_count').get(function() {
    return this._size || 0;
  });

  schema.virtual('_type').get(function() {
    return 'local#playlist';
  });
};

exports.methods = {
  getItemCount: function(done) {
    var self = this;
    var agg = this.constructor
      .aggregate()
      .match({
        _id: this._id
      })
      .project({
        item_count: {
          $size: '$order'
        }
      });
    if (done) {
      agg.exec(function(err, docs) {
        if (docs && docs[0]) self._size = docs[0].item_count;
        done(err);
      });
    } else {
      return agg.exec().then(function(docs) {
        if (docs && docs[0]) self._size = docs[0].item_count;
      });
    }
  }
};

exports.statics = {
  getItemImage: function(item, access_token) {
    return new Promise(function(resolve, reject) {
      switch (item.resource_type) {
        case 'youtube#video':
          youtube.videos.list(
            {
              part: 'snippet',
              id: item.resource_id,
              key: config.youtube.key,
              maxResults: 1
            },
            function(err, response) {
              if (err) {
                reject(err);
              } else {
                if (response.items[0]) {
                  resolve(response.items[0].snippet.thumbnails.medium.url);
                } else {
                  reject(new Error('No result'));
                }
              }
            }
          );
          break;
        case 'youtube#playlist':
          youtube.playlistItems.list(
            {
              part: 'snippet',
              playlistId: item.resource_id,
              key: config.youtube.key,
              maxResults: 1
            },
            function(err, response) {
              if (err) {
                reject(err);
              } else {
                if (response.items[0]) {
                  resolve(response.items[0].snippet.thumbnails.medium.url);
                } else {
                  reject(new Error('No result'));
                }
              }
            }
          );
          break;
        case 'youtube#channel':
          youtube.search.list(
            {
              part: 'snippet',
              channelId: item.resource_id,
              type: 'video',
              key: config.youtube.key,
              maxResults: 1
            },
            function(err, response) {
              if (err) {
                reject(err);
              } else {
                if (response.items[0]) {
                  resolve(response.items[0].snippet.thumbnails.medium.url);
                } else {
                  reject(new Error('No result'));
                }
              }
            }
          );
          break;
        case 'local#list':
          presetList
            .get(item.resource_id)
            .then(function(objList) {
              if (_.startsWith(objList.kind, 'local#')) {
                resolve(null);
                return;
              }
              objList.params.key = config.youtube.key;
              objList.params.access_token = access_token;
              return request.get(
                {
                  url: objList.link,
                  json: true,
                  qs: objList.params
                },
                function(err, res, body) {
                  if (err) {
                    reject(new Error('No result'));
                  } else {
                    if (body && body.items && body.items[0]) {
                      resolve(body.items[0].snippet.thumbnails.medium.url);
                    } else {
                      reject(new Error('No result'));
                    }
                  }
                }
              );
            })
            .catch(reject);
          break;
        case 'local#playlist':
          resolve(null);
          // NOTICE: Add thumbnail
          break;
        default:
          reject(new Error('Invalid playlist item type'));
          break;
      }
    });
  }
};
