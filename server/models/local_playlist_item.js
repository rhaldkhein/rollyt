'use strict';

// Imports.
const mongoose = require('mongoose'),
    types = mongoose.Schema.Types;

// Name of the schema.
exports.name = 'LocalPlaylistItem';

// Schema definition.
exports.schema = {
    local_playlist: {
        type: types.ObjectId,
        ref: 'LocalPlaylist'
    },
    resource_type: {
        type: String
    },
    resource_id: {
        type: String
    },
    flags: {
        type: types.Mixed
    }
};

exports.config = function(schema) {
    schema.index({
        local_playlist: 1,
        resource_id: 1
    }, {
        unique: true,
        name: 'playlist_resource_index'
    });
};