'use strict';

// Imports.
const mongoose = require('mongoose'),
  types = mongoose.Schema.Types;

// Name of the schema.
exports.name = 'SavedRoom';

// Schema definition.
exports.schema = {
  owner: {
    type: types.ObjectId,
    index: true,
    required: true,
    ref: 'User'
  },
  user: {
    type: types.ObjectId,
    index: true,
    required: true,
    ref: 'User'
  },
  name: {
    type: String,
    index: true,
    required: true
  },
  picture: String,
  password: {
    type: String,
    select: false // Ok
  }
};

exports.hidden = ['password'];