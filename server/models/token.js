'use strict';

// Name of the schema.
exports.name = 'Token';

// Schema definition.
exports.schema = {
	account_type: {
		type: String
	},
	account_id: {
		type: String
	},
	id_token: {
		type: String
	},
	access_token: {
		type: String
	}
};