'use strict';

/**
 * This file is the constructor for all models.
 */
const _ = require('lodash'),
    config = global.CONFIG,
    fs = require('fs'),
    mongoose = require('mongoose');

function listRefs(schema) {
    let list = [];
    _.each(schema, function(value, key) {
        if (value.ref)
            list.push(key);
    });
    return list;
}

// Load and plug all plugins to schemas.
fs.readdirSync(config.paths.plugins).forEach(function(file) {
    mongoose.plugin(require(config.paths.plugins + '/' + file));
});

// Load all schemas and create its model.
// readdirSync is fine as this will execute once on startup.
let models = {};

// Export all models.
module.exports = models;

// Populate
_.pull(fs.readdirSync(PATH.model), 'index.js').forEach(function(file) {
    if (_.endsWith(file, '.js')) {
        let schemaDef = require(PATH.model + '/' + file),
            schemaIns = new mongoose.Schema(schemaDef.schema, schemaDef.options || {});

        // WARNING!!! CONFLICT: Might get conflict in the future.
        // List all fields that is reference to other model.
        schemaIns.references = listRefs(schemaDef.schema);

        // WARNING!!! CONFLICT: Might get conflict in the future.
        schemaIns.cleaners = _.defaults(schemaDef.cleaners, {});

        // WARNING!!! CONFLICT: Might get conflict in the future.
        schemaIns.hidden = schemaDef.hidden || [];

        // Additional schema methods.
        _.extend(schemaIns.methods, _.defaults(schemaDef.methods, {}));

        // Additional schema statics.
        _.extend(schemaIns.statics, _.defaults(schemaDef.statics, {
            projection: function() {
                return schemaDef.projection;
            }
        }));

        // Configure schema.
        if (schemaDef.config) {
            schemaDef.config(schemaIns);
        }

        // Process hidden fields
        if (schemaDef.hidden) {
            schemaDef.options = schemaDef.options || {};
            schemaDef.options.toJSON = schemaDef.options.toJSON || {};
            schemaDef.options.toJSON.transform = function(doc, ret) {
                // Applies when the documents toJSON method is called.
                // Everytime we send object to client, toJSON is called.
                // Useful if we want to HIDE data from the client. 
                return _.omit(ret, schemaDef.hidden);
            };
            schemaIns.set('toJSON', schemaDef.options.toJSON);
        }

        // Create new model.
        let model = models[schemaDef.name] = mongoose.model(schemaDef.name, schemaIns);

        // Post config.
        if (schemaDef.postconfig) {
            schemaDef.postconfig(model);
        }
    }
});