'use strict';

// Imports.
const shortid = require('shortid'),
  mongoose = require('mongoose'),
  types = mongoose.Schema.Types;

// Name of the schema.
exports.name = 'ActiveRoom';

// Schema definition.
exports.schema = {
  _id: {
    type: String,
    required: true,
    unique: true,
    default: shortid.generate
  },
  user: {
    type: types.ObjectId,
    required: true,
    unique: true,
    ref: 'User'
  },
  picture: String,
  name: {
    type: String,
    index: true,
    required: true
  },
  socket: {
    type: String,
    required: true
  },
  token: {
    // For QR code connection
    type: String,
    required: true
  },
  password: {
    // For search connection (auto generated)
    // Null to use user specified password
    type: String
  },
  open: {
    type: Boolean,
    required: true,
    default: false
  },
  sharekey: {
    type: Boolean,
    default: false
  },
  hidden: {
    type: Boolean,
    default: false
  },
  private: {
    type: Boolean,
    default: true
  },
  date_active: {
    type: Date,
    default: Date.now
  }
};