'use strict';

// Imports.
const mongoose = require('mongoose'),
  types = mongoose.Schema.Types,
  _ = require('lodash'),
  shortid = require('shortid');

// Name of the schema.
exports.name = 'User';

// Schema definition.
exports.schema = {
  sid: {
    type: String,
    index: true,
    unique: true,
    default: shortid.generate
  },
  codename: {
    type: String,
    trim: true,
    index: true,
    unique: true,
    sparse: true
  },
  password: {
    type: String,
    trim: true,
    select: false // Ok
  },
  current_account: {
    type: types.ObjectId,
    ref: 'Account'
  },
  date_created: {
    type: Date,
    default: Date.now
  },
  date_modified: {
    type: Date,
    default: Date.now
  }
};

// Schema options.
exports.options = {};

// Hidden fields and will NOT be send to client.
exports.hidden = ['email', 'password'];

// Additional methods for schema. For document.
exports.methods = {};

// Additional statics for schema. For model.
exports.statics = {};

// This is called right after schema is created.
// For us to configure the schema.
exports.config = function(schema) {

  // schema.index({
  //     sid: 1
  // }, {
  //     unique: true
  // });

  schema.pre('save', function(next) {
    this.date_modified = new Date();
    next();
  });
};

let publicProps = ['_id', 'codename', 'current_account', 'date_created', 'date_modified'];

exports.cleaners = {
  onlypublic: function(obj) {
    // Unsetting all as this does not need to update.
    if (obj) {
      return _.pick(obj, publicProps);
    }
    // Return false if cleaning is not successful.
    return false;
  }
};