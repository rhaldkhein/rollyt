'use strict';

// Imports.
const mongoose = require('mongoose'),
    types = mongoose.Schema.Types;

// Name of the schema.
exports.name = 'SkipVideo';

// Schema definition.
exports.schema = {
    user: {
        type: types.ObjectId,
        ref: 'User',
        unique: true
    },
    videos: [String]
};