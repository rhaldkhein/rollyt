'use strict';

// Imports.
const _ = require('lodash'),
  mongoose = require('mongoose'),
  types = mongoose.Schema.Types,
  config = global.CONFIG;

// Name of the schema.
exports.name = 'AppSetting';

// Schema definition.
exports.schema = {
  name: {
    type: String,
    required: true,
    unique: true
  },
  description: String,
  value_type: String,
  value_default: String,
  value: String
};

function parseValue(value, type) {
  switch (type) {
    case 'object':
    case 'array':
      return JSON.parse(value);
    case 'int':
      return parseInt(value);
    case 'float':
      return parseFloat(value);
    case 'boolean':
      return value == 'true' || value != '0' || value != '';
    case 'string':
    default:
      return value;
  }
}

function validate(value, type) {
  switch (type) {
    case 'object':
      return _.isPlainObject(value);
    case 'array':
      return _.isArray(value);
    case 'int':
      return _.isInteger(value);
    case 'float':
      return _.isNumber(value);
    case 'boolean':
      return _.isBoolean(value);
    case 'string':
    default:
      return _.isString(value);
  }
}

exports.statics = {
  get: function(name, callback) {
    return this.findOne({
      name: name
    })
      .exec()
      .then(function(doc) {
        if (doc) {
          doc = parseValue(doc.value || doc.value_default, doc.value_type);
          if (callback) callback(null, doc);
        } else {
          throw new Error('No Document');
        }
        return doc;
      })
      .catch(function(err) {
        if (callback) callback(err);
      });
  },
  set: function(name, value, callback) {
    this.findOne(
      {
        name: name
      },
      function(err, doc) {
        if (!err && doc) {
          try {
            if (validate(value, doc.value_type)) {
              doc.value = JSON.stringify(value);
              doc.save(function(err) {
                if (!err) callback();
                else callback(err);
              });
            } else {
              callback('REBR');
            }
          } catch (e) {
            callback(e);
          }
        } else {
          callback(err);
        }
      }
    );
  }
};
