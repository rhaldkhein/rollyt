'use strict';

// Imports.
const _ = require('lodash'),
    mongoose = require('mongoose'),
    types = mongoose.Schema.Types;

// Name of the schema.
exports.name = 'Account';

// Schema definition.
exports.schema = {
    type: {
        type: String,
        required: true
    },
    type_id: {
        type: String,
        index: true,
        required: true
    },
    firstname: String,
    lastname: String,
    avatar: String,
    email: String,
    friends: [String],
    access_token: String,
    user: {
        type: types.ObjectId,
        ref: 'User'
    },
    date_created: {
        type: Date,
        default: Date.now
    },
    date_modified: {
        type: Date,
        default: Date.now
    }
};

exports.projection = {
    type: 1,
    type_id: 1,
    firstname: 1,
    lastname: 1,
    avatar: 1,
    email: 1,
    user: 1,
    date_modified: 1,
    date_created: 1
};

// Hidden fields and will NOT be send to client.
exports.hidden = ['access_token'];

// Cleaner functions are use to clean object before
// doing something like create, save, update or etc.
// Note that this is NOT automatic. This can also
// act as sanitizer or custom validator.

let publicProps = ['_id', 'type', 'type_id', 'firstname', 'lastname', 'avatar', 'date_created', 'date_modified', 'user'];

exports.cleaners = {
    onlypublic: function(obj) {
        // Unsetting all as this does not need to update.
        if (obj) {
            return _.pick(obj, publicProps);
        }
        // Return false if cleaning is not successful.
        return false;
    }
};

exports.config = function(schema) {
    schema.pre('save', function(next) {
        this.date_modified = new Date();
        next();
    });
};