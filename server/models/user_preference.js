'use strict';

// Imports.
const mongoose = require('mongoose'),
	types = mongoose.Schema.Types,
	config = global.CONFIG;

// Name of the schema.
exports.name = 'UserPreference';

// Schema definition.
exports.schema = {
	user: {
		type: types.ObjectId,
		ref: 'User'
	},
	preference: {
		type: types.ObjectId,
		ref: 'Preference'
	},
	preference_value: {
		type: types.ObjectId,
		ref: 'PreferenceValue'
	},
	value: String
};