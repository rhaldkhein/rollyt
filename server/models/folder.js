'use strict';

// Imports.
const mongoose = require('mongoose'),
	types = mongoose.Schema.Types;

// Name of the schema.
exports.name = 'Folder';

// Schema definition.
exports.schema = {
	user: {
		type: types.ObjectId,
		ref: 'User'
	},
	parent: {
		type: types.ObjectId,
		ref: 'Folder'
	},
	name: {
		type: String
	},
	is_public: {
		type: Boolean,
		default: false
	},
	date_created: {
		type: Date,
		default: Date.now
	}
};
