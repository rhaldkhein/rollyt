'use strict';

// Imports.
const _ = require('lodash');

/**
 * Common schema plugins
 */
module.exports = function(schema, options) {
	_.assign(schema.statics, {

		clean: function(obj, cleaner) {
			return this.schema.cleaners[cleaner](obj);
		},

		findByIdAndPopulateAll: function(id, proj, opts, callback) {
			return this.findByIdAndPopulate(id, this.schema.references, proj, opts, callback);
		},

		findByIdAndPopulate: function(id, fields, proj, opts, callback) {
			if (_.isFunction(proj)) {
				callback = proj;
				proj = null;
			} else if (_.isFunction(opts)) {
				callback = opts;
				opts = null;
			}
			let query = this.findById(id, proj, opts);
			if (arguments.length < 3)
				throw new Error('Method `findByIdAndPopulate` require 3 arguments');
			if (!_.isArray(fields))
				fields = [fields];
			_.each(fields, function(value) {
				query.populate(value);
			});
			if (callback)
				query.exec(callback);
			else
				return query;
		}

	});
};
