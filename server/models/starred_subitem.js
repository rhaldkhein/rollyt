'use strict';

// Imports.
const mongoose = require('mongoose'),
	types = mongoose.Schema.Types,
	config = global.CONFIG;

// Name of the schema.
exports.name = 'StarredSubitem';

// Schema definition.
exports.schema = {
	user: {
		type: types.ObjectId,
		ref: 'User'
	},
	item_id: String,
	subitem_id: String,
	date_created: {
		type: Date,
		default: Date.now
	}
};