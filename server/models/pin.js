'use strict';

// Imports.
const mongoose = require('mongoose'),
	types = mongoose.Schema.Types;

// Name of the schema.
exports.name = 'Pin';

// Schema definition.
exports.schema = {
	user: {
		type: types.ObjectId,
		ref: 'User'
	},
	resource_type: {
		type: String
	},
	resource_id: {
		type: String
	},
	flags: {
		type: types.Mixed
	},
	date_created: {
		type: Date,
		default: Date.now
	}
};

exports.config = function(schema) {
    schema.index({
        user: 1,
        resource_id: 1
    }, {
        unique: true,
        name: 'user_resource_index'
    });
};