'use strict';

/**
 * List of public modules. Generates based on packer/loader.files.js.
 * 
 * Eg. Format: "node_modules/<path to public file>"
 */

// Node Modules
exports.node_modules = [
  'node_modules/normalize.css',
  'node_modules/loaderjs',
  'node_modules/bluebird/js/browser',
  'node_modules/raven-js/dist',
  'node_modules/jquery/dist',
  'node_modules/toastr/build',
  'node_modules/moment/min',
  'node_modules/numeral/min',
  'node_modules/lodash',
  'node_modules/mithril',
  'node_modules/mithril-data',
  // Additional
  '/node_modules/async/dist',
  '/node_modules/mousetrap',
  '/node_modules/enquire.js/dist',
  '/node_modules/validator',
  '/node_modules/perfect-scrollbar/dist/js',
  '/node_modules/awesomplete',
  '/node_modules/store',
  '/node_modules/push.js/bin',
  '/node_modules/tippy.js/dist'
];

let prefix_node = 'node_modules';
exports.node_prefix = prefix_node;