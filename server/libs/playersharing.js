'use strict';

// Modules
const _ = require('lodash'),
  config = global.CONFIG,
  redis = require('socket.io-redis'),
  errors = require('../modules/codes'),
  rndm = require('rndm'),
  pref = require('./preference'),
  model = require(PATH.model),
  bcrypt = require('bcrypt'),
  moment = require('moment');

let ns;

/*

Mongo collection for rooms is ActiveRoom.

# Key Type
  - QR Code: "QC.<RoomId>.<Token>"
  - Search Result: "SR.<RoomId>.<UserPassword>"

*/

const userPrefs = ['player_hidden', 'player_private', 'player_sharekey'],
  roomProps = ['_id', 'user', 'picture', 'name', 'private', 'token', 'password'],
  roomPropsNoKey = ['_id', 'user', 'picture', 'name', 'private'];

// function dump(obj) {
//   var out = {};
//   for (let key in obj) out[key] = typeof obj[key];
//   return out;
// }

/**
 * Cluster hooks
 */

const hooks = {
  is_socket_exists: function(data, reply) {
    // data = string socket id
    reply(!!ns.connected[ns.name + '#' + data]);
  },
  get_socket_info: function(data, reply) {
    // data = { id: <socket id>, props: <array of properties to get> }
    let _sock = ns.connected[ns.name + '#' + data.id];
    reply(_sock ? _.pick(_sock, data.props) : null);
  },
  get_sockets_info: function(data, reply) {
    // data = { id: <array socket id>, props: <array of properties to get> }
    let _sock;
    reply(
      _.compact(
        _.map(data.ids, function(id) {
          _sock = ns.connected[data.long ? id : ns.name + '#' + id];
          return _sock ? _.pick(_sock, data.props) : null;
        })
      )
    );
  },
  set_socket_info: function(data, reply) {
    // data = { id: <socket id>, props: <object to assign> }
    let _sock = ns.connected[ns.name + '#' + data.id];
    if (_sock) {
      _.assign(_sock, data.props);
      reply(true);
    } else {
      reply(false);
    }
  }
};

/**
 * Cluster requests
 */

function sendRequest(cmd, data) {
  return new Promise(function(resolve, reject) {
    ns.adapter.customRequest({ cmd: cmd, data: data }, function(err, reps) {
      if (err) return reject(err);
      resolve(reps);
    });
  });
}

function isSocketExists(sid) {
  return sendRequest('is_socket_exists', sid).then(function(reps) {
    if (!_.includes(reps, true)) throw new Error('no_socket');
  });
}

function isSocketInRoom(rid, sid) {
  return new Promise(function(resolve, reject) {
    ns.in(rid).clients(function(err, clients) {
      if (err) return reject(err);
      if (_.includes(clients, ns.name + '#' + sid)) resolve();
      else reject(new Error('not_in_room'));
    });
  });
}

function getSocketInfo(sid, props) {
  return sendRequest('get_socket_info', {
    id: sid,
    props: props
  }).then(function(reps) {
    let info = _.compact(reps)[0];
    if (!info) throw new Error('no_socket');
    return info;
  });
}

function setSocketInfo(sid, props) {
  return sendRequest('set_socket_info', {
    id: sid,
    props: props
  }).then(function(reps) {
    if (!_.includes(reps, true)) throw new Error('no_socket');
    return true;
  });
}

function getAccountsInRoom(rid) {
  return new Promise(function(resolve, reject) {
    ns.in(rid).clients(function(err, clients) {
      if (err) return reject(err);
      sendRequest('get_sockets_info', {
        long: true, // Included namespace
        ids: clients,
        props: ['account.id']
      })
        .then(function(reps) {
          reps.push('account.id');
          resolve(
            _.map(_.unionBy.apply(_, reps), function(value) {
              return value.account.id;
            })
          );
        })
        .catch(reject);
    });
  });
}

/**
 * In process methods
 */

function generateToken() {
  return rndm(16);
}

function generatePassword() {
  return rndm.base10(6);
}

function readSession(socket) {
  return socket.handshake.sessionStore.get(socket.handshake.sessionID, _.noop);
}

function writeSession(socket, key, value) {
  return readSession(socket)
    .then(function(session) {
      if (_.isUndefined(value)) {
        // Remove property
        delete session[key];
      } else {
        _.set(session, key, value);
      }
      return session;
    })
    .then(function(session) {
      return socket.handshake.sessionStore.set(socket.handshake.sessionID, session, _.noop);
    });
}

function getRoomByUserId(id) {
  return model.ActiveRoom.findOne({
    user: id
  })
    .exec()
    .then(function(room) {
      if (room) return room;
      throw errors.generate('SENF');
    });
}

function getRoomByRoomId(id) {
  return model.ActiveRoom.findById(id)
    .exec()
    .then(function(room) {
      if (room) return room;
      throw errors.generate('SENF');
    });
}

function getRoomByName(name) {
  return model.ActiveRoom.findOne({
    name: name
  })
    .exec()
    .then(function(room) {
      if (room) return room;
      throw errors.generate('SENF');
    });
}

function getSavedRoom(ownerId, playerUserId) {
  return model.SavedRoom.findOne(
    {
      owner: ownerId,
      user: playerUserId
    },
    'password'
  ).exec();
}

/**
 * The same with `getSocketById` which only works for `emit`.
 * No data is attached to socket.
 */
function getHostSocket(clientSocket) {
  return readSession(clientSocket)
    .then(function(session) {
      return getRoomByUserId(session.room_joined.user);
    })
    .then(function(room) {
      // Returns the host socket
      return getSocketById(room.socket);
    });
}

function getUserPassword(userId) {
  return model.User.findById(userId, 'password').then(function(user) {
    // Return encrypted password
    return user.password;
  });
}

function getName(userAccount) {
  let name;
  if (userAccount.codename) {
    name = _.trim(userAccount.codename);
  } else {
    name =
      'Player' +
      _.trim((userAccount.firstname || '') + ' ' + (userAccount.lastname || ''))
        .replace(/\s+/g, '')
        .substring(0, 24);
  }
  return name;
}

function isPasswordCorrect(userId, password) {
  return model.User.findById(userId, 'password')
    .then(function(user) {
      return bcrypt.compare(password || '', user.password).then(function(yes) {
        return {
          yes: yes,
          password: user.password
        };
      });
    })
    .catch(function() {
      return false;
    });
}

/**
 * The returned socket will not contain session data.
 * But will be able to emit to it and will successfully send data.
 * Even if the actual socket is in different node process in cluster.
 */
function getSocketById(socketId) {
  return ns.to(ns.name + '#' + socketId);
}

function getSocketInRoom(roomId, socketId) {
  // `isSocketInRoom` will reject if socket is not in room.
  return isSocketInRoom(roomId, socketId).then(function() {
    return getSocketById(socketId);
  });
}

function isValidSocket(socketId) {
  return isSocketExists(socketId);
}

function createRoom(userAccount, socketId) {
  // Create player room
  return model.ActiveRoom.create({
    user: userAccount.user,
    name: getName(userAccount),
    picture: userAccount.avatar,
    socket: socketId,
    open: true,
    token: generateToken(),
    password: userAccount.password ? null : generatePassword(),
    hidden: userAccount.hidden,
    private: userAccount.private,
    sharekey: userAccount.sharekey
  });
}

function tryDestroyRoom(room) {
  // Try to delete room, if inactive for very long time
  if (moment(room.date_active).isBefore(moment().subtract(1, 'days'))) {
    destroyRoomById(room.id);
  }
}

function destroyRoomById(roomId) {
  return model.ActiveRoom.remove({ _id: roomId }).exec();
}

function setupNamespace(ns, sessionConfig) {
  // Authentication
  ns.use(function(socket, next) {
    // Inject express session to socket
    sessionConfig(socket.handshake, {}, next);
  });
  ns.use(function(socket, next) {
    // If user exist in the session
    if (_.get(socket.handshake, 'session.passport.user')) {
      // Add info to socket.
      model.Account.findById(socket.handshake.session.passport.user)
        .populate('user', 'codename password')
        .exec()
        .then(function(account) {
          // CHECK: Stored to socket object in memory
          // OK: Should be cleaned later
          socket.account = {
            id: account.id,
            user: account.user.id,
            firstname: account.firstname,
            lastname: account.lastname,
            avatar: account.avatar,
            codename: account.user.codename,
            password: !!account.user.password, // boolean
            hidden: null,
            private: null,
            sharekey: null
          };
          return pref.getUserPrefMany(account.user.id, userPrefs);
        })
        .then(function(prefs) {
          socket.account.hidden = prefs.player_hidden;
          socket.account.private = prefs.player_private;
          socket.account.sharekey = prefs.player_sharekey;
        })
        .then(next)
        .catch(function() {
          next(new Error(errors.get('SEUA').code));
        });
    } else {
      // No session
      next(new Error(errors.get('SEUA').code));
    }
  });
  // Setup custom hook
  ns.adapter.customHook = function(payload, cb) {
    if (hooks[payload.cmd]) hooks[payload.cmd](payload.data, cb);
  };
  // Setup connection
  ns.on('connection', setupSocket);
}

function setupSocket(socket) {
  // At this point, socket is connected and authenticated.
  Logger.console.debug('User connected /', socket.account.firstname);

  // Send cluster index and socket id
  socket.emit('node', process.env.NODE_APP_INSTANCE, socket.id);

  socket.on('disconnect', function() {
    Logger.console.debug('User disconnected /', socket.account.firstname);
    if (socket.account.roomhost) {
      getRoomByUserId(socket.account.user)
        .then(function(room) {
          // Do not destroy immediately, wait for some time
          // just close the room. He might come back shortly
          room.open = false;
          ns.to(room.id).emit('room_closed');
          return room.save();
        })
        .delay(1000 * 60 * config.playershare.hostrecontimeout)
        .then(function(room) {
          return getRoomByUserId(room.user);
        })
        .then(function(roomNew) {
          // If room is still close. Then he did not come back.
          if (!roomNew.open) {
            destroyRoomById(roomNew.id).then(function() {
              Logger.console.debug('Room destroyed /', socket.account.firstname, roomNew.id);
            });
          }
        });
    } else {
      // Room joiner automatically leave room on disconnect
      // Logger.console.debug('Room left /', socket.account.firstname);
    }
  });

  socket.on('clear_disconnect', function() {
    // Clear everything related to socket for client to disconnect
    if (socket.account.roomhost) {
      socket.emit('clear_disconnect', null);
      Logger.console.debug('Clear host /', socket.account.firstname);
    } else {
      Logger.console.debug('Clear joiner /', socket.account.firstname);
      writeSession(socket, 'room_joined')
        .then(function() {
          socket.emit('clear_disconnect', null);
        })
        .catch(function() {
          socket.emit('clear_disconnect', errors.generate('SEIE'));
        });
    }
  });

  socket.on('create_room', function(force) {
    // `force` - force to create room using this socket, even though its
    // already created with different socket/browser
    getRoomByUserId(socket.account.user)
      .then(function(room) {
        return isValidSocket(room.socket)
          .then(function() {
            // Check if hosted in another browser
            if (room.open) {
              if (force) {
                // Notify previous socket and continue
                return setSocketInfo(room.socket, {
                  account: {
                    roomhost: false
                  }
                }).then(function() {
                  getSocketById(room.socket).emit('room_replaced');
                });
              } else {
                throw errors.generate('SEDR');
              }
            }
          })
          .catch(function(err) {
            // If 'SEDR' (Duplicate Record),
            // We do not want to execute the next `then`.
            if (err.code === 'SEDR') throw err;
          })
          .return(room);
      })
      .then(function(room) {
        // Continue room generation
        Logger.console.debug('Room exist /', socket.account.firstname, room.id);
        // Update room info
        room.open = true; // It was closed by disconnection
        room.socket = socket.client.id; // New socket id
        room.name = getName(socket.account);
        room.hidden = socket.account.hidden;
        room.private = socket.account.private;
        room.password = socket.account.password ? null : room.password;
        room.sharekey = socket.account.sharekey;
        room.date_active = Date.now();
        return room.save();
      })
      .catch(function(err) {
        if (err.code === 'SENF') {
          return createRoom(socket.account, socket.client.id).then(function(room) {
            Logger.console.debug('Room created /', socket.account.firstname, room.id);
            return room;
          });
        } else if (err.code === 'SEDR') {
          throw err;
        }
      })
      .then(function(room) {
        socket.account.roomhost = true;
        socket.account.roomid = room.id;
        socket.emit('create_room', null, room);
        Logger.console.debug('Room sent /', socket.account.firstname);
        // Clean `socket.account`
        socket.account.firstname = null;
        socket.account.lastname = null;
        socket.account.avatar = null;
        socket.account.codename = null;
        socket.account.password = null; // boolean
        socket.account.hidden = null;
        socket.account.private = null;
        socket.account.sharekey = null;
      })
      .catch(function(err) {
        if (err.code === 'SEDR') {
          socket.emit('create_room', err.code);
        }
      });
  });

  socket.on('join_room', function(key) {
    // If `key` is missing and `room_joined` exist in session,
    // omit the `key` but only if not its yet expired
    let roomJoined = socket.handshake.session.room_joined;
    let promGetRoom;
    if (!key) {
      if (roomJoined) promGetRoom = getRoomByUserId(roomJoined.user);
    } else {
      // QR Code: "QC.<RoomId>.<Token>"
      // Search Result: "SR.<RoomId>.<UserPassword>" (Private)
      // Search Result: "SR.<RoomId>" (Public)
      let parts = key.split('.');
      if (parts.length <= 3) {
        switch (parts[0]) {
          case 'QC':
            // If not private, allow connection via playername
            promGetRoom = getRoomByRoomId(parts[1])
              .catch(function() {
                // Try playername
                return getRoomByName(parts[1]);
              })
              .then(function(room) {
                if (room.private) {
                  if (room.token === parts[2]) return room;
                  throw errors.generate('SEIK');
                }
                return room;
              });
            break;
          case 'SR':
            promGetRoom = getRoomByRoomId(parts[1]).then(function(room) {
              if (room.private) {
                // Check password
                if (room.password) {
                  // Auto generated password
                  if (room.password === parts[2]) return room;
                  // Check with saved room
                  return getSavedRoom(socket.account.user, room.user).then(function(saved) {
                    if (saved && room.password === saved.password) return room;
                    throw errors.generate('SEIK');
                  });
                } else {
                  // User password
                  return isPasswordCorrect(room.user, parts[2]).then(function(data) {
                    if (data && data.yes) return room;
                    return getSavedRoom(socket.account.user, room.user).then(function(saved) {
                      if (saved && data.password === saved.password) return room;
                      throw errors.generate('SEIK');
                    });
                  });
                }
              } else {
                // No password required
                return room;
              }
            });
            break;
          default:
            promGetRoom = null;
        }
      }
    }
    if (promGetRoom) {
      promGetRoom
        .then(function(room) {
          // Guaranteed room exist and authenticated. Will catch if not.
          if (room.open) {
            // Check if socket exists and usable
            return isSocketExists(room.socket)
              .catch(function() {
                // If not, clean up the room
                // Destroy if inactive for very long time
                tryDestroyRoom(room);
                throw errors.generate('SENF');
              })
              .then(function() {
                // Safe to join now
                socket.join(room.id, function(err) {
                  if (!err) {
                    writeSession(socket, 'room_joined', {
                      time_joined: Date.now(),
                      time_left: null,
                      user: room.user
                    })
                      .then(function() {
                        // If room password is missing,
                        // Copy encrypted user specified password to saved rooms
                        if (!room.password) return getUserPassword(room.user);
                        return room.password;
                      })
                      .then(function(pw) {
                        // Add to saved rooms
                        return model.SavedRoom.findOneAndUpdate(
                          {
                            owner: socket.account.user,
                            user: room.user
                          },
                          {
                            owner: socket.account.user,
                            user: room.user,
                            name: room.name,
                            picture: room.picture,
                            password: pw
                          },
                          {
                            upsert: true
                          }
                        );
                      })
                      .then(function() {
                        // Send room info to client
                        socket.emit('join_room', null, _.pick(room, room.sharekey ? roomProps : roomPropsNoKey));
                        Logger.console.debug('Room joined /', socket.account.firstname, '->', room.id);
                      })
                      .catch(function(err) {
                        throw err;
                      });
                  } else {
                    throw err;
                  }
                });
              });
          } else {
            throw errors.generate('SENA');
          }
        })
        .catch(function(err) {
          socket.emit('join_room', err);
        });
    } else {
      socket.emit('join_room', errors.generate('SEBR'));
    }
  });

  socket.on('users_list', function() {
    getAccountsInRoom(socket.account.roomid)
      .then(function(users) {
        socket.emit('users_list', null, users);
      })
      .catch(function() {
        socket.emit('users_list', errors.generate('SEIE'));
      });
  });

  socket.on('project', function(player, tag) {
    // Project to all clients in room
    ns.to(socket.account.roomid).emit('project', player, tag);
  });

  socket.on('project_one', function(cid, player, tag) {
    // Project to only one client
    // WARNING: Make sure `cid` does NOT cross-send to another room
    // Will reject if socket is not in room
    getSocketInRoom(socket.account.roomid, cid)
      .then(function(_sock) {
        _sock.emit('project', player, tag);
      })
      .catch(_.noop);
  });

  socket.on('get_project', function() {
    // TODO: Setup a CSRF token for `project_one` event
    getHostSocket(socket).then(function(socketHost) {
      socketHost.emit('get_project', socket.client.id);
    });
  });

  socket.on('host_command', function(command, args) {
    getHostSocket(socket).then(function(socketHost) {
      socketHost.emit('host_command', command, args, socket.client.id);
    });
  });

  socket.on('notify_dll', function(msg, type) {
    ns.to(socket.account.roomid).emit('notify', msg, type);
  });

  socket.on('player_error', function(msg, cid) {
    // WARNING: Make sure `cid` does NOT cross-send to another room // Ok
    // Will reject if socket is not in room
    getSocketInRoom(socket.account.roomid, cid)
      .then(function(_sock) {
        _sock.emit('player_error', msg);
      })
      .catch(_.noop);
  });

  socket.on('player_add', function(kind, id, flags) {
    getHostSocket(socket).then(function(socketHost) {
      socketHost.emit('player_add', kind, id, flags, socket.client.id);
    });
  });

  socket.on('player_play', function(kind, id) {
    getHostSocket(socket).then(function(socketHost) {
      socketHost.emit('player_play', kind, id, socket.client.id);
    });
  });

  socket.on('player_remove', function(kind, id) {
    getHostSocket(socket).then(function(socketHost) {
      socketHost.emit('player_remove', kind, id, socket.client.id);
    });
  });

  socket.on('player_remove_many', function(list) {
    getHostSocket(socket).then(function(socketHost) {
      socketHost.emit('player_remove_many', list, socket.client.id);
    });
  });

  socket.on('player_clear', function() {
    getHostSocket(socket).then(function(socketHost) {
      socketHost.emit('player_clear', socket.client.id);
    });
  });

  socket.on('player_skipnext', function() {
    getHostSocket(socket).then(function(socketHost) {
      socketHost.emit('player_skipnext', socket.client.id);
    });
  });

  socket.on('player_skipprev', function() {
    getHostSocket(socket).then(function(socketHost) {
      socketHost.emit('player_skipprev', socket.client.id);
    });
  });
}

exports.init = function(_io, sessionConfig) {
  // Cluster across multiple node instances using redis
  _io.adapter(redis({ host: process.env.APP_REDIS_HOST || 'localhost', port: process.env.APP_REDIS_PORT || 6379 }));
  // Create namespace
  ns = _io.of('/playersharing');
  // Setup namespace
  setupNamespace(ns, sessionConfig);
};
