'use strict';

const _ = require('lodash'),
  model = require(PATH.model);

let _prefs;

function parse(to, value) {
  // Continue parsing
  switch (to) {
    case 'number':
    case 'float':
      value = parseFloat(value);
      break;
    case 'integer':
      value = parseInt(value);
      break;
    case 'boolean':
      value = (_.trim(value) === 'true');
      break;
    case 'array':
      value = _.isEmpty(value) ? [] : value;
      value = _.isArray(value) ? value : JSON.parse(value);
      break;
    case 'object':
      value = _.isEmpty(value) ? {} : value;
      value = _.isObject(value) ? value : JSON.parse(value);
      break;
  }
  return value;
}

exports.init = function() {
  model.Preference.find({})
    .exec()
    .then(function(prefs) {
      _prefs = _.transform(prefs, function(result, pref) {
        result[pref.name] = {
          id: pref.id,
          data_type: pref.data_type,
          default_value: parse(pref.data_type, pref.default_value)
        };
        return true;
      }, {});
    });
};

exports.getPrefIds = function(prefNames) {
  // CHECK: Improve this
  return _.map(_.pick(_prefs, prefNames), function(pref) {
    return pref.id;
  });
};

exports.getDefaultValue = function(prefName) {
  if (_prefs[prefName]) return _prefs[prefName].default_value;
};

exports.getUserPrefMany = function(userId, prefNames) {
  return model.UserPreference.find({
      user: ObjectId(userId),
      preference: { $in: exports.getPrefIds(prefNames) }
    })
    .populate('preference', 'name data_type')
    .exec()
    .then(function(prefs) {
      let i, pref, prefsObj = _.zipObject(prefNames);
      for (i = prefs.length - 1; i >= 0; i--) {
        pref = prefs[i];
        prefsObj[pref.preference.name] = parse(pref.preference.data_type, pref.value);
      }
      for (let k in prefsObj) {
        if (prefsObj[k] === undefined) prefsObj[k] = exports.getDefaultValue(k);
      }
      return prefsObj;
    });
};