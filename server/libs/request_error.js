'use strict';

const _ = require('lodash');

function RequestError(code, message, category) {
    this.code = code || 'REBR';
    this.message = message || 'Request error';
    this.category = category || 'REST_ERROR';
}

RequestError.prototype = _.create(Error.prototype, {
    name: 'RequestError'
});

module.exports = RequestError;