'use strict';

const async = require('async'),
	Models = require(PATH.model);

module.exports = function(done) {

	// - - - START: Add your preset documents - - -

	var preferences = [];

	// - - - END: Add your preset documents - - -

	async.every(preferences, function(document, callback) {
		Models.PreferenceValue.update({
			name: document.name
		}, document, {
			upsert: true,
			setDefaultsOnInsert: true
		}, callback);
	}, done);

};