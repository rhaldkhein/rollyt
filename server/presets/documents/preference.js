'use strict';

const async = require('async'),
  Models = require(PATH.model);

module.exports = function(done) {
  // - - - START: Add your preset documents - - -

  var preferences = [
    {
      name: 'test_string',
      data_type: 'string',
      default_value: 'Test String',
      max_value: 24
    },
    {
      name: 'home_blocks',
      data_type: 'array',
      default_value: '[]',
      max_value: 5 // Maximum elements
    },
    {
      name: 'order_channel',
      data_type: 'array',
      default_value: '[]',
      max_value: 30
    },
    {
      name: 'order_playlist',
      data_type: 'array',
      default_value: '[]',
      max_value: 30
    },
    {
      name: 'auto_play_related',
      data_type: 'boolean',
      default_value: 'true'
    },
    {
      name: 'startup_playlist',
      data_type: 'string',
      default_value: '',
      max_value: 50
    },
    {
      name: 'play_notification',
      data_type: 'boolean',
      default_value: 'true'
    },
    {
      name: 'tooltip',
      data_type: 'boolean',
      default_value: 'true'
    },
    {
      name: 'list_video_library',
      data_type: 'boolean',
      default_value: 'false'
    },
    {
      name: 'skip_length_min',
      data_type: 'float',
      default_value: '0'
    },
    {
      name: 'skip_length_max',
      data_type: 'float',
      default_value: '0'
    },
    {
      name: 'clear_on_play',
      data_type: 'boolean',
      default_value: 'true'
    },
    {
      name: 'player_hidden',
      data_type: 'boolean',
      default_value: 'false'
    },
    {
      name: 'player_private',
      data_type: 'boolean',
      default_value: 'true'
    },
    {
      name: 'player_sharekey',
      data_type: 'boolean',
      default_value: 'false'
    }
  ];

  // - - - END: Add your preset documents - - -

  async.every(
    preferences,
    function(doc, callback) {
      Models.Preference.update(
        {
          name: doc.name
        },
        doc,
        {
          upsert: true,
          setDefaultsOnInsert: true
        },
        callback
      );
    },
    done
  );
};
