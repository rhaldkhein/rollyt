'use strict';

const async = require('async'),
	Models = require(PATH.model);

module.exports = function(done) {

	// - - - START: Add your preset documents - - -

	var settings = [{
		name: 'test_string',
		value_type: 'string',
		value_default: 'Test String',
	}, {
		name: 'features',
		value_type: 'object',
		value_default: '{}',
	}, {
		name: 'messages',
		value_type: 'array',
		value_default: '[]',
	}];

	// - - - END: Add your preset documents - - -

	async.every(settings, function(doc, callback) {
		Models.AppSetting.create(doc, callback);
	}, function(err) {
		// Duplicate is fine
		if (!err || err.code === 11000) {
			done();
		} else {
			done(err);
		}
	});

};
