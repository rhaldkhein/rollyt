'use strict';

const config = global.CONFIG,
	async = require('async'),
	glob = require('glob');

function load(work, folder) {
	var docs = glob.sync('**/*', {
		cwd: config.paths.presets + '/' + folder
	});
	docs.forEach(function(doc) {
		work.push(require(config.paths.presets + '/' + folder + '/' + doc));
	});
}

module.exports = function(done) {
	var work = [];
	// Documents
	load(work, 'documents');
	// Do the work
	async.parallel(work, done);
};