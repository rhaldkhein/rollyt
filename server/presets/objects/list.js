'use strict';

const _ = require('lodash'),
  config = global.CONFIG,
  util = require(MOD.util),
  validator = require(MOD.validator),
  Model = require(PATH.model),
  category = require('./category'),
  topic = require('./topic'),
  country = require(config.paths.static + '/app/country-codes.json'),
  baseLink = 'https://www.googleapis.com/youtube/v3',
  _dates = ['today', 'yesterday', 'week', 'month', 'year'],
  _orders = ['date', 'rating', 'relevance', 'title', 'viewCount'],
  _starTypes = ['video', 'playlist', 'channel'];

exports.get = function(id) {
  let parts = id.split('.');
  return parts[0] && this[parts[0]] ? this[parts[0]](id, parts) : Promise.reject(new Error('invalid_id'));
};

function valYoutubeId(id, type) {
  if (_.isEmpty(id) || id === 'undefined') return Promise.reject(new Error('invalid_id'));
  return new Promise(function(resolve, reject) {
    validator.localPlaylistItem(
      {
        resource_type: 'youtube#' + type,
        resource_id: id
      },
      function(err) {
        if (err) reject(new Error('invalid_id'));
        else resolve();
      }
    );
  });
}

function valVideoId(id) {
  return valYoutubeId(id, 'video');
}

function valChannelId(id) {
  return valYoutubeId(id, 'channel');
}

function valPlaylistId(id) {
  return valYoutubeId(id, 'playlist');
}

function valCategory(cat) {
  return category.get(cat) ? Promise.resolve() : Promise.reject(new Error('invalid_category'));
}

function valTopic(id) {
  return topic.get(id) ? Promise.resolve() : Promise.reject(new Error('invalid_category'));
}

function valCountry(cntry) {
  return (_.isEmpty(cntry) ? null : _.find(country, ['ISO3166-1-Alpha-2', cntry]))
    ? Promise.resolve()
    : Promise.reject(new Error('invalid_country'));
}

function valDate(date) {
  return _.indexOf(_dates, date) > -1 ? Promise.resolve() : Promise.reject(new Error('invalid_date'));
}

function valOrder(order) {
  return _.indexOf(_orders, order) > -1 ? Promise.resolve() : Promise.reject(new Error('invalid_order'));
}

function valStarType(type) {
  return _.indexOf(_starTypes, type) > -1 ? Promise.resolve() : Promise.reject(new Error('invalid_star_type'));
}

function valFolderId(id) {
  return Model.Folder.findById(id).then(function(doc) {
    if (!doc) throw new Error('invalid_star_folder');
  });
}

exports.liked = function() {
  return Promise.resolve({
    title: 'Liked Videos',
    link: baseLink + '/videos',
    params: {
      part: 'snippet',
      myRating: 'like'
    }
  });
};

exports.related = function(id, parts) {
  // id = related.<id>
  let rec = {
    id: id,
    title: 'Related',
    link: baseLink + '/search',
    params: {
      part: 'snippet',
      type: 'video'
    }
  };
  parts.shift();
  let videoId = _.trim(parts.join('.'));
  // Validate video id
  return valVideoId(videoId).then(function() {
    rec.params.relatedToVideoId = videoId;
    return rec;
  });
};

exports.popular = function(id, parts) {
  // id = popular.<category>.<country>
  let rec = {
    id: id,
    title: 'Popular',
    link: baseLink + '/videos',
    params: {
      part: 'snippet',
      chart: 'mostPopular'
    }
  };
  let proms = [];
  if (parts[1]) {
    proms.push(
      valCategory(parts[1]).then(function() {
        rec.params.videoCategoryId = parts[1]; // Category
      })
    );
  }
  if (parts[2]) {
    let code = parts[2].toUpperCase();
    proms.push(
      valCountry(code).then(function() {
        rec.params.regionCode = code; // Region
      })
    );
  }
  return Promise.all(proms).return(rec);
};

exports.search = function(id, parts) {
  // id = search.<query>.<category>.<country>.<date>.<order>
  let rec = {
    id: id,
    title: 'Search',
    link: baseLink + '/search',
    params: {
      part: 'snippet'
    }
  };
  parts.shift();
  let order = parts.pop(),
    date = parts.pop(),
    country = parts.pop(),
    category = parts.pop(),
    query = _.trim(parts.join('.')),
    proms = [];
  if (query && query !== 'undefined') rec.params.q = query;
  if (category) {
    proms.push(
      valCategory(category).then(function() {
        rec.params.videoCategoryId = category;
      })
    );
  }
  if (country) {
    proms.push(
      valCountry(country).then(function() {
        rec.params.regionCode = country;
      })
    );
  }
  if (date) {
    proms.push(
      valDate(date).then(function() {
        rec.params.publishedAfter = util.dateToISOString(date);
      })
    );
  }
  if (order) {
    proms.push(
      valOrder(order).then(function() {
        rec.params.order = order;
      })
    );
  }
  return Promise.all(proms).return(rec);
};

exports.channel = function(id, parts) {
  // id = channel.<id>.<date>.<order>
  let rec = {
    id: id,
    title: 'Channel',
    link: baseLink + '/search',
    params: {
      part: 'snippet'
    }
  };
  parts.shift();
  let order = parts.pop(),
    date = parts.pop(),
    channelId = _.trim(parts.join('.')),
    proms = [
      valChannelId(channelId).then(function() {
        rec.params.channelId = channelId;
      })
    ];
  if (date) {
    proms.push(
      valDate(date).then(function() {
        rec.params.publishedAfter = util.dateToISOString(date);
      })
    );
  }
  if (order) {
    proms.push(
      valOrder(order).then(function() {
        rec.params.order = order;
      })
    );
  }
  return Promise.all(proms).return(rec);
};

exports.playlist = function(id, parts) {
  // id = playlist.<id>
  let rec = {
    id: id,
    title: 'Playlist',
    link: baseLink + '/playlistItems',
    params: {
      part: 'snippet'
    }
  };
  parts.shift();
  let playlistId = _.trim(parts.join('.'));
  return valPlaylistId(playlistId).then(function() {
    rec.params.playlistId = playlistId;
    return rec;
  });
};

exports.starred = function(id, parts) {
  // id = starred.<type>.<userid>.<folder>
  let rec = {
    kind: 'local#starred',
    id: id,
    title: 'Starred Videos',
    link: '/star/all',
    params: {
      type: 'video'
    }
  };
  var folder = parts.pop(),
    userid = parts.pop(),
    type = parts.pop(),
    proms = [valStarType(type)];
  if (folder) {
    proms.push(valFolderId(folder));
  }
  return Promise.all(proms).return(rec);
};

exports.topic = function(id, parts) {
  // id = topic.<main>.<sub>
  let rec = {
    id: id,
    title: 'Topic',
    link: baseLink + '/search',
    params: {
      part: 'snippet'
    }
  };
  let sub = parts.pop(),
    main = parts.pop(),
    proms = [];
  if (sub) {
    proms.push(
      valTopic(main + '.sub.' + sub).then(function() {
        rec.params.topicId = sub;
      })
    );
  } else {
    proms.push(
      valTopic(main).then(function() {
        rec.params.topicId = main;
      })
    );
  }
  return Promise.all(proms).return(rec);
};
