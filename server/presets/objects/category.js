'use strict';

const _records = {
	'1': {
		_id: '1',
		title: 'Film & Animation'
	},
	'2': {
		_id: '2',
		title: 'Autos & Vehicles'
	},
	'10': {
		_id: '10',
		title: 'Music'
	},
	'15': {
		_id: '15',
		title: 'Pets & Animals'
	},
	'17': {
		_id: '17',
		title: 'Sports'
	},
	'18': {
		_id: '18',
		title: 'Short Movies'
	},
	'19': {
		_id: '19',
		title: 'Travel & Events'
	},
	'20': {
		_id: '20',
		title: 'Gaming'
	},
	'21': {
		_id: '21',
		title: 'Videoblogging'
	},
	'22': {
		_id: '22',
		title: 'People & Blogs'
	},
	'23': {
		_id: '23',
		title: 'Comedy'
	},
	'24': {
		_id: '24',
		title: 'Entertainment'
	},
	'25': {
		_id: '25',
		title: 'News & Politics'
	},
	'26': {
		_id: '26',
		title: 'Howto & Style'
	},
	'27': {
		_id: '27',
		title: 'Education'
	},
	'28': {
		_id: '28',
		title: 'Science & Technology'
	},
	'29': {
		_id: '29',
		title: 'Nonprofits & Activism'
	},
	'30': {
		_id: '30',
		title: 'Movies'
	},
	'31': {
		_id: '31',
		title: 'Anime/Animation'
	},
	'32': {
		_id: '32',
		title: 'Action/Adventure'
	},
	'33': {
		_id: '33',
		title: 'Classics'
	},
	'34': {
		_id: '34',
		title: 'Comedy'
	},
	'35': {
		_id: '35',
		title: 'Documentary'
	},
	'36': {
		_id: '36',
		title: 'Drama'
	},
	'37': {
		_id: '37',
		title: 'Family'
	},
	'38': {
		_id: '38',
		title: 'Foreign'
	},
	'39': {
		_id: '39',
		title: 'Comedy'
	},
	'40': {
		_id: '40',
		title: 'Sci-Fi/Fantasy'
	},
	'41': {
		_id: '41',
		title: 'Thriller'
	},
	'42': {
		_id: '42',
		title: 'Shorts'
	},
	'43': {
		_id: '43',
		title: 'Shows'
	},
	'44': {
		_id: '44',
		title: 'Trailers'
	}
};

const _ = require('lodash');

exports.get = function(id) {
	if (id) {
		return _records[id];
	} else {
		return _.values(_records);
	}
};