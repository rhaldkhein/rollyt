'use strict';

// Modules
const passport = require('passport'),
    request = require('request'),
    async = require('async'),
    model = require(PATH.model),
    config = global.CONFIG;

/**
 * PASSPORT
 */

passport.serializeUser(function(account, done) {
    // This method will inject user info to session. Using only
    // the ID to minimize database consumption and data transfer.
    done(null, account.id);
});

passport.deserializeUser(function(id, done) {
    // This method will convert ID to object for use to express.
    // model.Account.findById(id).populate('user').exec(function(err, account) {
    //  done(err, account);
    // });
    model.Account.findById(id).populate('user').exec(done);
});

/**
 * HELPERS
 */

/**
 * This callback method should be used in all strategies.
 *
 * Profile object must contain properties:
 * - profile.id
 * - profile.type // facebook or google or ...
 * - profile.firstname
 * - profile.lastname
 * - profile.avatar // a picture
 * - profile.friends // array of friends ids
 * - profile.access_token
 */

function authCallback(profile, callback) {
    (new Promise(function(resolve, reject) {
        // First we must find it
        model.Account.findOne({
            type_id: profile.id
        }, function(err, doc) {
            if (err) reject(err);
            else if (doc) resolve(doc);
            else reject(new Error('Not Found'));
        });
    }))
    .then(function(account) {
            // Found it, update and return the account.
            return new Promise(function(resolve, reject) {
                account.firstname = profile.firstname;
                account.lastname = profile.lastname;
                account.avatar = profile.avatar;
                account.email = profile.email;
                account.friends = profile.friends;
                account.access_token = profile.access_token;
                account.save(function(err) {
                    if (err) reject(err);
                    else resolve(account);
                });
            });
        }, function() {
            // Not found, create one and return the account.
            return new Promise(function(resolve, reject) {
                let user = new model.User(),
                    account = new model.Account({
                        type: profile.type,
                        type_id: profile.id,
                        user: user.id,
                        firstname: profile.firstname,
                        lastname: profile.lastname,
                        avatar: profile.avatar,
                        email: profile.email,
                        friends: profile.friends,
                        access_token: profile.access_token
                    });
                // First document, set as default account.
                user.current_account = account.id;
                // Time to save all.
                user.save(function(err) {
                    if (err) {
                        reject(err);
                    } else {
                        account.save(function(err) {
                            if (err) {
                                // Remove user because it was created
                                user.remove();
                                reject(err);
                            } else {
                                resolve(account);
                            }
                        });
                    }
                });
            });
        })
        .then(function(account) {
            // We should have an updated account.
            callback(null, account);
            return null;
        })
        .catch(function(err) {
            // We can't continue.
            Logger.console.info('Passport: login_error /', profile.id);
            callback(err, null);
        });
}

/**
 * BACKDOOR USER
 */

// Get strategy.
let BasicStrategy = require('passport-http').BasicStrategy;
const _userid = 'admin';
const _password = 'undead111';
// Use the strategy.
passport.use(new BasicStrategy(
    function(userid, password, done) {
        if (userid === _userid && password === _password) {
            done(null, {});
        } else {
            done('Access Denied');
        }
    }
));

/**
 * GOOGLE VERIFY TOKEN
 */

let CustomStrategy = require('passport-custom').Strategy,
    GoogleAuth = require('google-auth-library'),
    authFactory = new GoogleAuth(),
    oAuth2Client = new authFactory.OAuth2();

passport.use(new CustomStrategy(
    function(req, done) {
        async.parallel([
                function(callback) {
                    oAuth2Client.verifyIdToken(req.get('id_token'), config.google.client_id, function(err, data) {
                        if (!err) {
                            let profile = data.getPayload();
                            if (req.get('account_id') === profile.sub) {
                                callback(null, {
                                    type: 'google',
                                    id: profile.sub,
                                    firstname: profile.given_name,
                                    lastname: profile.family_name,
                                    avatar: profile.picture,
                                    email: profile.email,
                                    friends: [], // Array of ids.
                                    access_token: req.get('access_token')
                                });
                                return;
                            }
                        }
                        callback('invalid id_token');
                    });
                },
                function(callback) {
                    request('https://www.googleapis.com/oauth2/v3/tokeninfo?access_token=' + req.get('access_token'),
                        function(error, response, body) {
                            if (!error && response.statusCode == 200) {
                                body = JSON.parse(body);
                                if (body.aud === config.google.client_id) {
                                    callback(null);
                                    return;
                                }
                            }
                            callback('invalid access_token');
                        }
                    );
                }
            ],
            function(err, results) {
                if (err) {
                    done(err);
                } else {
                    authCallback(results[0], done);
                }
            });
    }
));


/**
 * GOOGLE
 */

// Get strategy.
let GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
// Use the strategy.
passport.use(new GoogleStrategy({
        clientID: '285836800359-iffif5lcf7mevsk7p72rul9vddgd53re.apps.googleusercontent.com',
        clientSecret: '23B4_nArxiiiuxEaey5-j_VA',
        callbackURL: '/login/google/done'
    },
    function(accessToken, refreshToken, profile, callback) {
        authCallback({
            type: 'google',
            id: profile.id,
            firstname: profile.name.givenName,
            lastname: profile.name.familyName,
            avatar: profile.photos[0].value,
            friends: [], // Array of ids.
            access_token: accessToken
        }, callback);
    }
));

/**
 * FACEBOOK
 */

// Get strategy.
// let FacebookStrategy = require('passport-facebook').Strategy;
// Use the strategy.
// passport.use(new FacebookStrategy({
//      clientID: '1039922452767642',
//      clientSecret: '4c68f01999ccc073b814180a5fa213a7',
//      profileFields: ['id', 'name', 'gender', 'birthday', 'photos']
//  },
//  function(accessToken, refreshToken, profile, callback) {
//      authCallback({
//          type: 'facebook',
//          id: profile.id,
//          firstname: profile.name.givenName,
//          lastname: profile.name.familyName,
//          avatar: profile.photos[0].value,
//          friends: [], // Array of ids.
//          access_token: accessToken
//      }, callback);
//  }
// ));