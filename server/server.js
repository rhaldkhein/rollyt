'use strict';

const _ = require('lodash'),
  config = global.CONFIG,
  express = require('express'),
  http = require('http'),
  https = require('https'),
  validator = require('express-validator'),
  app = express(),
  session = require('express-session'),
  bodyparser = require('body-parser'),
  mongoose = require('mongoose'),
  fs = require('fs'),
  path = require('path'),
  async = require('async'),
  MongoStore = require('connect-mongo')(session),
  passport = require('passport'),
  helmet = require('helmet'),
  cors = require('cors'),
  glob = require('glob'),
  socket = require('socket.io'),
  sharing = require('./libs/playersharing'),
  Raven = require('raven'),
  util = require('./modules/util'),
  prefs = require('./libs/preference'),
  model = require(PATH.model);

const serverPortHttp = config.server.port,
  serverHostHttp = config.server.host,
  serverPortHttps = config.server.httpsPort || serverPortHttp + 1,
  serverHostHttps = config.server.httpsHost || serverHostHttp;

let routePaths,
  routeNames,
  // statusName,
  sessionConfig,
  // statusFolder = config.paths.status,
  viewsFolder = config.paths.views;

// Configure express
function expressConfig() {
  return new Promise(function(resolve) {
    Logger.console.debug(`Express: configuring...`);
    // Initialize config for app object
    config.initapp(app);
    // Settings.
    app.set('trust proxy', true);
    // Enable security features by Helmet.
    if (config.is_server) app.use(helmet());
    // Configure passport.
    require(config.passport);
    // Add body parsers. Only `json and form-urlencoded`, NOT multipart.
    app.use(bodyparser.json());
    app.use(
      bodyparser.urlencoded({
        extended: true
      })
    );
    // Use the validator.
    app.use(validator());
    // Using mongoDB as session storage.
    sessionConfig = session({
      name: config.session.id,
      secret: config.session.salt,
      store: new MongoStore({
        mongooseConnection: mongoose.connection
      }),
      resave: false,
      saveUninitialized: false,
      cookie: {
        domain: config.domain, // Set domain to share session to subdomain
        secure: config.session.cookieSecure,
        httpOnly: true,
        maxAge: 60000 * 60 * config.session.expiration
      }
    });
    app.use(sessionConfig);
    // Initialize passport.
    app.use(passport.initialize());
    // Create passport session.
    app.use(passport.session());
    // CORS
    app.use(cors(config.cors));
    // Inject own middlewares.
    require(config.paths.middlewares)(app);
    // All good in session.
    process.nextTick(function() {
      Logger.console.debug(`Express: configured`);
      resolve();
    });
  });
}

/**********************************
 * ROUTES ORDER
 **********************************/

function routesLoad() {
  return new Promise(function(resolve) {
    Logger.console.debug(`Routes: opening...`);

    /**
     * Set view engine.
     */
    Logger.console.debug(`Routes: setting view engine...`);
    app.set('views', viewsFolder);
    app.set('view engine', config.view_engine);

    /**
     * Route registration must be in order for following reason.
     * 1. '/routename' <- Will execute
     * 2. '/routename' <- Will NOT execute as it's already executed
     */

    // Health check
    app.get('/health', function(req, res) {
      res.status(200).send();
      return;
    });

    /**
     * Important routes. These routes will execute no matter what, because it's first
     * in the line.
     */
    Logger.console.debug(`Routes: setting important routes...`);
    // WARNING: Make sure security group will only allow HTTPS 443
    // Redirect to secure route on production
    // if (config.production && config.server.onlyHttps) {
    //   app.all('*', function(req, res, next) {
    //     let proto = config.is_server ? req.get('X-Forwarded-Proto') : req.protocol;
    //     if (proto !== 'https') {
    //       if (proto && req.method === 'GET') {
    //         res.redirect('https://' + req.hostname + req.originalUrl);
    //       } else {
    //         res.endError();
    //       }
    //       return;
    //     }
    //     next();
    //   });
    // }

    // Other important routes
    routePaths = config.paths.important;
    routeNames = fs.readdirSync(routePaths);
    routeNames.forEach(function(route) {
      route = path.basename(route, '.js');
      app.use('/' + route, require(routePaths + '/' + route));
    });

    /**
     * Catch all routes if status is not open and process the status page.
     */
    // WARNING: CloudFront should server status pages
    // if (config.status !== 'open') {
    //   Logger.console.debug(`Routes: setting maintenance routes...`);
    //   // Match all files and NOT routes or directory.
    //   // Regex: /\.[^\/]*$/
    //   // Match all route and directory but NOT files.
    //   // Regex: /\/[^\.]*$/
    //   statusName = statusFolder + '/' + config.status;
    //   app.get(/\/[^\.]*$/, function(req, res, next) {
    //     // Prioratize back door user
    //     if (req.session && req.session.backdoor === true) {
    //       next();
    //       return;
    //     }
    //     // Authenticate user and continue
    //     if (
    //       _.has(req.session, config.session.status_exception_key) &&
    //       _.indexOf(config.session.status_exception_values, _.get(req.session, config.session.status_exception_key)) >
    //         -1
    //     ) {
    //       next();
    //     } else {
    //       res.render(statusName + '/index');
    //     }
    //   });
    // }

    /**
     * Serve static files.
     */
    Logger.console.debug(`Routes: setting static files...`);
    config.paths.statics.forEach(function(folder) {
      app.use('/static', express.static(config.paths.root + '/' + folder));
    });
    var _node_prefix = config.paths.static_modules.node_prefix;
    config.paths.static_modules.node_modules.forEach(function(folder) {
      app.use('/' + _node_prefix, express.static(config.paths.root + '/' + folder));
    });

    /**
     * Compile all routers. 'readdirSync' is fine as this will only run on server startup.
     * And need to be ran before the 404 Not Found fallback.
     */
    Logger.console.debug(`Routes: setting all other routes...`);
    var routeFolders = [];
    routePaths = config.paths.routes;
    routeNames = glob.sync('**/*', {
      cwd: routePaths
    });
    routeNames.forEach(function(route) {
      var _isFolder = !_.endsWith(route, '.js');
      route = '/' + route.replace(/\.[^/.]+$/, '');
      if (!_.endsWith(route, 'index')) {
        var _router = require(routePaths + route);
        app.use(route, _router);
        if (_isFolder) {
          routeFolders.push(route);
        }
      }
    });
    routeFolders.forEach(function(route) {
      var _pathDeindex = routePaths + route + '/deindex.js';
      if (fs.existsSync(_pathDeindex)) {
        app.use(route, require(_pathDeindex));
      }
    });

    /**
     * Load home and global router.
     */
    Logger.console.debug(`Routes: setting default home route...`);
    app.use('/', require(routePaths + '/' + config.home));

    /**
     * Can't find the requested route.
     * 404 error response for GET method.
     */
    Logger.console.debug(`Routes: setting error routes...`);
    app.get('*', function(req, res) {
      if (_.includes(req.get('content-type'), 'json')) res.jsonError(404, 'GENF');
      else res.status(404).render(viewsFolder + '/errors/404');
    });

    // 404 error response for other http methods.
    app.all('*', function(req, res) {
      return res.status(404).end();
    });

    Logger.console.debug(`Routes: done setting up`);
    // Done
    process.nextTick(function() {
      Logger.console.debug(`Routes: opened`);
      resolve();
    });
  });
}

/**********************************
 * SERVER STARTUP
 **********************************/

function databaseConnect() {
  // Connect to database.
  Logger.console.debug(`Database: connecting...`);
  let url,
    host = config.database.host + (config.database.port ? ':' + config.database.port : ''),
    credentials = config.database.user ? config.database.user + ':' + config.database.password + '@' : '';
  if (config.database.url) {
    url = config.database.url;
  } else {
    url = 'mongodb://' + credentials;
    if (!_.isEmpty(config.database.replicaSet)) {
      url += _.join(config.database.replicaSet, ',');
    } else {
      url += host;
    }
    url += '/' + config.database.name;
  }
  mongoose.Promise = global.Promise;
  return mongoose
    .connect(
      url,
      config.database.options
    )
    .then(function() {
      Logger.console.debug(`Database: connected / ${url}`);
    });
}

function databasePresets() {
  return new Promise(function(resolve, reject) {
    // Updating preset documents.
    Logger.console.debug(`Database: updating...`);
    require(config.paths.presets)(function(err) {
      Logger.console.debug(`Database: updated`);
      if (err) {
        return reject(err);
      } else {
        model.Preference.find({}, '-__v')
          .exec()
          .then(function(prefs) {
            app.locals.prefs = JSON.stringify(prefs);
            resolve();
          })
          .catch(reject);
      }
    });
  });
}

function serverStart() {
  return new Promise(function(resolve, reject) {
    // Start the express server.
    Logger.console.debug(`Server: starting...`);
    let serverHttp, serverHttps;
    async.parallel(
      [
        function(done) {
          if (serverHttp) serverHttp.close();
          if (config.server.onlyHttps) return done();
          serverHttp = http.createServer(app).listen(serverPortHttp, serverHostHttp, function() {
            Logger.console.debug(`Server: started / http://${serverHostHttp || ''}:${serverPortHttp}`);
            done();
          });
          serverHttp.on('error', function(err) {
            done(err);
          });
        },
        function(done) {
          if (config.server.https) {
            if (serverHttps) serverHttps.close();
            serverHttps = https
              .createServer(config.server.httpsOptions, app)
              .listen(serverPortHttps, serverHostHttps, function() {
                Logger.console.debug(`Server: started / https://${serverHostHttps || ''}:${serverPortHttps}`);
                done();
              });
            serverHttps.on('error', function(err) {
              done(err);
            });
          } else {
            done();
          }
        }
      ],
      function(err) {
        if (err) {
          reject(err);
        } else {
          sharing.init(socket(serverHttps || serverHttp, config.socket), sessionConfig);
          resolve();
        }
      }
    );
  });
}

function startMisc() {
  return prefs.init();
}

function startServer() {
  // Adding delay to give mongodb time to start
  Promise.delay(1000 * (config.production ? 10 : 0))
    .then(databaseConnect)
    .then(databasePresets)
    .then(expressConfig)
    .then(routesLoad)
    .then(startMisc)
    .then(serverStart)
    .then(function() {
      Logger.console.debug(`Server: SUCCESS / running...`);
      if (config.production) {
        let proc = util.prettifyJson(util.processToJson(['title', 'version', 'arch', 'platform', 'pid', 'execPath']));
        let env = util.prettifyJson(util.envToJson());
        Logger.slack.info(`A server is now up and running... (ver:${config.app.version})`, `\n${env}\n${proc}`);
      }
    })
    .catch(function(err) {
      Logger.console.debug(err.name + ':', err.message);
      Logger.console.debug(`Server: ERROR / not running`);
      if (config.production) {
        let env = util.prettifyJson(util.envToJson());
        Logger.slack.error(`A server started with error! (ver:${config.app.version})`, `\n${err.message}\n${env}`);
        Logger.file.error(`${err.stack}`);
      }
    });
}

if (config.production) {
  Raven.config('https://f549d2f8b3e2447d85daf217af53a967:094f93a0a2c24531acfd5c29cdfc8ce6@sentry.io/285775').install();
  Raven.context(startServer);
} else {
  startServer();
}
