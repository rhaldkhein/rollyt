'use strict';

/**
 * Server configuration file. This config file must be called in start.js in root directory.
 *
 * *** ONLY THIS FILE SHOULD SET THE CONFIG PROPERTIES ***
 *
 * --- IMPORTANT NOTE --------------------------------
 * Some configurations will NOT be used if this is ported on the live server.
 * Like server host & port, because live server's host & port should be used.
 *
 * --- LEGEND ----------------------------------------
 * [P] =    Production only. This config field is used only in production enviromnent.
 * [S] =    Secondary option. This config field might be used if Primary config is available
 *          somewhere else and not in this config file.
 *
 * --- GLOBAL EXPORTS --------------------------------
 * CONFIG - The whole config object.
 * PATH - Paths to specific directories.
 * MOD - Modules paths for easy require.
 *
 */
let _ = require('lodash'),
  path = require('path'),
  fs = require('fs'),
  pkg = require('../package.json'),
  config = {},
  production = process.env.NODE_ENV == 'production';

/**
 * GLOBALS & OVERRIDES
 */

require('./config/globals');

/**
 * APP LOCALS
 */
config.initapp = function(app) {
  if (!app) throw new Error('Config initapp requires app object from express');
  app.locals.app = global.CONFIG.app;
};

/**
 * CONFIG
 */

// App info
config.app = {
  name: 'Rollyt',
  version: pkg.version
};

// Export the whole config.
module.exports = global.CONFIG = config;

// Reference process environment variables.
config.env = process.env;
// Production flag.
config.production = production;
// Short hand of production flag.
config.prod = config.production;
// Development flag.
config.development = !config.production;
// Short hand of development flag.
config.dev = config.development;
// Host type
config.host = process.env.APP_HOST || 'local';
config.is_server = config.host === 'server';
config.is_local = !config.is_server;

// Domain
config.domain = config.is_server ? 'www.rollyt.com' : 'rollyt-dev.com';
// Filename of home page. Instead of index.
config.home = 'home';
// Supported browsers
config.browsers = JSON.stringify({
  chrome: 40, // 40,
  firefox: 46, // 36,
  opera: 37, // 27,
  safari: 6, // 6,
  edge: 14 // 14,
});

/**
 * [P] Status of the server. A folder for each status must exist in status folder.
 * open | close | maintenance | construction
 */
config.statuses = ['open', 'close', 'maintenance', 'construction'];
config.status = _.indexOf(config.statuses, process.env.APP_STATUS) > -1 ? process.env.APP_STATUS : 'close';

/**
 * PATHS
 */
config.paths = {};
// Project root directory
config.paths.root = path.resolve(__dirname, '..');
// Applications data folder
config.paths.data = config.is_server ? '/efs/data' : path.resolve(__dirname, '../..');
// Admin directory.
config.paths.admin = config.paths.root + '/admin';
// Client application directory.
config.paths.app = config.paths.root + '/app';
// Base static folder. Other folders are added in `paths.statics`.
config.paths.static = config.paths.root + '/static';
// Server folder.
config.paths.server = config.paths.root + '/server';
// Middlewares path for Express.
config.paths.middlewares = config.paths.server + '/middlewares';
// Modules path on server side.
config.paths.modules = config.paths.server + '/modules';
// Path to our model directory. (Mongoose)
config.paths.models = config.paths.server + '/models';
// Models plugins directory. (Mongoose)
config.paths.plugins = config.paths.models + '/plugins';
// Routes folder in server folder.
config.paths.routes = config.paths.server + '/routes';
// Views folder for express.
config.paths.views = config.paths.server + '/views';
// Important routes in server folder.
config.paths.important = config.paths.server + '/important';
// Status folder in static folder.
config.paths.status = config.paths.static + '/status';
// Collection of folders that will serve static files.
config.paths.statics = ['static']; // 'node_modules'
// Get public static modules.
config.paths.static_modules = require('./public_modules');
// Presets path
config.paths.presets = config.paths.server + '/presets';

/**
 * SERVER
 */
config.server = {};
// [S] Host name. Express will provide if not defined.
config.server.host = config.prod ? undefined : process.env.APP_IP || '127.0.0.1'; // undefined;
// [S] Host port to use.
config.server.port = process.env.APP_PORT ? parseInt(process.env.APP_PORT) : 80; // 80, 3001
// [S] Host name for https. Express will provide if not defined.
config.server.httpsHost = config.server.host;
// [S] Host port for https.
config.server.httpsPort = config.server.port == 80 ? 443 : config.server.port + 1; // 443, 3002
// [S] Always redirect http to https
config.server.onlyHttps = config.env.APP_ONLY_HTTPS == 'true';
// [S] Set to enable secure http.
config.server.https = config.env.APP_HTTPS == 'true';
// [S] Options object to https server.
try {
  config.server.httpsOptions = !config.server.https
    ? {}
    : {
        cert: fs.readFileSync(
          path.resolve(config.paths.root, process.env.APP_SSL_CERT || '../certificates/local/server.crt')
        ),
        key: fs.readFileSync(path.resolve(config.paths.root, process.env.APP_SSL_KEY || '../certificates/local/server.key'))
      };
} catch (ex) {
  config.server.httpsOptions = {};
}

/**
 * SESSION & LOGIN
 */
config.session = {};
// Set session id.
config.session.id = 'RLSESSID';
// String salt for session security.
config.session.salt = 'q5CupreBaweW57draDruPEc63QUfufaw';
// Session expiration by hours.
config.session.expiration = 24 * 14; // 14 Days
// The key to use in the session to bypass the status page.
// Ex: req.session.user_type = 'admin', this will exempt to view
// the status page and will continue to normal page.
config.session.status_exception_key = 'user.type';
// The values to of key to bypass the status page.
config.session.status_exception_values = ['admin', 'developer', 'client'];
// Use for express session.
config.session.cookieSecure = config.server.onlyHttps;
// Timeout for token validation in seconds.
config.session.tokenValidationTimeout = 30;
// The root-admin id. This is a Google Account ID. Any users under this id will have the privileges.
config.session.root_admin_id = '104018961193236795516';

/**
 * SOCKET
 */
config.socket = {
  transports: ['websocket']
};

/**
 * SESSION & LOGIN
 */
config.playershare = {};
// Timeout to remove room after host is disconnected. In minutes.
config.playershare.hostrecontimeout = 1;
// Timeout to reconnect room after joiner is disconnected. In hours.
config.playershare.joinrecontimeout = 2;

/**
 * CORS
 */
config.cors = {};
// Origins whitelisting
config.cors.origins = ['https://www.rollyt.com'];
// Add test origins for devel flag
if (!production) config.cors.origins.push('https://rollyt-dev.com:3001');

/**
 * DATABASE (MongoDB)
 */
config.database = {};
// Database name.
config.database.name = process.env.APP_DB_NAME || 'rollyt';
// Database user.
config.database.user = process.env.APP_DB_USER || '';
// Databse password.
config.database.password = process.env.APP_DB_PASS || '';
// Url for connect method. Priority use if defined.
config.database.url = undefined;
// Server hostname. For docker, this is the database container name.
config.database.host = process.env.APP_DB_HOST || 'localhost';
// Server port.
config.database.port = process.env.APP_DB_PORT || 27017;
// Comma separated `<host>:<ip>` of replica set.
// This will override above `host:port` settings.
config.database.replicaSet = process.env.APP_DB_RELPSET ? _.split(process.env.APP_DB_RELPSET, ',') : [];
// Salt for database string encryption
config.database.saltrounds = 10;
// Options
config.database.options = {
  useMongoClient: true,
  reconnectTries: Number.MAX_VALUE, // 20 * 60, // 1 Hour
  reconnectInterval: 3000 // Interval * Tries
};

/**
 * YOUTUBE
 */
config.youtube = {};
// API Key
config.youtube.key = config.is_server
  ? 'AIzaSyB8k_TTrIHUnckPLHpEsz844z6Q1ZZJtqo'
  : 'AIzaSyBin9nzMOFkXvOLlIywhe4FNIg8oGidPXE';

/**
 * ADVANCED
 */

// Number of times to try when failed to do something.
config.retry_times = 5;
// Interval to do the next try. In miliseconds.
config.retry_interval = 3000;
// Template engine for express.
config.view_engine = 'ejs';
// Passport configuration file for express.
config.passport = config.paths.server + '/passport.js';

/**
 * DEFAULT USER PREFERENCE
 */

config.default_user_preferences = {
  test: 'Hello World'
};

/**
 * VALIDATION
 */

config.validation = {};
config.validation.playlistItemsSampleSize = 2;
config.validation.playerItemsLimit = config.prod ? 50 : 10; // 30
config.validation.playlistMaxLimit = config.prod ? 200 : 30; // 180

/**
 * GOOGLE
 */
config.google = {};
// Client ID
config.google.client_id = '285836800359-iffif5lcf7mevsk7p72rul9vddgd53re.apps.googleusercontent.com';

/**
 * OTHER CONFIGS
 */
// Globalize paths namespace.
global.PATH = config.paths;
global.PATH.model = config.paths.models;

// Globalize local modules path.
global.MOD = {};
fs.readdirSync(config.paths.modules).forEach(function(mod) {
  mod = path.basename(mod, '.js');
  global.MOD[mod] = config.paths.modules + '/' + mod;
});

// Globalize __basedir.
global.__basedir = config.paths.root;

/**
 * CLIENT FLAGS
 */
config.client = require('./config/client');
