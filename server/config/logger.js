'use strict';

/**
 * Configuration for Logger
 */
const config = global.CONFIG;
const production = process.env.NODE_ENV == 'production';
const appname = process.env.APP_NAME || 'Logger';
const apphost = process.env.APP_HOST || 'Local';

// Log Levels: ALL < TRACE < DEBUG < INFO < WARN < ERROR < FATAL < MARK < OFF

module.exports = {
  pm2: true,
  appenders: {
    console: {
      type: 'console'
    },
    file: {
      type: 'file',
      filename: config.paths.data + '/node-server.log',
      maxLogSize: 10485760,
      backups: 3,
      compress: true
    },
    email: {
      type: 'smtp',
      SMTP: {
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
          user: 'logger.rollyt@gmail.com',
          pass: 's4=Va6U+utH2-eTr'
        },
        tls: {
          rejectUnauthorized: false
        }
      },
      subject: `[${apphost.toUpperCase()}] [${appname.toUpperCase()}] Notification`,
      recipients: 'dev.rkv@gmail.com'
    },
    slack: {
      type: 'slack',
      token: 'xoxp-314174976631-313020210483-314181910903-3d3835ab3045354ddb91a124550e73a0',
      channel_id: '#logs_server',
      username: `${apphost.toUpperCase()} / ${appname.toUpperCase()}`
    }
  },
  categories: {
    default: { appenders: ['console'], level: (production ? 'off' : 'all') },
    file: { appenders: ['file'], level: 'info' },
    email: { appenders: ['email', 'file'], level: 'info' },
    slack: { appenders: ['slack', 'file'], level: 'info' }
  }
};