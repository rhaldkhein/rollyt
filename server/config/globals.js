'use strict';

const log4js = require('log4js'),
  mongoose = require('mongoose'),
  util = require('../modules/util');

/**
 * GLOBAL OBJECTS OVERRIDES
 */

// Promise
global.Promise = require('bluebird');
global.Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false
  }
});

// Mongoose
global.ObjectId = mongoose.Types.ObjectId;

// Request Error
global.RequestError = require('../libs/request_error');

// Cluster Processes Info
global.Processes = {};


/**
 * GLOBAL HANDLERS
 */

process.on('message', function(packet) {
  switch (packet.topic) {
    case 'server:procsync':
      if (process.send) {
        process.send({
          topic: 'server:procinfo',
          data: util.processToJson(['pid', 'uptime', 'memoryUsage', 'cwd', 'config',
            'platform', 'arch', 'features', 'versions', 'execPath', 'title'
          ])
        });
      }
      break;
    case 'server:procinfo':
      global.Processes[packet.data.pid] = packet.data;
      // Clean after 1 minute
      setTimeout(function() { global.Processes = {}; }, 1000 * 60);
      break;
  }
});

// Cleanup
function cleanUp() {
  Logger.file.warn('Received SIGINT/SIGTERM signal. Shutting down...');
  Promise.resolve()
    // Disconnect database
    .then(function() {
      return mongoose.disconnect();
    })
    // Graceful exit
    .then(function() {
      Logger.slack.info('Shutdown Signal', `The process (id:${process.pid},ver:${global.CONFIG.app.version}) received a shutdown signal and is shutting down gracefully`);
      return 0;
    })
    // Exit with error
    .catch(function(err) {
      Logger.slack.error('Shutdown Signal', `The process (id:${process.pid},ver:${global.CONFIG.app.version}) received a shutdown signal and unable to cleanup due to ${err}`);
      return 1;
    })
    // Shutdown logger
    .then(function(code) {
      setTimeout(function() {
        log4js.shutdown(function() {
          setTimeout(function() {
            process.exit(code);
          }, 1000);
        });
      }, 1000);
    });
}
// Terminal uses SIGINT
process.on('SIGINT', cleanUp);
// Docker uses SIGTERM to stop container
process.on('SIGTERM', cleanUp);

/**
 * Errors are handled by Sentry.io in `server.js` file
 */

// // Handle promise unhandled rejection.
// process.on('uncaughtException', function(error) {
//     // This should log to external/remote host
//     if (global.Logger) Logger.slack.error('X-Uncaught-Exception', `\nName: ${error.name}\nMessage: ${error.message}\nStack: ${error.stack}`);
//     else throw error;
// });

// // Handle promise unhandled rejection.
// process.on('unhandledRejection', function(error) {
//     // This should log to external/remote host
//     if (global.Logger) Logger.slack.error('X-Unhandled-Rejection', `\nName: ${error.name}\nMessage: ${error.message}\nStack: ${error.stack}`);
//     else throw (error instanceof Error ? error : new Error(error));
// });