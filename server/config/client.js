'use strict';

/**
 * Configuration for Client
 */

const config = global.CONFIG;

module.exports = {
	// Limit for generic list
	list_limit: 5,
	// Limit of playlist items per page in player
	playlist_limit: config.validation.playerItemsLimit,
	// Maximum limit of local playlist items
	playlist_max_limit: config.validation.playlistMaxLimit
};
