'use strict';

const config = global.CONFIG,
    _ = require('lodash'),
    express = require('express'),
    codes = require(MOD.codes),
    viewsFolder = config.paths.views;

/**
 * WARNING!!! CONFLICT: Might get conflict in the future.
 *
 * Actually NOT a middleware.
 * Directly extend express's response prototype to avoid overhead.
 * Overhead that creates function on each and every request.
 *
 * ExpressJS does NOT send error stack trace on NODE_ENV=production mode.
 *
 */

express.response.sendError = function(httpCode, errorMessage) {
    if (!_.isNumber(httpCode)) {
        errorMessage = httpCode;
        httpCode = 400;
    }
    this.status(httpCode).render(viewsFolder + '/errors/' + httpCode, {
        code: httpCode,
        message: errorMessage
    });
};

express.response.endError = function(httpCode) {
    this.status(httpCode || 400).end();
};

express.response.jsonError = function(httpCode, errorCode, errorJson) {
    if (!_.isNumber(httpCode)) {
        if (httpCode instanceof RequestError) {
            errorJson = httpCode;
            errorCode = httpCode.code;
        } else {
            if (errorCode) errorJson = errorCode;
            errorCode = httpCode;
        }
        httpCode = 400;
    }
    var json = codes.get(errorCode);
    json.error = true;
    if (errorJson) {
        // Do not send messages from generated Error object.
        json.errors = _.isArray(errorJson) ?
            errorJson : [(config.dev ? errorJson.message : '') || errorJson];
    }
    this.status(httpCode).json(json);
};

express.response.frameError = function(httpCode, errorMessage) {
    this.render('frame/errors/' + httpCode, {
        message: config.dev ? errorMessage : ''
    });
};

express.response.partialError = function(httpCode, errorMessage) {
    this.render('partials/errors/' + httpCode, {
        message: config.dev ? errorMessage : ''
    });
};