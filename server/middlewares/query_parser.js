'use strict';

const _ = require('lodash');

function parseObjectValues(obj) {
    var key, value;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            value = obj[key];
            if (typeof value === 'string') {
                // Numbers
                // parseFloat requires less that 17 char of string to convert
                if (!isNaN(value) && value.length < 14) {
                    obj[key] = parseFloat(value);
                }
                // Boolean / true
                else if (value === 'true') {
                    obj[key] = true;
                }
                // Boolean / false
                else if (value === 'false') {
                    obj[key] = false;
                }
            }
            // Objects
            else if (_.isObject(value)) {
                parseObjectValues(value);
            }
        }
    }
}

module.exports = function(req, res, next) {
    if (!_.isEmpty(req.query)) {
        parseObjectValues(req.query);
    }
    next();
};