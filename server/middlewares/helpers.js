'use strict';

const express = require('express');

/**
 * WARNING!!! Might get conflict in the future.
 */

express.request.getUserId = function() {
	return this.user ? this.user.user._id : '';
};

express.request.getUserObjectId = function() {
	return ObjectId(this.getUserid());
};
