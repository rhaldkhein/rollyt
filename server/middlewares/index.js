'use strict';

/* * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Express Extensions
 *
 * Not a middleware. As it is extending express directly
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

require('./helpers');
require('./error_response');
require('./success_response');


/* * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Express Middlewares
 * 
 * Put all middlewares here. Order is important.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

module.exports = function(app) {

	app.use(require('./query_parser'));

};