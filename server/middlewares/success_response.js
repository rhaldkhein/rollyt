'use strict';

const _ = require('lodash'),
	express = require('express');

/**
 * WARNING!!! CONFLICT: Might get conflict in the future.
 *
 * Actually NOT a middleware.
 * Directly extend express's response prototype to avoid overhead.
 * Overhead that creates function on each and every request.
 */

express.response.sendSuccess = function(successMessage) {
	this.send(successMessage || 'Success');
};

express.response.jsonSuccess = function(successMessage, data) {
	if(_.isObject(successMessage)){
		data = successMessage;
		successMessage = 'Success';
	}
	this.json({
		error: false,
		status: 'OK',
		message: successMessage,
		data: data
	});
};
