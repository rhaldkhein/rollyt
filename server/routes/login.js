'use strict';

// Modules
const express = require('express'),
  router = express.Router(),
  passport = require('passport');


/* * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Google
 */

router.get('/google/authstatus', function(req, res) {
  if (req.isAuthenticated()) {
    res.json({
      auth: true,
      id: req.user._id // Account ID
    });
  } else {
    res.json({
      auth: false
    });
  }
});

router.post('/google/token',
  passport.authenticate('custom'),
  function(req, res) {
    res.json({
      id: req.user._id // Account ID
    });
    Logger.file.debug('Passport: login_success /', req.user.type_id, req.user.firstname);
  }
);


module.exports = router;