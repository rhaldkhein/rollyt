'use strict';

const config = global.CONFIG,
    express = require('express'),
    router = express.Router(),
    Model = require(PATH.model);

// Exports
module.exports = router;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Security
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Login user to admin area
router.post('/login', function(req, res) {
    if (req.session.admin) {
        res.end();
        return;
    }
    if (req.user && req.user.type_id == config.session.root_admin_id) {
        req.session.admin = {
            root: true
        };
        res.end();
    } else {
        res.endError(404);
    }
});

// Logout user from admin area
router.get('/logout', function(req, res) {
    if (req.session.admin) {
        delete req.session.admin;
        res.send('Successfully Logged Out');
    } else {
        res.sendError(404);
    }
});

// IMPORTANT: Make sure all request are authenticated.
router.all('*', function(req, res, next) {
    if (req.session.admin) {
        next();
    } else {
        return res.sendError(404);
    }
});

/* * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Dashboard
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

router.get('/', function(req, res) {
    res.render('admin/index');
});

/* * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Users & Accounts
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

router.get('/users/meta', function(req, res) {
    Promise.map([
        Model.User.count(),
        Model.Account.count()
    ], function(item) {
        return item;
    }).then(function(result) {
        res.json({
            totalUsers: result[0],
            totalAccounts: result[1]
        });
    }).catch(function() {
        res.jsonError('REBR');
    });
});