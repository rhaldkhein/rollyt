'use strict';

const _ = require('lodash'),
    config = global.CONFIG,
    express = require('express'),
    router = express.Router();

// Exports
module.exports = router;

// Command to start syncing process info
router.get('/process/sync', function(req, res) {
    process.send({
        topic: 'server:procsync',
        data: 'NULL'
    });
    res.end();
});

// Return syncronized process info across clusters
router.get('/process/info', function(req, res) {
    res.json(global.Processes);
});

router.get('/logger/dump', function(req, res) {
    res.sendFile(config.paths.data + '/node-server.log');
});

// Test for logger
router.get('/logger/test/:type?', function(req, res) {
    req.checkParams('type').notEmpty().isIn(['file', 'slack']);
    if (req.validationErrors()) return res.status(500).end();
    if (req.params.type == 'file') {
        Logger.file.info('This is a test log to FILE.');
    } else if (req.params.type == 'slack') {
        Logger.slack.info('This is a test log to SLACK.');
    }
    res.end();
});

/**
 * If we're using PM2, this will restart the server
 */

router.get('/process/exit/:code?', function(req, res) {
    req.checkParams('code').notEmpty().isIn([0, 1]);
    if (req.validationErrors()) return res.status(500).end();
    process.exit(parseInt(req.params.code));
});

const signals = [
    'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
    'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
];

router.get('/process/kill/:signal?', function(req, res) {
    req.checkParams('signal').notEmpty().isIn(signals);
    if (req.validationErrors()) return res.status(500).end();
    process.emit('SIGINT');
});