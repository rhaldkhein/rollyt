'use strict';

const _ = require('lodash'),
    express = require('express'),
    router = express.Router(),
    Model = require(PATH.model);

// Exports
module.exports = router;

/**
 * These routes will NOT be served if user is not authenticated
 */

router.get('/list', function(req, res) {

    req.checkQuery('q').notEmpty(); // Ok
    req.checkQuery('page').optional().isInt(); // Ok
    req.checkQuery('limit').optional().isInt(); // Ok

    let errors = req.validationErrors();
    if (errors) return res.jsonError('REBR', errors);
    _.defaults(req.query, {
        page: 1,
        limit: 10
    });
    // Prepare find object
    let regexValue = { $regex: req.query.q, $options: 'i' },
        findObj = {
            $or: [
                { sid: regexValue },
                { codename: regexValue }
            ]
        };
    // Add object id, if exists
    if (ObjectId.isValid(req.query.q)) findObj.$or.push({ _id: ObjectId(req.query.q) });
    // Find documents
    Promise.map(
            [
                Model.User.count(findObj).exec(),
                Model.User.aggregate()
                .match(findObj)
                .skip((req.query.page - 1) * req.query.limit)
                .limit(req.query.limit)
                .exec()
            ],
            function(result) { return result; }
        )
        .then(function(results) {
            res.json({
                query: req.query,
                total: results[0],
                results: results[1]
            });
        }).catch(function(err) {
            res.jsonError('REBR', err);
        });

});