'use strict';

const express = require('express'),
	router = express.Router(),
	Model = require(PATH.model);

// Exports
module.exports = router;

/**
 * These routes will NOT be served if user is not admin authenticated
 */

router.get('/:name', function(req, res) {
	Model.AppSetting.get(req.params.name, function(err, value) {
		if (err) res.jsonError(err);
		else res.json(value);
	});
});

router.post('/:name', function(req, res) {
	Model.AppSetting.set(req.params.name, req.body, function(err) {
		if (err) res.jsonError(err);
		else res.end();
	});
});
