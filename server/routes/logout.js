'use strict';

// Modules
const express = require('express'),
  router = express.Router(),
  _guest = { id: 'guest' }; // Acccount ID

router.get('/', function(req, res) {
  req.session.destroy(function() {
    // Either err or not, should redirect to home.
    res.redirect('/');
  });
});

router.get('/ajax', function(req, res) {
  req.session.destroy(function() {
    res.end();
  });
});

router.get('/token', function(req, res) {
  if (req.isAuthenticated()) {
    let isBackdoor = req.session.backdoor;
    req.session.destroy(function() {
      if (isBackdoor) res.status(302).end();
      else res.json(_guest);
    });
  } else {
    res.json(_guest);
  }
});

module.exports = router;