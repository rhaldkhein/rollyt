'use strict';

const config = global.CONFIG,
	express = require('express'),
	router = express.Router(),
	Model = require(PATH.model),
	geoip = require('geoip-lite'),
	default_ip = '207.97.227.239',
	default_geo = {
		country: 'US',
		region: 'TX',
		city: 'San Antonio'
	};

const jsonConfig = JSON.stringify(config.client);

router.get('/', function(req, res) {
	// Current location
	var geo = geoip.lookup(config.production ? req.ip : default_ip);
	if (!geo) geo = default_geo;
	// User authentication
	var auth;
	if (req.isAuthenticated()) {
		auth = {
			auth: true,
			id: req.user._id // Account ID
		};
	} else {
		auth = {
			auth: false
		};
	}
	Promise.all([Model.AppSetting.get('messages')]).then(function(results) {
		// Render view
		res.render('home', {
			msgs: JSON.stringify(results[0]),
			auth: JSON.stringify(auth),
			config: jsonConfig,
			browsers: config.browsers,
			cookie: !!req.session.cookieAccepted,
			geoip: JSON.stringify({
				country: geo.country || default_geo.country,
				region: geo.region || '',
				city: geo.city || ''
			})
		});
	});
});

module.exports = router;
