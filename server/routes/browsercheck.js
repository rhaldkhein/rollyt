'use strict';

const _ = require('lodash'),
  config = global.CONFIG,
  express = require('express'),
  router = express.Router(),
  requiredVersions = JSON.parse(config.browsers),
  defaultBrowser = {
    title: 'Unknown Browser',
    image: '/static/images/logos/logo-rollyt-medium.png',
    link_update: '#',
    link_download: '#'
  },
  browsersData = {
    chrome: {
      title: 'Google Chrome',
      image: '/static/images/logos/browsers/google-chrome.png',
      link_update: 'https://support.google.com/chrome/answer/95414',
      link_download: 'https://www.google.com/chrome'
    },
    firefox: {
      title: 'Mozilla Firefox',
      image: '/static/images/logos/browsers/mozilla-firefox.png',
      link_update: 'https://support.mozilla.org/kb/update-firefox-latest-version',
      link_download: 'https://www.mozilla.org/firefox'
    },
    opera: {
      title: 'Opera',
      image: '/static/images/logos/browsers/opera.png',
      link_update: 'https://www.google.com/search?q=how+to+update+opera+browser',
      link_download: 'https://www.opera.com'
    },
    safari: {
      title: 'Apple Safari',
      image: '/static/images/logos/browsers/apple-safari.png',
      link_update: 'https://support.apple.com/HT204416',
      link_download: 'https://www.apple.com/safari'
    },
    edge: {
      title: 'Microsoft Edge',
      image: '/static/images/logos/browsers/microsoft-edge.png',
      link_update: 'https://www.bing.com/search?q=how+to+update+microsoft+edge',
      link_download: 'https://www.microsoft.com/windows/microsoft-edge'
    }
  };

function getAgentVersion(ua) {
  let tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
  if (/trident/i.test(M[1])) {
    tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
    return 'IE ' + (tem[1] || '');
  }
  if (M[1] === 'Chrome') {
    tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
    if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
  }
  M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
  if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
  return M.join(' ');
}

function getUserAgent(ua) {
  let agent = getAgentVersion(ua).split(' ');
  return {
    codename: agent[0].toLowerCase(),
    version: parseFloat(agent[1])
  };
}

function isInAppBrowser(ua) {
  // Facebook
  return (_.indexOf(ua, 'FBAN') > -1) || (_.indexOf(ua, 'FBAV') > -1);
}

router.get('/', function(req, res) {
  let userAgent = getUserAgent(req.headers['user-agent']);
  let isInApp = isInAppBrowser(userAgent);
  res.render('browsercheck', {
    useragent: userAgent,
    inapp: isInApp,
    supported: userAgent.version >= requiredVersions[userAgent.codename],
    unknownBrowser: !requiredVersions[userAgent.codename],
    // `undefined` if browser is unknown
    browser: (isInApp ? defaultBrowser : (browsersData[userAgent.codename] || defaultBrowser)),
    // Needs to do this. Also `undefined` if browser is unknown
    requiredVersion: requiredVersions[userAgent.codename],
    browsers: browsersData
  });
});

module.exports = router;