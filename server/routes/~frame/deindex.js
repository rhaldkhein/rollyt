'use strict';

const config = global.CONFIG,
    express = require('express'),
    router = express.Router();

router.get('*', function(req, res) {
    res.render('frame' + req.path, {
        production: config.production
    }, function(err, html) {
        if (err) res.frameError(404, err.message);
        else res.send(html);
    });
});

module.exports = router;