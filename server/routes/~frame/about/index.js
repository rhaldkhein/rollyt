'use strict';

const config = global.CONFIG, 
	express = require('express'),
	router = express.Router();

module.exports = router;