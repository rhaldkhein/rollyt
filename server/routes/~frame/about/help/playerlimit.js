'use strict';

const config = global.CONFIG,
	express = require('express'),
	router = express.Router();


router.get('/', function(req, res, next) {
	res.locals.playerItemsLimit = config.validation.playerItemsLimit;
	next();
});

module.exports = router;
