'use strict';

const express = require('express'),
	router = express.Router();

var styles = ['/static/app/dist/settings.css'];
var scripts = ['/static/app/dist/settings.js'];

router.get('/', function(req, res, next) {
	if (req.isAuthenticated()) {
		res.locals.styles = styles;
		res.locals.scripts = scripts;
		next();
	} else {
		res.frameError(404);
	}
});

module.exports = router;
