'use strict';

const config = global.CONFIG,
  express = require('express'),
  router = express.Router();

router.get('*', function(req, res) {
  res.render('page' + req.path, function(err, html) {
    if (err) res.render('partials/errors/404', {
      message: config.dev ? err.message : ''
    });
    else res.send(html);
  });
});

module.exports = router;