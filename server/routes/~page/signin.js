'use strict';

const express = require('express'),
	router = express.Router();


router.get('/', function(req, res, next) {
	res.locals.user = req.user;
	next();
});

module.exports = router;
