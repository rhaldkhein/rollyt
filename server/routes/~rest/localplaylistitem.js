'use strict';

const _ = require('lodash'),
    config = global.CONFIG,
    express = require('express'),
    router = express.Router(),
    async = require('async'),
    validate = require(MOD.validator),
    Model = require(PATH.model);


const defaultQuery = {
    'page': 1,
    'limit': config.client.playlist_limit
};

/* * * * * * * * * * * * * * * * *
 * GET
 * * * * * * * * * * * * * * * * */

router.get('/', function(req, res) {

    req.checkQuery('_id').isMongoId(); // Ok

    var errors = req.validationErrors();
    if (errors) {
        res.jsonError('REBR', errors);
        return;
    }

    Model.LocalPlaylistItem.findByIdAndPopulateAll(req.query._id, {
        '__v': 0
    }, function(err, item) {
        if (!err && item) {
            res.json(item);
        } else {
            res.jsonError('RENF', err);
        }
    });

});

router.get('/list', function(req, res) {

    req.checkQuery('page').optional().isInt(); // Ok
    req.checkQuery('limit').optional().isInt(); // Ok
    req.checkQuery('playlistid').isMongoId(); // Ok

    var errors = req.validationErrors();
    if (errors) {
        res.jsonError('REBR', errors);
        return;
    }

    _.defaults(req.query, defaultQuery);

    Model.LocalPlaylist.findById(req.query.playlistid, {
        '__v': 0,
        'title': 0,
        'image': 0,
        'date_created': 0,
        'date_modified': 0,
        'order': {
            $slice: [(req.query.page - 1) * req.query.limit, req.query.limit]
        }
    }, function(err, docLocalPlaylist) {
        if (!err && docLocalPlaylist) {
            // Owner Check
            if (docLocalPlaylist.is_private && docLocalPlaylist.user.toString() != req.getUserId()) {
                // Error
                res.jsonError('RENO');
            } else {
                // Model
                async.parallel([
                    function(done) {
                        Model.LocalPlaylistItem.find({
                            _id: {
                                $in: docLocalPlaylist.order
                            }
                        }, done);
                    },
                    function(done) {
                        docLocalPlaylist.getItemCount(done);
                    }
                ], function(err, results) {
                    if (!err) {
                        res.json({
                            query: req.query,
                            total: docLocalPlaylist.item_count,
                            order: docLocalPlaylist.order,
                            results: results[0]
                        });
                    } else {
                        res.jsonError('REBR', err);
                    }
                });
            }
        } else {
            res.jsonError('REBR', err);
        }
    });
});

router.get('/list/ids', function(req, res) {
    req.checkQuery('_id').notEmpty(); // Ok
    var errors = req.validationErrors();
    if (errors) {
        res.jsonError('REBR', errors);
        return;
    }
    Model.LocalPlaylistItem.find({
        _id: {
            $in: _.map(req.query._id.split(','), function(id) {
                return ObjectId(id);
            })
        }
    }, function(err, docs) {
        if (err) {
            res.jsonError('REBR', err);
        } else {
            res.json({
                query: req.query,
                total: docs.length,
                results: docs
            });
        }
    });
});

router.get('/videos', function(req, res) {

    req.checkQuery('page').optional().isInt(); // Ok
    req.checkQuery('limit').optional().isInt(); // Ok
    req.checkQuery('playlistid').isMongoId(); // Ok

    var errors = req.validationErrors();
    if (errors) return res.jsonError('REBR', errors);

    _.defaults(req.query, defaultQuery);
    if (req.query.page < 1) req.query.page = 1;

    Promise.all([
        // Get videos with pagination
        Model.LocalPlaylist.findOne({
            _id: ObjectId(req.query.playlistid),
        }).populate({
            path: 'order',
            match: {
                resource_type: 'youtube#video'
            },
            options: {
                limit: req.query.limit,
                skip: (req.query.page - 1) * req.query.limit
            }
        }).exec(),
        // Get count of all videos
        Model.LocalPlaylistItem.count({
            local_playlist: ObjectId(req.query.playlistid)
        })
    ]).then(function(results) {
        // results = [<localplaylist>, <count>]
        if (results[0]) {
            if (results[0].is_private && results[0].user.toString() != req.getUserId()) {
                return res.jsonError('RENO');
            }
            res.json({
                query: req.query,
                total: results[1] || 0,
                results: results[0].order
            });
            return;
        }
        throw new Error('Invalid results');
    }).catch(function(err) {
        res.jsonError('REBR', err);
    });

});

/* * * * * * * * * * * * * * * * *
 * POST
 * * * * * * * * * * * * * * * * */

router.post('/all', function(req, res) {
    req.checkBody('_id').isMongoId(); // Ok
    var errors = req.validationErrors();
    if (errors) {
        res.jsonError('REBR', errors);
        return;
    }
    Model.LocalPlaylist.findById(req.body._id, {
            __v: 0,
            order: 0
        })
        .then(function(playlist) {
            // Owner Check and validate items
            if (playlist.user.toString() == req.getUserId()) {
                return validate.localPlaylistItems(req.body.items).return(playlist);
            } else {
                throw new RequestError('RENO');
            }
        })
        .then(function(playlist) {
            // Items are validated
            var items = _.compact(_.map(req.body.items, function(item) {
                // Prevent circular playlist
                if (item.resource_type === 'local#playlist' && item.resource_id === playlist.id) return null;
                return {
                    // Pre-generate id for later use
                    _id: ObjectId(),
                    local_playlist: req.body._id,
                    resource_type: item.resource_type,
                    resource_id: item.resource_id,
                    flags: item.flags
                };
            }));
            if (!items.length) throw new RequestError('RECR');
            Model.LocalPlaylistItem.insertMany(items, {
                    ordered: false
                }, function(err) {
                    // Error `11000` is duplicate error
                    if (err && err.code != 11000) {
                        res.jsonError('REBR', err);
                    } else {
                        // Get duplicate documents
                        var errorIds;
                        if (err) {
                            if (err.writeErrors) {
                                errorIds = _.map(err.writeErrors, function(item) {
                                    return item.getOperation()._id.toString();
                                });
                            } else if (err.getOperation) {
                                errorIds = [err.getOperation()._id.toString()];
                            } else {
                                errorIds = [];
                            }
                        }
                        // parts[[<duplicates>...], [<success>...]]
                        var parts = _.partition(items, function(item) {
                            return _.indexOf(errorIds, item._id.toString()) > -1;
                        });
                        Promise.map(parts[0], function(item) {
                                return Model.LocalPlaylistItem.findOne({
                                    local_playlist: item.local_playlist,
                                    resource_id: item.resource_id,
                                });
                            })
                            .then(function(results) {
                                // Add to order field
                                playlist.update({
                                    $addToSet: {
                                        order: {
                                            $each: _.concat(_.map(parts[1], '_id'), _.map(results, '_id'))
                                        }
                                    }
                                }, {
                                    safe: true,
                                    upsert: true
                                }, function(err) {
                                    if (err) {
                                        res.jsonError('REBR', err);
                                    } else {
                                        res.jsonSuccess({
                                            add_length: parts[1].length,
                                            items_new: parts[1],
                                            items_exist: results
                                        });
                                    }
                                });
                            })
                            .catch(function(err) {
                                res.jsonError('REIE', err);
                            });
                    }
                })
                // Silently catch duplicate error
                .catch(_.noop);
        })
        .catch(function(err) {
            res.jsonError('REBR', err);
        });
});

/* * * * * * * * * * * * * * * * *
 * PUT
 * * * * * * * * * * * * * * * * */

router.put('/', function(req, res) {
    req.checkBody('_id').notEmpty().isMongoId(); // Ok
    var errors = req.validationErrors();
    if (errors) {
        res.jsonError('REBR', errors);
        return;
    }
    Model.LocalPlaylistItem.findById(req.body._id, function(err, item) {
        if (!err && item) {
            Model.LocalPlaylist.findOne({
                _id: item.local_playlist,
                user: ObjectId(req.getUserId())
            }, {
                __v: 0,
                order: 0
            }, function(err, playlist) {
                if (!err && playlist) {
                    if (req.body.flags) {
                        item.flags = _.pick(req.body.flags, [
                            'category',
                            'date',
                            'duration',
                            'itemLimit',
                            'order',
                            'playLimit',
                            'shuffle'
                        ]);
                        item.save(function(err) {
                            if (!err) res.json(item);
                            else res.jsonError('REBR', err);
                        });
                    } else {
                        res.json(item);
                    }
                } else {
                    res.jsonError('REBR', err);
                }
            });
        } else {
            res.jsonError('REBR', err);
        }
    });

});

/* * * * * * * * * * * * * * * * *
 * DELETE
 * * * * * * * * * * * * * * * * */

router.delete('/', function(req, res) {
    req.checkBody('_id').notEmpty().isMongoId(); // Ok
    var errors = req.validationErrors();
    if (errors) {
        res.jsonError('REBR', errors);
        return;
    }
    Model.LocalPlaylistItem.findById(req.body._id,
        function(err, item) {
            if (!err && item) {
                // Validate owner
                Model.LocalPlaylist.findById(item.local_playlist, {
                        __v: 0,
                        order: 0
                    },
                    function(err, playlist) {
                        // Owner Check
                        if (playlist && playlist.user.toString() == req.getUserId()) {
                            // 1st: Remove from collection.
                            item.remove(function(err) {
                                if (err) {
                                    res.jsonError('REBR', 'Error deleting item');
                                } else {
                                    playlist.update({
                                        // 2nd: Remove from order in localplaylist.
                                        $pull: {
                                            'order': item.id
                                        }
                                    }, function(err) {
                                        if (err) {
                                            res.jsonError('REBR', 'Error deleting order');
                                        } else {
                                            res.jsonSuccess();
                                        }
                                    });
                                }
                            });

                        } else {
                            res.jsonError('RENO'); // Not owner
                        }
                    });
            } else {
                res.jsonError('REBR', 'Error finding item');
            }
        });
});

module.exports = router;