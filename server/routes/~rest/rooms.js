'use strict';

const _ = require('lodash'),
  express = require('express'),
  router = express.Router(),
  Model = require(PATH.model);

router.all('*', function(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    return res.jsonError('REUA');
  }
});


/* * * * * * * * * * * * * * * * *
 * GET
 * * * * * * * * * * * * * * * * */

router.get('/', function(req, res) {

  req.checkQuery('name').notEmpty(); // Ok
  req.checkQuery('user').optional().isMongoId(); //Ok
  let errors = req.validationErrors();
  if (errors) return res.jsonError('REBR', errors);

  let objAR = { name: req.query.name };
  let objSR = { name: req.query.name, owner: req.getUserId() };
  if (req.query.user) {
    objAR.user = req.query.user;
    objSR.user = objAR.user;
  }

  Promise.all([
      // Active room
      Model.ActiveRoom.findOne(objAR, 'user name private password')
      .exec(),
      // Saved room
      Model.SavedRoom.findOne(objSR, 'password')
      .exec()
    ])
    .then(function(results) {
      if (results[0] && results[0].private) {
        // Active room exists
        let pass = results[0].password;
        let saved = !!results[1];
        // WARNING: Do not send password! // Ok
        results[0].password = null; // CHECK: Ok, password nulled (Active Room)
        if (pass) {
          // Generated password
          return {
            room: results[0],
            saved: saved,
            samepass: (saved ? pass === results[1].password : false)
          };
        } else {
          // User password
          return Model.User.findById(results[0].user, 'password')
            .then(function(user) {
              return {
                room: results[0],
                saved: saved,
                samepass: (saved ? user.password === results[1].password : false)
              };
            });
        }
      } else {
        // Room is null or not private
        return {
          room: results[0],
          saved: !!results[1] // CHECK: Ok, password converted to boolean (Saved Room)
        };
      }
    })
    .then(function(data) {
      res.json(data);
    })
    .catch(function(err) {
      res.jsonError(err);
    });

});

router.get('/list', function(req, res) {

  req.checkQuery('q').notEmpty(); // Ok
  req.checkQuery('idx').isInt(); // Ok
  let errors = req.validationErrors();
  if (errors) return res.jsonError('REBR', errors);

  let objFind = {
    name: { $regex: '^' + req.query.q, $options: 'i' },
    hidden: false,
    open: true
  };

  Promise.all([
    Model.ActiveRoom.count(objFind),
    Model.ActiveRoom.find(objFind, 'user name private picture')
    .sort('name')
    .skip((req.query.idx || 0) * 15).limit(15)
    .exec()
  ]).then(function(results) {
    res.json({
      total: results[0],
      limit: 15,
      query: req.query,
      result: results[1]
    });
  }).catch(function(err) {
    res.jsonError(err);
  });

});

router.get('/saved', function(req, res) {

  req.checkQuery('idx').isInt(); // Ok
  let errors = req.validationErrors();
  if (errors) return res.jsonError('REBR', errors);

  let objFind = {
    owner: ObjectId(req.getUserId())
  };

  Promise.all([
    Model.SavedRoom.count(objFind),
    Model.SavedRoom.find(objFind, '-_id user name picture')
    .sort('name')
    .skip((req.query.idx || 0) * 15).limit(15)
    .exec()
  ]).then(function(results) {
    res.json({
      total: results[0],
      limit: 15,
      query: req.query,
      result: results[1]
    });
  }).catch(function(err) {
    res.jsonError(err);
  });

});

router.delete('/saved', function(req, res) {

  req.checkBody('user').isMongoId(); // Ok
  let errors = req.validationErrors();
  if (errors) return res.jsonError('REBR', errors);

  Model.SavedRoom.findOneAndRemove({
    owner: req.getUserId(),
    user: req.body.user
  }).then(function() {
    res.end();
  }).catch(function(err) {
    res.jsonError(err);
  });
});

module.exports = router;