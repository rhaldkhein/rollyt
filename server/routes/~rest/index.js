'use strict';

const _ = require('lodash'),
  config = global.CONFIG,
  async = require('async'),
  express = require('express'),
  router = express.Router(),
  geoip = require('geoip-lite'),
  empty = Object.freeze({}),
  model = require(PATH.model),
  bcrypt = require('bcrypt'),
  default_ip = '207.97.227.239',
  default_geo = {
    country: 'US',
    region: 'TX',
    city: 'San Antonio'
  };

/* * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Home Rest
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

router.get('/', function(req, res) {
  res.json(empty);
});

// Client Configuration
router.get('/config', function(req, res) {
  res.json(config.client);
});

// Geo Location
router.get('/geoip', function(req, res) {
  var geo = geoip.lookup(config.production ? req.ip : default_ip);
  if (!geo) geo = default_geo;
  res.json({
    country: geo.country || default_geo.country,
    region: geo.region || '',
    city: geo.city || ''
  });
});

// Accept cookie consent
router.get('/acceptcookie', function(req, res) {
  req.session.cookieAccepted = true;
  res.end();
});

/* * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Account
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

router.get('/account/id', function(req, res) {
  res.json({
    id: req.isAuthenticated() ? req.user._id : 'guest'
  });
});

router.get('/account', function(req, res) {
  if (req.query._id && req.query._id !== 'guest') {
    model.Account.findByIdAndPopulateAll(req.query._id, function(err, docAcc) {
      if (err) {
        res.jsonError('REBR', err);
      } else {
        res.json(model.Account.clean(docAcc, 'onlypublic'));
      }
    });
  } else if (req.isAuthenticated()) {
    // Client may request with req.query._id, but it's
    // not necessary as it's getting info from session
    res.json(model.Account.clean(req.user, 'onlypublic'));
  } else {
    res.json({
      _id: 'guest',
      firstname: 'Guest',
      lastname: 'User',
      user: {
        _id: 'guest'
      }
    });
  }
});

router.get('/account/list', function(req, res) {

  req.checkQuery('ids').notEmpty(); // Ok

  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }

  model.Account.find({
      _id: { '$in': req.query.ids.split(',') }
    }, {
      _id: 1,
      user: 1,
      firstname: 1,
      lastname: 1
    }).exec()
    .then(function(result) {
      res.json(result);
    })
    .catch(function(err) {
      res.jsonError('REBR', err);
    });
});


/* * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * User
 * * * * * * * * * * * * * * * * * * * * * * * * * * * */

router.get('/user', function(req, res) {
  req.checkQuery('_id').isMongoId(); // Ok
  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }
  // This is similar to /account, except that it's for User collection
  if (req.query._id) {
    async.parallel([
      function(done) {
        model.User.findByIdAndPopulateAll(req.query._id, function(err, docUser) {
          if (err)
            done(err);
          else
            done(null, model.User.clean(docUser, 'onlypublic'));
        });
      },
      function(done) {
        model.LocalPlaylist.count({
          user: ObjectId(req.query._id),
        }, function(err, numCount) {
          if (err)
            done(err);
          else
            done(null, numCount);
        });
      }
    ], function(err, results) {
      if (err) {
        res.jsonError('REBR', err);
      } else {
        // Count all playlists owned by user
        results[0].item_count = results[1] || 0;
        res.json(results[0]);
      }
    });
  } else if (req.isAuthenticated()) {
    res.json(model.User.clean(req.user.user, 'onlypublic'));
  } else {
    res.json({
      _id: 'guest'
    });
  }
});

router.get('/user/list', function(req, res) {

  req.checkQuery('ids').notEmpty(); // Ok

  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }

  model.User.find({
      _id: { '$in': req.query.ids.split(',') }
    }, {
      _id: 1,
      current_account: 1
    })
    .populate('current_account')
    .exec()
    .then(function(result) {
      res.json(result);
    })
    .catch(function(err) {
      res.jsonError('REBR', err);
    });
});

router.put('/user', function(req, res) {

  // Make request is authenticated
  if (!req.isAuthenticated()) return res.jsonError('REUA'); // Ok

  // No specific chars, like [a-zA-Z].
  // To support multiple language writings.
  req.checkBody('codename')
    .notEmpty()
    .isLength({
      min: 2,
      max: 32
    })
    .withMessage('Must be 2 to 32 characters long'); // Ok

  var errors = req.validationErrors();
  if (errors) return res.jsonError('REBR', errors);

  // Should not start with "Player" as will conflict with auto generated room name
  if (_.startsWith(req.body.codename, 'Player')) return res.jsonError('REBR', {
    msg: 'Must not start with "Player" word'
  });

  // Should not contain '.' (dot)
  if (_.indexOf(req.body.codename, '.') > -1) return res.jsonError('REBR', {
    msg: 'Must not contain "." (dot)'
  });

  model.User.findByIdAndUpdate(req.getUserId(), {
      codename: req.body.codename
    }, {
      new: true
    })
    .exec()
    .then(function(user) {
      res.json(user);
    })
    .catch(function(err) {
      if (err.code === 11000) res.jsonError('REDK');
      else res.jsonError('REBR', err);
    });

});

router.put('/user/password', function(req, res) {

  // Make request is authenticated
  if (!req.isAuthenticated()) return res.jsonError('REUA');

  req.checkBody('pw')
    .notEmpty()
    .isLength({
      min: 8,
      max: 128
    })
    .withMessage('Must be 8+ characters long'); // Ok

  var errors = req.validationErrors();
  if (errors) return res.jsonError('REBR', errors);

  bcrypt.hash(_.trim(req.body.pw), config.database.saltrounds)
    .then(function(hash) {
      return model.User.findByIdAndUpdate(req.getUserId(), {
        password: hash // CHECK: Encrypt password! // Ok
      }).exec();
    })
    .then(function() {
      res.end();
    })
    .catch(function(err) {
      res.jsonError('REBR', err);
    });

});

// Exports
module.exports = router;