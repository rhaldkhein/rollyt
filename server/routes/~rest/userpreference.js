'use strict';

const _ = require('lodash'),
    express = require('express'),
    router = express.Router(),
    Model = require(PATH.model);

router.all('*', function(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        return res.jsonError('REUA');
    }
});

router.get('/all', function(req, res) {
    Model.UserPreference.find(
        {
            user: ObjectId(req.getUserId())
        },
        function(err, docs) {
            if (!err) res.json(docs);
            else res.jsonError('REBR');
        }
    );
});

function validateValue(pref, value) {
    switch (pref.data_type) {
        case 'number':
        case 'float':
        case 'integer':
            return _.isNumber(value);
        case 'boolean':
            value = _.trim(value);
            return value === 'true' || value === 'false';
        case 'array':
            return _.isArray(value) && value.length <= pref.max_value;
        case 'object':
            if (_.isObject(value)) {
                var json = JSON.stringify(value);
                return json.length <= pref.max_value;
            }
            break;
        default:
            // string
            return _.isString(value) && value.length <= pref.max_value;
    }
    return false;
}

function updateOrCreate(req, res) {
    // Must be mongo id
    req.checkBody('preference').isMongoId();
    // No need to validate `value` here
    // req.checkBody('value').notEmpty();
    // Validate
    var errors = req.validationErrors();
    if (errors) return res.jsonError('REBR', errors);
    // WARNING: Validate value base on each properties
    var body = _.pick(req.body, ['preference', 'value']);
    Model.Preference.findById(body.preference)
        .exec()
        .then(function(pref) {
            if (!pref) throw new Error('not_found');
            if (validateValue(pref, body.value)) {
                // Validated
                if (_.isObject(body.value)) body.value = JSON.stringify(body.value);
                return Model.UserPreference.findOneAndUpdate(
                    {
                        user: ObjectId(req.getUserId()),
                        preference: ObjectId(body.preference)
                    },
                    body,
                    {
                        upsert: true,
                        setDefaultsOnInsert: true
                    }
                ).exec();
            } else {
                throw new Error('invalid_value');
            }
        })
        .then(function() {
            res.json(body);
        })
        .catch(function(err) {
            res.jsonError('REBR', err);
        });
}

router.post('/', updateOrCreate);
router.put('/', updateOrCreate);

module.exports = router;
