'use strict';

const _ = require('lodash'),
  config = global.CONFIG,
  express = require('express'),
  router = express.Router(),
  async = require('async'),
  validate = require(MOD.validator),
  Model = require(PATH.model);

const defaultQuery = {
  page: 1,
  limit: config.client.list_limit
};

/** * * * * * * * * * * * * * * *
 * GET
 * * * * * * * * * * * * * * * * */

router.get('/', function(req, res) {
  req.checkQuery('_id').isMongoId(); // Ok

  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }

  Model.LocalPlaylist.findByIdAndPopulateAll(
    req.query._id,
    {
      __v: 0,
      order: 0
    },
    function(err, playlist) {
      if (!err && playlist) {
        if (playlist.is_private && playlist.user.id.toString() != req.getUserId()) {
          res.jsonError('RENO'); // Not Owner
          return;
        }
        playlist.getItemCount(function(err) {
          if (err) res.jsonError('REBR', err);
          else {
            res.json(playlist);
          }
        });
      } else {
        res.jsonError('RENF', err);
      }
    }
  );
});

router.get('/list', function(req, res) {
  req.checkQuery('_id').notEmpty(); // Ok
  req
    .checkQuery('own')
    .optional()
    .isBoolean(); // Ok
  req
    .checkQuery('page')
    .optional()
    .isInt(); // Ok
  req
    .checkQuery('limit')
    .optional()
    .isInt(); // Ok

  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }

  _.defaults(req.query, defaultQuery);

  var objQuery = {
    _id: {
      $in: _.map(req.query._id.split(','), function(id) {
        return ObjectId(id);
      })
    }
  };

  // Dealing with private playlists
  if (req.query.own) {
    objQuery.$or = [{ user: ObjectId(req.getUserId()) }, { is_private: false }];
  } else {
    objQuery.is_private = false;
  }

  Model.LocalPlaylist.find(objQuery, function(err, docs) {
    if (err) {
      res.jsonError('REBR', err);
    } else {
      res.json({
        query: req.query,
        total: docs.length,
        results: docs
      });
    }
  });
});

router.get('/list/profile', function(req, res) {
  req
    .checkQuery('page')
    .optional()
    .isInt(); // Ok
  req
    .checkQuery('limit')
    .optional()
    .isInt(); // Ok
  req
    .checkQuery('folder')
    .optional({
      checkFalsy: true
    })
    .isMongoId(); // Ok
  req
    .checkQuery('profileid')
    .optional({
      checkFalsy: true
    })
    .isMongoId(); // Ok

  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }

  _.defaults(req.query, defaultQuery, {
    profileid: req.getUserId()
  });

  if (!req.query.profileid) {
    res.jsonError('REUA');
    return;
  }

  var objDoc = {
    user: ObjectId(req.query.profileid)
  };

  if (req.query.folder) {
    objDoc.folder = ObjectId(req.query.folder);
  }

  if (req.getUserId() != req.query.profileid) {
    objDoc.is_private = false;
  }

  async.parallel(
    [
      function(done) {
        // Total
        Model.LocalPlaylist.count(objDoc, done);
      },
      function(done) {
        // Paginate
        Model.LocalPlaylist.aggregate()
          .match(objDoc)
          .sort('-date_created')
          .skip((req.query.page - 1) * req.query.limit)
          .limit(req.query.limit)
          .project(Model.LocalPlaylist.projection())
          .exec(done);
      }
    ],
    function(err, results) {
      if (err) {
        res.jsonError('REBR', err);
      } else {
        res.json({
          query: req.query,
          total: results[0],
          results: results[1]
        });
      }
    }
  );
});

/** * * * * * * * * * * * * * * *
 * POST
 * * * * * * * * * * * * * * * * */

router.post('/', function(req, res) {
  // No specific chars, like [a-zA-Z].
  // To support multiple language writings.
  req
    .checkBody('title')
    .isLength({
      min: 2,
      max: 64
    })
    .withMessage('Must be 2 to 64 characters long'); // Ok

  req
    .checkBody('folder')
    .optional({
      checkFalsy: true
    })
    .isMongoId(); // Ok

  req.checkBody('flags').isLength({
    max: 50
  }); // Ok

  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }

  var data = { playlist: null, items: null };
  Promise.resolve()
    .then(function() {
      // 1. Create a local playlist
      var objDoc = {
        user: ObjectId(req.getUserId()),
        title: req.body.title
      };
      if (req.body.folder) objDoc.folder = ObjectId(req.body.folder);
      if (req.body.flags) objDoc.flags = _.pick(req.body.flags, ['repeat', 'shuffle']);
      objDoc.is_private = !!req.body.is_private;
      return Model.LocalPlaylist.create(objDoc).then(function(docPlaylist) {
        data.playlist = docPlaylist;
        return data;
      });
    })
    .then(function(data) {
      // 2. Validate playlist items
      var bodyItems = req.body.items;
      if (!_.isEmpty(bodyItems) && bodyItems.length <= config.validation.playerItemsLimit) {
        return validate.localPlaylistItems(bodyItems).then(function() {
          data.items = req.body.items;
          return data;
        });
      } else {
        data.items = _.isArray(bodyItems) ? bodyItems : [];
        return data;
      }
    })
    .then(function(data) {
      // 2.1 Find image for playlist
      if (data.items.length) {
        return Model.LocalPlaylist.getItemImage(data.items[0], req.user.access_token).then(function(url) {
          if (url) data.playlist.image = url;
          return data;
        });
      } else {
        return data;
      }
    })
    .then(function(data) {
      // 2.2 Storing items
      var items = _.map(data.items, function(item) {
        return {
          local_playlist: data.playlist.id,
          resource_type: item.resource_type,
          resource_id: item.resource_id,
          flags: item.flags
        };
      });
      // Convert items array to documents
      if (!_.isEmpty(items)) {
        return Model.LocalPlaylistItem.insertMany(items).then(function(docItems) {
          data.items = docItems;
          return data;
        });
      } else {
        data.items = items;
        return data;
      }
    })
    .then(function(data) {
      // 3. Create the order field
      data.playlist.order = _.map(data.items, 'id');
      return data.playlist.save().return(data);
    })
    .then(function(data) {
      // Add done!
      data.playlist.getItemCount().then(function() {
        res.json(data.playlist);
      });
    })
    .catch(function(err) {
      res.jsonError('REBR', err);
      if (data.playlist) {
        // Remove all items for the playlist
        return Model.LocalPlaylistItem.remove({
          local_playlist: data.playlist.id
        }).then(function() {
          // Remove the playlist
          return data.playlist.remove();
        });
      }
    });
});

router.post('/moveitem', function(req, res) {
  req
    .checkBody('_id')
    .notEmpty()
    .isMongoId(); // Ok
  req
    .checkBody('src')
    .notEmpty()
    .isMongoId(); // Ok
  req
    .checkBody('des')
    .optional()
    .isMongoId(); // Ok
  req
    .checkBody('top')
    .optional()
    .isBoolean(); // Ok
  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }

  var idDes = ObjectId(req.body.des);
  var objFindDoc = {
    user: ObjectId(req.getUserId()), // Owner Check
    _id: ObjectId(req.body._id)
  };

  Model.LocalPlaylist.aggregate()
    .match(objFindDoc)
    .project({
      indexDes: {
        $indexOfArray: ['$order', idDes]
      },
      orderSize: {
        $size: '$order'
      }
    })
    .exec(function(err, results) {
      if (err || !results.length) {
        res.jsonError('REBR', err);
        return;
      }
      var indexDes = results[0].indexDes;
      if (indexDes === -1 && _.isBoolean(req.body.top)) {
        // Move to top or bottom
        indexDes = req.body.top ? 0 : results[0].orderSize - 1;
      }
      // At this point, valid owner and index des.
      var idSrc = ObjectId(req.body.src);
      Model.LocalPlaylist.findOneAndUpdate(
        {
          _id: results[0]._id
        },
        {
          $pull: {
            order: idSrc
          }
        },
        {
          fields: '_id' // Acted as projection, exclude `order` array
        },
        function(err, playlist) {
          if (err || !playlist) {
            res.jsonError('REBR', err);
            return;
          }
          playlist.update(
            {
              $push: {
                order: {
                  $each: [idSrc],
                  $position: indexDes
                }
              }
            },
            function(err) {
              if (err) {
                res.jsonError('REBR', err);
              } else {
                res.jsonSuccess();
              }
            }
          );
        }
      );
    });
});

/** * * * * * * * * * * * * * * *
 * PUT
 * * * * * * * * * * * * * * * * */

router.put('/', function(req, res) {
  req.checkBody('_id').isMongoId(); // Ok

  req
    .checkBody('title')
    .isLength({
      min: 2,
      max: 64
    })
    .withMessage('Must be 2 to 64 characters long'); // Ok

  req
    .checkBody('folder')
    .optional({
      checkFalsy: true
    })
    .isMongoId(); // Ok

  req.checkBody('flags').isLength({
    max: 50
  }); // Ok

  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }

  var objDoc = {
    title: req.body.title,
    image: req.body.image,
    folder: req.body.folder ? ObjectId(req.body.folder) : undefined,
    is_private: !!req.body.is_private
  };

  if (req.body.flags) objDoc.flags = _.pick(req.body.flags, ['repeat', 'shuffle']);

  Model.LocalPlaylist.findOneAndUpdate(
    {
      user: ObjectId(req.getUserId()), // Owner Check
      _id: ObjectId(req.body._id)
    },
    objDoc,
    {
      new: true,
      select: '_id'
    },
    function(err, playlist) {
      if (!err && playlist) {
        playlist.getItemCount().then(function() {
          res.json(playlist);
        });
      } else {
        res.jsonError('REBR', err);
      }
    }
  );
});

/** * * * * * * * * * * * * * * *
 * DELETE
 * * * * * * * * * * * * * * * * */

router.delete('/', function(req, res) {
  req
    .checkBody('_id')
    .notEmpty()
    .isMongoId(); // Ok
  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }
  var objFindDoc = {
    user: ObjectId(req.getUserId()), // Owner Check
    _id: req.body._id
  };
  Model.LocalPlaylist.findOneAndRemove(objFindDoc, function(err) {
    if (err) {
      res.jsonError('REBR');
    } else {
      res.jsonSuccess();
    }
  });
});

module.exports = router;
