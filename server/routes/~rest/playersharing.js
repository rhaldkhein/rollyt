'use strict';

const express = require('express'),
  router = express.Router();

router.all('*', function(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    return res.jsonError('REUA');
  }
});


/* * * * * * * * * * * * * * * * *
 * GET
 * * * * * * * * * * * * * * * * */

router.get('/', function(req, res) {
  res.end();
});

module.exports = router;