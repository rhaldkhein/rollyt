'use strict';

const express = require('express'),
	router = express.Router(),
	model = require(PATH.model);

router.get('/', function(req, res) {
	model.Preference.find({}, function(err, prefs) {
		res.json(prefs);
	});
});

module.exports = router;