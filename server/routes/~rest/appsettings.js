'use strict';

const express = require('express'),
	router = express.Router(),
	Model = require(PATH.model);

// Exports
module.exports = router;

router.get('/:name', function(req, res) {
	Model.AppSetting.get(req.params.name, function(err, value) {
		if (err) res.jsonError(err);
		else res.json(value);
	});
});
