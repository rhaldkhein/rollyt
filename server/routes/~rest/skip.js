'use strict';

const _ = require('lodash'),
    express = require('express'),
    router = express.Router(),
    Model = require(PATH.model);

router.all('*', function(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        return res.jsonError('REUA');
    }
});


/* * * * * * * * * * * * * * * * *
 * GET
 * * * * * * * * * * * * * * * * */

router.get('/list', function(req, res) {
    Model.SkipVideo.findOne({
        user: ObjectId(req.getUserId())
    }, function(err, doc) {
        if (!err) {
            res.json(doc ? doc.videos : []);
        } else {
            res.jsonError('RENF', err);
        }
    });
});

/* * * * * * * * * * * * * * * * *
 * POST
 * * * * * * * * * * * * * * * * */

router.post('/', function(req, res) {
    Model.SkipVideo.update({
        user: ObjectId(req.getUserId())
    }, {
        $addToSet: {
            videos: req.body.id
        }
    }, {
        upsert: true
    }, function(err, result) {
        if (!err && result) {
            res.json(result.ok);
        } else {
            res.jsonError('RENF', err);
        }
    });
});

/* * * * * * * * * * * * * * * * *
 * PUT
 * * * * * * * * * * * * * * * * */

/* * * * * * * * * * * * * * * * *
 * DELETE
 * * * * * * * * * * * * * * * * */

router.delete('/', function(req, res) {
    Model.SkipVideo.update({
        user: ObjectId(req.getUserId())
    }, {
        $pull: {
            videos: req.body.id
        }
    }, function(err, result) {
        if (!err && result) {
            res.json(result.ok);
        } else {
            res.jsonError('RENF', err);
        }
    });
});

router.delete('/clear', function(req, res) {
    Model.SkipVideo.update({
        user: ObjectId(req.getUserId())
    }, {
        $set: {
            videos: []
        }
    }, {
        multi: true
    }, function(err, result) {
        if (!err && result) {
            res.json(result.ok);
        } else {
            res.jsonError('RENF', err);
        }
    });
});

module.exports = router;
