'use strict';

const _ = require('lodash'),
  express = require('express'),
  router = express.Router(),
  Model = require(PATH.model);

router.all('*', function(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    return res.jsonError('REUA');
  }
});


/* * * * * * * * * * * * * * * * *
 * GET
 * * * * * * * * * * * * * * * * */

router.get('/', function(req, res) {

  req.checkQuery('_id').isMongoId();

  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }

  Model.Pin.findByIdAndPopulateAll(req.query._id, {
    '__v': 0
  }, function(err, item) {
    if (!err && item) {
      res.json(item);
    } else {
      res.jsonError('RENF', err);
    }
  });

});

router.get('/list', function(req, res) {
  Model.Pin.find({
    user: ObjectId(req.getUserId()),
    resource_type: {
      $in: req.query.resource_type.split(',')
    }
  }, null, {
    sort: {
      date_created: 1
    }
  }, function(err, pins) {
    if (!err && pins) {
      res.json(pins);
    } else {
      res.jsonError('RENF', err);
    }
  });
});

/* * * * * * * * * * * * * * * * *
 * POST
 * * * * * * * * * * * * * * * * */

let propsFlag = [
  'category',
  'date',
  'duration',
  'itemLimit',
  'order',
  'playLimit',
  'shuffle'
];

router.post('/', function(req, res) {

  req.checkBody('resource_type').isLength({ max: 128 });
  req.checkBody('resource_id').isLength({ max: 128 });
  req.checkBody('flags').isLength({ max: 128 });

  var errors = req.validationErrors();
  if (errors) return res.jsonError('REBR', errors);

  // WARNING: Add validation // Ok
  Model.Pin.create({
    user: ObjectId(req.getUserId()),
    resource_type: req.body.resource_type,
    resource_id: req.body.resource_id,
    flags: _.pick(req.body.flags, propsFlag)
  }, function(err, pin) {
    if (!err && pin) {
      res.json(pin);
    } else {
      res.jsonError('RENF', err);
    }
  });
});

/* * * * * * * * * * * * * * * * *
 * PUT
 * * * * * * * * * * * * * * * * */

router.put('/', function(req, res) {
  req.checkBody('_id').isMongoId();
  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }
  // Only update flags
  Model.Pin.updateOne({
    user: ObjectId(req.getUserId()),
    _id: ObjectId(req.body._id)
  }, {
    flags: _.pick(req.body.flags, propsFlag)
  }, function(err) {
    if (err) res.jsonError('RENF', err);
    else res.json({});
  });
});

/* * * * * * * * * * * * * * * * *
 * DELETE
 * * * * * * * * * * * * * * * * */

router.delete('/', function(req, res) {
  req.checkBody('_id').isMongoId();
  var errors = req.validationErrors();
  if (errors) {
    res.jsonError('REBR', errors);
    return;
  }
  Model.Pin.remove({
    user: ObjectId(req.getUserId()),
    _id: ObjectId(req.body._id)
  }, function(err) {
    if (err) res.jsonError('RENF', err);
    else res.json();
  });
});

module.exports = router;