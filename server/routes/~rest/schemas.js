'use strict';

// Imports.
const _ = require('lodash'),
	express = require('express'),
	router = express.Router(),
	model = require(PATH.model);

let publicSchemas = [
	'Folder',
	'LocalPlaylist',
	'LocalPlaylistItem',
	'StarredChannel',
	'StarredPlaylist',
	'StarredVideo',
	'StarredSubitem',
	'Preference',
	'PreferenceValue',
	'UserPreference',
	'User',
	'Account',
	'Pin'
];

let schemaCache = null;
let excludeKeys = ['__v', 'email'];

router.get('/', function(req, res) {
	return res.json(
		schemaCache || (
			schemaCache = _.mapValues(
				_.pick(model, publicSchemas),
				function(value) {
					let _keys = _.keys(value.schema.paths);
					_.pullAll(_keys, _.concat(value.schema.hidden, excludeKeys));
					return _keys;
				}
			)
		)
	);
});

module.exports = router;