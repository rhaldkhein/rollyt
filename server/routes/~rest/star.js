'use strict';

const _ = require('lodash'),
    config = global.CONFIG,
    express = require('express'),
    router = express.Router(),
    Model = require(PATH.model),
    google = require('googleapis'),
    youtube = google.youtube('v3');

const mapModels = {
    channel: 'StarredChannel',
    playlist: 'StarredPlaylist',
    video: 'StarredVideo',
    subitem: 'StarredSubitem',
    localplaylist: 'StarredLocalPlaylist'
};

const _objStarFalse = {
    star: false
};

function youtubeValidateId(req, res, next) {
    req.checkBody('id').notEmpty();
    req.checkBody('type').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.jsonError('REBR', errors);
        return;
    }

    // Except non-youtube types
    if (req.body.type === 'localplaylist') {
        next();
        return;
    }

    youtube[req.body.type + 's'].list(
        {
            part: 'snippet',
            id: req.body.id,
            key: config.youtube.key,
            maxResults: 1
        },
        function(err, response) {
            if (err) {
                res.jsonError('REBR', err.errors);
                return;
            } else {
                if (response.items[0]) {
                    next();
                } else {
                    res.jsonError('REBR');
                    return;
                }
            }
        }
    );
}

router.all('*', function(req, res, next) {
    if (req.isAuthenticated()) {
        next();
    } else {
        return res.jsonError('REUA');
    }
});

router.get('/', function(req, res) {
    req.checkQuery('id').notEmpty();
    req.checkQuery('type').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.jsonError('REBR', errors);
        return;
    }
    var modelName = mapModels[req.query.type];
    if (modelName) {
        var objDoc = {
            user: ObjectId(req.getUserId())
        };
        objDoc[req.query.type + '_id'] = req.query.id;
        Model[modelName].findOne(objDoc, function(err, doc) {
            if (err) {
                res.jsonError('REBR');
            } else if (doc) {
                res.send({
                    star: true,
                    folder: doc.folder
                });
            } else {
                res.send(_objStarFalse);
            }
        });
    } else {
        res.jsonError('REBR');
    }
});

router.get('/all', function(req, res) {
    // Validation
    req.checkQuery('type').notEmpty();
    req
        .checkQuery('page')
        .optional()
        .isInt();
    req
        .checkQuery('limit')
        .optional()
        .isInt();
    req
        .checkQuery('userid')
        .optional({
            checkFalsy: true
        })
        .isMongoId();
    req
        .checkQuery('folder')
        .optional({
            checkFalsy: true
        })
        .isMongoId();
    var errors = req.validationErrors();
    if (errors) {
        res.jsonError('REBR', errors);
        return;
    }
    // Defaults
    _.defaults(req.query, {
        userid: req.getUserId(),
        page: 1,
        limit: config.client.list_limit
    });
    // Query
    var modelName = mapModels[req.query.type];
    if (modelName) {
        var objDoc = {
            user: ObjectId(req.query.userid)
        };
        if (req.query.folder) objDoc.folder = ObjectId(req.query.folder);
        Promise.all([
            // Total
            Model[modelName].count(objDoc),
            // Find all documents
            Model[modelName]
                .find(
                    objDoc,
                    {
                        __v: 0,
                        date_created: 0,
                        user: 0
                    },
                    {
                        skip: (req.query.page - 1) * req.query.limit,
                        limit: req.query.limit,
                        sort: 'field -date_created'
                    }
                )
                .exec(),
            // Check if folder is public
            req.query.folder ? Model.Folder.findById(req.query.folder).exec() : null
        ])
            .then(function(results) {
                if (req.getUserId() == req.query.userid || (results[2] && results[2].is_public)) {
                    // Owner or public folder
                    return res.json({
                        query: req.query,
                        total: results[0],
                        results: results[1]
                    });
                }
                // Check folder publicity
                throw new Error('not_public'); // Ok
            })
            .catch(function(err) {
                if (err.message === 'not_public') res.jsonError('REPC');
                else res.jsonError('REBR');
            });
    } else {
        res.jsonError('REBR');
    }
});

router.post('/', youtubeValidateId, function(req, res) {
    req.checkBody('type').notEmpty();
    req
        .checkBody('of') // Omit folder update
        .optional()
        .isBoolean();
    req
        .checkBody('folder')
        .optional({
            checkFalsy: true
        })
        .isMongoId();
    var errors = req.validationErrors();
    if (errors) return res.jsonError('REBR', errors);
    var modelName = mapModels[req.body.type];
    if (modelName) {
        var objFind = {
            user: ObjectId(req.getUserId())
        };
        objFind[req.body.type + '_id'] = req.body.id;
        // Setup folder property
        var objNewDoc = _.clone(objFind);
        if (!req.body.of) {
            if (req.body.folder) objNewDoc.folder = ObjectId(req.body.folder);
            else objNewDoc.folder = null;
        }
        // Write to database
        Model[modelName].findOneAndUpdate(
            objFind,
            objNewDoc,
            {
                upsert: true,
                setDefaultsOnInsert: true,
                new: true
            },
            function(err, doc) {
                if (err) {
                    res.jsonError('REBR');
                } else {
                    res.send({
                        star: true,
                        folder: doc.folder || null
                    });
                }
            }
        );
    } else {
        res.jsonError('REBR');
    }
});

router.delete('/', function(req, res) {
    req.checkBody('id').notEmpty();
    req.checkBody('type').notEmpty();
    var errors = req.validationErrors();
    if (errors) {
        res.jsonError('REBR', errors);
        return;
    }
    var modelName = mapModels[req.body.type];
    if (modelName) {
        var objDoc = {
            user: ObjectId(req.getUserId())
        };
        objDoc[req.body.type + '_id'] = req.body.id;
        Model[modelName].findOneAndRemove(objDoc, function(err) {
            if (err) {
                res.jsonError('REBR');
            } else {
                res.send(_objStarFalse);
            }
        });
    } else {
        res.jsonError('REBR');
    }
});

module.exports = router;
