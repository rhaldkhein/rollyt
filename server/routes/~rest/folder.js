'use strict';

const _ = require('lodash'),
  express = require('express'),
  router = express.Router(),
  Model = require(PATH.model);

function validateBody(req, res, next) {
  req
    .checkBody('name')
    .isLength({
      min: 2,
      max: 24
    })
    .withMessage('Must be 2 to 24 characters long');
  req
    .checkBody('is_public')
    .optional()
    .isBoolean();
  var errors = req.validationErrors();
  if (errors) return res.jsonError('REBR', errors);
  else next();
}

function validateBodyId(req, res, next) {
  req
    .checkBody('_id')
    .notEmpty()
    .isMongoId();
  var errors = req.validationErrors();
  if (errors) return res.jsonError('REBR', errors);
  else next();
}

router.all('*', function(req, res, next) {
  if (req.isAuthenticated()) next();
  else return res.jsonError('REUA');
});

router.get('/', function(req, res) {
  req.checkQuery('_id').isMongoId(); // Ok
  var errors = req.validationErrors();
  if (errors) return res.jsonError('REBR', errors);
  Model.Folder.findOne({
    _id: req.query._id,
    is_public: true
  })
    .then(function(doc) {
      if (doc) return res.json(doc);
      else res.jsonError('RENF');
    })
    .catch(function() {
      res.jsonError('REBR');
    });
});

router.get('/all', function(req, res) {
  Model.Folder.find({
    user: ObjectId(req.getUserId())
  })
    .sort('name')
    .then(function(docs) {
      res.json(docs);
    })
    .catch(function() {
      res.jsonError('REBR');
    });
});

router.get('/item_count', function(req, res) {
  req.checkQuery('id').isMongoId(); // Ok
  var errors = req.validationErrors();
  if (errors) return res.jsonError('REBR', errors);
  let objMatch = { folder: ObjectId(req.query.id) };
  Promise.all([
    Model.LocalPlaylist.count(objMatch),
    Model.StarredLocalPlaylist.count(objMatch),
    Model.StarredVideo.count(objMatch),
    Model.StarredPlaylist.count(objMatch),
    Model.StarredChannel.count(objMatch)
  ])
    .then(function(result) {
      res.json({
        id: req.query.id,
        sum: _.sum(result)
      });
    })
    .catch(function() {
      res.jsonError('REBR');
    });
});

router.post('/', validateBody, function(req, res) {
  // CHECK: Validate // Ok
  // Validation is inside `validateBody` middleware
  var objDoc = {
    user: ObjectId(req.getUserId()),
    name: req.body.name
  };

  Model.Folder.findOne(objDoc, function(err, doc) {
    if (doc) {
      res.jsonError('REAX');
    } else {
      Model.Folder.create(objDoc, function(err, doc) {
        if (err) {
          res.jsonError('REBR');
        } else {
          res.json(doc);
        }
      });
    }
  });
});

router.put('/', validateBody, validateBodyId, function(req, res) {
  // CHECK: Validate // Ok
  // Validation is inside `validateBody` middleware
  var objFindDoc = {
    user: ObjectId(req.getUserId()),
    _id: req.body._id
  };
  var objFindDuplicate = {
    user: ObjectId(req.getUserId()),
    name: req.body.name
  };
  Model.Folder.findOne(objFindDuplicate, function(err, doc) {
    if (doc) {
      // Already exists. But check if he just want to change the public property.
      if (doc.is_public != req.body.is_public) {
        doc.is_public = req.body.is_public;
        doc.save();
        res.send(doc);
      } else {
        res.jsonError('REAX');
      }
    } else {
      Model.Folder.findOneAndUpdate(
        objFindDoc,
        {
          name: req.body.name
        },
        {
          new: true
        },
        function(err, doc) {
          if (err) {
            res.jsonError('REBR');
          } else {
            res.send(doc);
          }
        }
      );
    }
  });
});

router.delete('/', validateBodyId, function(req, res) {
  // Validation is inside `validateBodyId` middleware
  var objFindDoc = {
    user: ObjectId(req.getUserId()),
    _id: req.body._id
  };
  Model.Folder.findOneAndRemove(objFindDoc, function(err) {
    if (err) {
      res.jsonError('REBR');
    } else {
      res.json({});
    }
  });
});

module.exports = router;
