'use strict';

const express = require('express'),
    router = express.Router();

// Exports
module.exports = router;

// Main page
router.get('/', function(req, res) {
    res.render('about/index');
});

// Disclaimer
router.get('/disclaimer', function(req, res) {
    res.render('about/disclaimer');
});

// Bug report
router.get('/bugreport', function(req, res) {
    res.render('about/bugreport');
});

// Bug report
router.get('/social', function(req, res) {
    res.render('about/social');
});