'use strict';

/**
 * Security for admin files
 */

const express = require('express'),
	router = express.Router();

router.get('*', function(req, res, next) {
	if (req.session.admin) {
		next();
	} else {
		res.sendError(404);
	}
});

module.exports = router;