'use strict';

const config = global.CONFIG,
  express = require('express'),
  router = express.Router();

module.exports = router;

router.get('/status', function(req, res) {
  res.send('Public Server Status');
});

if (config.dev) {
  router.get('/console/log', function(req, res) {
    Logger.console.debug(req.query.data);
    res.end();
  });
}
