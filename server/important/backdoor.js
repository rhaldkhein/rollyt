'use strict';

const express = require('express'),
    config = global.CONFIG,
    uuidRandom = require('uuid/v4'),
    router = express.Router();

module.exports = router;

router.get('/openbeta', function(req, res) {
    if (config.status === 'construction') {
        req.session.openbetatoken = uuidRandom();
        res.render('server/openbeta', {
            token: req.session.openbetatoken
        });
    } else {
        res.sendError();
    }
});

router.get('/openbeta/agree', function(req, res) {
    req.checkQuery('agree')
        .notEmpty()
        .withMessage('You must accept the agreement to continue.');
    req.checkQuery('token')
        .equals(req.session.openbetatoken)
        .withMessage('Security token mismatched. Please try again.');
    var errors = req.validationErrors();
    if (!errors) {
        req.session.backdoor = true;
        req.session.openbetatoken = null;
    }
    res.render('server/openbetaagree', {
        errors: errors
    });
});