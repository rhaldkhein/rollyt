'use strict';

/**
 * Start a server!
 *
 * Environment Variables:
 *
 *		NODE_ENV = production | development (default)
 *		APP_HOST = server | local (default)
 *		APP_STATUS = open | close (default) | maintenance | construction
 *		APP_HTTPS = true | false (default)
 *
 *		APP_IP = *.*.*.* | 127.0.0.1 (default) (dev only)
 *
 */

// Get config file. 'process.env' will be merged in config.
let config = require('./server/config.js');

// Create local console class.
require('./server/modules/logger.js');

Logger.console.debug(`Application: pid ${process.pid}`);
Logger.console.debug(`NODE_ENV=${process.env.NODE_ENV || '<default>'}`);
Logger.console.debug(`APP_NAME=${process.env.APP_NAME || '<default>'}`);
Logger.console.debug(`APP_HOST=${process.env.APP_HOST || '<default>'}`);
Logger.console.debug(`APP_STATUS=${process.env.APP_STATUS || '<default>'}`);
Logger.console.debug(`APP_HTTPS=${process.env.APP_HTTPS || '<default>'}`);

// Boot up the server.
require(config.paths.server + '/server.js');
