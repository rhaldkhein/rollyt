
## LOG FILES

### MongoDB

/var/log/mongodb/mongod.log

### Redis

For windows development
```sh
$ redis-server --daemonize yes
```
Host: localhost (process.env.APP_REDIS_HOST)
Port: 6379 (process.env.APP_REDIS_PORT)

### PM2

```sh
$ pm2 log
```

## COMMANDS

### Check the status of `/etc/rc.local` in systemd. This is used to auto-start the server on boot.

Links:
https://askubuntu.com/questions/9853/how-can-i-make-rc-local-run-on-startup

```sh
$ systemctl status rc-local.service
```

## BITBUCKET

Auto enable git agent to remove manuall credential input.
Used to add in script for auto pull and npm install.

`id_rsa_alpha` must be present in `.ssh` folder.

```sh
$ eval `ssh-agent`
$ ssh-add /root/.ssh/id_rsa_alpha
```