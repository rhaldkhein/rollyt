## Replica Set

### Set Info

Id 	| Name       		| Host										| Path									| Priority		| Tier $ per H        | Comments
- - | - - - - - - - | - - - - - - - - - - - - | - - - - - - - - - - - | - - - - - - | - - - - - - - - - - | - - -
0 	| Database1			| 172.31.5.219:27017			| /data/db							| 1						| t2.small $0.023/h   |
1 	|	Database2			| xxx.xxx.xxx.xxx:27017		| /data/db							| 1						| t2.small $0.023/h   |
2 	|	Database3			| xxx.xxx.xxx.xxx:27017		| /efs/data/rollyt/db		| 0						| t2.nano $0.0058/h   | Should not be primary

### Mongoose Connection

```js
mongoose.connect('mongodb://[username:password@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database][?options]]', options);
```
