#### BROWSER COMPATIBILITY LOG

[0] - Checking and no errors at the moment
[1] - Fully Working (Everything is working)
[2] - Partial (Loaded and displayed but not working futher)
[3] - Not Working (Not loading and displayed)

Development started on 2016-04-11 (11th April, 2016)

### Internet Explorer (Windows 7)

IE 6-10									[3]
IE 11									[0] - Not working: Library page

### Edge (Windows 8 & 10)

Edge Desktop 25.10586/13 	2015-11-05	[2] - Working except: Library page
Edge Desktop 38.14393/14 *	2016-08-02	[0] - Working except: iframe page
Edge Desktop 41.16299/16	2017-09-26	[0] - Working except: iframe page

### Opera (Desktop & Mobile)

Opera 27					2015-01-27
Opera 37 *					2016-05-04
Opera 50.0.2762							[1] 2018-01-07

### Safari (Desktop & Mobile)

Safari 5.1.7				2012-05-09
Safari 6.2.8 *				2015-08-13

### Firefox (Desktop & Mobile)

Firefox 36					2015-02-24
Firefox 46 *				2016-04-26
Firefox 57.0.2 				2017-12-07	[1] 2018-01-07
Firefox 57.0.4 				2018-01-04	[1] 2018-01-07

### Chrome (Desktop & Mobile)

Chrome 40					2015-01-21	[0]
Chrome 50 *					2016-04
Chrome 63.0.3239			2017-12-06	[1] 2018-01-07

### Vilvaldi -> Chrome
### Maxthon -> Chrome


#### FEATURES LOG

### CSS

Feature				| Chrome	| Edge	| Firefox	| Opera	| Safari
- - - - - - - - - -	| - - - - - | - - - | - - - - - | - - - | - - - -
flex 				| 29		| Yes	| 20		| 12.1	| 9
box-sizing			| 10		| Yes	| 29		| 7		| 5.1
:before, :after		| Yes		| Yes	| 1.5		| 7		| 4
content				| 1			| Yes	| 1			| 7		| 1